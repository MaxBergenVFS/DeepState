using UnityEngine;

public class AlertNearbyNPC : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<NPCHearing>() != null) other.GetComponent<NPCHearing>().SetAlerted();
    }
}