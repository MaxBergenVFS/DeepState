using UnityEngine;

public class PlayerHandsBehaviour : StateMachineBehaviour
{
    private PlayerHands playerHands;

    [Header("OnStateEnter")]
    [SerializeField]
    private bool setUseOnEnter;
    [SerializeField]
    private bool removeUseOnEnter;
    [SerializeField]
    private bool setHolsterOnEnter;
    [SerializeField]
    private bool removeHolsterOnEnter;
    [SerializeField]
    private Enums.itemType modelToShow;
    [Header("OnStateExit")]
    [SerializeField]
    private bool setUseOnExit;
    [SerializeField]
    private bool removeUseOnExit;
    [SerializeField]
    private bool setHolsterOnExit;
    [SerializeField]
    private bool removeHolsterOnExit;
    [SerializeField]
    private bool unequipIfDepleted;
    [SerializeField]
    private Enums.itemType modelToHide;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerHands = animator.GetComponent<PlayerHands>();

        if (setUseOnEnter) animator.SetBool(AnimParams.PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION, true);
        else if (removeUseOnEnter) animator.SetBool(AnimParams.PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION, false);

        if (setHolsterOnEnter) animator.SetBool(AnimParams.PLAYER_BOOL_HOLSTERING, true);
        else if (removeHolsterOnEnter) animator.SetBool(AnimParams.PLAYER_BOOL_HOLSTERING, false);

        if (modelToShow != Enums.itemType.None)
        {
            playerHands.HideAllModels();
            playerHands.ShowModel(modelToShow, true);
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (unequipIfDepleted) playerHands.UnequipIfDepleted();

        if (setUseOnExit) animator.SetBool(AnimParams.PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION, true);
        else if (removeUseOnExit) animator.SetBool(AnimParams.PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION, false);

        if (setHolsterOnExit) animator.SetBool(AnimParams.PLAYER_BOOL_HOLSTERING, true);
        else if (removeHolsterOnExit) animator.SetBool(AnimParams.PLAYER_BOOL_HOLSTERING, false);

        if (modelToHide != Enums.itemType.None) playerHands.ShowModel(modelToHide, false);
    }
}
