using UnityEngine;

public class GunPickup : BaseItem
{
    private string ammoName;
    private InventoryItem ammoInventoryItem;
    private PlayerHeldItem playerHeldItem;

    [SerializeField]
    private Enums.ammoType ammoType;
    [SerializeField]
    private int ammoAmountMin = 5, ammoAmountMax = 30;

    public bool IsInInventory { get { return (GetAmountInInventoryDatabase() > 0); } }

    protected override void Start()
    {
        base.Start();
        playerHeldItem = playerController.GetComponent<PlayerHeldItem>();
        ammoInventoryItem = inventoryItemDatabase.GetInventoryItem(ammoType);
        ammoName = ammoInventoryItem.title;
    }

    public override void Interact()
    {
        int ammoAmount = Random.Range(ammoAmountMin, ammoAmountMax);
        PickUp();
        if (IsInInventory)
        {
            string appendingText = $"{ammoAmount} {ammoName}";
            DisplayNotificationText(true, appendingText);
        }
        else
        {
            DisplayNotificationText();
            AddToPlayerInventory();
        }
        playerInventory.AddAmmoToInventory(ammoType, ammoAmount);
    }

    public override void UseFromInventory(out bool success)
    {
        base.UseFromInventory(out success);
        playerHeldItem.Equip(itemType);
    }

}
