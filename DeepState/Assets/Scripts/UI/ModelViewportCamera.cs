using UnityEngine;

public class ModelViewportCamera : ViewportCamera
{
    [SerializeField]
    private GameObject[] models;

    protected override void Awake()
    {
        base.Awake();
        HideAllModels();
    }

    public void DisplayModel(Enums.itemType itemType)
    {
        HideAllModels();

        switch (itemType)
        {
            case (Enums.itemType.AkRifle):
                models[0].SetActive(true);
                break;
            case (Enums.itemType.Wrench):
                models[1].SetActive(true);
                break;
            case (Enums.itemType.Chips):
                models[2].SetActive(true);
                break;
            case (Enums.itemType.Soda):
                models[3].SetActive(true);
                break;
            case (Enums.itemType.RedKeycard):
                models[4].SetActive(true);
                break;
            case (Enums.itemType.BlueKeycard):
                models[5].SetActive(true);
                break;
            case (Enums.itemType.Medpack):
                models[6].SetActive(true);
                break;
            case (Enums.itemType.BodyArmor):
                models[7].SetActive(true);
                break;
            case (Enums.itemType.LockPick):
                models[8].SetActive(true);
                break;
            case (Enums.itemType.HackingTool):
                models[9].SetActive(true);
                break;
            case (Enums.itemType.Silencer):
                models[10].SetActive(true);
                break;
            case (Enums.itemType.NightVisionGoggles):
                models[11].SetActive(true);
                break;
            case (Enums.itemType.Pistol):
                models[12].SetActive(true);
                break;
            case (Enums.itemType.CheckpointDevice):
                models[13].SetActive(true);
                break;
            case (Enums.itemType.M4Rifle):
                models[14].SetActive(true);
                break;
            default:
                break;
        }
    }

    public void HideAllModels()
    {
        foreach (GameObject model in models)
        {
            model.SetActive(false);
        }
    }

}
