using UnityEngine;

public class Inputs : MonoBehaviour
{
    protected const string INPUT_CANCEL = "Cancel";
    protected const string INPUT_INVENTORY = "Inventory";
    protected const string INPUT_MOUSE_X = "Mouse X";
    protected const string INPUT_MOUSE_Y = "Mouse Y";
    protected const string INPUT_HORIZONTAL = "Horizontal";
    protected const string INPUT_VERTICAL = "Vertical";
    protected const string INPUT_JUMP = "Jump";
    protected const string INPUT_INTERACT = "Interact";
    protected const string INPUT_MELEE = "Melee";
    protected const string INPUT_CROUCH = "Crouch";
    protected const string INPUT_SPRINT = "Sprint";
    protected const string INPUT_FIRE_1 = "Fire1";
    protected const string INPUT_FIRE_2 = "Fire2";
    protected const string INPUT_RELOAD = "Reload";
    protected const string INPUT_SLOT_1 = "Slot1";
    protected const string INPUT_SLOT_2 = "Slot2";
    protected const string INPUT_SLOT_3 = "Slot3";
    protected const string INPUT_SLOT_4 = "Slot4";
    protected const string INPUT_SLOT_5 = "Slot5";
    protected const string INPUT_SLOT_6 = "Slot6";
    protected const string INPUT_SLOT_7 = "Slot7";
    protected const string INPUT_SLOT_8 = "Slot8";
    protected const string INPUT_SLOT_9 = "Slot9";
    protected const string INPUT_SLOT_10 = "Slot10";
    protected const string INPUT_ENTER = "Submit";
}
