using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIEventListener : MonoBehaviour
{
    [SerializeField]
    private GameObject ammoCounterUI, pistolBulletUI, rifleBulletUI, missionFailedUI;
    [SerializeField]
    private TextMeshProUGUI ammoInClip, ammoInInventory, health, armor, alertTimer, nvGoggleBatteryText;
    [SerializeField]
    private CrosshairUI crosshair;
    [SerializeField]
    private Image nvGoggleBatteryImage;

    private PlayerHands playerHands;
    private RectTransform nvGoggleBatteryImageRect;

    private void Awake()
    {
        nvGoggleBatteryImageRect = nvGoggleBatteryImage.GetComponent<RectTransform>();
        HideAmmoUI();
        HideMissionFailedUI();
    }
    private void Start()
    {
        PlayerHealth.onHealthChanged += UpdateHealthUI;
        PlayerHealth.onArmorChanged += UpdateArmorUI;
        PlayerGun.onAmmoInClipChanged += UpdateAmmoInClipUI;
        PlayerGun.onGunEquipped += DisplayCrosshair;
        PlayerGun.onEnemyHit += DisplayHitMarker;
        PlayerInventory.onAmmoInInventoryChanged += UpdateAmmoInInventoryUI;
        AlertZoneManager.onTimerCountdown += UpdateAlertTimer;
        PlayerHands.onItemEquipped += DisplayItemSpecificUI;
        PlayerController.onMovementSpeedUpdate += UpdateCrosshairScale;
        PlayerController.onPlayerDeath += HideAllUI;
        NightVisionGogglesBattery.onBatteryChargeAmountChanged += UpdateNVGoggleBatteryUI;
        ItemBar.onItemAddedToSlot += SetNvGoggleBatteryImagePos;
        NPCController.onMissionFailed += DisplayMissionFailedUI;
        CheckpointManager.onResetGameStateToLastCheckpoint += HideMissionFailedUI;

        playerHands = PlayerController.Instance.GetComponent<PlayerHeldItem>().PlayerHands;
    }

    private void OnDestroy()
    {
        PlayerHealth.onHealthChanged -= UpdateHealthUI;
        PlayerHealth.onArmorChanged -= UpdateArmorUI;
        PlayerGun.onAmmoInClipChanged -= UpdateAmmoInClipUI;
        PlayerGun.onGunEquipped -= DisplayCrosshair;
        PlayerGun.onEnemyHit -= DisplayHitMarker;
        PlayerInventory.onAmmoInInventoryChanged -= UpdateAmmoInInventoryUI;
        AlertZoneManager.onTimerCountdown -= UpdateAlertTimer;
        PlayerHands.onItemEquipped -= DisplayItemSpecificUI;
        PlayerController.onMovementSpeedUpdate -= UpdateCrosshairScale;
        PlayerController.onPlayerDeath -= HideAllUI;
        NightVisionGogglesBattery.onBatteryChargeAmountChanged -= UpdateNVGoggleBatteryUI;
        ItemBar.onItemAddedToSlot -= SetNvGoggleBatteryImagePos;
        NPCController.onMissionFailed -= DisplayMissionFailedUI;
        CheckpointManager.onResetGameStateToLastCheckpoint -= HideMissionFailedUI;
    }

    private void DisplayItemSpecificUI(Enums.itemType itemType)
    {
        switch (itemType)
        {
            case (Enums.itemType.Pistol):
                DisplayPistolAmmoUI();
                break;
            case (Enums.itemType.AkRifle):
            case (Enums.itemType.M4Rifle):
                DisplayRifleAmmoUI();
                break;
            case (Enums.itemType.None):
            default:
                HideAmmoUI();
                break;
        }
    }

    private void DisplayPistolAmmoUI()
    {
        pistolBulletUI.SetActive(true);
        rifleBulletUI.SetActive(false);
        ammoCounterUI.SetActive(true);
    }
    private void DisplayRifleAmmoUI()
    {
        pistolBulletUI.SetActive(false);
        rifleBulletUI.SetActive(true);
        ammoCounterUI.SetActive(true);
    }
    private void HideAmmoUI()
    {
        ammoCounterUI.SetActive(false);
    }
    private void HideAllUI(bool playerIsDead)
    {
        if (!playerIsDead) return; //--------------
        HideAmmoUI();
        DisplayCrosshair(false);
    }
    private void DisplayMissionFailedUI()
    {
        missionFailedUI.SetActive(true);
    }
    private void HideMissionFailedUI()
    {
        missionFailedUI.SetActive(false);
    }

    private void UpdateAmmoInInventoryUI(Enums.ammoType ammoType, int amt)
    {
        Enums.itemType equippedItemType = playerHands.EquippedItemType;
        bool typeMatch = ((ammoType == Enums.ammoType.PistolAmmo && equippedItemType == Enums.itemType.Pistol) ||
            (ammoType == Enums.ammoType.RifleAmmo && (equippedItemType == Enums.itemType.AkRifle ||
            equippedItemType == Enums.itemType.M4Rifle)));

        if (typeMatch) SetNumToText(ammoInInventory, amt, false);
    }

    private void UpdateNVGoggleBatteryUI(float amt)
    {
        SetNumToText(nvGoggleBatteryText, (int)amt, false);
        nvGoggleBatteryImage.fillAmount = amt / 100f;
    }
    private void SetNvGoggleBatteryImagePos(Enums.itemType type, ItemSlot slot)
    {
        if (type != Enums.itemType.NightVisionGoggles) return; //---------
        nvGoggleBatteryImageRect.position = slot.ItemChargeBarPos.position;
    }

    private void UpdateAlertTimer(float amt) => SetNumToText(alertTimer, (int)amt, true);
    private void UpdateArmorUI(int amt) => SetNumToText(armor, amt, true);
    private void UpdateHealthUI(int amt) => SetNumToText(health, amt, false);
    private void UpdateAmmoInClipUI(int amt) => SetNumToText(ammoInClip, amt, false);
    private void SetNumToText(TextMeshProUGUI text, int amt, bool setZeroToEmpty)
    {
        if (setZeroToEmpty && amt <= 0) text.text = "";
        else text.text = amt.ToString();
    }

    private void UpdateCrosshairScale(float playerMovementSpeed) => crosshair.SetScale(playerMovementSpeed);
    private void DisplayCrosshair(bool active) => crosshair.DisplayImage(active);
    private void DisplayHitMarker() => crosshair.DisplayHitMarker();
    
}
