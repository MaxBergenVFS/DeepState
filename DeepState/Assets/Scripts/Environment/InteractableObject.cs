using System.Collections;
using UnityEngine;

[RequireComponent(typeof(ObjectSFX))]
public class InteractableObject : MonoBehaviour, IInteractable
{
    private const string ALPHA_PARAM = "_alphascale";

    private float defaultAlpha;
    private Collider col;
    private Renderer rend;
    private Material mat;
    private ObjectSFX sfx;

    [SerializeField]
    private float activeTime = 2.0f, lerpDuration = 0.2f;
    [SerializeField]
    private Renderer mirroredMesh;
    [SerializeField]
    private string targetedName;

    public string Name { get { return targetedName; } }

    private void Awake()
    {
        col = GetComponent<Collider>();
        rend = GetComponent<Renderer>();
        mat = rend.material;
        defaultAlpha = mat.GetFloat(ALPHA_PARAM);
        sfx = GetComponent<ObjectSFX>();
    }

    private void Start()
    {
        rend.enabled = false;
        if (mirroredMesh != null) mirroredMesh.enabled = false;
    }

    public void Interact()
    {
        StopAllCoroutines();
        ActivateMesh();
        Invoke("DeactivateMesh", activeTime);
        sfx.PlaySound();
    }

    private void ActivateMesh()
    {
        StartCoroutine(LerpAlphaScale(0, defaultAlpha));
        rend.enabled = true;
        if (mirroredMesh != null) mirroredMesh.enabled = true;
        if (col != null) col.enabled = false;
    }

    private void DeactivateMesh()
    {
        StartCoroutine(LerpAlphaScale(defaultAlpha, 0));
        if (mirroredMesh != null) mirroredMesh.enabled = false;
        if (col != null) col.enabled = true;
    }

    private IEnumerator LerpAlphaScale(float startValue, float endValue)
    {
        float t = 0;
        float a = startValue;
        while (t < lerpDuration)
        {
            a = Mathf.Lerp(startValue, endValue, t / lerpDuration);
            t += Time.deltaTime;
            mat.SetFloat(ALPHA_PARAM, a);
            yield return null;
        }
        a = endValue;
        if (endValue <= 0) rend.enabled = false;
    }
}
