using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private Queue<GameObject> inactiveQueue, activeQueue;
    [SerializeField] private GameObject objectToPool;
    [SerializeField] private int poolSize;
    [SerializeField] private float timeBeforeDeactivate;

    private void Start()
    {
        inactiveQueue = new Queue<GameObject>();
        activeQueue = new Queue<GameObject>();
        GameObject obj;
        for (int i = 0; i < poolSize; i++)
        {
            obj = Instantiate(objectToPool);
            obj.SetActive(false);
            inactiveQueue.Enqueue(obj);
        }
    }

    public GameObject GetPooledObject()
    {
        GameObject obj = null;
        if (inactiveQueue.Count > 0)
        {
            obj = inactiveQueue.Dequeue();
            obj.SetActive(true);
            activeQueue.Enqueue(obj);
            if (timeBeforeDeactivate > 0f) StartCoroutine(DeactivateObj(timeBeforeDeactivate, obj));
        }
        else
        {
            obj = activeQueue.Dequeue();
            activeQueue.Enqueue(obj);
        }
        return obj;
    }

    IEnumerator DeactivateObj(float duration, GameObject obj)
    {
        while (duration >= 0)
        {
            duration -= Time.deltaTime;
            yield return null;
        }
        obj.SetActive(false);
        inactiveQueue.Enqueue(obj);
    }

}
