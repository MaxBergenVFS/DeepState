using UnityEngine.UI;
using UnityEngine;

public class AlphaFadeOnEnable : MonoBehaviour
{
    [SerializeField]
    private float fadeInTime = 0.1f, fadeOutTime = 0.1f;

    private Image image;
    private Color currentColor;
    private float defaultAlpha, currentAlpha,
        fadeInTimeElapsed, fadeOutTimeElapsed;

    private void Awake()
    {
        image = GetComponent<Image>();
        currentColor = image.color;
        defaultAlpha = currentColor.a;
        this.enabled = false;
    }

    private void OnEnable()
    {
        SetAlpha(0f);
        fadeOutTimeElapsed = fadeInTimeElapsed = 0f;
    }

    private void Update()
    {
        if (fadeInTimeElapsed < fadeInTime)
        {
            currentAlpha = Mathf.Lerp(0f, defaultAlpha, fadeInTimeElapsed / fadeInTime);
            SetAlpha(currentAlpha);
            fadeInTimeElapsed += Time.deltaTime;
        }
        else if (fadeOutTimeElapsed < fadeOutTime)
        {
            currentAlpha = Mathf.Lerp(defaultAlpha, 0f, fadeOutTimeElapsed / fadeOutTime);
            SetAlpha(currentAlpha);
            fadeOutTimeElapsed += Time.deltaTime;
        }
        else
        {
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        SetAlpha(0f);
    }

    private void SetAlpha(float a)
    {
        currentColor.a = a;
        image.color = currentColor;
    }
}
