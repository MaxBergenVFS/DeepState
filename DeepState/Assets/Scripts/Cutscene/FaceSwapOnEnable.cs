using UnityEngine;

public class FaceSwapOnEnable : MonoBehaviour
{
    [SerializeField]
    private NPCFaceManager targetCharacter;
    [SerializeField]
    private float secBetweenFaceMatChanges;

    private float timeRemaining;

    private void OnEnable()
    {
        targetCharacter.ResetFace(false);
        timeRemaining = secBetweenFaceMatChanges;
    }

    private void Update()
    {
        timeRemaining -= Time.deltaTime;
        if (timeRemaining < 0)
        {
            targetCharacter.SetRandTalkingFace();
            timeRemaining = secBetweenFaceMatChanges;
        }
    }

    private void OnDisable()
    {
        targetCharacter.ResetFace(false);
    }
}
