using UnityEngine;

public class EquipmentViewportCamera : ViewportCamera
{
    [SerializeField]
    private GameObject redKeycardModel, blueKeycardModel, nvGogglesModel;

    public void DisplayModel(Enums.itemType itemType)
    {
        HideAllModels();
        switch (itemType)
        {
            case (Enums.itemType.RedKeycard):
                redKeycardModel.SetActive(true);
                break;
            case (Enums.itemType.BlueKeycard):
                blueKeycardModel.SetActive(true);
                break;
            case (Enums.itemType.NightVisionGoggles):
                nvGogglesModel.SetActive(true);
                break;
            case (Enums.itemType.None):
            default:
                break;
        }
        EnableCamera(true);
    }

    private void HideAllModels()
    {
        redKeycardModel.SetActive(false);
        blueKeycardModel.SetActive(false);
        nvGogglesModel.SetActive(false);
    }
}
