using System.Collections.Generic;

public class ItemsDroppedOnPlayerDeath : ItemContainer
{
    private Dictionary<int, int> itemsToDropAmounts = new Dictionary<int, int>();
    private List<IInventoryItem> inventoryItemsToDrop = new List<IInventoryItem>();

    private void Awake()
    {
        containsItems = true;
    }

    public void SetDroppedItemsList()
    {
        if (inventoryItemDatabase == null) Init();

        itemsToDropAmounts.Clear();
        inventoryItemsToDrop.Clear();
        foreach (int id in inventoryItemDatabase.GetListOfAllIds())
        {
            int databaseAmount = inventoryItemDatabase.GetInventoryItemAmount(id);
            int snapshotAmount = inventoryItemDatabase.GetSnapshotItemAmount(id);
            int difference = databaseAmount - snapshotAmount;

            //player has NOT picked up more of this item since last checkpoint
            if (difference < 1) continue;

            //add <id, amt> of relevant item(s) to itemsToDropAmounts
            itemsToDropAmounts.Add(id, difference);
            Enums.itemType type = inventoryItemDatabase.GetInventoryItem(id).itemType;
            foreach (IInventoryItem item in playerInventory.IInventoryItemsListRef)
            {
                //add relevant IInventoryItem refs to list
                if (item.ItemType == type) inventoryItemsToDrop.Add(item);
            }
        }
    }

    public override void ReturnItemsToPlayer()
    {
        foreach (KeyValuePair<int, int> item in itemsToDropAmounts)
        {
            int id = item.Key;
            int amt = item.Value;
            inventoryItemDatabase.AddToInventoryItemAmount(id, amt);
        }
        foreach (IInventoryItem item in inventoryItemsToDrop)
        {
            playerInventory.AddItemToGrid(item);
        }
    }
}
