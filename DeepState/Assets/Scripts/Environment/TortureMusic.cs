using UnityEngine;

public class TortureMusic : SFX, IInteractable
{
    [SerializeField]
    private string title = "Music Player";
    [SerializeField]
    private Transform speaker;

    private FMOD.Studio.EventInstance instance;
    private MusicManager musicManager;

    public string Name { get { return title; } }

    private void Start()
    {
        musicManager = GameManager.Instance.MusicManager;
        instance = FMODUnity.RuntimeManager.CreateInstance(SFX_PREFIX + TORTURE_METAL_MUSIC_PATH);
        GameManager.onGamePaused += StopPlaying;
    }

    private void OnDestroy()
    {
        GameManager.onGamePaused -= StopPlaying;
        instance.release();
    }

    private void OnDisable()
    {
        StopPlaying();
    }

    public void Interact()
    {
        if (InstanceIsPlaying(instance)) StopPlaying();
        else StartPlaying();
    }

    private void StartPlaying()
    {
        musicManager.StopGameplayMusic();
        StartInstanceAttached(instance, speaker);
    }
    private void StopPlaying()
    {
        if (!InstanceIsPlaying(instance)) return; //-----------------------
        musicManager.ResumeGameplayMusic();
        StopInstance(instance, true);
    }
}
