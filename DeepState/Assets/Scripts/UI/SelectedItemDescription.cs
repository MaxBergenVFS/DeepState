using TMPro;
using UnityEngine;

public class SelectedItemDescription : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI itemName, itemDescription, itemCriticalInfo;

    public void SetItemDescription(string name, string description, string criticalInfo)
    {
        itemName.text = name;
        itemDescription.text = description;
        itemCriticalInfo.text = criticalInfo;
    }

    public void Reset()
    {
        itemName.text = "";
        itemDescription.text = "";
        itemCriticalInfo.text = "";
    }
}
