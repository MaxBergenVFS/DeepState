using UnityEngine;

public class CutsceneDoor : Door
{
    [SerializeField]
    private GameObject cutsceneObj;

    private bool cutsceneHasBeenActivated;

    protected override void Awake()
    {
        base.Awake();
        cutsceneHasBeenActivated = false;
    }

    protected override void Open()
    {
        if (cutsceneHasBeenActivated)
        {
            base.Open();
        }
        else
        {
            cutsceneObj.SetActive(true);
            cutsceneHasBeenActivated = true;
        }
    }
}
