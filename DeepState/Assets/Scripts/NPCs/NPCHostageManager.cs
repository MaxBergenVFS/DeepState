using System.Collections.Generic;
using UnityEngine;

public class NPCHostageManager : MonoBehaviour
{
    [SerializeField]
    private NPCController[] hostages;

    private Dictionary<NPCController, bool> captorDeathDict = new Dictionary<NPCController, bool>();

    private void Start()
    {
        InitHostages();
    }

    public void AddCaptor(NPCController npc)
    {
        captorDeathDict.Add(npc, false);
    }
    public void SetCaptorDead(NPCController npc)
    {
        captorDeathDict[npc] = true;
        if (AllCaptorsAreDead()) FreeHostages();
    }
    private bool AllCaptorsAreDead()
    {
        bool allCaptorsAreDead = true;
        foreach (var captor in captorDeathDict)
        {
            if (captor.Value == false) allCaptorsAreDead = false;
        }
        return allCaptorsAreDead;
    }

    private void FreeHostages()
    {
        foreach (NPCController hostage in hostages)
        {
            hostage.SetIsCaptive(false);
            hostage.ForceRemoveAlert();
        }
    }
    private void InitHostages()
    {
        foreach (NPCController hostage in hostages)
        {
            hostage.SetIsCaptive(true);
        }
    }
}
