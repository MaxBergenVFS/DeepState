public static class Enums
{
    public enum collisionType
    {
        LeftArm,
        LeftLeg,
        RightArm,
        RightLeg,
        Chest,
        Head,
        Groin
    };

    public enum ammoType
    {
        None,
        RifleAmmo,
        PistolAmmo
    };

    public enum itemType
    {
        None,
        AkRifle,
        Wrench,
        Medpack,
        Chips,
        Soda,
        RedKeycard,
        BlueKeycard,
        BodyArmor,
        LockPick,
        HackingTool,
        Silencer,
        NightVisionGoggles,
        Pistol,
        Ammo,
        Credentials,
        CheckpointDevice,
        M4Rifle
    };

    public enum gunType
    {
        None,
        Pistol,
        AK,
        M4
    };

    public enum damageType
    {
        None,
        Melee,
        RangedPlayer,
        RangedNPC,
        RangedTurret
    };

    public enum simpleObjSfxType
    {
        None,
        HandDryer,
        Faucet,
        VendingMachine,
        Window,
        Crate,
        Hatch,
        SlidingDoor,
        StallDoor,
        SecurityCamera,
        LockerDoor,
        Vent,
        CardboardBox,
        Binder,
        Paper,
        Plate,
        Computer,
        ComputerMouse,
        Keyboard,
        Trashcan,
        VegetableBox,
        Datapad
    };

    public enum musicTrack
    {
        None,
        Unatco,
        Frigate,
        Afghanistan,
        Combat1,
        Combat2,
        AfghanistanDynamic,
        ResultsScreen,
        Combat3
    };

    public enum groundType
    {
        None,
        Dirt,
        Concrete,
        Metal
    };

    public enum itemSlotType
    {
        None,
        Grid,
        Bar
    };
}
