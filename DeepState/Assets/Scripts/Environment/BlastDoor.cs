using UnityEngine;

[RequireComponent(typeof(PauseAnimation))]
public class BlastDoor : MonoBehaviour
{
    private PauseAnimation pauseAnimation;
    private Animator anim;
    private bool freezeAnim, isOpen;
    
    [SerializeField]
    private bool closePermanently;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        pauseAnimation = GetComponent<PauseAnimation>();
        freezeAnim = false;
        isOpen = true;
    }

    public void Open()
    {
        if (closePermanently || isOpen) return;

        pauseAnimation.UnPauseAnim();
        anim.SetTrigger(AnimParams.DOOR_TRIGGER_OPEN);
        isOpen = true;
    }

    public void Close()
    {
        if (freezeAnim || !isOpen) return;

        pauseAnimation.UnPauseAnim();
        anim.SetTrigger(AnimParams.DOOR_TRIGGER_CLOSE);
        isOpen = false;

        if (closePermanently) freezeAnim = true;
    }
}
