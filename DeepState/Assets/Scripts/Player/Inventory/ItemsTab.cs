using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemsTab : InventoryTab
{
    [SerializeField]
    private TextMeshProUGUI title, description, amount;
    [SerializeField]
    private GameObject usePrompt;
    
    private PlayerInventory playerInventory;
    private InventoryItemDatabase inventoryItemDatabase;
    private int currentValidIdIndex, maxValidIdIndex;
    private InventoryItem currentInventoryItem = null;
    private List<int> validDatabaseIds;

    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
        SetDatabaseIDs();
    }

    public override void Display(bool cond)
    {
        if (cond)
        {
            SetDatabaseIDs();
            DisplayInventoryItem(validDatabaseIds[currentValidIdIndex]);
        }
        else
        {
            currentInventoryItem = null;
        }
        base.Display(cond);
    }

    // public override void Increment()
    // {
    //     base.Increment();
    //     currentValidIdIndex++;
    //     if (currentValidIdIndex >= maxValidIdIndex) currentValidIdIndex = 0;
    //     DisplayInventoryItem(validDatabaseIds[currentValidIdIndex]);
    // }

    // public override void Decrement()
    // {
    //     base.Decrement();
    //     currentValidIdIndex--;
    //     if (currentValidIdIndex < 0) currentValidIdIndex = maxValidIdIndex - 1;
    //     DisplayInventoryItem(validDatabaseIds[currentValidIdIndex]);
    // }

    // public override void Select()
    // {
    //     if (!currentInventoryItem.isUsable) return;

    //     base.Select();
    //     if (currentInventoryItem.healing > 0)
    //     {
    //         playerInventory.UseHealingItem(currentInventoryItem.itemType, currentInventoryItem.healing);
    //         amount.text = currentInventoryItem.amount.ToString();
    //     }
    // }

    private void SetDatabaseIDs()
    {
        validDatabaseIds = inventoryItemDatabase.GetListOfValidIds();
        maxValidIdIndex = validDatabaseIds.Count;
        if (currentValidIdIndex > maxValidIdIndex - 1) currentValidIdIndex = 0;
    }

    private void DisplayInventoryItem(int id)
    {
        currentInventoryItem = inventoryItemDatabase.GetInventoryItem(id);
        title.text = currentInventoryItem.title;
        description.text = currentInventoryItem.description;
        amount.text = currentInventoryItem.amount.ToString();
        usePrompt.SetActive(currentInventoryItem.isUsable);
    }
}
