using UnityEngine;

public class FaceBlinkOnEnable : MonoBehaviour
{
    [SerializeField]
    private NPCFaceManager targetCharacter;

    private void OnEnable()
    {
        targetCharacter.ResetFace(false);
        targetCharacter.SetBlinkFace();
    }

    private void OnDisable()
    {
        targetCharacter.ResetFace(false);
    }
}
