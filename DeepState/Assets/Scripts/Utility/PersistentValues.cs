using System.Collections.Generic;
using UnityEngine;

public class PersistentValues : MonoBehaviour
{
    [SerializeField]
    private Color bloodColor = Color.red;
    [SerializeField]
    private float bloodSize = 0.25f;

    private static PersistentValues instance;
    private Dictionary<int, bool> triggeredCodecCalls = new Dictionary<int, bool>();
    private UserSettings gammaSettings = new UserSettings();
    private UserSettings mouseSensitivitySettings = new UserSettings();
    private bool hasDisplayedFakeLoadingScreen = false;

    public Color BloodColor { get { return bloodColor; } }
    public float BloodSize { get { return bloodSize; } }
    public UserSettings GammaSettings { get { return gammaSettings; } }
    public UserSettings MouseSensitivitySettings { get { return mouseSensitivitySettings; } }
    public bool HasDisplayedFakeLoadingScreen { get { return hasDisplayedFakeLoadingScreen; } }

    public static PersistentValues Instance { get { return instance; } }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void InitCodecCall(int num)
    {
        if (triggeredCodecCalls.ContainsKey(num)) return; //--------------------
        triggeredCodecCalls.Add(num, false);
    }
    public void TriggerCodecCall(int num)
    {
        triggeredCodecCalls[num] = true;
    }
    public bool CodecCallHasBeenTriggered(int num)
    {
        return triggeredCodecCalls[num];
    }

    public void SetHasDisplayedFakeLoadingScreen()
    {
        hasDisplayedFakeLoadingScreen = true;
    }
   
    public void SetGamma(float gammaVal, float sliderVal)
    {
        if (!gammaSettings.isCalibrated) gammaSettings.isCalibrated = true;
        gammaSettings.val = gammaVal;
        gammaSettings.slider = sliderVal;
    }
    public void SetMouseSensitivity(float sensitivityVal, float sliderVal)
    {
        if (!mouseSensitivitySettings.isCalibrated) mouseSensitivitySettings.isCalibrated = true;
        mouseSensitivitySettings.val = sensitivityVal;
        mouseSensitivitySettings.slider = sliderVal;
    }
}
