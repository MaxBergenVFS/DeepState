﻿using UnityEngine;

public class InputManager : Inputs
{
    #region inits
    private const float IGNORE_INPUT_DURATION = 0.1f;

    private PlayerController playerController;
    private CameraController cameraController;
    private PlayerHeldItem playerHeldItem;
    private PlayerInventory playerInventory;
    private PauseGame pauseGame;
    private bool inventoryIsOpen, toggleSprint, inputIsDisabled, readyForInput;

    public bool InventoryIsOpen { get { return inventoryIsOpen; } }
    public bool InputIsDisabled { get { return inputIsDisabled; } }
    #endregion

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        cameraController = GetComponent<CameraController>();
        playerHeldItem = GetComponent<PlayerHeldItem>();
        playerInventory = GetComponent<PlayerInventory>();
        pauseGame = GetComponent<PauseGame>();
        readyForInput = true;
    }
    private void Start()
    {
        playerInventory.DisplaySprintUI(toggleSprint);
    }

    private void OnDisable() => FreezeMovement();

    void Update()
    {
        if (Input.GetButtonDown(INPUT_CANCEL))
        {
            if (inventoryIsOpen) playerInventory.ToggleInventoryUI();
            else if (pauseGame.UseExitTabAsPauseScreen) playerInventory.ToggleInventoryUIExitTab();
            else pauseGame.TogglePauseScreen();
        }

        if (inputIsDisabled) return; //----------------------------------------------------------------------------------------

        if (Input.GetButtonDown(INPUT_INVENTORY)) playerInventory.ToggleInventoryUI();

        if (inventoryIsOpen) return; //----------------------------------------------------------------------------------------

        #region movement
        cameraController.ControlCamera(Input.GetAxis(INPUT_MOUSE_X), Input.GetAxis(INPUT_MOUSE_Y));
        playerController.ControlPlayer(Input.GetAxis(INPUT_HORIZONTAL), Input.GetAxis(INPUT_VERTICAL));
        if (Input.GetButtonDown(INPUT_JUMP)) playerController.Jump();
        if (Input.GetButtonDown(INPUT_INTERACT)) cameraController.Interact();
        if (Input.GetButtonDown(INPUT_CROUCH)) playerController.CrouchDown();
        if (Input.GetButtonUp(INPUT_CROUCH)) playerController.CrouchUp();
        if (toggleSprint)
        {
            if (Input.GetButtonDown(INPUT_SPRINT)) playerController.ToggleSprint();
        }
        else
        {
            if (Input.GetButtonDown(INPUT_SPRINT)) playerController.SprintDown();
            if (Input.GetButton(INPUT_SPRINT)) playerController.SprintHold();
            if (Input.GetButtonUp(INPUT_SPRINT)) playerController.SprintUp();
        }
        #endregion

        #region actions
        if (Input.GetButtonDown(INPUT_FIRE_1)) playerHeldItem.PrimaryButtonDown();
        if (Input.GetButton(INPUT_FIRE_1)) playerHeldItem.PrimaryButtonHold();
        if (Input.GetButtonDown(INPUT_FIRE_2)) playerHeldItem.SecondaryButtonDown();
        if (Input.GetButtonDown(INPUT_RELOAD)) playerHeldItem.ReloadEquipped();
        if (Input.GetButtonDown(INPUT_MELEE)) playerHeldItem.ToggleMelee();

        if (!readyForInput) return; //-----------------------------------------------------------------------------------------

        if (Input.GetButtonDown(INPUT_SLOT_1))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(0);
        }
        if (Input.GetButtonDown(INPUT_SLOT_2))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(1);
        }
        if (Input.GetButtonDown(INPUT_SLOT_3))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(2);
        }
        if (Input.GetButtonDown(INPUT_SLOT_4))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(3);
        }
        if (Input.GetButtonDown(INPUT_SLOT_5))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(4);
        }
        if (Input.GetButtonDown(INPUT_SLOT_6))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(5);
        }
        if (Input.GetButtonDown(INPUT_SLOT_7))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(6);
        }
        if (Input.GetButtonDown(INPUT_SLOT_8))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(7);
        }
        if (Input.GetButtonDown(INPUT_SLOT_9))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(8);
        }
        if (Input.GetButtonDown(INPUT_SLOT_10))
        {
            StartReadyForInputTimer();
            playerInventory.UseItemAtSlotIndex(9);
        }
        if (Input.mouseScrollDelta.y > 0f)
        {
            StartReadyForInputTimer();
            playerInventory.IncrementEquippedItemOnBar();
        }
        if (Input.mouseScrollDelta.y < 0f)
        {
            StartReadyForInputTimer();
            playerInventory.DecrementEquippedItemOnBar();
        }
        #endregion
    }

    private void FreezeMovement()
    {
        playerController.ControlPlayer(0f, 0f);
        cameraController.ControlCamera(0f, 0f);
    }

    private void StartReadyForInputTimer()
    {
        readyForInput = false;
        CancelInvoke("ReadyForInput");
        Invoke("ReadyForInput", IGNORE_INPUT_DURATION);
    }
    private void ReadyForInput()
    {
        readyForInput = true;
    }

    #region API
    public void SetInventoryIsOpen(bool active)
    {
        inventoryIsOpen = active;
        if (inventoryIsOpen) FreezeMovement();
    }

    public void DisableInput(bool active)
    {
        inputIsDisabled = active;
        if (inputIsDisabled) FreezeMovement();
    }

    public void ToggleSprint()
    {
        toggleSprint = !toggleSprint;
        //TODO this sometimes causes an order of operations error
        playerInventory?.DisplaySprintUI(toggleSprint);
    }
    #endregion
}

