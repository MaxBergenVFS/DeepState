using UnityEngine;

public class AmmoPickup : BaseItem
{
    private string ammoName;
    private InventoryItem ammoInventoryItem;
    private PlayerHeldItem playerHeldItem;

    [SerializeField]
    private Enums.ammoType ammoType;
    [SerializeField]
    private int ammoAmountMin = 5, ammoAmountMax = 30;

    protected override void Start()
    {
        base.Start();
        playerHeldItem = playerController.GetComponent<PlayerHeldItem>();
        ammoInventoryItem = inventoryItemDatabase.GetInventoryItem(ammoType);
        ammoName = ammoInventoryItem.title;
        nameOverride = ammoName;
    }

    public override void Interact()
    {
        int ammoAmount = Random.Range(ammoAmountMin, ammoAmountMax);
        PickUp();
        string appendingText = $"{ammoAmount} {ammoName}";
        DisplayNotificationText(true, appendingText);
        playerInventory.AddAmmoToInventory(ammoType, ammoAmount);
    }
}
