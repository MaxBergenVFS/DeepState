using UnityEngine;

public class OpenNearbyDoors : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Door>() != null) other.GetComponent<Door>().OpenIfUnlockedAndClosed();
    }
}
