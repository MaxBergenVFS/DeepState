using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChangeManager : MonoBehaviour, IInteractable
{
    [SerializeField]
    private int sceneIndex;
    [SerializeField]
    private string sceneName;
    [SerializeField]
    private bool goToNextScene;
    [SerializeField]
    private float changeAfterSeconds;
    [SerializeField]
    private bool changeOnEnable;

    public string Name { get { return ""; } }

    private void Start()
    {
        if (changeAfterSeconds > 0f) Invoke("ChangeScene", changeAfterSeconds);
    }

    public void Interact()
    {
        ChangeScene();
    }

    private void OnEnable()
    {
        if (changeOnEnable) ChangeScene();
    }

    public void ChangeSceneFromMainMenu()
    {
        if (PersistentValues.Instance.HasDisplayedFakeLoadingScreen) SceneManager.LoadScene(sceneName);
        else SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ChangeScene()
    {
        //go to next scene in build settings
        if (goToNextScene)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            return;
        } //---------------------------------------------------------------

        //go to scene by name
        if (sceneName != "")
        {
            SceneManager.LoadScene(sceneName);
            return;
        } //----------------------------------------------------------------

        //go to scene by index
        SceneManager.LoadScene(sceneIndex);
    }

    public void Quit()
    {
        print("You've quit!");
        Application.Quit();
    }
}
