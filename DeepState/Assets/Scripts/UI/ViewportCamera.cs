using UnityEngine;

public class ViewportCamera : MonoBehaviour
{
    private Camera cam;

    protected virtual void Awake()
    {
        cam = GetComponent<Camera>();
        EnableCamera(false);
    }

    public void EnableCamera(bool active)
    {
        cam.enabled = active;
    }
}
