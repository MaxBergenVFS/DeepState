public class ToolPickup : BaseItem
{
    private PlayerHeldItem playerHeldItem;

    protected override void Start()
    {
        base.Start();
        playerHeldItem = playerController.GetComponent<PlayerHeldItem>();
    }

    public override void UseFromInventory(out bool success)
    {
        base.UseFromInventory(out success);
        playerHeldItem.Equip(itemType);
    }
}
