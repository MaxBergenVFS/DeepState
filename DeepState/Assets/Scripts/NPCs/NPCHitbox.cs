using UnityEngine;

public class NPCHitbox : MonoBehaviour, IDamageable
{
    [SerializeField]
    private NPCController controller;
    [SerializeField]
    private Enums.collisionType bodyPart;

    public NPCController Controller { get { return controller; } }
    public bool IsFriendly { get { return controller.IsFriendly; } }
    public bool IsDead { get { return controller.IsDead; } }

    public void TakeDamage(int amt, Vector3 hitPoint, float force, Enums.damageType type) => controller.DecreaseHealth(amt, bodyPart, hitPoint, force, type);
    public void VertexBloodColouring(Vector3 hitPoint) => controller.MaterialManager.VertexBloodColouring(hitPoint);
}
