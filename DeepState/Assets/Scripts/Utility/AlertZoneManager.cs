using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AlertZoneManager : MonoBehaviour
{
    [SerializeField]
    private float defaultCountdownTime = 30f;
    [SerializeField]
    private Alarm[] alarms;

    private Dictionary<int, bool> alertZoneStatus = new Dictionary<int, bool>();
    private float countdownTimeRemaining;
    private int alertZonesInScene;

    public delegate void OnAlertZoneStatusChange(int zone, bool alert);
    public static event OnAlertZoneStatusChange onAlertZoneStatusChange;
    public delegate void OnEnterHighAlert();
    public static event OnEnterHighAlert onEnterHighAlert;
    public delegate void OnDropCombat();
    public static event OnDropCombat onDropCombat;
    public delegate void OnTimerCountdown(float time);
    public static event OnTimerCountdown onTimerCountdown;

    public Dictionary<int, bool> AlertZoneStatus { get { return alertZoneStatus; } }

    private void Start()
    {
        InitAlertZones();
    }

    private void InitAlertZones()
    {
        alertZonesInScene = 0;
        foreach (Alarm alarm in alarms)
        {
            int alertZone = alarm.AlertZone;
            if (alertZone > alertZonesInScene)
            {
                alertZonesInScene = alertZone;
            }
        }
        for (int i = 0; i < alertZonesInScene; i++)
        {
            alertZoneStatus.Add(i, false);
        }
    }
    public void ResetAlertZones()
    {
        foreach (var status in alertZoneStatus.ToList())
        {
            alertZoneStatus[status.Key] = false;
            onAlertZoneStatusChange(status.Key, false);
        }
        if (onDropCombat != null) onDropCombat();
    }

    private IEnumerator StartTimer()
    {
        countdownTimeRemaining = defaultCountdownTime;
        while (countdownTimeRemaining > 0f)
        {
            countdownTimeRemaining -= Time.deltaTime;
            onTimerCountdown(countdownTimeRemaining);
            yield return null;
        }
        ResetAlertZones();
    }

    public void SetLevelOnHighAlert()
    {
        if (onEnterHighAlert != null) onEnterHighAlert();
    }

    public void SetAlertZoneStatus(int zone, bool alerted, bool includeCountdown)
    {
        if (zone >= alertZonesInScene || zone < 0) return; //--------------------------------------------------
        HandleAlertAndCombatEvents(zone, alerted);

        if (!includeCountdown || defaultCountdownTime <= 0f) return; //-------------------------------------------------------------
        StopCoroutine(StartTimer());
        StartCoroutine(StartTimer());
    }
    public void SetAlertZoneStatus(bool alerted)
    {
        for (int i = 0; i < alertZonesInScene; i++)
        {
            SetAlertZoneStatus(i, alerted, false);
        }
    }

    public bool TryGetAlarmRef(Transform npc, out Alarm alarm)
    {
        if (alarms.Length > 0)
        {
            alarm = FindClosestAlarm(npc);
            return !alarm.AssignedToNPC && !alarm.AlreadyActivated;
        }
        //NO ALARMS IN SCENE
        alarm = null;
        return false;
    }
    private Alarm FindClosestAlarm(Transform npc)
    {
        Alarm closestAlarm = null;
        float smallestDistance = 0f;
        foreach (Alarm alarm in alarms)
        {
            float distance = (npc.position - alarm.PatPoint.transform.position).magnitude;
            if (closestAlarm == null || distance < smallestDistance)
            {
                closestAlarm = alarm;
                smallestDistance = distance;
            }
        }
        return closestAlarm;
    }

    private void HandleAlertAndCombatEvents(int zone, bool alerted)
    {
        alertZoneStatus[zone] = alerted;
        if (!alertZoneStatus.ContainsValue(true)) ResetAlertZones();
        if (onAlertZoneStatusChange != null) onAlertZoneStatusChange(zone, alerted);
    }

}
