using TMPro;
using UnityEngine;

public class SavedNoteUIElement : MonoBehaviour
{   
    [HideInInspector]
    public int id;

    public TextMeshProUGUI title, content;
}
