// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "shader_lit_alphablend_fadeout_transformation"
{
	Properties
	{
		_distortionscale("distortion scale", Range( 0 , 5)) = 1
		_waterscale("water scale", Range( 0 , 50)) = 1
		_Gore("Gore", 2D) = "white" {}
		_base1("base 1", 2D) = "white" {}
		_Alpha("Alpha", Range( 0 , 1)) = -1
		_noisescale("noise scale", Range( 0 , 5)) = 1
		_noisefactor("noise factor", Range( 0 , 5)) = 1
		_waterscroll("water scroll", Vector) = (0,0,0,0)
		_distortionscroll("distortion scroll", Vector) = (0,0,0,0)
		_falloff_gore("falloff_gore", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Custom"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "Amplify" = "True"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha , SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
			float4 screenPos;
		};

		uniform sampler2D _base1;
		uniform float4 _base1_ST;
		sampler2D _Gore;
		uniform float2 _distortionscroll;
		uniform float _distortionscale;
		uniform float _noisescale;
		uniform float _noisefactor;
		uniform float2 _waterscroll;
		uniform float _waterscale;
		uniform float _falloff_gore;
		uniform float _Alpha;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline float4 TriplanarSampling17( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = tex2D( topTexMap, tiling * worldPos.zy * float2(  nsign.x, 1.0 ) );
			yNorm = tex2D( topTexMap, tiling * worldPos.xz * float2(  nsign.y, 1.0 ) );
			zNorm = tex2D( topTexMap, tiling * worldPos.xy * float2( -nsign.z, 1.0 ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			o.Normal = float3(0,0,1);
			float2 uv_base1 = i.uv_texcoord * _base1_ST.xy + _base1_ST.zw;
			float4 tex2DNode52 = tex2D( _base1, uv_base1 );
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 temp_cast_1 = (_distortionscale).xx;
			float2 uv_TexCoord37 = i.uv_texcoord * temp_cast_1;
			float2 panner40 = ( 1.0 * _Time.y * _distortionscroll + uv_TexCoord37);
			float simplePerlin2D42 = snoise( panner40*_noisescale );
			simplePerlin2D42 = simplePerlin2D42*0.5 + 0.5;
			float temp_output_45_0 = ( ase_screenPosNorm.w + ( simplePerlin2D42 * _noisefactor ) );
			float2 temp_cast_2 = (_waterscale).xx;
			float2 uv_TexCoord47 = i.uv_texcoord * temp_cast_2;
			float2 panner49 = ( 1.0 * _Time.y * _waterscroll + uv_TexCoord47);
			float2 temp_output_50_0 = ( temp_output_45_0 + panner49 );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			ase_vertexNormal = normalize( ase_vertexNormal );
			float4 triplanar17 = TriplanarSampling17( _Gore, ase_vertex3Pos, ase_vertexNormal, 1.0, temp_output_50_0, 1.0, 0 );
			float lerpResult28 = lerp( triplanar17.a , 0.0 , _falloff_gore);
			float4 lerpResult57 = lerp( tex2DNode52 , triplanar17 , lerpResult28);
			o.Albedo = lerpResult57.xyz;
			o.Alpha = _Alpha;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18921
-1269;337.3333;1268;763;1156.299;1498.858;1.323452;True;False
Node;AmplifyShaderEditor.RangedFloatNode;36;-2607.63,-1610.354;Inherit;False;Property;_distortionscale;distortion scale;0;0;Create;True;0;0;0;False;0;False;1;5;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;37;-2234.389,-1593.43;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;38;-2241.489,-1438.535;Inherit;False;Property;_distortionscroll;distortion scroll;14;0;Create;True;0;0;0;False;0;False;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PannerNode;40;-1944.806,-1512.593;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-2151.489,-1296.535;Inherit;False;Property;_noisescale;noise scale;10;0;Create;True;0;0;0;False;0;False;1;0.5;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;42;-1735.488,-1511.535;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-2341.914,-991.0512;Inherit;False;Property;_waterscale;water scale;2;0;Create;True;0;0;0;False;0;False;1;1;0;50;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-1846.42,-1210.196;Inherit;False;Property;_noisefactor;noise factor;11;0;Create;True;0;0;0;False;0;False;1;0.05;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;48;-1859.844,-863.953;Inherit;False;Property;_waterscroll;water scroll;12;0;Create;True;0;0;0;False;0;False;0,0;0,-0.075;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ScreenPosInputsNode;43;-1485.421,-1702.196;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;47;-1888.024,-1015.897;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-1438.387,-1340.07;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;45;-1264.421,-1441.196;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;49;-1586.447,-1026.939;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;50;-1100.057,-1398.26;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TriplanarNode;17;-786.1052,-1165.433;Inherit;True;Spherical;Object;False;Gore;_Gore;white;4;None;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Gore;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;23;-640.5508,-968.9348;Inherit;False;Property;_falloff_gore;falloff_gore;17;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;28;-258.8127,-1111.048;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;52;-431.4305,-779.0089;Inherit;True;Property;_base1;base 1;6;0;Create;True;0;0;0;False;0;False;-1;1645149555c535a4a9d0f1476d804fd8;1645149555c535a4a9d0f1476d804fd8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;51;-784.199,-1356.317;Inherit;True;Spherical;Object;False;Top Texture 0;_TopTexture0;white;3;None;Mid Texture 2;_MidTexture2;white;-1;None;Bot Texture 2;_BotTexture2;white;-1;None;Gore;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;55;-778.4013,-363.0667;Inherit;False;Property;_Float0;Float 0;16;0;Create;True;0;0;0;False;0;False;0;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;32;-748.0748,-1623.174;Inherit;True;Property;_gore1;gore1;8;0;Create;True;0;0;0;False;0;False;-1;1645149555c535a4a9d0f1476d804fd8;1645149555c535a4a9d0f1476d804fd8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;21;-1250.159,-965.644;Inherit;False;Property;_Tiling_alpha;Tiling_alpha;15;0;Create;True;0;0;0;False;0;False;0;0;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;1;-975.7059,-228.8691;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-860.0846,-33.83221;Inherit;False;Property;_Darkness;Darkness;13;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ObjSpaceViewDirHlpNode;33;-1266.523,-808.6112;Inherit;False;1;0;FLOAT4;0,0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;31;-819.0205,-747.0298;Inherit;True;Property;_alpha1;alpha1;7;0;Create;True;0;0;0;False;0;False;-1;1645149555c535a4a9d0f1476d804fd8;1645149555c535a4a9d0f1476d804fd8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-114.2433,-471.8178;Inherit;False;Property;_Alpha;Alpha;9;0;Create;True;0;0;0;False;0;False;-1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.VirtualTextureObject;34;-1084.445,-623.6204;Inherit;True;Property;_Texture0;Texture 0;20;0;Create;True;0;0;0;False;0;False;-1;None;None;False;white;Auto;Unity5;0;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.ColorNode;13;-802.3085,-226.9255;Inherit;False;Constant;_Color0;Color 0;2;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;35;-1302.938,-648.3831;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-887.7317,57.20919;Inherit;True;Property;_TextureSample0;Texture Sample 0;5;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;57;30.1134,-1269.28;Inherit;False;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BlendOpsNode;3;-503.2271,-88.91053;Inherit;False;Multiply;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-417.5712,-866.4512;Inherit;False;Property;_gore_alpha;gore_alpha;19;0;Create;True;0;0;0;False;0;False;0;0;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;14;5.342566,-1060.43;Inherit;False;HardMix;True;3;0;FLOAT;1;False;1;COLOR;1,1,1,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-799.127,-855.948;Inherit;False;Property;_swap;swap;18;0;Create;True;0;0;0;False;0;False;0;0;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;10;365.0801,-1239.927;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;shader_lit_alphablend_fadeout_transformation;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;1;True;False;0;True;Custom;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;2;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;37;0;36;0
WireConnection;40;0;37;0
WireConnection;40;2;38;0
WireConnection;42;0;40;0
WireConnection;42;1;39;0
WireConnection;47;0;46;0
WireConnection;44;0;42;0
WireConnection;44;1;41;0
WireConnection;45;0;43;4
WireConnection;45;1;44;0
WireConnection;49;0;47;0
WireConnection;49;2;48;0
WireConnection;50;0;45;0
WireConnection;50;1;49;0
WireConnection;17;3;50;0
WireConnection;28;0;17;4
WireConnection;28;2;23;0
WireConnection;51;9;50;0
WireConnection;32;1;45;0
WireConnection;31;1;21;0
WireConnection;57;0;52;0
WireConnection;57;1;17;0
WireConnection;57;2;28;0
WireConnection;3;0;13;0
WireConnection;3;1;2;0
WireConnection;3;2;12;0
WireConnection;14;0;28;0
WireConnection;14;1;52;0
WireConnection;14;2;24;0
WireConnection;10;0;57;0
WireConnection;10;9;11;0
ASEEND*/
//CHKSM=EE28A88EF44A1F3538B4B35BB759F76B054BE21A