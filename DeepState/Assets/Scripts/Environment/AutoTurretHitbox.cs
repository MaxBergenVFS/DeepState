using UnityEngine;

public class AutoTurretHitbox : MonoBehaviour, IDamageable
{
    [SerializeField]
    private AutoTurret autoTurret;
    [SerializeField]
    private Health health;

    public void TakeDamage(int amt, Vector3 pos, float force, Enums.damageType type)
    {
        if (amt < 0 || type == Enums.damageType.Melee) return; //------------------------
        
        health.DecreaseRemainingHealth(amt);
        if (health.RemainingHealth < 1) autoTurret.Destroy();
    }
}
