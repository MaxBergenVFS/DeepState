using UnityEngine;

public class BulletTracerFade : MonoBehaviour
{
    [SerializeField]
    private Color color;
    [SerializeField]
    private float fadeSpeed = 10f;

    private float defaultAlpha;

    LineRenderer rend;

    private void Awake()
    {
        rend = GetComponent<LineRenderer>();
        defaultAlpha = color.a;
    }

    private void OnEnable()
    {
        color.a = defaultAlpha;
    }

    private void Update()
    {
        color.a = Mathf.Lerp (color.a, 0, Time.deltaTime * fadeSpeed);
        rend.startColor = color;
        rend.endColor = color;
    }
}
