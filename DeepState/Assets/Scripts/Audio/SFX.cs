﻿using UnityEngine;

public class SFX : MonoBehaviour
{
    #region prefixes
    //private
    private const string GUN_PREFIX = "Gun/";
    private const string AK_PREFIX = GUN_PREFIX + "AK/";
    private const string M4_PREFIX = GUN_PREFIX + "M4/";
    private const string PISTOL_PREFIX = GUN_PREFIX + "Pistol/";
    private const string NPC_PREFIX = "NPC/";
    private const string WRENCH_PREFIX = "Wrench/";
    private const string LOCKPICK_PREFIX = "Lockpick/";
    private const string HACKING_TOOL_PREFIX = "HackingTool/";
    private const string CHECKPOINT_DEVICE_PREFIX = "CheckpointDevice/";
    private const string ITEM_PREFIX = PLAYER_PREFIX + "Items/";
    private const string NV_GOGGLES_PREFIX = ITEM_PREFIX + "NVGoggles/";
    private const string ENVIRONMENT_PREFIX = "Environment/";
    private const string PLAYER_GUN_PREFIX = PLAYER_PREFIX + GUN_PREFIX;
    private const string KEYPAD_PREFIX = OBJECT_PREFIX + "Keypad/";
    private const string SLIDING_DOOR_PREFIX = "SlidingDoor/";
    private const string VENDING_MACHINE_PREFIX = "VendingMachine/";
    private const string CHECKPOINT_DEVICE_OBJECT_PREFIX = OBJECT_PREFIX + "CheckpointDevice/";
    //protected
    protected const string SFX_PREFIX = "event:/SFX/";
    protected const string PLAYER_PREFIX = "Player/";
    protected const string TOOL_PREFIX = PLAYER_PREFIX + "Tool/";
    protected const string OBJECT_PREFIX = ENVIRONMENT_PREFIX + "Object/";
    #endregion
    #region paths
    //---------------GUNS------------------
    protected const string BULLET_HOLE_SFX = GUN_PREFIX + "Ricochet";
    protected const string BULLET_IMPACT_PATH = GUN_PREFIX + "Impact";
    protected const string OUT_OF_AMMO_PATH = GUN_PREFIX + "OutOfAmmo";
    protected const string SHOOT_SILENCED_PATH = GUN_PREFIX + "ShootSilenced";
    protected const string PISTOL_SHOOT_PATH = PISTOL_PREFIX + "Shoot";
    protected const string AK_SHOOT_PATH = AK_PREFIX + "Shoot";
    protected const string M4_SHOOT_PATH = M4_PREFIX + "Shoot";
    protected const string PISTOL_RELOAD_PATH = PISTOL_PREFIX + "Reload";
    protected const string AK_RELOAD_PATH = AK_PREFIX + "Reload";
    protected const string M4_RELOAD_PATH = M4_PREFIX + "Reload";
    //---------------NPC---------------------
    protected const string NPC_PAIN_PATH = NPC_PREFIX + "Pain";
    protected const string NPC_DEATH_PATH = NPC_PREFIX + "Death";
    protected const string NPC_FOOTSTEP_PATH = NPC_PREFIX + "Footstep";
    protected const string NPC_BLOOD_SPLATTER_PATH = NPC_PREFIX + "BloodSplatter";
    protected const string NPC_HEADSHOT_DIEGETIC_PATH = NPC_PREFIX + "HeadshotDiegetic";
    protected const string NPC_HEADSHOT_NONDIEGETIC_PATH = NPC_PREFIX + "HeadshotNonDiegetic";
    //-------------OBJECTS--------------------
    //with object prefix
    protected const string ALARM_PATH = OBJECT_PREFIX + "Alarm";
    protected const string SLIDING_DOOR_LOCKED_PATH = OBJECT_PREFIX + SLIDING_DOOR_PREFIX + "Locked";
    protected const string VENDING_MACHINE_NO_MONEY_PATH = OBJECT_PREFIX + VENDING_MACHINE_PREFIX + "NoMoney";
    protected const string KEYPAD_ACCESS_GRANTED_PATH = KEYPAD_PREFIX + "AccessGranted";
    protected const string KEYPAD_ACCESS_DENIED_PATH = KEYPAD_PREFIX + "AccessDenied";
    protected const string KEYPAD_BUTTON_PRESS_PATH = KEYPAD_PREFIX + "ButtonPress";
    protected const string CHECKPOINT_DEVICE_OBJECT_DEPLOY = CHECKPOINT_DEVICE_OBJECT_PREFIX + "Deploy";
    protected const string CHECKPOINT_DEVICE_OBJECT_IDLE = CHECKPOINT_DEVICE_OBJECT_PREFIX + "Idle";
    //without object prefix
    protected const string VENDING_MACHINE_PATH = VENDING_MACHINE_PREFIX + "Dispense";
    protected const string SLIDING_DOOR_PATH = SLIDING_DOOR_PREFIX + "Open";
    protected const string HAND_DRYER_PATH = "HandDryer";
    protected const string FAUCET_PATH = "Faucet";
    protected const string WINDOW_PATH = "GlassShatter";
    protected const string VENT_PATH = "VentBreak";
    protected const string CRATE_PATH = "CrateBreaking";
    protected const string HATCH_PATH = "Hatch";
    protected const string STALL_DOOR_PATH = "StallDoor";
    protected const string SECURITY_CAMERA_PATH = "SecurityCamera";
    protected const string LOCKER_DOOR_PATH = "LockerDoor";
    protected const string CARDBOARD_BOX_PATH = "CardboardBox";
    protected const string BINDER_PATH = "Binder";
    protected const string PAPER_PATH = "Paper";
    protected const string PLATE_PATH = "Plate";
    protected const string COMPUTER_MOUSE_PATH = "ComputerMouse";
    protected const string KEYBOARD_PATH = "Keyboard";
    protected const string TRASHCAN_PATH = "Trashcan";
    protected const string VEGETABLE_BOX_PATH = "VegetableBox";
    protected const string COMPUTER_PATH = "Computer";
    protected const string DATAPAD_PATH = "Datapad";
    //----------------TOOLS-----------------------
    protected const string WRENCH_SWING_PATH = WRENCH_PREFIX + "Swing";
    protected const string WRENCH_HIT_PATH = WRENCH_PREFIX + "Hit";
    protected const string WRENCH_ACTION_PATH = WRENCH_PREFIX + "Action";
    protected const string WRENCH_POSITIVE_FEEDBACK_PATH = WRENCH_PREFIX + "Feedback/Positive";
    protected const string WRENCH_NEGATIVE_FEEDBACK_PATH = WRENCH_PREFIX + "Feedback/Negative";
    protected const string HACKING_TOOL_ACTION_PATH = HACKING_TOOL_PREFIX + "Action";
    protected const string HACKING_TOOL_POSITIVE_FEEDBACK_PATH = HACKING_TOOL_PREFIX + "Feedback/Positive";
    protected const string HACKING_TOOL_NEGATIVE_FEEDBACK_PATH = HACKING_TOOL_PREFIX + "Feedback/Negative";
    protected const string LOCKPICK_ACTION_PATH = LOCKPICK_PREFIX + "Action";
    protected const string LOCKPICK_POSITIVE_FEEDBACK_PATH = LOCKPICK_PREFIX + "Feedback/Positive";
    protected const string LOCKPICK_NEGATIVE_FEEDBACK_PATH = LOCKPICK_PREFIX + "Feedback/Negative";
    protected const string CHECKPOINT_DEVICE_ACTION_PATH = CHECKPOINT_DEVICE_PREFIX + "Action";
    protected const string CHECKPOINT_DEVICE_POSITIVE_FEEDBACK_PATH = CHECKPOINT_DEVICE_PREFIX + "Feedback/Positive";
    protected const string CHECKPOINT_DEVICE_NEGATIVE_FEEDBACK_PATH = CHECKPOINT_DEVICE_PREFIX + "Feedback/Negative";
    //-------------------PLAYER--------------------
    protected const string PLAYER_FOOTSTEP_PATH = PLAYER_PREFIX + "Footstep";
    protected const string PLAYER_JUMP_PATH = PLAYER_PREFIX + "Jump/Up";
    protected const string PLAYER_PAIN_PATH = PLAYER_PREFIX + "Pain";
    protected const string PLAYER_DEATH_PATH = PLAYER_PREFIX + "Death";
    protected const string PLAYER_MONEY_PATH = PLAYER_PREFIX + "Money";
    protected const string PLAYER_AMMO_PICKUP_PATH = PLAYER_PREFIX + "AmmoPickup";
    protected const string MELEE_SWING_NO_HIT_PATH = PLAYER_PREFIX + "MeleeSwingNoHit";
    protected const string MELEE_SWING_HIT_PATH = PLAYER_PREFIX + "MeleeSwingHit";
    protected const string INVENTORY_CLOSE_PATH = PLAYER_PREFIX + "CloseInventory";
    protected const string INVENTORY_OPEN_PATH = PLAYER_PREFIX + "OpenInventory";
    protected const string ITEM_PICK_UP_PATH = ITEM_PREFIX + "PickUp";
    protected const string ITEM_DROP_PATH = ITEM_PREFIX + "Drop";
    protected const string ITEM_ADD_TO_BAR_PATH = ITEM_PREFIX + "AddToBar";
    protected const string EQUIP_BODY_ARMOR_PATH = ITEM_PREFIX + "BodyArmor";
    protected const string NV_GOGGLES_ON_PATH = NV_GOGGLES_PREFIX + "On";
    protected const string NV_GOGGLES_OFF_PATH = NV_GOGGLES_PREFIX + "Off";
    protected const string CONSUME_MEDPACK_PATH = ITEM_PREFIX + "Medpack";
    protected const string CONSUME_CHIPS_PATH = ITEM_PREFIX + "Chips";
    protected const string CONSUME_SODA_PATH = ITEM_PREFIX + "Soda";
    protected const string AT_FULL_HEALTH_PATH = PLAYER_PREFIX + "AtFullHealth";
    protected const string HIT_MARKER_PATH = PLAYER_GUN_PREFIX + "HitMarker";
    protected const string NOTE_ADDED_PATH = PLAYER_PREFIX + "NoteAdded";
    //------------------MISC------------------------
    protected const string TORTURE_METAL_MUSIC_PATH = ENVIRONMENT_PREFIX + "TortureMetal";
    #endregion

    //TODO
    protected const string NOTIFICATION_PATH = INVENTORY_OPEN_PATH;
    protected const string ITEM_SELECT_ON_BAR_PATH = ITEM_ADD_TO_BAR_PATH;

    #region params
    //footsteps
    protected const string FOOTSTEP_DIRT_PARAM = "Dirt";
    protected const string FOOTSTEP_CONCRETE_PARAM = "Concrete";
    protected const string FOOTSTEP_METAL_PARAM = "Metal";
    #endregion

    protected void PlaySound2D(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(SFX_PREFIX + path);
    }
    protected void PlaySound3D(string path) => PlaySound3D(path, transform.position);
    protected void PlaySound3D(string path, Vector3 pos)
    {
        FMODUnity.RuntimeManager.PlayOneShot(SFX_PREFIX + path, pos);
    }

    protected void StartInstance(FMOD.Studio.EventInstance instance)
    {
        instance.start();
    }
    protected void StopInstance(FMOD.Studio.EventInstance instance) => StopInstance(instance, false);
    protected void StopInstance(FMOD.Studio.EventInstance instance, bool fadeOut)
    {
        if (fadeOut) instance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        else instance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    protected void StartInstanceAttached(FMOD.Studio.EventInstance instance) => StartInstanceAttached(instance, transform);
    protected void StartInstanceAttached(FMOD.Studio.EventInstance instance, Transform transformOverride)
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(instance, transformOverride);
        instance.start();
    }
    protected void StartInstanceAttached(FMOD.Studio.EventInstance instance, Rigidbody rb)
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(instance, transform, rb);
        instance.start();
    }

    protected bool InstanceIsPlaying(FMOD.Studio.EventInstance instance)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState(out state);
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }
}
