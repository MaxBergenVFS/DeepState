using UnityEngine.AI;
using UnityEngine;

public class NPCPatrolBehaviour : StateMachineBehaviour
{
    private NavMeshAgent agent;
    private Vector3 destination;
    private Transform transform;
    private NPCController controller;
    private enum stateModifier
    {
        None,
        RunForCover,
        RunForAlarm,
        Searching
    }

    [SerializeField]
    private stateModifier modifier = stateModifier.None;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent = animator.GetComponent<NavMeshAgent>();
        transform = animator.GetComponent<Transform>();
        controller = animator.GetComponent<NPCController>();

        controller.EnableDialogue();

        if (modifier == stateModifier.None || modifier == stateModifier.Searching)
        {
            //NPC IS PATROLLING
            if (animator.GetBool(AnimParams.NPC_BOOL_HAS_PAT_POINTS))
            {
                if (animator.GetBool(AnimParams.NPC_BOOL_STOPPED_MID_PAT)) destination = controller.GetCurrentPatPoint();
                else destination = controller.GetNextPatPoint();
            }
            else
            {
                destination = controller.StartingPos;
            }
            animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, true);
            controller.SetNavSpeedToWalk();
        }
        else
        {
            //NPC IS RUNNING FOR COVER OR ALARM
            switch (modifier)
            {
                case (stateModifier.RunForCover):
                    destination = controller.GetRunForCoverPoint();
                    break;
                case (stateModifier.RunForAlarm):
                default:
                    destination = controller.GetNearbyAlarmPoint();
                    break;
            }
            animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, true);
            controller.SetNavSpeedToRun();
            if (!animator.GetBool(AnimParams.NPC_BOOL_ALERTED))
            {
                animator.SetBool(AnimParams.NPC_BOOL_ALERTED, true);
            }
        }
        animator.SetBool(AnimParams.NPC_BOOL_STOPPED_MID_PAT, false);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(destination);
        float distanceToDestination = (transform.position - destination).magnitude;
        if (distanceToDestination <= agent.stoppingDistance)
        {
            //enter IDLE, COWER, or RAISE ALARM state
            if (controller.IdleTime > 0f || modifier == stateModifier.RunForAlarm || modifier == stateModifier.RunForCover) animator.SetBool(AnimParams.NPC_BOOL_AT_DEST, true);
            //get new dest and keep patrolling
            else destination = controller.GetNextPatPoint();
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (modifier != stateModifier.Searching) animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, false);
    }
}
