using UnityEngine;

public class CheckpointDevice : PlayerTool
{
    private const float HEIGHT_OFFSET = 1.8f;
    private const float RAYCAST_DISTANCE = 7f;
    private const float PLACE_ITEM_DELAY = 1.4f;

    [SerializeField]
    private GameObject checkpointNoColPrefab;
    [SerializeField]
    private LayerMask groundLayer;

    private Transform player, marker;
    private Camera mainCamera;
    private Renderer markerRend;
    private Vector3 placementPos = new Vector3();

    protected override void Start()
    {
        base.Start();
        player = playerController.transform;
        mainCamera = playerController.GetComponent<CameraController>().MainCamera;
        marker = GameManager.Instance.CheckpointItemPlacementMarker.transform;
        markerRend = marker.GetComponent<Renderer>();
        markerRend.enabled = false;
    }

    private void PlaceItem()
    {
        GameObject checkpoint = Instantiate(checkpointNoColPrefab.gameObject, placementPos, Quaternion.identity);
        checkpoint.GetComponent<Checkpoint>().InitAndActivateThisCheckpoint();
        markerRend.enabled = inventoryItemDatabase.GetInventoryItemAmount(itemType) > 0;
        playerHands.UnequipIfDepleted();
        playerInventory.RefreshItemsOnBar();
    }

    private void Update()
    {
        if (playerHands.EquippedItemType != Enums.itemType.CheckpointDevice || playerHands.CurrentlyUsingItem) return; //----------------

        marker.position = GetPos();
    }

    private Vector3 GetPos()
    {
        Vector3 pos = new Vector3();
        Ray ray = mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, RAYCAST_DISTANCE, groundLayer))
        {
            pos = hit.point;
            pos.y = player.position.y - HEIGHT_OFFSET;
        }
        else
        {
            pos = player.position;
            pos.y -= HEIGHT_OFFSET;
        }
        return pos;
    }

    #region API
    public override void UsePrimary()
    {
        if (inventoryItemDatabase.GetInventoryItemAmount(itemType) < 1) return; //------------------------------------------
        if (playerHands.CurrentlyUsingItem || playerHands.CurrentlyHolsteringItem) return; //-----

        base.UsePrimary();
        placementPos = GetPos();
        marker.position = placementPos;
        inventoryItemDatabase.AddToInventoryItemAmount(itemType, -1);
        Invoke("PlaceItem", PLACE_ITEM_DELAY);
    }

    public override void Equip()
    {
        base.Equip();
        markerRend.enabled = true;
    }

    public override void Unequip()
    {
        base.Unequip();
        markerRend.enabled = false;
    }
    #endregion
}
