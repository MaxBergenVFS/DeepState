using System.Collections.Generic;
using UnityEngine;

public class SavedNotesManager : MonoBehaviour
{
    [SerializeField]
    private GameObject contentPanel, savedNotePrefab;
    [SerializeField]
    private string missionDebriefTitle;
    [SerializeField]
    [TextArea(3, 10)]
    private string missionDebriefContent;

    private NotificationUI notification;
    private int currentNoteId;
    private List<SavedNote> savedNotesSinceLastCheckpoint = new List<SavedNote>();
    private List<SavedNote> savedNotes = new List<SavedNote>();

    public List<SavedNote> SavedNotes { get { return savedNotes; } }

    public delegate void OnNoteSaved();
    public static event OnNoteSaved onNoteSaved;

    private void Awake()
    {
        currentNoteId = 0;
    }
    private void Start()
    {
        notification = NotificationUI.Instance;
        AddToSavedNotes(missionDebriefTitle, missionDebriefContent, false);
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    private void SnapshotState()
    {
        savedNotesSinceLastCheckpoint.Clear();
    }
    private void RevertState()
    {
        foreach (SavedNote note in savedNotesSinceLastCheckpoint)
        {
            RemoveFromSavedNotes(note);
        }
        savedNotesSinceLastCheckpoint.Clear();
    }

    public void AddToSavedNotes(string title, string content) => AddToSavedNotes(title, content, true);
    private void AddToSavedNotes(string title, string content, bool notif)
    {
        SavedNote note = new SavedNote(title, content, currentNoteId++);
        savedNotes.Add(note);
        AddNoteToContentPanel(note);
        savedNotesSinceLastCheckpoint.Add(note);
        if (notif) notification.NoteAdded();
        if (onNoteSaved != null) onNoteSaved();
    }
    private void RemoveFromSavedNotes(SavedNote note)
    {
        savedNotes.Remove(note);
        RemoveNoteFromContentPanel(note.id);
    }

    private void AddNoteToContentPanel(SavedNote note)
    {
        GameObject obj = Instantiate(savedNotePrefab) as GameObject;
        SavedNoteUIElement noteUI = obj.GetComponent<SavedNoteUIElement>();
        noteUI.title.text = note.title;
        noteUI.content.text = note.content;
        noteUI.id = note.id;
        obj.transform.parent = contentPanel.transform;
        obj.transform.localScale = Vector3.one;
    }
    private void RemoveNoteFromContentPanel(int id)
    {
        SavedNoteUIElement[] notes = contentPanel.GetComponentsInChildren<SavedNoteUIElement>();
        foreach (SavedNoteUIElement note in notes)
        {
            if (note.id == id)
            {
                Destroy(note.gameObject);
                return; //----------------------------------------------
            }
        }
    }
}
