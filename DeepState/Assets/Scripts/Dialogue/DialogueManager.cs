﻿using TMPro;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DialogueManager : MonoBehaviour
{
    private const string DIALOGUE_TYPING_PATH = "event:/UI/dialogueTyping";
    private const string REX_NAME_LOWER = "rex";

    private static DialogueManager instance;
    public static DialogueManager Instance { get { return instance; } }

    [SerializeField]
    private TextMeshProUGUI dialogueText, characterName;
    [SerializeField]
    private float timeBetweenLetters = 0.1f;
    [SerializeField]
    private Codec codec;

    private Queue<string> sentences = new Queue<string>();
    private bool sentenceIsStillBeingTyped, currentlyInDialogue;
    private Animator anim;
    private string currentSentenceRef;
    private InputManager playerInput;
    private PlayerHands playerHands;

    public bool SentenceIsStillBeingTyped { get { return sentenceIsStillBeingTyped; } }
    public bool CurrentlyInDialogue { get { return currentlyInDialogue; } }
    // public delegate void OnDisplayDialogueUI(bool active);
    // public static event OnDisplayDialogueUI onDisplayDialogueUI;

    private void Awake()
    {
        instance = this;
        anim = GetComponent<Animator>();
    }
    private void Start()
    {
        playerHands = PlayerController.Instance.GetComponent<PlayerHeldItem>().PlayerHands;
        PlayerController.onPlayerDeath += HideUI;
    }

    private void OnDestroy()
    {
        PlayerController.onPlayerDeath -= HideUI;
    }

    public void StartDialogue(Dialogue dialogue) => StartDialogue(dialogue, characterName, dialogueText);
    private void StartDialogue(Dialogue dialogue, TextMeshProUGUI characterNameElement, TextMeshProUGUI textElement)
    {
        if (!currentlyInDialogue)
        {
            anim.SetBool(AnimParams.UI_DIALOGUE_BOOL_IS_OPEN, true);
            characterNameElement.text = dialogue.characterName;
            sentences.Clear();
            foreach (string sentence in dialogue.sentences)
            {
                sentences.Enqueue(sentence);
            }
            //if (onDisplayDialogueUI != null) onDisplayDialogueUI(true);
        }
        DisplayNextSentence(textElement);
    }

    private void DisplayNextSentence(TextMeshProUGUI textElement)
    {
        currentlyInDialogue = true;

        if (sentences.Count <= 0 && !sentenceIsStillBeingTyped)
        {
            EndDialogue();
            return;
        }

        if (sentenceIsStillBeingTyped)
        {
            StopAllCoroutines();
            textElement.text = currentSentenceRef;
            sentenceIsStillBeingTyped = false;
        }
        else
        {
            string sentence = sentences.Dequeue();
            currentSentenceRef = sentence;
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence, textElement));
        }
    }

    IEnumerator TypeSentence(string sentence, TextMeshProUGUI textElement)
    {
        sentenceIsStillBeingTyped = true;
        textElement.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            textElement.text += letter;
            if (textElement.text == sentence) sentenceIsStillBeingTyped = false;
            FMODUnity.RuntimeManager.PlayOneShot(DIALOGUE_TYPING_PATH);
            yield return new WaitForSeconds(timeBetweenLetters);
        }
    }

    public void EndDialogue()
    {
        StopAllCoroutines();
        sentenceIsStillBeingTyped = false;
        currentlyInDialogue = false;
        dialogueText.text = "";
        characterName.text = "";
        anim.SetBool(AnimParams.UI_DIALOGUE_BOOL_IS_OPEN, false);
        //if (onDisplayDialogueUI != null) onDisplayDialogueUI(false);
    }

    public void StartCodecDialogue(Dialogue dialogue)
    {
        if (!anim.GetBool(AnimParams.UI_DIALOGUE_BOOL_IS_CODEC)) anim.SetBool(AnimParams.UI_DIALOGUE_BOOL_IS_CODEC, true);
        if (dialogue.characterName.ToLower() == REX_NAME_LOWER)
        {
            codec.ShowRexDialogue();
            StartDialogue(dialogue, codec.CharacterName, codec.CodecTextRex);
        }
        else
        {
            codec.ShowNPCDialogue();
            StartDialogue(dialogue, codec.CharacterName, codec.CodecTextNPC);
        }
    }

    public void StartCodecCall(CodecTrigger codecTrigger) => codec.StartCodecCall(codecTrigger);
    public void EndCodecCall()
    {
        anim.SetBool(AnimParams.UI_DIALOGUE_BOOL_IS_CODEC, false);
        playerHands.ReEquipItem();
        StopAllCoroutines();
        sentenceIsStillBeingTyped = false;
        currentlyInDialogue = false;
        codec.EndCodecCall();
    }

    private void HideUI(bool playerIsDead)
    {
        if (playerIsDead) EndDialogue();
    }
}
