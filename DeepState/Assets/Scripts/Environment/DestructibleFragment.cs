using System.Collections;
using UnityEngine;

public class DestructibleFragment : Fragment
{
    private const string PARAM_ALPHA = "_Alpha";

    private Material mat;
    private Crate crate;
    private float explosionRadius = 10f;

    protected override void Awake()
    {
        base.Awake();
        mat = GetComponent<Renderer>().material;
        crate = GetComponentInParent<Crate>();
    }

    public override void Reset()
    {
        base.Reset();
        StopAllCoroutines();
        mat.SetFloat(PARAM_ALPHA, 1f);
        this.gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        rb.AddExplosionForce(crate.ExplosiveForce, crate.Origin, explosionRadius);
        StartCoroutine(FadeOutMaterial());
    }

    IEnumerator FadeOutMaterial()
    {
        yield return new WaitForSeconds(crate.SecondsBeforeFadeout);

        float t = 0;
        float alpha;
        float fadeoutTime = crate.FadeoutTime;

        while (t < 1f)
        {
            alpha = Mathf.Lerp(1f, 0f, t);
            t += Time.deltaTime / fadeoutTime;
            mat.SetFloat(PARAM_ALPHA, alpha);
            yield return null;
        }
        this.gameObject.SetActive(false);
    }
}
