using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    private const float VERTICAL_OFFSET = 0.2f;

    private Vector3 playerStartingPos = new Vector3();
    private int playerHealthAtCheckpoint, playerArmorAtCheckpoint;
    private Dictionary<int, bool> alertStatusSnapshot = new Dictionary<int, bool>();
    private List<IRespawnable> respawnablesInScene;
    private Checkpoint currentCheckpoint;
    private AlertZoneManager alertZoneManager;
    private FogSettings fogSettingsAtCheckpoint;
    private PlayerHealth playerHealth;

    public delegate void OnResetGameStateToLastCheckpoint();
    public static event OnResetGameStateToLastCheckpoint onResetGameStateToLastCheckpoint;
    public delegate void OnSetCheckpoint();
    public static event OnSetCheckpoint onSetCheckpoint;
    public FogSettings FogSettingsAtCheckpoint { get { return fogSettingsAtCheckpoint; } }
    public int PlayerHealthAtCheckpoint
    {
        get
        {
            if (playerHealthAtCheckpoint <= 0) return 1;
            if (playerHealthAtCheckpoint > playerHealth.MaxHealth) return playerHealth.MaxHealth;
            return playerHealthAtCheckpoint;
        }
    }
    public int PlayerArmorAtCheckpoint
    {
        get
        {
            if (playerArmorAtCheckpoint < 0) return 0;
            if (playerArmorAtCheckpoint > playerHealth.MaxArmor) return playerHealth.MaxArmor;
            return playerArmorAtCheckpoint;
        }
    }

    private void Awake()
    {
        respawnablesInScene = new List<IRespawnable>();
    }
    private void Start()
    {
        playerStartingPos = PlayerController.Instance.transform.position;
        alertZoneManager = GameManager.Instance.AlertZoneManager;
        playerHealth = PlayerController.Instance.GetComponent<PlayerHealth>();
    }

    public Vector3 GetCurrentCheckpointPos()
    {
        if (currentCheckpoint != null) return currentCheckpoint.PlayerCheckpointPos + Vector3.up * VERTICAL_OFFSET;
        return playerStartingPos;
    }

    public void SetCurrentCheckpoint(Checkpoint checkpoint)
    {
        currentCheckpoint = checkpoint;
        playerHealthAtCheckpoint = playerHealth.RemainingHealth;
        playerArmorAtCheckpoint = playerHealth.RemainingArmor;
        fogSettingsAtCheckpoint = new FogSettings(RenderSettings.fogMode, RenderSettings.fogColor, RenderSettings.fogDensity, RenderSettings.fogStartDistance, RenderSettings.fogEndDistance);
        ResetRespawnables();
        if (onSetCheckpoint != null) onSetCheckpoint();
    }

    public void ResetGameStateToLastCheckpoint()
    {
        RespawnRespawnables();
        ResetRespawnables();
        alertZoneManager.ResetAlertZones();
        if (onResetGameStateToLastCheckpoint != null) onResetGameStateToLastCheckpoint();
    }

    private void RespawnRespawnables()
    {
        foreach (IRespawnable respawnable in respawnablesInScene)
        {
            respawnable.Respawn();
        }
    }
    private void ResetRespawnables()
    {
        respawnablesInScene.Clear();
    }
    public void AddRespawnable(IRespawnable respawnable)
    {
        respawnablesInScene.Add(respawnable);
    }
}
