using UnityEngine;
using UnityEngine.AI;

public class NPCRepositionBehaviour : StateMachineBehaviour
{
    private NavMeshAgent agent;
    private Transform transform;
    private NPCController controller;
    private Vector3 repositionPoint;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, true);
        animator.SetBool(AnimParams.NPC_BOOL_IN_DIALOGUE, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_STRAFING, false);
        animator.SetBool(AnimParams.NPC_BOOL_AT_REPOSITION_POINT, false);
        if (!animator.GetBool(AnimParams.NPC_BOOL_ALERTED)) animator.SetBool(AnimParams.NPC_BOOL_ALERTED, true);
        agent = animator.GetComponent<NavMeshAgent>();
        controller = animator.GetComponent<NPCController>();
        transform = animator.GetComponent<Transform>();

        bool success;
        repositionPoint = controller.GetRepositionPoint(out success);
        if (success) controller.SetNavSpeedToRun();
        else animator.SetBool(AnimParams.NPC_BOOL_IS_REPOSITIONING, false);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(repositionPoint);
        Vector3 distanceToWalkPoint = transform.position - repositionPoint;
        if (distanceToWalkPoint.magnitude <= agent.stoppingDistance) animator.SetBool(AnimParams.NPC_BOOL_AT_REPOSITION_POINT, true);
        controller.RemoveAlertIfPlayerNotInLoS();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_REPOSITIONING, false);
    }
}
