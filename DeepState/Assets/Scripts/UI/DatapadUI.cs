using TMPro;
using UnityEngine;

public class DatapadUI : PadUI
{
    [SerializeField]
    private TextMeshProUGUI titleText, contentText;

    protected override void Awake()
    {
        base.Awake();
        titleText.text = "";
        contentText.text = "";
    }

    public void Show(string title, string content)
    {
        titleText.text = title;
        contentText.text = content;
        base.Show();
    }

    public override void Hide()
    {
        if (!background.activeSelf) return;

        titleText.text = "";
        contentText.text = "";
        base.Hide();
    }

}
