using UnityEngine;

public class FootstepTrigger : MonoBehaviour
{
    private PlayerFootstep playerFootstep;
    private Collider observed;

    [SerializeField]
    private Enums.groundType type;

    private void Awake()
    {
        ResetObserved();
    }
    private void Start()
    {
        playerFootstep = PlayerController.Instance.GetComponent<PlayerFootstep>();
        CheckpointManager.onResetGameStateToLastCheckpoint += ResetObserved;
    }

    private void OnDestroy()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint -= ResetObserved;
    }

    private void Update()
    {
        if (observed == null) return; //------------
        playerFootstep.SetFootstepParam(type, true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER)) observed = other;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER))
        {
            playerFootstep.SetFootstepParam(type, false);
            ResetObserved();
        }
    }

    private void ResetObserved()
    {
        observed = null;
    }

}
