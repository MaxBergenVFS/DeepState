using UnityEngine;
using UnityEngine.AI;

public class NavMeshRandomPoint : MonoBehaviour
{
    private const float SAMPLE_POSITION_MAX_DIST = 4f;
    private const int ATTEMPT_MAX = 4;

    [SerializeField]
    private float repositionRadius, patrolRadius, strafeRadius;
    [SerializeField]
    private Transform patrolOrigin;

    private NavMeshAgent agent;
    private Transform player;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        if (patrolOrigin == null) patrolOrigin = transform;
    }
    private void Start()
    {
        player = PlayerController.Instance.transform;
    }

    public Vector3 GetStrafePoint(out bool success) => GetRandomPoint(player, strafeRadius, out success);
    public Vector3 GetRepositionPoint(out bool success) => GetRandomPoint(transform, repositionRadius, out success);
    public Vector3 GetPatrolPoint(out bool success) => GetRandomPoint(patrolOrigin, patrolRadius, out success);
    private Vector3 GetRandomPoint(Transform origin, float radius, out bool success)
    {
        int attempt = 0;
        while (attempt < ATTEMPT_MAX)
        {
            NavMeshHit hit;
            NavMeshPath path = new NavMeshPath();
            Vector3 randomPoint = origin.position + (Random.insideUnitSphere * radius);
            randomPoint.y = transform.position.y;
            if (NavMesh.SamplePosition(randomPoint, out hit, SAMPLE_POSITION_MAX_DIST, NavMesh.AllAreas))
            {
                agent.CalculatePath(hit.position, path);
                if (path.status == NavMeshPathStatus.PathComplete)
                {
                    if ((transform.position - hit.position).magnitude > agent.stoppingDistance)
                    {
                        //SUCCESS
                        success = true;
                        return hit.position;
                    }
                    //TOO CLOSE
                    attempt++;
                    continue;
                }
                //CAN'T REACH
                attempt++;
                continue;
            }
            //COULDN'T FIND
            attempt++;
            continue;
        }
        //FAILED
        success = false;
        return origin.position;
    }
}
