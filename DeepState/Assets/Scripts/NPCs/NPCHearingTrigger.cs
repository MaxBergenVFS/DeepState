using UnityEngine;

public class NPCHearingTrigger : NPCHearing
{
    private Collider observed;

    protected override void Awake()
    {
        base.Awake();
        ResetObserved();
    }
    private void Start()
    {
        if (!controller.EnterAlertOnPlayerSeen) Disable();
        CheckpointManager.onResetGameStateToLastCheckpoint += ResetObserved;
    }

    private void OnDestroy()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint -= ResetObserved;
    }

    private void OnDisable()
    {
        ResetObserved();
    }

    private void ResetObserved()
    {
        observed = null;
    }

    private void Update()
    {
        if (disable || observed == null) return; //-------------------
        if (!controller.PlayerOutsideOfDistThreshold) SetAlerted();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (disable) return; //----------------
        if (other.CompareTag(GeneralParams.TAG_PLAYER)) observed = other;
    }

    private void OnTriggerExit(Collider other)
    {
        if (disable) return; //----------------
        if (other.CompareTag(GeneralParams.TAG_PLAYER)) observed = null;
    }
}
