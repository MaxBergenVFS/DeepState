using UnityEngine;

public class NPCIdleBehaviour : StateMachineBehaviour
{
    private float idleTime;
    private float idleTimeRemaining;
    private NPCController controller;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponent<NPCController>();
        if (controller == null)
        {
            Debug.Log($"missing NPCController ref on {this.name}");
        }
        else
        {
            controller.SetNavSpeedToStop();
            controller.EnableDialogue();
            controller.TryRotateToFacePatPointTarget();
            idleTime = controller.IdleTime;
            animator.SetFloat(AnimParams.NPC_FLOAT_IDLE_TIME, idleTime);
            idleTimeRemaining = idleTime;
        }
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        idleTimeRemaining -= Time.deltaTime;
        animator.SetFloat(AnimParams.NPC_FLOAT_IDLE_TIME, idleTimeRemaining);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetFloat(AnimParams.NPC_FLOAT_IDLE_TIME, idleTime);
        animator.SetBool(AnimParams.NPC_BOOL_AT_DEST, false);
    }
}
