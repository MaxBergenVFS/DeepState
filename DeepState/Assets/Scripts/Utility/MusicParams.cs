public static class MusicParams
{
    //prefix
    public const string MUSIC_PREFIX = "event:/Music/";
    //events
    public const string PATH_PAUSE_MENU = "PauseScreen";
    public const string PATH_DEATH_SCREEN = "DeathScreen";
    public const string PATH_CODEC = "CodecTheme";
    public const string PATH_UNACTO = "Unatco";
    public const string PATH_FRIGATE = "Frigate";
    public const string PATH_AFGHANISTAN = "Afghanistan";
    public const string PATH_AFGHANISTAN_DYNAMIC = "AfghanistanDynamic";
    public const string PATH_COMBAT_1 = "Combat1";
    public const string PATH_COMBAT_2 = "Combat2";
    public const string PATH_COMBAT_3 = "Combat3";
    public const string PATH_RESULTS_SCREEN = "ResultsScreen";
    public const string PATH_CUTSCENE_MONSTER = "MonsterCutscene";
    public const string PATH_CUTSCENE_BASHIR = "BashirCutscene";
    //buses
    public const string PATH_MUSIC_BUS = "bus:/Music";
    //params
    public const string PARAM_INTENSITY = "Intensity_01";
    public const string PARAM_MUFFLED = "Muffled";
}
