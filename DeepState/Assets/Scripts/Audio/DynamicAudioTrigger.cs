using UnityEngine;

public class DynamicAudioTrigger : MonoBehaviour
{
    private DynamicAudioManager dynamicAudioManager;
    private Collider observed;
    private bool disabled;

    private void Awake()
    {
        dynamicAudioManager = GetComponentInParent<DynamicAudioManager>();
        observed = null;
        disabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER))
        {
            observed = other;
            dynamicAudioManager.IncrementCount();
            if (!disabled && dynamicAudioManager.Count == 1) Activate(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER))
        {
            observed = null;
            dynamicAudioManager.DecrementCount();
            if (!disabled && dynamicAudioManager.Count == 0) Activate(false);
        }
    }

    private void Activate(bool active)
    {
        dynamicAudioManager.Activate(active);
        if (active)
        {
            CancelInvoke("DeactivateIfPlayerNoLongerInVolume");
            Invoke("DeactivateIfPlayerNoLongerInVolume", dynamicAudioManager.LerpDuration);
        }
        TemporarilyDisable();
    }

    private void DeactivateIfPlayerNoLongerInVolume()
    {
        if (observed == null && dynamicAudioManager.Count == 0) Activate(false);
    }
    private void TemporarilyDisable()
    {
        if (disabled) return; //------------------------------------
        disabled = true;
        Invoke("ReEnable", dynamicAudioManager.LerpDuration);
    }
    private void ReEnable()
    {
        disabled = false;
    }
    
}
