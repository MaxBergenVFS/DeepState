using UnityEngine;

public class PlayerAutoPickup : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<GunPickup>())
        {
            TryToGetAmmoFromGunPickup(other.GetComponent<GunPickup>());
        }
    }

    private void TryToGetAmmoFromGunPickup(GunPickup gun)
    {
        if (gun == null) return; //---------------

        if (gun.IsInInventory) gun.Interact();
    }
}