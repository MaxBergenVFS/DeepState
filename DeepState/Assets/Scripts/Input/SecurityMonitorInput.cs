using UnityEngine;

public class SecurityMonitorInput : InputOverride
{
    [SerializeField]
    private SecurityMonitorCameraManager securityMonitorCameraManager;

    private void Update()
    {
        if (InputManagerIsEnabled) return; //---------------------------------

        if (Input.GetButtonDown(INPUT_CANCEL))
            securityMonitorCameraManager.DisableCamera();
        if (Input.GetButtonDown(INPUT_INTERACT) || Input.GetButtonDown(INPUT_ENTER))
            securityMonitorCameraManager.NextFeed();
        if (Input.GetButtonDown(INPUT_MELEE))
            securityMonitorCameraManager.PreviousFeed();
        if (Input.GetButtonDown(INPUT_RELOAD))
            securityMonitorCameraManager.HackAutoTurret();
    }
}
