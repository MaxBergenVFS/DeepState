using UnityEngine;

public class NPCBulletFlyByBehaviour : StateMachineBehaviour
{
    private const int BULLET_FLY_BY_ANIM_VARIATION_AMT = 5;

    private int variation;
    private NPCController controller;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponent<NPCController>();
        controller.SetNavSpeedToStop();
        variation = Random.Range(1, BULLET_FLY_BY_ANIM_VARIATION_AMT + 1);
        animator.SetInteger(AnimParams.NPC_INT_BULLET_FLY_BY_VARIATION, variation);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.NPC_BOOL_ALERTED, true);
    }
}
