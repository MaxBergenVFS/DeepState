using UnityEngine;

public class DamageObjsOnEnable : MonoBehaviour
{
    private const int DAMAGE_AMOUNT = 500;

    [SerializeField]
    private Health[] objsToDamage;

    private void OnEnable()
    {
        foreach (Health obj in objsToDamage)
        {
            obj.DecreaseRemainingHealth(DAMAGE_AMOUNT);
        }
    }
}
