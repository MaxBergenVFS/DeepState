using UnityEngine;

public class MirroredRig : MonoBehaviour
{
    private const float MOVEMENT_CHECKER_DELAY = 0.1f;

    private Animator anim;
    private float x, z, delay;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        x = transform.position.x;
        z = transform.position.z;
        delay = MOVEMENT_CHECKER_DELAY;
    }

    private void Update()
    {
        delay -= Time.deltaTime;
        if (delay < 0f)
        {
            CheckIfHasMoved();
            delay = MOVEMENT_CHECKER_DELAY;
        }
    }

    public void PlayFootstepSound() {}

    private void CheckIfHasMoved()
    {
        bool hasMoved;
        hasMoved = (x != transform.position.x || z != transform.position.z);
        x = transform.position.x;
        z = transform.position.z;

        if (anim.GetBool(AnimParams.MIRRORED_RIG_BOOL_MOVING) == hasMoved) return; //-----------------------------------------

        anim.SetBool(AnimParams.MIRRORED_RIG_BOOL_MOVING, hasMoved);
    }
}
