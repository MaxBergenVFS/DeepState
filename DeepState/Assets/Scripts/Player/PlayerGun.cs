using UnityEngine;

public class PlayerGun : Gun, IEquippable
{
    private const Enums.damageType DAMAGE_TYPE = Enums.damageType.RangedPlayer;
    private const float SPREAD_MULTIPLIER_CROUCHED = 0.5f;

    [SerializeField]
    private float recoilAmt = 0.2f, sphereCastRadius = 1.7f;
    [SerializeField]
    private GameObject gunModel, silencer;
    [SerializeField]
    private Enums.gunType gunType;
    [SerializeField]
    private string shootAnimationName;
    [SerializeField]
    private bool automaticFire, startSilenced;
    [SerializeField]
    private ParticleSystem muzzleFlash, muzzleFlashSilenced;

    private Enums.itemType itemType;
    private Enums.ammoType ammoType;
    private PlayerController playerController;
    private PlayerInventory playerInventory;
    private PlayerHands playerHands;
    private InventoryItemDatabase inventoryItemDatabase;
    private CameraController cameraController;
    private float remainingFireRateTime;
    private bool playerCanShoot;

    public bool ModelIsVisible { get { return gunModel.activeSelf; } }

    public delegate void OnAmmoInClipChanged(int amt);
    public static event OnAmmoInClipChanged onAmmoInClipChanged;
    public delegate void OnGunEquipped(bool active);
    public static event OnGunEquipped onGunEquipped;
    public delegate void OnEnemyHit();
    public static event OnEnemyHit onEnemyHit;

    protected override void Awake()
    {
        base.Awake();
        InitGunType(gunType);
        damageType = DAMAGE_TYPE;
        playerCanShoot = true;
    }
    protected override void Start()
    {
        base.Start();
        sfx.SetPlayerWeapon(gunType);
        playerController = PlayerController.Instance;
        playerInventory = playerController.GetComponent<PlayerInventory>();
        playerHands = playerController.GetComponent<PlayerHeldItem>().PlayerHands;
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
        cameraController = playerController.GetComponent<CameraController>();
        base.EquipSilencer(startSilenced);
        PlayerHands.onMeleeEquipped += HideModelOnMeleeEquipped;
    }

    private void OnDestroy()
    {
        PlayerHands.onMeleeEquipped -= HideModelOnMeleeEquipped;
    }

    private void InitGunType(Enums.gunType type)
    {
        switch (type)
        {
            case (Enums.gunType.Pistol):
                itemType = Enums.itemType.Pistol;
                ammoType = Enums.ammoType.PistolAmmo;
                break;
            case (Enums.gunType.AK):
                itemType = Enums.itemType.AkRifle;
                ammoType = Enums.ammoType.RifleAmmo;
                break;
            case (Enums.gunType.M4):
                itemType = Enums.itemType.M4Rifle;
                ammoType = Enums.ammoType.RifleAmmo;
                break;
            case (Enums.gunType.None):
            default:
                itemType = Enums.itemType.None;
                ammoType = Enums.ammoType.None;
                break;
        }
    }

    protected override Vector3 GetDirection()
    {
        float dynamicSpread = GetDynamicSpread();
        float x = Random.Range(-dynamicSpread, dynamicSpread);
        float y = Random.Range(-dynamicSpread, dynamicSpread);
        return origin.forward + new Vector3(x, y, 0);
    }
    private float GetDynamicSpread()
    {
        float dynamicSpread = spread;
        float speedMultiplier = playerController.MovementSpeed;
        if (speedMultiplier < 1) speedMultiplier = 1;
        dynamicSpread *= speedMultiplier;
        if (playerController.IsCrouching) dynamicSpread *= SPREAD_MULTIPLIER_CROUCHED;
        if (dynamicSpread < spread) dynamicSpread = spread;
        return dynamicSpread;
    }

    protected override void UpdateAmmoRemainingInClip(int bulletsRemainingInClip)
    {
        base.UpdateAmmoRemainingInClip(bulletsRemainingInClip);
        onAmmoInClipChanged(bulletsRemainingInClip);
    }

    private void TryReload(out bool success)
    {
        int ammoInInventory = inventoryItemDatabase.GetInventoryItemAmount(ammoType);

        if (reloading || ammoInInventory <= 0 || bulletsRemaining >= magazineSize)
        {
            success = false;
        }
        else
        {
            int bulletAmountToReload = magazineSize - bulletsRemaining;

            if (ammoInInventory < bulletAmountToReload)
            {
                Reload(bulletsRemaining + ammoInInventory);
                playerInventory.AddAmmoToInventory(ammoType, -ammoInInventory);
            }
            else
            {
                Reload();
                playerInventory.AddAmmoToInventory(ammoType, -bulletAmountToReload);
            }
            success = true;
        }
    }

    protected override void PlayShootAnim() => anim.Play(shootAnimationName, -1, 0f);

    public override void Shoot()
    {
        playerCanShoot = readyToShoot;
        base.Shoot();

        if (!playerCanShoot || bulletsRemaining <= 0 || reloading) return; //-------------------

        GunShotSound(origin.position);
        cameraController.Recoil(recoilAmt);
        if (automaticFire) remainingFireRateTime = fireRate;

        if (HitTransform == null) return; //----------------------------------------------------

        if (HitTransform.GetComponent<NPCHitbox>() != null) HandleNPCHit(HitTransform.GetComponent<NPCHitbox>());
    }

    private void HandleNPCHit(NPCHitbox npc)
    {
        if (npc.IsFriendly || npc.IsDead) return; //---------------------
        sfx.PlayHitMarkerSFX();
        if (onEnemyHit != null) onEnemyHit();
    }

    protected override void GunShotImpactSound(Vector3 pos)
    {
        base.GunShotImpactSound(pos);
        GunShotSound(pos);
    }
    private void GunShotSound(Vector3 pos)
    {
        Collider[] hitColliders = Physics.OverlapSphere(pos, soundRadius);
        foreach (var hitCollider in hitColliders)
        {
            hitCollider.GetComponent<NPCHearing>()?.SetAlertedIfHostile();
        }
    }

    protected override void SphereCastBulletFlyBy(Ray ray)
    {
        RaycastHit hit;
        Physics.SphereCast(ray, sphereCastRadius, out hit, range, whatIsDamagable);

        if (hit.transform == null) return; //----------------------------------

        NPCHitbox npc = hit.transform.GetComponent<NPCHitbox>();
        if (npc != null) npc.Controller.FlinchOnBulletFlyBy();
    }

    protected override void MuzzleFlash()
    {
        if (silenced && muzzleFlashSilenced != null) muzzleFlashSilenced.Play();
        else muzzleFlash.Play();
    }

    public void EquipSilencer() => EquipSilencer(!silencer.activeSelf);
    public override void EquipSilencer(bool active)
    {
        if (!gunModel.activeSelf) return; //----------------------------------------
        base.EquipSilencer(active);
        silencer.SetActive(active);
    }

    private void HideModelOnMeleeEquipped(bool active)
    {
        if (active) ShowModel(false);
    }
    // private void HideModel(Enums.itemType equippedType)
    // {
    //     if (equippedType == itemType || playerHands.CurrentlyPlayingHolsteringAnimation()) return; //--------
    //     ShowModel(false);
    // }

    #region IEquippable API
    public void ShowModel(bool active)
    {
        gunModel.SetActive(active);
        if (silencer != null && active) silencer.SetActive(silenced);
        else if (silencer != null && !active) silencer.SetActive(false);
    }

    public void Equip()
    {
        playerInventory.EquipItemInDatabase(itemType, true);
        onAmmoInClipChanged(bulletsRemaining);
        onGunEquipped(true);
    }
    public void Unequip()
    {
        playerInventory.EquipItemInDatabase(itemType, false);
        onGunEquipped(false);
    }

    public void UsePrimary()
    {
        if (!playerHands.CurrentlyHolsteringItem) Shoot();
    }
    public void UsePrimaryHeld()
    {
        if (!automaticFire || playerHands.CurrentlyHolsteringItem) return; //-------

        remainingFireRateTime -= Time.deltaTime;
        if (remainingFireRateTime <= 0f) Shoot();
    }

    public void UseSecondary() => playerHands.ReloadEquipped();
    public void UseSecondaryHeld() { }

    public void ReloadEquipped(out bool success)
    {
        bool canReload;
        TryReload(out canReload);
        success = canReload;
    }
    #endregion
}
