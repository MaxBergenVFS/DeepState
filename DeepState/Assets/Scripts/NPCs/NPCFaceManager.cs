using System.Collections;
using UnityEngine;

public class NPCFaceManager : MonoBehaviour
{
    private SkinnedMeshRenderer rend;

    [SerializeField]
    private int faceMatRef = 0;
    [SerializeField]
    private Material[] talkingFaces;
    [SerializeField]
    private Material defaultFace, blinkFace, deathFace;
    [SerializeField]
    private float secBetweenFaceMatChanges = 0.1f,
        blinkTime = 0.1f, secBetweenBlinks = 2.5f;

    private DialogueManager dialogueManager;
    private bool bypassUpdate, blinking;
    private float blinkTimeRemaining, secBetweenBlinksTimeRemaining;
    private Color bloodColor;
    private float bloodSize;
    private Mesh defaultMesh;

    private void Awake()
    {
        InitMesh();
        ResetBlinkInUpdate();
        bypassUpdate = false;
    }
    private void Start()
    {
        if (DialogueManager.Instance != null) dialogueManager = DialogueManager.Instance;
        bloodColor = PersistentValues.Instance.BloodColor;
        bloodSize = PersistentValues.Instance.BloodSize;
        SetDefaultFace();
    }

    private void InitMesh()
    {
        rend = GetComponent<SkinnedMeshRenderer>();
        defaultMesh = Instantiate(rend.sharedMesh);
        Mesh mesh = Instantiate(defaultMesh);
        rend.sharedMesh = mesh;
    }

    private void Update()
    {
        if (bypassUpdate) return; //----------------------------------------------

        secBetweenBlinksTimeRemaining -= Time.deltaTime;
        if (secBetweenBlinksTimeRemaining < 0f)
        {
            if (!blinking)
            {
                SetBlinkFace();
                blinking = true;
            }
            blinkTimeRemaining -= Time.deltaTime;
            if (blinkTimeRemaining < 0f)
            {
                SetDefaultFace();
                ResetBlinkInUpdate();
            }
        }
    }

    private void ResetBlinkInUpdate()
    {
        blinking = false;
        secBetweenBlinksTimeRemaining = Random.Range(secBetweenBlinks / 2, secBetweenBlinks * 2);
        blinkTimeRemaining = blinkTime;
    }

    public void SetRandTalkingFace() => SetFaceMat(talkingFaces[Random.Range(0, talkingFaces.Length)]);
    private void SetDefaultFace() => SetFaceMat(defaultFace);
    public void SetBlinkFace() => SetFaceMat(blinkFace);
    public void SetDeathFace()
    {
        if (deathFace == null) SetFaceMat(blinkFace);
        else SetFaceMat(deathFace);
        bypassUpdate = true;
    }

    private void SetFaceMat(Material mat)
    {
        if (rend == null) return; //----------------------------
        var materials = rend.materials;
        materials[faceMatRef] = mat;
        rend.materials = materials;
    }

    private IEnumerator UpdateFaceOnTimer()
    {
        bypassUpdate = true;
        while (dialogueManager.CurrentlyInDialogue)
        {
            if (dialogueManager.SentenceIsStillBeingTyped)
            {
                SetRandTalkingFace();
                yield return new WaitForSeconds(secBetweenFaceMatChanges);
            }
            else
            {
                SetDefaultFace();
                yield return new WaitForSeconds(secBetweenBlinks);
                SetBlinkFace();
                yield return new WaitForSeconds(blinkTime);
            }
        }
        SetDefaultFace();
        ResetBlinkInUpdate();
        bypassUpdate = false;
    }

    public void StartUpdateFaceOnTimer()
    {
        StopAllCoroutines();
        StartCoroutine(UpdateFaceOnTimer());
    }

    private void ResetVertexColouring()
    {
        Mesh mesh = Instantiate(defaultMesh);
        rend.sharedMesh = mesh;
    }
    public void ResetFace(bool stopAllCoroutines)
    {
        if (stopAllCoroutines)
        {
            bypassUpdate = false;
            StopAllCoroutines();
        }
        SetDefaultFace();
    }
    public void Reset()
    {
        ResetFace(true);
        ResetVertexColouring();
    }

    public void VertexBloodColouring(Vector3 hitPoint)
    {
        if (rend == null)
        {
            Debug.LogError($"Missing SkinnedMeshRenderer ref on {this.gameObject.name}");
            return;
        } //-------------------------------------

        Mesh mesh = rend.sharedMesh;
        Vector3[] vertices = mesh.vertices;
        Color32[] colors = new Color32[vertices.Length];
        colors = mesh.colors32;
        for (int i = 0; i < vertices.Length; i++)
        {
            Vector3 vertex = transform.TransformPoint(vertices[i]);
            float distance = Vector3.Distance(vertex, hitPoint);
            if (distance < bloodSize) colors[i] = bloodColor;
        }
        mesh.colors32 = colors;
    }
}
