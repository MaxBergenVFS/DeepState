using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour, IRespawnable
{
    #region Inits
    private const int DAMAGE_MULTIPLIER_TORSO = 3;
    private const int DAMAGE_MULTIPLIER_HEAD = 10;
    private const int FLINCH_CHANCE = 50;
    private const int FLINCH_THRESHOLD = 75;
    private const float RAGDOLL_EXPLOSIVE_RADIUS = 5f;
    private const float ROTATE_TO_FACE_PLAYER_THRESHOLD = 90f;
    private const float ROTATE_TO_FACE_TARGET_SPEED = 2f;
    private const float DEATH_FUNC_DELAY_SECONDS = 7f;
    private const float PLAYER_LOS_TIMEOUT_SECONDS = 20f;
    private const float SEARCH_STATE_TIMEOUT_SECONDS = 30f;
    private const float DIST_TO_PLAYER_THRESHOLD = 75f;
    private const float UPDATE_TICK_RATE = 1f;
    private const float FRIENDLY_ALERTED_TIMEOUT_DURATION = 15f;
    private const float BLOOD_IMPACT_OFFSET = 0.2f;
    private const float OVERRIDE_START_ANIM_ON_CHECKPOINT_DELAY = 0.5f;

    [SerializeField]
    private Transform[] patrolPoints;
    [SerializeField]
    private Transform runForCoverPoint, bloodPoolOrigin;
    [SerializeField]
    private float idleTime = 4f, walkSpeed = 1.25f, runSpeed = 4.5f, cowerTime = 10f,
        attackRangeMin = 7f, attackRangeMax = 20f;
    [SerializeField]
    private EnemyGun gun;
    [SerializeField]
    private bool startAlerted, alwaysFlinchOnDamage, randomPatrol, missionFailedIfKilled, inCutsceneMode, inGuardTower;
    [SerializeField]
    private NPCDialogue dialogueTrigger;
    [SerializeField]
    private GameObject cancelDialogueTrigger, shadow;
    [SerializeField]
    private bool enterAlertOnPlayerSeen, isFriendly;
    [SerializeField]
    private int alertZone = 0;
    [SerializeField]
    private string overrideStartingAnim;
    [SerializeField]
    private GameObject alertNearbyNPC, LoS;
    [SerializeField]
    private NPCHearingTrigger hearingTrigger;
    [SerializeField]
    private LayerMask bloodPoolLayers;
    [SerializeField]
    private NPCHostageManager hostageManager;
    [SerializeField]
    private GameObject[] objsToEnableOnDeath;

    private Dictionary<int, float> patPointIdleTimeDict = new Dictionary<int, float>();
    private Dictionary<int, Transform> patPointRotationTargetDict = new Dictionary<int, Transform>();
    private NPCHealth health;
    private PlayerController playerController;
    private CheckpointManager checkpointManager;
    private Transform player;
    private Animator anim;
    private Rigidbody[] rbs;
    private NavMeshAgent agent;
    private NPCFaceManager faceManager;
    private NPCSFX sfx;
    private Alarm alarmRef;
    private NavMeshRandomPoint navMeshRandomPoint;
    private NPCMultiAimConstraintController multiAimConstraintController;
    private AlertZoneManager alertZoneManager;
    private ObjectPool bloodPool, bloodImpactParticlePool;
    private Vector3 currentHitPoint = new Vector3();
    private Vector3 startingPos = new Vector3();
    private Quaternion startingRot = new Quaternion();
    private NPCSettings snapshot = new NPCSettings();
    private int patIndex;
    private float ragDollExplosiveForce, defaultIdleTime, timeSincePlayerInLosSeconds, updateTickRate, remainingFriendlyAlertedDuration, remainingSearchTime;
    private bool lastHitWasOtherNPC, onHighAlert, cantBeAlerted, playerOutsideOfDistThreshold, isCaptive;

    public delegate void OnNPCDeath(bool isFriendly, Enums.damageType killingBlowDamageType);
    public static event OnNPCDeath onNPCDeath;
    public delegate void OnNPCHeadshot(bool isFriendly);
    public static event OnNPCHeadshot onNPCHeadshot;
    public delegate void OnMissionFailed();
    public static event OnMissionFailed onMissionFailed;

    public bool IsFriendly
    {
        get
        {
            if (isFriendly)
            {
                //friendly unarmed npc
                if (gun == null) return true;
                //friendly armed npc is not friendly while alerted
                return !anim.GetBool(AnimParams.NPC_BOOL_ALERTED);
            }
            //if not friendly then always return false
            return false;
        }
    }
    public bool IsAlerted
    {
        get
        {
            if (anim == null) return false;
            return anim.GetBool(AnimParams.NPC_BOOL_ALERTED) && anim.GetBool(AnimParams.NPC_BOOL_CAN_SEE_PLAYER);
        }
    }
    public bool IsDead { get { return anim.GetBool(AnimParams.NPC_BOOL_IS_DEAD); } }
    public bool EnterAlertOnPlayerSeen { get { return enterAlertOnPlayerSeen; } }
    public bool PlayerOutsideOfDistThreshold { get { return playerOutsideOfDistThreshold; } }
    public bool InCutsceneMode { get { return inCutsceneMode; } }
    public float IdleTime { get { return idleTime; } }
    public float CowerTime { get { return cowerTime; } }
    public float AttackRangeMin { get { return attackRangeMin; } }
    public float AttackRangeMax { get { return attackRangeMax; } }
    public Vector3 StartingPos { get { return startingPos; } }
    public Alarm AlarmRef { get { return alarmRef; } }
    public NPCFaceManager MaterialManager { get { return faceManager; } }
    #endregion

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        health = GetComponent<NPCHealth>();
        anim = GetComponent<Animator>();
        sfx = GetComponent<NPCSFX>();
        navMeshRandomPoint = GetComponent<NavMeshRandomPoint>();
        multiAimConstraintController = GetComponent<NPCMultiAimConstraintController>();

        defaultIdleTime = idleTime;
        patIndex = 0;
        onHighAlert = isCaptive = false;
        cantBeAlerted = missionFailedIfKilled;
        updateTickRate = UPDATE_TICK_RATE;
        remainingSearchTime = SEARCH_STATE_TIMEOUT_SECONDS;
        startingPos = transform.position;
        startingRot = transform.rotation;
        ResetFriendlyAlertedDuration();
        ResetLosTimeout();
    }
    private void Start()
    {
        checkpointManager = GameManager.Instance.CheckpointManager;
        playerController = PlayerController.Instance;
        player = playerController.transform;
        alertZoneManager = GameManager.Instance.AlertZoneManager;
        bloodPool = GameManager.Instance.BloodPool;
        bloodImpactParticlePool = GameManager.Instance.BloodImpactParticlePool;
        faceManager = dialogueTrigger.FaceManager;
        rbs = GetComponentsInChildren<Rigidbody>();

        StartCoroutine(EnableKinematic(true, 0));
        InitPatPointDicts();
        hostageManager?.AddCaptor(this);

        anim.SetBool(AnimParams.NPC_BOOL_ALERTED, startAlerted);
        anim.SetBool(AnimParams.NPC_BOOL_IS_SEARCHING, startAlerted);
        anim.SetBool(AnimParams.NPC_BOOL_IS_IN_GUARD_TOWER, inGuardTower);
        anim.SetBool(AnimParams.NPC_BOOL_HAS_PAT_POINTS, (patrolPoints.Length > 0 || randomPatrol));
        anim.SetBool(AnimParams.NPC_BOOL_HAS_WEAPON, (gun != null));

        if (LoS != null) LoS.SetActive(enterAlertOnPlayerSeen);
        TryOverrideStartAnim();

        AlertZoneManager.onAlertZoneStatusChange += SetAlerted;
        AlertZoneManager.onEnterHighAlert += EnterHighAlert;
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void TryOverrideStartAnim()
    {
        if (overrideStartingAnim == "") return; //---------------
        CullUpdateTransforms();
        anim.Play(overrideStartingAnim);
    }

    private void Update()
    {
        if (inCutsceneMode) return; //----------------------------------------

        updateTickRate -= Time.deltaTime;
        if (updateTickRate > 0f) return; //---------------------------------

        CullingAndLoSHandler();
        FriendlyAlertedTimeoutHandler();
        SearchTimeoutHandler();

        updateTickRate = UPDATE_TICK_RATE;
    }

    private void InitPatPointDicts()
    {
        for (int i = 0; i < patrolPoints.Length; i++)
        {
            if (patrolPoints[i].GetComponent<PatPoint>() != null)
            {
                PatPoint patPoint = patrolPoints[i].GetComponent<PatPoint>();
                //add idle time override
                float idleTimeOverride = patPoint.IdleTimeOverride;
                if (idleTimeOverride != idleTime)
                {
                    patPointIdleTimeDict.Add(i, idleTimeOverride);
                }
                //add rotation target
                Transform rotTarget = patPoint.RotationTarget;
                if (rotTarget != null)
                {
                    patPointRotationTargetDict.Add(i, rotTarget);
                }
            }
        }
    }

    public void DecreaseHealth(int amt, Enums.collisionType bodyPartHit, Vector3 hitPoint, float explosiveForce, Enums.damageType type)
    {
        if (!cantBeAlerted && !anim.GetBool(AnimParams.NPC_BOOL_ALERTED)) anim.SetBool(AnimParams.NPC_BOOL_ALERTED, true);

        if (type == Enums.damageType.RangedPlayer || type == Enums.damageType.RangedTurret) BloodImpactHandler(hitPoint, type);

        int damage = amt;
        currentHitPoint = hitPoint;
        ragDollExplosiveForce = explosiveForce;

        switch (bodyPartHit)
        {
            case (Enums.collisionType.Chest):
                if (type == Enums.damageType.RangedPlayer || type == Enums.damageType.RangedTurret) damage *= DAMAGE_MULTIPLIER_TORSO;
                if (FlinchOnDamage())
                {
                    int randVal = Random.Range(0, 2);
                    if (randVal == 0) anim.SetTrigger(AnimParams.NPC_TRIGGER_FLINCH_BODY_RIGHT);
                    else if (randVal == 1) anim.SetTrigger(AnimParams.NPC_TRIGGER_FLINCH_BODY_LEFT);
                }
                break;
            case (Enums.collisionType.Groin):
                if (type == Enums.damageType.RangedPlayer || type == Enums.damageType.RangedTurret) damage *= DAMAGE_MULTIPLIER_TORSO;
                break;
            case (Enums.collisionType.Head):
                if (type == Enums.damageType.RangedPlayer)
                {
                    HeadShotHandler();
                    damage *= DAMAGE_MULTIPLIER_HEAD;
                }
                else if (type == Enums.damageType.RangedTurret) damage *= DAMAGE_MULTIPLIER_HEAD;
                break;
            case (Enums.collisionType.LeftArm):
                if (FlinchOnDamage()) anim.SetTrigger(AnimParams.NPC_TRIGGER_FLINCH_SHOULDER_LEFT);
                break;
            case (Enums.collisionType.LeftLeg):
                if (FlinchOnDamage()) anim.SetTrigger(AnimParams.NPC_TRIGGER_FLINCH_LEG_LEFT);
                break;
            case (Enums.collisionType.RightArm):
                if (FlinchOnDamage()) anim.SetTrigger(AnimParams.NPC_TRIGGER_FLINCH_SHOULDER_RIGHT);
                break;
            case (Enums.collisionType.RightLeg):
                if (FlinchOnDamage()) anim.SetTrigger(AnimParams.NPC_TRIGGER_FLINCH_LEG_RIGHT);
                break;
            default:
                break;
        }
        health.DecreaseRemainingHealth(damage, type);
    }

    private void HeadShotHandler()
    {
        if (anim.GetBool(AnimParams.NPC_BOOL_IS_DEAD)) return; //--------------------

        sfx.PlayDiegeticHeadShotSound();
        if (!isFriendly) sfx.PlayNonDiegeticHeadShotSound();
        if (onNPCHeadshot != null) onNPCHeadshot(isFriendly);
    }

    private void BloodImpactHandler(Vector3 hitPoint, Enums.damageType type)
    {
        GameObject bloodImpactParticle = bloodImpactParticlePool.GetPooledObject();
        Vector3 offset = new Vector3();

        if (type == Enums.damageType.RangedPlayer) offset = ((player.position - hitPoint) * BLOOD_IMPACT_OFFSET);
        else offset = Vector3.zero;

        bloodImpactParticle.transform.position = hitPoint + offset;
        bloodImpactParticle.transform.rotation = Quaternion.identity;
        sfx.PlayBloodSplatterSound();
    }

    public void SetAlerted(bool active) => SetAlerted(alertZone, active);
    private void SetAlerted(int zone, bool active)
    {
        if (cantBeAlerted || zone != alertZone || anim == null) return; //--------------

        if (active) StartSearching();

        if (anim.GetBool(AnimParams.NPC_BOOL_ALERTED) == active) return; //-------------

        //NPC is in alertZone and is NOT already alerted
        alertNearbyNPC.SetActive(active);
        anim.SetBool(AnimParams.NPC_BOOL_ALERTED, active);
        if (active)
        {
            TryRunForAlarm();
            ResetLosTimeout();
            if (isFriendly) ResetFriendlyAlertedDuration();
        }
        else
        {
            anim.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
            anim.SetBool(AnimParams.NPC_BOOL_IN_ATTACK_RANGE, false);
            anim.SetBool(AnimParams.NPC_BOOL_CAN_SEE_PLAYER, false);
        }
    }

    private void StartSearching()
    {
        remainingSearchTime = SEARCH_STATE_TIMEOUT_SECONDS;
        if (!anim.GetBool(AnimParams.NPC_BOOL_IS_SEARCHING)) anim.SetBool(AnimParams.NPC_BOOL_IS_SEARCHING, true);
    }
    private void StopSearching()
    {
        //if (!anim.GetBool(AnimParams.NPC_BOOL_IS_WALKING)) anim.SetBool(AnimParams.NPC_BOOL_IS_WALKING, true);
        if (anim.GetBool(AnimParams.NPC_BOOL_IS_SEARCHING)) anim.SetBool(AnimParams.NPC_BOOL_IS_SEARCHING, false);
    }

    public void ForceRemoveAlert() => ForceRemoveAlert(false);
    private void ForceRemoveAlert(bool goToStartState)
    {
        if (anim.GetBool(AnimParams.NPC_BOOL_IS_DEAD)) return; //-------------
        SetAlerted(false);
        alertNearbyNPC.SetActive(false);
        if (goToStartState) anim.Play(AnimParams.NPC_STATE_START);
        anim.SetBool(AnimParams.NPC_BOOL_ALERTED, false);
    }

    private void SnapshotState()
    {
        snapshot.pos = transform.position;
        snapshot.rot = transform.rotation;
    }
    private void RevertRotAndPos()
    {
        transform.position = snapshot.pos;
        transform.rotation = snapshot.rot;
    }

    private void EnterHighAlert()
    {
        if (enterAlertOnPlayerSeen || gun == null) return; //-----------------------------

        //NPC has weapon and wasn't already set to be alerted by seeing player
        if (LoS != null)
        {
            LoS.SetActive(true);
            onHighAlert = enterAlertOnPlayerSeen = true;
        }
        else
        {
            Debug.LogError($"missing LoS ref on {this.gameObject.name}");
        }
    }

    private void TryRunForAlarm()
    {
        //IF ON HIGH ALERT THEN WILL ALWAYS JUST FIGHT PLAYER
        if (onHighAlert) return;

        Alarm alarm;
        if (alertZoneManager.TryGetAlarmRef(transform, out alarm))
        {
            alarm.AssignNPC(this);
            alarmRef = alarm;
            anim.SetTrigger(AnimParams.NPC_TRIGGER_RUN_FOR_ALARM);
        }
    }

    public void AlertIfPlayerInLoS()
    {
        if (LoS == null || !enterAlertOnPlayerSeen || anim.GetBool(AnimParams.NPC_BOOL_ALERTED)) return; //-------

        //NPC IS NOT YET ALERTED
        if (PlayerInLoS())
        {
            //PLAYER IS IN NPC SIGHT LINE
            if (playerController.InShadows) return; //----------------------------------------------
            //PLAYER IS NOT HIDDEN IN SHADOW
            SetAlerted(true);
        }
    }

    public void Die(Enums.damageType killingBlowDamageType)
    {
        if (anim.GetBool(AnimParams.NPC_BOOL_IS_DEAD)) return; //------------

        checkpointManager.AddRespawnable(this);
        anim.SetBool(AnimParams.NPC_BOOL_IS_DEAD, true);
        shadow.SetActive(false);
        alertNearbyNPC.SetActive(true);
        gun?.DropGun();
        alarmRef?.TryUnassignNPC(this);
        hostageManager?.SetCaptorDead(this);
        DisableDialogue();
        EnableObjsOnDeath();
        faceManager?.SetDeathFace();
        sfx?.PlayDeathSound();
        Invoke("SpawnBloodPool", DEATH_FUNC_DELAY_SECONDS);
        Invoke("DisableFaceManager", DEATH_FUNC_DELAY_SECONDS);
        MissionFailedHandler();
        if (onNPCDeath != null) onNPCDeath(isFriendly, killingBlowDamageType);
    }
    public void Respawn()
    {
        CancelInvoke();
        if (!faceManager.isActiveAndEnabled) faceManager.enabled = true;
        faceManager.Reset();
        gun?.ResetGun();
        anim.enabled = true;
        ResetRagdoll();
        health.SetHealthToFull();
        StopSearching();
        anim.SetBool(AnimParams.NPC_BOOL_IS_DEAD, false);
        anim.SetBool(AnimParams.NPC_BOOL_IS_DYING, false);
        anim.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
        shadow.SetActive(true);
        alertNearbyNPC.SetActive(false);
        ResetStateAndPos();
    }
    private void RevertState()
    {
        if (anim.GetBool(AnimParams.NPC_BOOL_IS_DEAD)) return; //-------------

        anim.SetBool(AnimParams.NPC_BOOL_IS_REPOSITIONING, false);
        anim.SetBool(AnimParams.NPC_BOOL_AT_REPOSITION_POINT, false);
        faceManager.Reset();
        ResetStateAndPos();
        if (enterAlertOnPlayerSeen && !LoS.activeSelf) LoS.SetActive(true);
    }

    private void DisableFaceManager()
    {
        if (faceManager == null) return; //--------------------------
        faceManager.enabled = false;
    }

    private void ResetStateAndPos()
    {
        ForceRemoveAlert(true);
        RevertRotAndPos();
        CancelInvoke("TryOverrideStartAnim");
        Invoke("TryOverrideStartAnim", OVERRIDE_START_ANIM_ON_CHECKPOINT_DELAY);
        anim.SetBool(AnimParams.NPC_BOOL_ALERTED, startAlerted);
    }

    private void EnableObjsOnDeath()
    {
        if (objsToEnableOnDeath.Length < 1) return;

        foreach (GameObject obj in objsToEnableOnDeath)
        {
            obj.SetActive(true);
        }
    }

    private void MissionFailedHandler()
    {
        if (!missionFailedIfKilled) return; //---------
        if (onMissionFailed != null) onMissionFailed();
    }

    public void Ragdoll()
    {
        foreach (Rigidbody rb in rbs)
        {
            rb.isKinematic = false;
            rb.AddExplosionForce(ragDollExplosiveForce, currentHitPoint, RAGDOLL_EXPLOSIVE_RADIUS);
        }
        anim.enabled = false;
        StartCoroutine(EnableKinematic(true, DEATH_FUNC_DELAY_SECONDS));
        StartCoroutine(EnableColliders(false, DEATH_FUNC_DELAY_SECONDS));
    }

    private IEnumerator EnableColliders(bool active, float delay)
    {
        yield return new WaitForSeconds(delay);
        Collider[] cols = GetComponentsInChildren<Collider>();
        foreach (Collider col in cols)
        {
            col.enabled = active;
        }
    }
    private IEnumerator EnableKinematic(bool active, float delay)
    {
        yield return new WaitForSeconds(delay);
        foreach (Rigidbody rb in rbs)
        {
            if (rb != null) rb.isKinematic = active;
        }
    }

    private void ResetRagdoll()
    {
        StopAllCoroutines();
        StartCoroutine(EnableKinematic(true, 0));
        StartCoroutine(EnableColliders(true, 0));
    }

    private bool FlinchOnDamage()
    {
        if (alwaysFlinchOnDamage) return true;
        int randVal = Random.Range(0, FLINCH_THRESHOLD);
        return randVal < FLINCH_CHANCE;
    }

    public void FlinchOnBulletFlyBy() => anim.SetTrigger(AnimParams.NPC_TRIGGER_BULLET_FLY_BY);

    public Vector3 GetCurrentPatPoint()
    {
        if (patrolPoints.Length < 1 || randomPatrol) return GetRandomPatrolPoint(); //---------------------------------------------
        return patrolPoints[patIndex].position;
    }
    public Vector3 GetNextPatPoint()
    {
        if (patrolPoints.Length < 1 || randomPatrol) return GetRandomPatrolPoint(); //---------------------------------------------

        patIndex++;
        if (patIndex > patrolPoints.Length - 1)
        {
            patIndex = 0;
        }
        TryPatPointIdleTimeOverride(patIndex);
        return patrolPoints[patIndex].position;
    }
    public Vector3 GetRunForCoverPoint()
    {
        if (runForCoverPoint == null) return GetCurrentPatPoint(); //------------------------------
        return runForCoverPoint.position;
    }
    public Vector3 GetNearbyAlarmPoint()
    {
        if (alarmRef == null)
        {
            Debug.LogError($"missing alarmRef on {this.gameObject.name}");
            return GetRunForCoverPoint();
        }
        return alarmRef.PatPoint.transform.position;
    }
    public Vector3 GetRepositionPoint(out bool success) => navMeshRandomPoint.GetRepositionPoint(out success);
    public Vector3 GetNewStrafePoint(out bool success) => navMeshRandomPoint.GetStrafePoint(out success);
    private Vector3 GetRandomPatrolPoint()
    {
        bool success;
        return navMeshRandomPoint.GetPatrolPoint(out success);
    }

    private void TryPatPointIdleTimeOverride(int patPointIndex)
    {
        if (patPointIdleTimeDict.ContainsKey(patPointIndex)) idleTime = patPointIdleTimeDict[patPointIndex];
        else if (idleTime != defaultIdleTime) idleTime = defaultIdleTime;
    }

    public void DisableDialogue()
    {
        dialogueTrigger.CancelDialogue();
        cancelDialogueTrigger.SetActive(false);
    }
    public void EnableDialogue()
    {
        cancelDialogueTrigger.SetActive(true);
    }

    public void ActivateBodyAim(bool active) => multiAimConstraintController.ActivateBodyAim(active);
    public void ActivateBodyAimHip(bool active) => multiAimConstraintController.ActivateBodyAimHip(active);
    public void DisableConstraints()
    {
        multiAimConstraintController.ActivateHeadAim(false);
        multiAimConstraintController.ActivateBodyAim(false);
        multiAimConstraintController.ActivateBodyAimHip(false);
    }

    public void RotateToFacePlayer(bool active) => RotateToFaceTarget(active, player, ROTATE_TO_FACE_PLAYER_THRESHOLD);
    public void TryRotateToFacePatPointTarget()
    {
        if (!patPointRotationTargetDict.ContainsKey(patIndex)) return; //--------------------------
        RotateToFaceTarget(true, patPointRotationTargetDict[patIndex], 0f);
    }
    public void TryRotateToFaceAlarm()
    {
        if (alarmRef == null) return; //-----------------------------------------
        RotateToFaceTarget(true, alarmRef.PatPoint.RotationTarget, 0f);
    }
    private void RotateToFaceTarget(bool active, Transform target, float threshold)
    {
        if (!active && patrolPoints.Length > 0) return; //-------------------------------------------

        if (patrolPoints.Length < 1 && !active)
        {
            if (transform.rotation == startingRot) return; //--------------------------------------

            //RETURN TO PREVIOUS ROTATION
            StopAllCoroutines();
            StartCoroutine(LerpYAxisRot(transform.rotation, startingRot));
        }
        else
        {
            Vector3 lookDir = target.position - transform.position;
            float angle = Vector3.Angle(lookDir, transform.forward);

            if (angle < threshold) return; //-----------------------------------------------------------

            //ROTATE TO FACE TARGET
            Quaternion newRot = Quaternion.LookRotation(lookDir);
            StopAllCoroutines();
            StartCoroutine(LerpYAxisRot(transform.rotation, newRot));
        }
    }
    private IEnumerator LerpYAxisRot(Quaternion start, Quaternion end)
    {
        float lerp = 0f;
        while (lerp < 1f)
        {
            lerp += Time.deltaTime;
            Quaternion newRot = new Quaternion();
            newRot = Quaternion.Lerp(start, end, lerp * ROTATE_TO_FACE_TARGET_SPEED);
            transform.rotation = new Quaternion(transform.rotation.x, newRot.y, transform.rotation.z, newRot.w);
            yield return null;
        }
    }

    public bool OtherNPCInLoS()
    {
        Transform hitTransform = gun.CheckLineOfSight().transform;
        if (hitTransform == null) return false;
        return hitTransform.GetComponent<NPCHitbox>() != null;
    }
    public bool PlayerInLoS()
    {
        Transform hitTransform = gun.CheckLineOfSight().transform;
        if (hitTransform == null) return false;
        bool playerInLos = hitTransform.CompareTag("Player");
        return playerInLos;
    }
    public void RemoveAlertIfPlayerNotInLoS()
    {
        //if npc isn't alerted then return
        if (!anim.GetBool(AnimParams.NPC_BOOL_ALERTED)) return; //---------------------------

        timeSincePlayerInLosSeconds += Time.deltaTime;

        //if player is in LoS then reset timeout
        if (PlayerInLoS())
        {
            ResetLosTimeout();
            return; //--------------------------------------------------------
        }

        //if player isn't in LoS for full timeout then remove alert status
        if (timeSincePlayerInLosSeconds >= PLAYER_LOS_TIMEOUT_SECONDS)
        {
            anim.SetBool(AnimParams.NPC_BOOL_ALERTED, false);
        }
    }
    private void ResetLosTimeout()
    {
        timeSincePlayerInLosSeconds = 0f;
    }
    private void ResetFriendlyAlertedDuration()
    {
        remainingFriendlyAlertedDuration = FRIENDLY_ALERTED_TIMEOUT_DURATION;
    }

    private void SearchTimeoutHandler()
    {
        if (!anim.GetBool(AnimParams.NPC_BOOL_IS_SEARCHING)) return; //---------------------

        remainingSearchTime -= UPDATE_TICK_RATE;
        if (remainingSearchTime <= 0f) StopSearching();
    }
    private void FriendlyAlertedTimeoutHandler()
    {
        if (!isFriendly || !anim.GetBool(AnimParams.NPC_BOOL_ALERTED)) return; //-----------

        remainingFriendlyAlertedDuration -= UPDATE_TICK_RATE;
        if (!isCaptive && remainingFriendlyAlertedDuration <= 0f) SetAlerted(false);
    }
    private void CullingAndLoSHandler()
    {
        playerOutsideOfDistThreshold = Vector3.Distance(player.position, transform.position) > DIST_TO_PLAYER_THRESHOLD;
        if (playerOutsideOfDistThreshold)
        {
            //NPC is not visible to player
            CullCompletely();
            if (enterAlertOnPlayerSeen && LoS.activeSelf) LoS.SetActive(false);
        }
        else
        {
            //NPC might be visible to player
            CullUpdateTransforms();
            if (enterAlertOnPlayerSeen && !anim.GetBool(AnimParams.NPC_BOOL_ALERTED) && !LoS.activeSelf) LoS.SetActive(true);
        }
    }
    private void CullCompletely()
    {
        if (anim.cullingMode != AnimatorCullingMode.CullCompletely) anim.cullingMode = AnimatorCullingMode.CullCompletely;
    }
    private void CullUpdateTransforms()
    {
        if (anim.cullingMode != AnimatorCullingMode.CullUpdateTransforms) anim.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
    }

    public void SetIsCaptive(bool active)
    {
        isCaptive = active;
    }

    public bool DamagedOtherNPC()
    {
        if (lastHitWasOtherNPC)
        {
            lastHitWasOtherNPC = false;
            return false;
        } //-------------------------------------------------------------------------------------------------

        Transform lastHitTransform = gun.HitTransform;

        if (lastHitTransform == null) return false;

        if (lastHitTransform.GetComponent<NPCHitbox>() != null)
        {
            //print($"{this.gameObject.name} just damaged {lastHitTransform.name}");
            lastHitWasOtherNPC = true;
            return true;
        }
        return false;
    }

    public void Attack() => gun?.Shoot();

    private void SpawnBloodPool()
    {
        RaycastHit hit;
        Physics.Raycast(bloodPoolOrigin.position, Vector3.down, out hit, 10f, bloodPoolLayers);

        //if hit nothing or hit self, don't do anything
        if (hit.transform == null || hit.transform.GetComponent<NPCHitbox>() != null) return; //-----------

        //Blood pool needs to spawn on the ground below NPC and ignore the colliders on the NPC itself.
        //To make this more consistent either use Layers or Physics.RaycastAll

        GameObject blood = bloodPool.GetPooledObject();
        blood.GetComponent<BloodPool>().Reset();
        blood.transform.position = hit.point + new Vector3(0, 0.05f, 0);
    }

    public void SetNavSpeedToWalk() => SetNavSpeed(walkSpeed);
    public void SetNavSpeedToRun() => SetNavSpeed(runSpeed);
    public void SetNavSpeedToStop() => SetNavSpeed(0f);
    private void SetNavSpeed(float speed)
    {
        if (!agent.enabled) return; //----------------
        agent.speed = speed;
        agent.isStopped = (speed <= 0f);
        if (agent.isStopped) agent.velocity = Vector3.zero;
    }

    private void OnDestroy()
    {
        AlertZoneManager.onAlertZoneStatusChange -= SetAlerted;
        AlertZoneManager.onEnterHighAlert -= EnterHighAlert;
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }
}
