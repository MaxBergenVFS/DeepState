using UnityEngine;

public class CodecNotificationTrigger : DialogueTrigger
{
    [SerializeField]
    private bool repeatable;

    private NotificationUI notification;
    private Collider col;

    protected override void Awake()
    {
        base.Awake();
        col = GetComponent<Collider>();
    }
    protected override void Start()
    {
        base.Start();
        notification = NotificationUI.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER)) HandleNotification();
    }

    private void HandleNotification()
    {
        notification.DisplayText(dialogue[0]);
        if (!repeatable) col.enabled = false;
    }
}
