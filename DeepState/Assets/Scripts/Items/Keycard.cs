public class Keycard : BaseItem
{
    private int accessLevel;
    public int AccessLevel { get { return accessLevel; } }

    protected override void Start()
    {
        base.Start();
        accessLevel = GameManager.Instance.DoorKeyManager.GetAccessLevelFromItemType(itemType);
        PlayerInventory.onAllEquipmentRemoved += HandleRemovingEquipment;
    }

    public override void UseFromInventory(out bool success)
    {
        playerInventory.RemoveAllEquipment(itemType);
        inventoryItemDatabase.SetInventoryItemIsEquipped(itemType, !inventoryItemDatabase.GetInventoryItemIsEquipped(itemType));
        playerInventory.EquipKeycard(this, inventoryItemDatabase.GetInventoryItemIsEquipped(itemType));
        success = true;
    }

    private void HandleRemovingEquipment(Enums.itemType itemTypeToIgnore)
    {
        switch (itemTypeToIgnore)
        {
            case (Enums.itemType.RedKeycard):
                inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.BlueKeycard, false);
                break;
            case (Enums.itemType.BlueKeycard):
                inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.RedKeycard, false);
                break;
            default:
                inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.RedKeycard, false);
                inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.BlueKeycard, false);
                break;
        }
    }

    private void OnDestroy()
    {
        PlayerInventory.onAllEquipmentRemoved -= HandleRemovingEquipment;
    }
}
