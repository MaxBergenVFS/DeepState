using UnityEngine;
using UnityEngine.AI;

public class NPCDeathBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private bool playDeathAnimation = false;

    private NPCController controller;
    private NavMeshAgent agent;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.NPC_BOOL_IS_DYING, true);
        controller = animator.GetComponent<NPCController>();
        controller.SetNavSpeedToStop();
        if (!playDeathAnimation) controller.Ragdoll();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (playDeathAnimation) controller.Ragdoll();
    }
}
