using System.Collections.Generic;
using UnityEngine;

public class MetalDetector : MonoBehaviour
{
    [SerializeField]
    private string notificationText = "All items removed";
    [SerializeField]
    private ConfiscatedItems confiscatedItems;

    private Collider col;
    private PlayerController playerController;
    private PlayerInventory playerInventory;
    private PlayerHands playerHands;
    private InventoryItemDatabase inventoryItemDatabase;
    private NotificationUI notificationUI;
    
    private void Awake()
    {
        col = GetComponent<Collider>();
    }

    private void Start()
    {
        notificationUI = NotificationUI.Instance;
        playerController = PlayerController.Instance;
        playerInventory = playerController.GetComponent<PlayerInventory>();
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
        playerHands = playerController.GetComponent<PlayerHeldItem>().PlayerHands;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            col.enabled = false;
            ConfiscateItems();
            playerHands.Unequip();
            notificationUI.DisplayText(notificationText);
            playerInventory.RefreshItems();
        }
    }

    private void ConfiscateItems()
    {
        List<int> validIds = inventoryItemDatabase.GetListOfValidIds();
        foreach (int id in validIds)
        {
            int amt = inventoryItemDatabase.GetInventoryItemAmount(id);
            // add to confiscated items dictionary
            confiscatedItems.AddItemToDict(id, amt);
            // remove from player
            inventoryItemDatabase.SetInventoryItemAmount(id, 0);
        }
        
        List<IInventoryItem> itemListRef = playerInventory.IInventoryItemsListRef;
        foreach (IInventoryItem item in itemListRef)
        {
            confiscatedItems.AddItemsToList(item);
        }
    }
}
