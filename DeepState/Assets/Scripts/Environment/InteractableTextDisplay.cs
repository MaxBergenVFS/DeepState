﻿using UnityEngine;

public class InteractableTextDisplay : MonoBehaviour, IInteractable
{
    [SerializeField]
    protected string interactTxt = "";

    public string Name { get { return ""; } }

    public void Interact()
    {
        NotificationUI.Instance.DisplayText(interactTxt);
    }
}
