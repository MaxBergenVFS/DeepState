// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "shader_foliage_lit_onesided"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_distortionscale("distortion scale", Range( 0 , 5)) = 1
		_SpecColor("Specular Color",Color)=(1,1,1,1)
		_vertexbrightness("vertex brightness", Range( 0 , 5)) = 1
		_noisescale("noise scale", Range( 0 , 5)) = 1
		_noisefactor("noise factor", Range( 0 , 5)) = 1
		_distortionscroll("distortion scroll", Vector) = (0,0,0,0)
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 5.0
		#pragma surface surf BlinnPhong keepalpha noshadow novertexlights nolightmap  nodynlightmap nodirlightmap 
		struct Input
		{
			float4 vertexColor : COLOR;
			float4 screenPos;
			float2 uv_texcoord;
		};

		uniform float _vertexbrightness;
		uniform sampler2D _TextureSample1;
		uniform float2 _distortionscroll;
		uniform float _distortionscale;
		uniform float _noisescale;
		uniform float _noisefactor;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutput o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 temp_cast_0 = (_distortionscale).xx;
			float2 uv_TexCoord21 = i.uv_texcoord * temp_cast_0;
			float2 panner23 = ( 1.0 * _Time.y * _distortionscroll + uv_TexCoord21);
			float simplePerlin2D26 = snoise( panner23*_noisescale );
			simplePerlin2D26 = simplePerlin2D26*0.5 + 0.5;
			float temp_output_30_0 = ( ase_screenPosNorm.w + ( simplePerlin2D26 * _noisefactor ) );
			float2 temp_cast_1 = (temp_output_30_0).xx;
			float2 uv_TexCoord58 = i.uv_texcoord * temp_cast_1;
			float4 tex2DNode57 = tex2D( _TextureSample1, uv_TexCoord58 );
			o.Albedo = ( ( i.vertexColor * _vertexbrightness ) * tex2DNode57 ).rgb;
			o.Alpha = tex2DNode57.a;
			clip( tex2DNode57.a - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18921
-2094.06;135.0314;1920;1007;1294.35;1461.71;1.757862;True;False
Node;AmplifyShaderEditor.RangedFloatNode;22;-1733.213,-513.9459;Inherit;False;Property;_distortionscale;distortion scale;0;0;Create;True;0;0;0;False;0;False;1;5;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;25;-1367.072,-342.1268;Inherit;False;Property;_distortionscroll;distortion scroll;7;0;Create;True;0;0;0;False;0;False;0,0;0.5,0.5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;21;-1359.972,-497.022;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-1277.072,-200.1268;Inherit;False;Property;_noisescale;noise scale;5;0;Create;True;0;0;0;False;0;False;1;1;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;23;-1070.389,-416.1849;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-972.0037,-113.7875;Inherit;False;Property;_noisefactor;noise factor;6;0;Create;True;0;0;0;False;0;False;1;0.01;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;26;-861.0715,-415.1268;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;31;-611.0037,-605.7875;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-563.9697,-243.6616;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-390.0037,-344.7875;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;54;157.4902,-646.1994;Inherit;False;Property;_vertexbrightness;vertex brightness;4;0;Create;True;0;0;0;False;0;False;1;1;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;58;-258.5599,-558.3146;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;56;189.9541,-870.0272;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;55;519.7148,-591.5239;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;57;-9.997658,-440.1684;Inherit;True;Property;_TextureSample1;Texture Sample 1;8;0;Create;True;0;0;0;False;0;False;-1;None;bf03750c43374bf459ed505b3cd25f27;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;230.6052,-1184.296;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;53;667.8587,-438.1856;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-41.25094,-1012.574;Inherit;False;Property;_emission;emission;3;0;Create;True;0;0;0;False;0;False;1;5;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;60;561.7625,-268.8009;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;49;-37.53036,-1199.428;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;540.2333,5.004241;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;740.0579,57.51303;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;413.5262,188.2291;Inherit;False;Property;_alphascale;alpha scale;2;0;Create;True;0;0;0;False;0;False;1;1;0;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.FaceVariableNode;62;888.915,-651.3358;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.InstanceIdNode;63;677.9713,-661.8829;Inherit;False;False;0;1;INT;0
Node;AmplifyShaderEditor.VertexIdVariableNode;61;716.6443,-804.2699;Inherit;False;0;1;INT;0
Node;AmplifyShaderEditor.ColorNode;50;-363.2709,-1225.839;Inherit;False;Property;_Color0;Color 0;9;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.7704402,1,0.8838593,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;1;1074.878,-163.1276;Float;False;True;-1;7;ASEMaterialInspector;0;0;BlinnPhong;shader_foliage_lit_onesided;False;False;False;False;False;True;True;True;True;False;False;False;False;False;True;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;AlphaTest;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;21;0;22;0
WireConnection;23;0;21;0
WireConnection;23;2;25;0
WireConnection;26;0;23;0
WireConnection;26;1;27;0
WireConnection;28;0;26;0
WireConnection;28;1;29;0
WireConnection;30;0;31;4
WireConnection;30;1;28;0
WireConnection;58;0;30;0
WireConnection;55;0;56;0
WireConnection;55;1;54;0
WireConnection;57;1;58;0
WireConnection;51;0;49;0
WireConnection;51;1;52;0
WireConnection;53;0;55;0
WireConnection;53;1;57;0
WireConnection;60;0;30;0
WireConnection;60;1;57;0
WireConnection;49;0;50;0
WireConnection;47;0;45;0
WireConnection;47;1;48;0
WireConnection;1;0;53;0
WireConnection;1;9;57;4
WireConnection;1;10;57;4
ASEEND*/
//CHKSM=6BF7542F5DDEDCE6902E1512E7CC30FC3BA8069A