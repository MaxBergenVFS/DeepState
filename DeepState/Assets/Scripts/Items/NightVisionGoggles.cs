public class NightVisionGoggles : BaseItem
{
    private NightVisionGogglesBattery nvGoggleBattery;

    protected override void Start()
    {
        base.Start();
        PlayerInventory.onAllEquipmentRemoved += HandleRemovingEquipment;
        nvGoggleBattery = playerInventory.NvGoggleBattery;
    }

    private void OnDestroy()
    {
        PlayerInventory.onAllEquipmentRemoved -= HandleRemovingEquipment;
    }

    public override void UseFromInventory(out bool success)
    {
        playerInventory.RemoveAllEquipment(itemType);
        inventoryItemDatabase.SetInventoryItemIsEquipped(itemType, !inventoryItemDatabase.GetInventoryItemIsEquipped(itemType));
        playerInventory.EquipNightVision(inventoryItemDatabase.GetInventoryItemIsEquipped(itemType));
        success = true;
    }

    public override void OnDrop()
    {
        base.OnDrop();
        playerInventory.EquipNightVision(false);
        nvGoggleBattery.ShowNvGoggleOnBarUI(false);
    }

    public override void OnPickup()
    {
        base.OnPickup();
        nvGoggleBattery.ShowNvGoggleOnBarUI(true);
    }

    private void HandleRemovingEquipment(Enums.itemType itemTypeToIgnore)
    {
        if (itemTypeToIgnore == itemType) return;

        inventoryItemDatabase.SetInventoryItemIsEquipped(itemType, false);
    }
}
