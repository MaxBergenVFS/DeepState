using UnityEngine;

public class PlayerHeldItem : MonoBehaviour
{
    private const float RETICLE_RANGE = 1000f;

    [SerializeField]
    private Camera reticleCamera;
    [SerializeField]
    private PlayerHands playerHands;

    private CameraController cameraController;

    public PlayerHands PlayerHands { get { return playerHands; } }

    private void Awake()
    {
        cameraController = GetComponent<CameraController>();
    }

    private void Update()
    {
        TryDisplayDamageableReticle();
    }

    private void TryDisplayDamageableReticle()
    {
        Ray ray = reticleCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        Physics.Raycast(ray, out hit, RETICLE_RANGE);
        Transform target = hit.transform;

        //target is damageable and is NOT player
        if (target != null && target.GetComponent<IDamageable>() != null && !target.CompareTag(GeneralParams.TAG_PLAYER))
        {
            NPCHitbox npc = target.GetComponent<NPCHitbox>();
            //damageable target is NOT an NPC
            if (npc == null) cameraController.DisplayDamageableReticle(true);
            //damageable target is an NPC
            else cameraController.DisplayDamageableReticle(!npc.IsDead, npc.IsFriendly);
            return; 
        } //-----------------------------------------------------------------------------------------------------
        
        if (target == null)
        {
            cameraController.DisplayDamageableReticle(false);
            return;
        } //----------------------------------------------------------------

        NPCDialogue dialogueBox = target.GetComponent<NPCDialogue>();

        //target is dialogue box
        if (dialogueBox != null) cameraController.DisplayDamageableReticle(true, dialogueBox.IsFriendly);
        //there is no target
        else cameraController.DisplayDamageableReticle(false);
    }

    public void PrimaryButtonDown() => playerHands.UsePrimary();
    public void PrimaryButtonHold() => playerHands.UsePrimaryHeld();
    public void SecondaryButtonDown() => playerHands.UseSecondary();
    public void ReloadEquipped() => playerHands.ReloadEquipped();

    public void Equip(Enums.itemType itemType)
    {
        if (itemType == playerHands.EquippedItemType) playerHands.Unequip();
        else playerHands.Equip(itemType);
    }

    public void ToggleMelee() => playerHands.ToggleMelee();

}
