using UnityEngine.AI;
using UnityEngine;

public class NPCSetParams : StateMachineBehaviour
{
    private NavMeshAgent agent;
    private NPCController controller;

    [SerializeField]
    private bool stopNav = true, disableDialogue = true, disableAimConstraint = false;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponent<NPCController>();
        
        if (disableDialogue) controller.DisableDialogue();
        if (disableAimConstraint) controller.DisableConstraints();
        if (stopNav) 
        {
            controller.SetNavSpeedToStop();
            animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
            animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, false);
        }
    }
}
