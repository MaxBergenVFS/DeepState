using System.Collections;
using UnityEngine;

[RequireComponent(typeof(GunSFX))]
public class Gun : MonoBehaviour
{
    #region Inits
    private const float SILENCER_RADIUS_MULTIPLIER = 0.25f;
    private const int SILENCER_DAMAGE_REDUCTION = 2;

    [SerializeField]
    protected int magazineSize;
    [SerializeField]
    protected Transform origin, tracerOrigin;
    [SerializeField]
    protected float spread;
    [SerializeField]
    protected Animator anim;
    [SerializeField]
    protected float fireRate, range;
    [SerializeField]
    protected LayerMask whatIsDamagable;
    [SerializeField]
    protected float soundRadius = 50f;

    [SerializeField]
    private bool endlessMagazine;
    [SerializeField]
    private int maxDamage = 5, minDamage = 2;
    [SerializeField]
    private float reloadTime, force;

    protected int bulletsRemaining;
    protected bool reloading, readyToShoot, silenced;
    protected GunSFX sfx;
    protected Enums.damageType damageType = Enums.damageType.None;

    private int bulletsShot, dynamicLayer;
    private float defaultSoundRadius, silencedSoundRadius;
    private ObjectPool bulletHolePool, bulletImpactParticlePool, bulletTracerPool;
    private bool lastHitWasNPC;
    private Transform hitTransform = null;

    public Transform HitTransform { get { return hitTransform; } }
    #endregion

    protected virtual void Awake()
    {
        bulletsRemaining = magazineSize;
        defaultSoundRadius = soundRadius;
        silencedSoundRadius = soundRadius * SILENCER_RADIUS_MULTIPLIER;
        readyToShoot = true;
        dynamicLayer = LayerMask.NameToLayer(GeneralParams.LAYER_DYNAMIC);
        sfx = GetComponent<GunSFX>();
        if (tracerOrigin == null) tracerOrigin = origin;
    }
    protected virtual void Start()
    {
        InitObjectPoolRefs();
    }

    private void InitObjectPoolRefs()
    {
        bulletHolePool = GameManager.Instance.BulletHolePool;
        bulletImpactParticlePool = GameManager.Instance.BulletImpactParticlePool;
        bulletTracerPool = GameManager.Instance.BulletTracerPool;
    }

    public virtual void Shoot()
    {
        if (!readyToShoot || reloading) return; //---------------------------------------------------

        if (bulletsRemaining <= 0)
        {
            NoBulletsRemainingInClip();
            return;
        } //------------------------------------------------------------------------------------------------------

        readyToShoot = false;
        sfx.PlayShootSFX(silenced);
        MuzzleFlash();
        PlayShootAnim();

        RaycastHit hit;
        Vector3 dir = new Vector3();
        dir = GetDirection();
        Physics.Raycast(origin.position, dir, out hit, range, whatIsDamagable);
        SphereCastBulletFlyBy(new Ray(origin.position, dir));

        AmmoHandler();
        ValidHitHandler(hit);

        Invoke("ResetShot", fireRate);
    }

    private void AmmoHandler()
    {
        if (endlessMagazine) return; //------------------------

        UpdateAmmoRemainingInClip(--bulletsRemaining);
        bulletsShot--;
    }
    private void ValidHitHandler(RaycastHit hit)
    {
        if (hit.transform == null) return; //-----------------------

        if (hit.transform.GetComponent<IDamageable>() != null)
        {
            //hit something damageable
            DoDamage(hit);
            VertexBloodColouring(hit);
        }
        else
        {
            //hit something NOT damageable
            SpawnBulletHole(hit);
        }
        SpawnBulletTracer(hit.point);
        GunShotImpactSound(hit.point);
        hitTransform = hit.transform;
    }

    private void DoDamage(RaycastHit hit)
    {
        IDamageable damageable = hit.transform.GetComponent<IDamageable>();
        damageable?.TakeDamage(GetDamageAmt(), hit.point, force, damageType);
    }
    private void VertexBloodColouring(RaycastHit hit)
    {
        NPCHitbox hitbox = hit.transform.GetComponent<NPCHitbox>();
        hitbox?.VertexBloodColouring(hit.point);
    }

    private int GetDamageAmt()
    {
        int dmg = Random.Range(minDamage, maxDamage + 1);
        if (silenced) dmg -= SILENCER_DAMAGE_REDUCTION;
        if (dmg < 1) dmg = 1;
        return dmg;
    }

    public virtual void EquipSilencer(bool active)
    {
        silenced = active;
        if (silenced) soundRadius = silencedSoundRadius;
        else soundRadius = defaultSoundRadius;
    }

    protected virtual void UpdateAmmoRemainingInClip(int bulletsRemainingInClip)
    {
        bulletsRemaining = bulletsRemainingInClip;
    }
    private void NoBulletsRemainingInClip()
    {
        sfx.PlayOutOfAmmoSFX();
        readyToShoot = false;
        Invoke("ResetShot", fireRate);
    }

    protected virtual void MuzzleFlash() { }

    protected virtual void GunShotImpactSound(Vector3 pos)
    {
        if (bulletsRemaining <= 0) return; //------------------
        sfx.PlayImpactSFX(pos);
    }

    protected virtual void SphereCastBulletFlyBy(Ray ray) { }

    private void SpawnBulletHole(RaycastHit hit)
    {
        if (hit.transform.GetComponent<Fragment>() != null || hit.transform.gameObject.layer == dynamicLayer) return;

        Vector3 pos = hit.point;
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, hit.normal);

        GameObject bulletHole = bulletHolePool.GetPooledObject();
        bulletHole.transform.position = pos;
        bulletHole.transform.rotation = rot;

        GameObject bulletImpact = bulletImpactParticlePool.GetPooledObject();
        bulletImpact.transform.position = pos;
        bulletImpact.transform.rotation = Quaternion.identity;
    }
    private void SpawnBulletTracer(Vector3 hitPoint)
    {
        GameObject bulletTracer = bulletTracerPool.GetPooledObject();
        bulletTracer.transform.position = tracerOrigin.position;
        bulletTracer.transform.rotation = Quaternion.identity;
        LineRenderer rend = bulletTracer.GetComponent<LineRenderer>();

        rend.SetPosition(0, tracerOrigin.position);
        rend.SetPosition(1, hitPoint);
    }

    private void ResetShot()
    {
        readyToShoot = true;
    }

    public void Reload() => Reload(magazineSize);
    public void Reload(int ammoAmountToPutInClip)
    {
        if (reloading) return; //-------------------------------------------------------------

        PlayReloadAnim();
        sfx.PlayReloadSFX();
        reloading = true;
        StopCoroutine(ReloadFinished(ammoAmountToPutInClip));
        StartCoroutine(ReloadFinished(ammoAmountToPutInClip));
    }
    private IEnumerator ReloadFinished(int ammoAmountToPutInClip)
    {
        yield return new WaitForSeconds(reloadTime);
        UpdateAmmoRemainingInClip(ammoAmountToPutInClip);
        reloading = false;
    }

    private void PlayReloadAnim()
    {
        if (anim == null) return;
        anim.SetTrigger(AnimParams.GUN_TRIGGER_RELOAD);
    }
    protected virtual void PlayShootAnim()
    {
        if (anim == null) return;
        anim.SetTrigger(AnimParams.GUN_TRIGGER_SHOOT);
    }

    protected virtual Vector3 GetDirection()
    {
        return origin.forward;
    }
}
