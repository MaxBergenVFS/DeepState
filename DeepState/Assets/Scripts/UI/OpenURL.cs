using UnityEngine;

public class OpenURL : MonoBehaviour
{
    private const string URL_DISCORD = "https://discord.gg/PpTpvbWy";
    private const string URL_TWITTER = "https://twitter.com/deepstategame";
    private const string URL_PATREON = "https://patreon.com/DeepStateGame";

    public void OpenDiscord() => Open(URL_DISCORD);
    public void OpenTwitter() => Open(URL_TWITTER);
    public void OpenPatreon() => Open(URL_PATREON);
    private void Open(string url)
    {
        Application.OpenURL(url);
        print($"Open {url}");
    }
}
