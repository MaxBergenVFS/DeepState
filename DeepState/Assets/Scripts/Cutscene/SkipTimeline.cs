using UnityEngine;
using UnityEngine.Playables;

public class SkipTimeline : MonoBehaviour
{
    [SerializeField]
    private bool skipOnEnable;
    [SerializeField]
    private PlayableDirector director;
    [SerializeField]
    private CutsceneMusic music;
    [SerializeField]
    private double skipTime = 226.00d;
    [SerializeField]
    private GameObject[] objsToDisableOnSkip;

    private bool skipped;
    
    public bool Skipped { get { return skipped; } }

    private void Awake()
    {
        skipped = false;
    }

    private void OnEnable()
    {
        if (skipOnEnable) Skip();
    }

    public void Skip()
    {
        if (skipped) return; //-----------------------

        foreach (GameObject obj in objsToDisableOnSkip)
        {
            obj.SetActive(false);
        }
        music.Stop();
        director.time = skipTime;
        skipped = true;
    }
}
