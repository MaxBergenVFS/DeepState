using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{
    private const float DEFAULT_MUSIC_VOLUME_SLIDER_VAL = 0.5f;
    private const float MUFFLED_LERP_SPEED = 3f;
    private const float INTENSITY_LERP_SPEED = 2f;

    private FMOD.Studio.EventInstance pauseMenuMusic, deathScreenMusic, codecMusic, cutsceneMusic,
        firstPhaseMusic, secondPhaseMusic, thirdPhaseMusic;
    private FMOD.Studio.Bus musicBus;
    private int currentPhase;
    private Slider musicVolumeSlider;
    private bool playerInScene;
    private Dictionary<string, float> paramSnapshot = new Dictionary<string, float>();

    [SerializeField]
    private Enums.musicTrack firstPhase, secondPhase, thirdPhase;

    public float MuffledLerpDuration { get { return 1f / MUFFLED_LERP_SPEED; } }
    public float IntensityLerpDuration { get { return 1f / INTENSITY_LERP_SPEED; } }

    private void Awake()
    {
        currentPhase = 1;
        InitSnapshotDict();
    }
    private void Start()
    {
        playerInScene = (PlayerController.Instance != null);
        if (playerInScene)
        {
            musicVolumeSlider = PlayerController.Instance.GetComponent<PlayerInventory>().SettingsTabUIRef.MusicVolume;
            musicVolumeSlider.value = DEFAULT_MUSIC_VOLUME_SLIDER_VAL;
            SetMusicVolume();
            musicVolumeSlider.onValueChanged.AddListener(delegate { SetMusicVolume(); });
        }

        musicBus = FMODUnity.RuntimeManager.GetBus(MusicParams.PATH_MUSIC_BUS);
        pauseMenuMusic = FMODUnity.RuntimeManager.CreateInstance(MusicParams.MUSIC_PREFIX + MusicParams.PATH_PAUSE_MENU);
        deathScreenMusic = FMODUnity.RuntimeManager.CreateInstance(MusicParams.MUSIC_PREFIX + MusicParams.PATH_DEATH_SCREEN);
        codecMusic = FMODUnity.RuntimeManager.CreateInstance(MusicParams.MUSIC_PREFIX + MusicParams.PATH_CODEC);

        if (firstPhase != Enums.musicTrack.None) firstPhaseMusic = FMODUnity.RuntimeManager.CreateInstance(GetPathFromEnum(firstPhase));
        if (secondPhase != Enums.musicTrack.None) secondPhaseMusic = FMODUnity.RuntimeManager.CreateInstance(GetPathFromEnum(secondPhase));
        if (thirdPhase != Enums.musicTrack.None) thirdPhaseMusic = FMODUnity.RuntimeManager.CreateInstance(GetPathFromEnum(thirdPhase));

        PlayGameplayMusic(true);

        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void SetMusicVolume()
    {
        musicBus.setVolume(musicVolumeSlider.value);
    }

    private string GetPathFromEnum(Enums.musicTrack track)
    {
        string path = MusicParams.MUSIC_PREFIX;
        switch (track)
        {
            case (Enums.musicTrack.Unatco):
                path += MusicParams.PATH_UNACTO;
                break;
            case (Enums.musicTrack.Frigate):
                path += MusicParams.PATH_FRIGATE;
                break;
            case (Enums.musicTrack.Afghanistan):
                path += MusicParams.PATH_AFGHANISTAN;
                break;
            case (Enums.musicTrack.Combat1):
                path += MusicParams.PATH_COMBAT_1;
                break;
            case (Enums.musicTrack.Combat2):
                path += MusicParams.PATH_COMBAT_2;
                break;
            case (Enums.musicTrack.Combat3):
                path += MusicParams.PATH_COMBAT_3;
                break;
            case (Enums.musicTrack.AfghanistanDynamic):
                path += MusicParams.PATH_AFGHANISTAN_DYNAMIC;
                break;
            case (Enums.musicTrack.ResultsScreen):
                path += MusicParams.PATH_RESULTS_SCREEN;
                break;
            case (Enums.musicTrack.None):
            default:
                break;
        }
        return path;
    }

    private void OnDestroy()
    {
        StopMusicInstance(pauseMenuMusic, true);
        StopMusicInstance(deathScreenMusic, true);
        StopMusicInstance(codecMusic, true);
        if (firstPhase != Enums.musicTrack.None) StopMusicInstance(firstPhaseMusic, true);
        if (secondPhase != Enums.musicTrack.None) StopMusicInstance(secondPhaseMusic, true);
        if (thirdPhase != Enums.musicTrack.None) StopMusicInstance(thirdPhaseMusic, true);
        if (playerInScene) musicVolumeSlider.onValueChanged.RemoveListener(delegate { SetMusicVolume(); });
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    private void InitSnapshotDict()
    {
        paramSnapshot.Add(MusicParams.PARAM_INTENSITY, 0f);
        paramSnapshot.Add(MusicParams.PARAM_MUFFLED, 0f);
    }
    private void SnapshotState()
    {
        float intensityVal;
        FMODUnity.RuntimeManager.StudioSystem.getParameterByName(MusicParams.PARAM_INTENSITY, out intensityVal);
        paramSnapshot[MusicParams.PARAM_INTENSITY] = intensityVal;

        float muffledVal;
        FMODUnity.RuntimeManager.StudioSystem.getParameterByName(MusicParams.PARAM_MUFFLED, out muffledVal);
        paramSnapshot[MusicParams.PARAM_MUFFLED] = muffledVal;
    }
    private void RevertState()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName(MusicParams.PARAM_INTENSITY, paramSnapshot[MusicParams.PARAM_INTENSITY]);
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName(MusicParams.PARAM_MUFFLED, paramSnapshot[MusicParams.PARAM_MUFFLED]);
    }

    private void StopMusicInstance(FMOD.Studio.EventInstance musicInstance, bool release)
    {
        musicInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        if (release) musicInstance.release();
    }
    private void PlayMusicInstance(FMOD.Studio.EventInstance musicInstance, bool play, bool resetOnPlay)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        musicInstance.getPlaybackState(out state);

        switch (state)
        {
            case (FMOD.Studio.PLAYBACK_STATE.STOPPED):
                if (play) musicInstance.start();
                break;
            case (FMOD.Studio.PLAYBACK_STATE.PLAYING):
                if (resetOnPlay)
                {
                    if (!play) StopMusicInstance(musicInstance, false);
                }
                else
                {
                    musicInstance.setPaused(!play);
                }
                break;
            default:
                return;
        }
    }

    private void PlayGameplayMusic(bool play)
    {
        switch (currentPhase)
        {
            case (2):
                if (secondPhase != Enums.musicTrack.None) PlayMusicInstance(secondPhaseMusic, play, false);
                break;
            case (3):
                if (thirdPhase != Enums.musicTrack.None) PlayMusicInstance(thirdPhaseMusic, play, false);
                break;
            case (1):
            default:
                if (firstPhase != Enums.musicTrack.None) PlayMusicInstance(firstPhaseMusic, play, false);
                break;
        }
    }

    public void StopGameplayMusic() => PlayGameplayMusic(false);
    public void ResumeGameplayMusic() => PlayGameplayMusic(true);

    public void PlayMenuMusic(bool active, bool playerIsDead, bool inCodecCall)
    {
        PlayGameplayMusic(!active);
        if (playerIsDead)
        {
            StopMusicInstance(pauseMenuMusic, false);
            StopMusicInstance(codecMusic, false);
            PlayMusicInstance(deathScreenMusic, active, true);
            return;
        } //----------------------------------------------------------------

        StopMusicInstance(deathScreenMusic, false);

        if (inCodecCall)
        {
            StopMusicInstance(pauseMenuMusic, false);
            PlayMusicInstance(codecMusic, active, true);
        }
        else
        {
            StopMusicInstance(codecMusic, false);
            PlayMusicInstance(pauseMenuMusic, active, true);
        }
    }

    public void InitCutsceneMusic(string path)
    {
        cutsceneMusic = FMODUnity.RuntimeManager.CreateInstance(path);
    }
    public void PlayCutsceneMusic() => cutsceneMusic.start();
    public void StopCutsceneMusic() => StopMusicInstance(cutsceneMusic, true);

    public void SetPhase(int num)
    {
        PlayGameplayMusic(false);
        currentPhase = num;
        PlayGameplayMusic(true);
    }

    public void Intensity(bool active) => SetGlobalParam(MusicParams.PARAM_INTENSITY, INTENSITY_LERP_SPEED, active);
    public void Muffle(bool active) => SetGlobalParam(MusicParams.PARAM_MUFFLED, MUFFLED_LERP_SPEED, active);
    private void SetGlobalParam(string param, float speed, bool active)
    {
        if (active == ParamIsActive(param)) return; //----------------------
        float val;
        if (active) val = 1f;
        else val = 0f;
        StartCoroutine(LerpGlobalParam(param, val, speed));
    }
    private IEnumerator LerpGlobalParam(string param, float target, float speed)
    {
        float t = 0f;
        float val, start;
        FMODUnity.RuntimeManager.StudioSystem.getParameterByName(param, out val);
        start = val;
        while (t < 1f)
        {
            val = Mathf.Lerp(start, target, t);
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName(param, val);
            t += Time.unscaledDeltaTime * speed;
            yield return null;
        }
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName(param, target);
    }

    private bool ParamIsActive(string param)
    {
        float val;
        FMODUnity.RuntimeManager.StudioSystem.getParameterByName(param, out val);
        return val > 0f;
    }

}
