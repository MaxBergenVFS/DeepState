public static class AnimParams
{
    #region NPC
    public const string NPC_STATE_START = "Start";

    public const string NPC_BOOL_ALERTED = "alerted";
    public const string NPC_BOOL_IN_ATTACK_RANGE = "inAttackRange";
    public const string NPC_BOOL_CAN_SEE_PLAYER = "canSeePlayer";
    public const string NPC_BOOL_AT_REPOSITION_POINT = "atRepositionPoint";
    public const string NPC_BOOL_IS_REPOSITIONING = "isRepositioning";
    public const string NPC_BOOL_HAS_PAT_POINTS = "hasPatPoints";
    public const string NPC_BOOL_HAS_WEAPON = "hasWeapon";
    public const string NPC_BOOL_IS_RUNNING = "isRunning";
    public const string NPC_BOOL_IS_WALKING = "isWalking";
    public const string NPC_BOOL_IS_DEAD = "isDead";
    public const string NPC_BOOL_IS_DYING = "isInDyingAnim";
    public const string NPC_BOOL_IN_DIALOGUE = "inDialogue";
    public const string NPC_BOOL_IS_STRAFING = "isStrafing";
    public const string NPC_BOOL_IS_CROUCHING = "isCrouching";
    public const string NPC_BOOL_IS_SEARCHING = "isSearching";
    public const string NPC_BOOL_AT_DEST = "atDest";
    public const string NPC_BOOL_STOPPED_MID_PAT = "stoppedMidPat";
    public const string NPC_BOOL_IS_IN_GUARD_TOWER = "isInGuardTower";

    public const string NPC_TRIGGER_RUN_FOR_ALARM = "runForAlarm";
    public const string NPC_TRIGGER_FLINCH_BODY_RIGHT = "flinchRightBody";
    public const string NPC_TRIGGER_FLINCH_BODY_LEFT = "flinchLeftBody";
    public const string NPC_TRIGGER_FLINCH_SHOULDER_RIGHT = "flinchRightShoulder";
    public const string NPC_TRIGGER_FLINCH_SHOULDER_LEFT = "flinchLeftShoulder";
    public const string NPC_TRIGGER_FLINCH_LEG_RIGHT = "flinchRightLeg";
    public const string NPC_TRIGGER_FLINCH_LEG_LEFT = "flinchLeftLeg";
    public const string NPC_TRIGGER_BULLET_FLY_BY = "bulletFlyBy";
    public const string NPC_TRIGGER_SKIP_UNCROUCH = "skipUncrouch";

    public const string NPC_INT_BULLET_FLY_BY_VARIATION = "bulletFlyByVariation";

    public const string NPC_FLOAT_COWER_TIME = "cowerTime";
    public const string NPC_FLOAT_IDLE_TIME = "idleTime";
    #endregion

    #region Player
    public const string PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION = "currentlyPlayingUseAnim";
    public const string PLAYER_BOOL_EQUIPPED_GUN_IS_EMPTY = "equippedGunIsEmpty";
    public const string PLAYER_BOOL_HOLSTERING = "holstering";

    public const string PLAYER_TRIGGER_EQUIP_WRENCH = "equipWrench";
    public const string PLAYER_TRIGGER_EQUIP_LOCKPICK = "equipLockPick";
    public const string PLAYER_TRIGGER_EQUIP_HACKING_TOOL = "equipHackingTool";
    public const string PLAYER_TRIGGER_EQUIP_CHECKPOINT_ITEM = "equipCheckpointItem";
    public const string PLAYER_TRIGGER_EQUIP_WATCH = "equipWatch";
    public const string PLAYER_TRIGGER_EQUIP_AK = "equipAK";
    public const string PLAYER_TRIGGER_EQUIP_M4 = "equipM4";
    public const string PLAYER_TRIGGER_EQUIP_PISTOL = "equipPistol";
    public const string PLAYER_TRIGGER_EQUIP_MELEE = "equipMelee";
    public const string PLAYER_TRIGGER_USE_PRIMARY = "usePrimary";
    public const string PLAYER_TRIGGER_USE_SECONDARY = "useSecondary";
    public const string PLAYER_TRIGGER_RELOAD = "reload";
    #endregion

    #region UI
    public const string UI_DIALOGUE_BOOL_IS_OPEN = "isOpen";
    public const string UI_DIALOGUE_BOOL_IS_CODEC = "isCodec";
    #endregion

    #region Gun
    public const string GUN_TRIGGER_SHOOT = "shoot";
    public const string GUN_TRIGGER_RELOAD = "reload";
    #endregion

    #region Door
    public const string DOOR_TRIGGER_OPEN = "open";
    public const string DOOR_TRIGGER_CLOSE = "close";
    public const string DOOR_TRIGGER_INTERACT = "interact";
    public const string DOOR_TRIGGER_UNLOCK = "unlock";
    public const string DOOR_BOOL_IS_OPEN = "isOpen";
    #endregion

    public const string MIRRORED_RIG_BOOL_MOVING = "moving";
}
