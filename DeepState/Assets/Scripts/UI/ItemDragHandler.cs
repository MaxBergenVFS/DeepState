using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private PlayerInventory playerInventory;
    private Image image;
    private Enums.itemSlotType slotType;

    public IInventoryItem ItemRef { get; set; }
    public Enums.itemSlotType SlotType { get { return slotType; } }

    private void Awake()
    {
        image = GetComponent<Image>();
    }
    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
    }

    public void InitType(Enums.itemSlotType type)
    {
        slotType = type;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (ItemRef != null) playerInventory.SelectItemOnGrid(ItemRef);
        image.raycastTarget = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = Vector3.zero;
        image.raycastTarget = true;
    }
}
