using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour
{
    private const float FADE_OUT_SPEED = 4f;
    private const float SLOT_SELECTED_ALPHA = 0.4f;

    [SerializeField]
    private Enums.itemSlotType slotType;
    [SerializeField]
    private Image slotSelectedImage;
    [SerializeField]
    private Transform itemChargeBarPos;

    private PlayerInventory playerInventory;
    private Color defaultBorderColor, equipmentBorderColor, slotSelectedColor;

    [HideInInspector]
    public Image ItemImage, ItemEquippedImage;

    public TextMeshProUGUI ItemName, ItemAmount;
    public ItemDragHandler ItemDragHandler;
    public Transform ItemChargeBarPos { get { return itemChargeBarPos; } }

    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        ItemDragHandler.InitType(slotType);
        if (ItemEquippedImage != null) defaultBorderColor = ItemEquippedImage.color;
        if (slotSelectedImage != null) slotSelectedImage.enabled = false;
    }

    public void DisplayItemCanBeUsedImage() => DisplaySlotSelectedImage();
    public void DisplaySlotSelectedImage() => DisplaySlotSelectedImage(slotSelectedColor);
    public void DisplayItemCannotBeUsedImage() => DisplaySlotSelectedImage(Color.red);
    private void DisplaySlotSelectedImage(Color color)
    {
        if (slotSelectedImage == null || !this.isActiveAndEnabled) return; //----------
        StopAllCoroutines();
        StartCoroutine(DisplayAndFadeOutSlotSelectedImage(color));
    }

    private IEnumerator DisplayAndFadeOutSlotSelectedImage(Color color)
    {
        float t = 0f;
        float a = SLOT_SELECTED_ALPHA;

        slotSelectedImage.color = new Color(color.r, color.g, color.b, a);
        if (!slotSelectedImage.isActiveAndEnabled) slotSelectedImage.enabled = true;

        while (t < 1f)
        {
            a = Mathf.Lerp(SLOT_SELECTED_ALPHA, 0f, t);
            t += Time.deltaTime * FADE_OUT_SPEED;
            slotSelectedImage.color = new Color(color.r, color.g, color.b, a);
            yield return null;
        }

        slotSelectedImage.enabled = false;
    }

    public void InitColors(Color equipmentBorder, Color slotSelected)
    {
        equipmentBorderColor = equipmentBorder;
        slotSelectedColor = slotSelected;
    }
    public void SetItemEquippedImageColor(bool isEquipment)
    {
        if (isEquipment) ItemEquippedImage.color = equipmentBorderColor;
        else ItemEquippedImage.color = defaultBorderColor;
    }

    public void SetSelectedItemDescription()
    {
        playerInventory.SelectItemOnGrid(ItemDragHandler.ItemRef);
    }
}
