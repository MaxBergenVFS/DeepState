using System.Collections;
using UnityEngine;
using UnityEngine.Animations;

public class PlayerCameraManager : MonoBehaviour
{
    private const float WATCH_CAM_TARGET_FOV = 5.5f;
    private const float WATCH_CAM_ZOOM_SPEED = 1.5f;
    private const float WATCH_CAM_ZOOM_DELAY = 0.8f;
    private const float WATCH_CAM_LOOK_SPEED = 3f;
    private const float WATCH_CAM_LOOK_DELAY = 0.5f;
    private const float WATCH_CAM_DISABLE_DELAY = 0.7f;

    [SerializeField]
    private Canvas playerUI;
    [SerializeField]
    private Camera mainCam, playerBodyCam, watchCam;
    [SerializeField]
    private HeadBob headBob;
    [SerializeField]
    private AimConstraint watchCamAimConstraint;

    private float defaultWatchCamFoV;
    private PlayerInventory playerInventory;
    private CameraPostProcessing postProcessing;

    private void Awake()
    {
        playerInventory = GetComponent<PlayerInventory>();
    }
    private void Start()
    {
        postProcessing = mainCam.GetComponent<CameraPostProcessing>();
        InitWatchCam();
    }

    private void InitWatchCam()
    {
        watchCamAimConstraint.weight = 0f;
        watchCamAimConstraint.constraintActive = false;
        defaultWatchCamFoV = watchCam.fieldOfView;
        EnableWatchCam(false);
    }

    private void EnableWatchCam(bool active)
    {
        EnableDefaultCams(!active);
        watchCam.enabled = active;
        watchCamAimConstraint.constraintActive = active;
    }
    private void EnableDefaultCams(bool active)
    {
        mainCam.enabled = active;
        playerBodyCam.enabled = active;
    }
    public void EnableDefaultCamsAndUI(bool active)
    {
        EnableDefaultCams(active);
        playerUI.enabled = active;
    }

    private IEnumerator LerpFoV(float target, bool displayInventoryUI)
    {
        float val = watchCam.fieldOfView;
        float t = 0f;
        while (t < 1f)
        {
            watchCam.fieldOfView = Mathf.Lerp(val, target, t);
            t += Time.deltaTime * WATCH_CAM_ZOOM_SPEED;
            yield return null;
        }
        watchCam.fieldOfView = target;
        if (target == WATCH_CAM_TARGET_FOV)
        {
            watchCam.fieldOfView = WATCH_CAM_TARGET_FOV;
            if (displayInventoryUI) playerInventory.PauseGameAndDisplayInventory();
        }
    }
    private IEnumerator LerpWeight(float target)
    {
        float val = watchCamAimConstraint.weight;
        float t = 0f;
        while (t < 1f)
        {
            watchCamAimConstraint.weight = Mathf.Lerp(val, target, t);
            t += Time.deltaTime * WATCH_CAM_LOOK_SPEED;
            yield return null;
        }
        watchCamAimConstraint.weight = target;
    }

    public void LookAtWatch(bool active) => LookAtWatch(active, true, 0f);
    public void LookAtWatch(bool active, float delay) => LookAtWatch(active, true, delay);
    public void LookAtWatch(bool active, bool displayInventoryUI, float delay)
    {
        StopAllCoroutines();
        CancelInvoke();
        if (active)
        {
            EnableWatchCam(true);
            if (displayInventoryUI) Invoke("EnableWatchCamForInventory", delay);
            else Invoke("EnableWatchCamForCodec", delay);
        }
        else
        {
            Invoke("DisableWatchCam", WATCH_CAM_DISABLE_DELAY);
            DecreaseWatchCamWeight();
            DecreaseWatchCamFoV();
        }
    }
    private void EnableWatchCamForInventory()
    {
        Invoke("IncreaseWatchCamWeight", WATCH_CAM_LOOK_DELAY);
        Invoke("IncreaseWatchCamFoVAndDisplayInventoryUI", WATCH_CAM_ZOOM_DELAY);
    }
    private void EnableWatchCamForCodec()
    {
        Invoke("IncreaseWatchCamWeight", WATCH_CAM_LOOK_DELAY);
        Invoke("IncreaseWatchCamFoV", WATCH_CAM_ZOOM_DELAY);
    }

    private void IncreaseWatchCamFoV() => StartCoroutine(LerpFoV(WATCH_CAM_TARGET_FOV, false));
    private void IncreaseWatchCamFoVAndDisplayInventoryUI() => StartCoroutine(LerpFoV(WATCH_CAM_TARGET_FOV, true));
    private void DecreaseWatchCamFoV() => StartCoroutine(LerpFoV(defaultWatchCamFoV, false));

    private void IncreaseWatchCamWeight() => StartCoroutine(LerpWeight(1f));
    private void DecreaseWatchCamWeight() => StartCoroutine(LerpWeight(0f));

    private void DisableWatchCam() => EnableWatchCam(false);

    public void ForceResetCam()
    {
        watchCamAimConstraint.constraintActive = true;
        watchCamAimConstraint.weight = 0f;
        watchCam.fieldOfView = defaultWatchCamFoV;
        DisableWatchCam();
    }

    public void ActivateHeadBob(bool active)
    {
        if (headBob.isActiveAndEnabled != active) headBob.enabled = active;
    }

    public void ResetPostProcessing(FogSettings fogSettings)
    {
        postProcessing.SetDefaultProfile();
        RenderSettings.fogColor = fogSettings.fogColor;
        RenderSettings.fogDensity = fogSettings.fogDensity;
    }
}
