using UnityEngine;

public class ReflectiveFloor : ReflectiveSurface
{
    private const float Y_AXIS_OFFSET = 2.5f;

    private float playerYPosOnEnter;

    protected override void OnPlayerEnter()
    {
        base.OnPlayerEnter();
        playerYPosOnEnter = player.position.y;
    }

    protected override void SetMirroredRigPos()
    {
        float x = player.position.x;
        float y = GetMirroredYPos() - Y_AXIS_OFFSET;
        float z = player.position.z;
        mirroredRigTransform.position = new Vector3(x, y, z);
    }

    private float GetMirroredYPos()
    {
        if (playerYPosOnEnter == player.position.y)
        {
            return playerYPosOnEnter;
        }
        else
        {
            //GET CORRECT REFLECTED JUMP DIRECTION
            float difference = player.position.y - playerYPosOnEnter;
            return playerYPosOnEnter - difference;
        }
    }

    protected override void SetMirroredRigRot()
    {
        Vector3 rot = new Vector3();
        rot = player.rotation.eulerAngles;
        rot.z -= 180f;
        mirroredRigTransform.rotation = Quaternion.Euler(rot);
    }
}
