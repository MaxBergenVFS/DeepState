using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    #region Init
    [SerializeField]
    private InventoryUIManager inventoryUI;
    [SerializeField]
    private ItemBar itemBar;
    [SerializeField]
    private float distanceToDropItemFromInventory = 2.5f;
    [SerializeField]
    private InventoryItemDatabase inventoryItemDatabase;
    [SerializeField]
    private DatapadUI datapadUI;
    [SerializeField]
    private KeypadUI keypadUI;
    [SerializeField]
    private SavedNotesManager savedNotesManager;
    [SerializeField]
    private RawImage equipmentRenderTexture;
    [SerializeField]
    private NightVisionGogglesBattery nvGoggleBattery;
    [SerializeField]
    private SettingsTabUIRef settingsTabUIRef;

    private List<IInventoryItem> items = new List<IInventoryItem>();
    private PlayerHealth playerHealth;
    private InputManager inputManager;
    private Keycard equippedKeycard;
    private CameraController cameraController;
    private PlayerSFX sfx;
    private PlayerController playerController;
    private PlayerHands playerHands;
    private PlayerCameraManager cameraManager;
    private EquipmentViewportCamera equipmentViewportCamera;
    private NotificationUI notification;
    private bool openingInventory;

    public event EventHandler<InventoryEventArgs> OnItemAddedToGrid;
    public event EventHandler<InventoryEventArgs> OnItemRemovedFromGrid;
    public event EventHandler<InventoryEventArgs> OnItemSelectedOnGrid;

    public delegate void OnAmmoInInventoryChanged(Enums.ammoType ammoType, int amt);
    public static event OnAmmoInInventoryChanged onAmmoInInventoryChanged;
    public delegate void OnItemsRefreshedOnBar();
    public static event OnItemsRefreshedOnBar onItemsRefreshedOnBar;
    public delegate void OnMoneyAmountChanged(int amt);
    public static event OnMoneyAmountChanged onMoneyAmountChanged;
    public delegate void OnItemsRefreshedOnGrid();
    public static event OnItemsRefreshedOnGrid onItemsRefreshedOnGrid;
    public delegate void OnAllEquipmentRemoved(Enums.itemType itemTypeToIgnore);
    public static event OnAllEquipmentRemoved onAllEquipmentRemoved;

    public float DistanceToDropItemFromInventory { get { return distanceToDropItemFromInventory; } }
    public InventoryItemDatabase InventoryItemDatabase { get { return inventoryItemDatabase; } }
    public List<IInventoryItem> IInventoryItemsListRef { get { return items; } }
    public DatapadUI DatapadUI { get { return datapadUI; } }
    public KeypadUI KeypadUI { get { return keypadUI; } }
    public SavedNotesManager SavedNotesManager { get { return savedNotesManager; } }
    public SettingsTabUIRef SettingsTabUIRef { get { return settingsTabUIRef; } }
    public NightVisionGogglesBattery NvGoggleBattery { get { return nvGoggleBattery; } }
    public InventoryUIManager InventoryUI { get { return inventoryUI; } }
    public GameObject EquipmentRenderObj { get { return equipmentRenderTexture.gameObject; } }
    #endregion

    private void Awake()
    {
        inputManager = GetComponent<InputManager>();
        playerHealth = GetComponent<PlayerHealth>();
        sfx = GetComponent<PlayerSFX>();
        cameraController = GetComponent<CameraController>();
        cameraManager = GetComponent<PlayerCameraManager>();
        playerController = GetComponent<PlayerController>();
        openingInventory = false;
    }
    private void Start()
    {
        inventoryUI.Init();
        inventoryUI.DisplayInventoryUI(false);
        playerHands = GetComponent<PlayerHeldItem>().PlayerHands;
        equipmentViewportCamera = GameManager.Instance.EquipmentViewportCamera;
        notification = NotificationUI.Instance;
    }

    public void ToggleInventoryUI() => ToggleInventoryUI(false);
    public void ToggleInventoryUIExitTab() => ToggleInventoryUI(true);
    private void ToggleInventoryUI(bool openOnExitTab)
    {
        if (openingInventory) return; //----------------------------------------------------------

        bool active = !inputManager.InventoryIsOpen;
        inputManager.SetInventoryIsOpen(active);
        if (active)
        {
            //OPEN
            float delay;
            openingInventory = true;
            playerHands.TriggerWatchAnimation(out delay);
            cameraManager.LookAtWatch(true, delay);
            sfx.PlayOpenInventorySound();
            inventoryUI.FadeInBackgroundImage();
            if (openOnExitTab) inventoryUI.DisplayExitTab();
            else if (inventoryUI.ExitTabIsDisplayed) inventoryUI.DisplayInventoryTab();
        }
        else
        {
            //CLOSE
            sfx.PlayCloseInventorySound();
            playerHands.ReEquipItem();
            cameraManager.LookAtWatch(false);
            inventoryUI.DisplayInventoryUI(false);
            GameManager.Instance.PauseGame(false, false, playerController.IsDead, false);
        }
    }
    public void PauseGameAndDisplayInventory()
    {
        //this is called by LerpFoV(WATCH_CAM_TARGET_FOV) in PlayerCameraManager
        inventoryUI.DisplayInventoryUI(true);
        GameManager.Instance.PauseGame(true, true, playerController.IsDead, false);
        openingInventory = false;
    }

    public void DisplaySprintUI(bool active)
    {
        settingsTabUIRef?.ToggleSprintCheckmark?.SetActive(active);
    }

    public void ForceCloseInventory()
    {
        inventoryUI.DisplayInventoryUI(false);
        inputManager.SetInventoryIsOpen(false);
        cameraManager.ForceResetCam();
    }

    public void AddAmmoToInventory(Enums.ammoType ammoType, int amt)
    {
        inventoryItemDatabase.AddToInventoryItemAmount(ammoType, amt);
        sfx.PlayPickupAmmoSound();

        int newAmt = inventoryItemDatabase.GetInventoryItemAmount(ammoType);
        if (onAmmoInInventoryChanged != null) onAmmoInInventoryChanged(ammoType, newAmt);
    }
    public void RefreshAmmoInInventoryDisplay(Enums.ammoType ammoType)
    {
        if (ammoType == Enums.ammoType.None) return; //--------------------------------------
        int amt = inventoryItemDatabase.GetInventoryItemAmount(ammoType);
        if (onAmmoInInventoryChanged != null) onAmmoInInventoryChanged(ammoType, amt);
    }

    public void UseHealingItem(Enums.itemType itemType, out bool success)
    {
        HandleHealingItem(itemType, playerHealth.AtMaxHealth, out success);
        if (success) playerHealth.IncreaseHealth(inventoryItemDatabase.GetInventoryItemHealing(itemType));
    }
    public void UseArmorItem(Enums.itemType itemType, out bool success)
    {
        HandleHealingItem(itemType, playerHealth.AtMaxArmor, out success);
        if (success) playerHealth.IncreaseArmor(inventoryItemDatabase.GetInventoryItemHealing(itemType));
    }
    private void HandleHealingItem(Enums.itemType itemType, bool failCondition, out bool success)
    {
        if (inventoryItemDatabase.GetInventoryItemAmount(itemType) < 1)
        {
            success = false;
            return;
        } //-----------------------------------------------------------------

        if (failCondition)
        {
            notification.ItemCannotBeUsed();
            sfx.PlayAlreadyAtFullHealthSound();
            success = false;
        }
        else
        {
            DecrementItemAmount(itemType);
            sfx.PlayConsumeHealingItemSound(itemType);
            success = true;
        }
    }

    public void DecrementItemAmount(Enums.itemType itemType) => DecrementItemAmount(itemType, false);
    public void DecrementItemAmount(Enums.itemType itemType, bool decrementUnique)
    {
        if (!decrementUnique && inventoryItemDatabase.GetInventoryItemIsUnique(itemType)) return; //--------------

        inventoryItemDatabase.AddToInventoryItemAmount(itemType, -1);
        RefreshItemsOnBar();
        RefreshItemsOnGrid();
    }

    public void AddToMoneyAmount(int amt)
    {
        inventoryItemDatabase.AddToMoneyAmount(amt);
        sfx.PlayMoneySound();
        if (onMoneyAmountChanged != null) onMoneyAmountChanged(inventoryItemDatabase.Money);
    }
    public void AttemptToRemoveMoneyAmount(int amt, out bool success)
    {
        if ((inventoryItemDatabase.Money - amt) < 0)
        {
            success = false;
        }
        else
        {
            inventoryItemDatabase.AddToMoneyAmount(-amt);
            onMoneyAmountChanged(inventoryItemDatabase.Money);
            success = true;
        }
    }

    public int GetEquippedKeycardAccessLevel()
    {
        if (equippedKeycard != null) return equippedKeycard.AccessLevel;
        return 0;
    }

    private void RemoveAllEquipment() => RemoveAllEquipment(Enums.itemType.None);
    public void RemoveAllEquipment(Enums.itemType itemTypeToIgnore)
    {
        equippedKeycard = null;
        cameraController.SetNightVision(false);
        nvGoggleBattery.StartBatteryRecharge();
        equipmentRenderTexture.enabled = false;
        equipmentViewportCamera.EnableCamera(false);
        UnequipAllEquipmentFromDatabase(itemTypeToIgnore);
        if (onAllEquipmentRemoved != null) onAllEquipmentRemoved(itemTypeToIgnore);
    }
    public void EquipKeycard(Keycard keycard, bool active)
    {
        if (active)
        {
            equippedKeycard = keycard;
            equipmentRenderTexture.enabled = true;
            equipmentViewportCamera.DisplayModel(keycard.ItemType);
        }
        else
        {
            equippedKeycard = null;
            equipmentRenderTexture.enabled = false;
            equipmentViewportCamera.EnableCamera(false);
        }
    }
    public void EquipNightVision(bool active)
    {
        cameraController.SetNightVision(active);
        equipmentRenderTexture.enabled = active;
        if (active)
        {
            //EQUIP
            equipmentViewportCamera.DisplayModel(Enums.itemType.NightVisionGoggles);
            nvGoggleBattery.StartBatteryDrain();
            sfx.PlayEquipNVGogglesSound();
        }
        else
        {
            //UNEQUIP
            nvGoggleBattery.StartBatteryRecharge();
            equipmentViewportCamera.DisplayModel(Enums.itemType.None);
            sfx.PlayUnequipNVGogglesSound();
            inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.NightVisionGoggles, false);
            RefreshItemsOnBar();
        }
    }

    public void AddItemToGrid(IInventoryItem item) => AddItemToGrid(item, true);
    public void AddItemToGridFromLoadout(IInventoryItem item) => AddItemToGrid(item, false);
    private void AddItemToGrid(IInventoryItem item, bool playSfx)
    {
        if (item.ItemType == Enums.itemType.None || item.ItemType == Enums.itemType.Ammo) return;

        items.Add(item);
        item.OnPickup();
        if (playSfx) sfx.PlayPickupItemSound();
        if (OnItemAddedToGrid != null) OnItemAddedToGrid(this, new InventoryEventArgs(item));
        itemBar.AddItemToAvailableSlotOnBar(item, playSfx);
        RefreshItemsOnBar();
    }
    public void RemoveItemFromGrid(IInventoryItem item)
    {
        if (items.Contains(item))
        {
            item.OnDrop();
            sfx.PlayDropItemSound();
            if (item.AmountInt < 1) items.Remove(item);
            if (OnItemRemovedFromGrid != null) OnItemRemovedFromGrid(this, new InventoryEventArgs(item));
        }
        RefreshItemsOnBar();
    }

    public void SelectItemOnGrid(IInventoryItem item)
    {
        if (OnItemSelectedOnGrid != null) OnItemSelectedOnGrid(this, new InventoryEventArgs(item));
    }
    private void RefreshItemsOnGrid()
    {
        if (onItemsRefreshedOnGrid != null) onItemsRefreshedOnGrid();
    }
    public void RefreshItemsOnBar()
    {
        if (onItemsRefreshedOnBar != null) onItemsRefreshedOnBar();
    }
    public void RefreshItems()
    {
        RefreshItemsOnGrid();
        RefreshItemsOnBar();
    }

    public void UseItemAtSlotIndex(int index)
    {
        itemBar.UseItemAtSlotIndex(index);
        sfx.PlaySelectSlotOnItemBarSound();
    }
    public void IncrementEquippedItemOnBar()
    {
        itemBar.IncrementEquippedItem();
        sfx.PlaySelectSlotOnItemBarSound();
    }
    public void DecrementEquippedItemOnBar()
    {
        itemBar.DecrementEquippedItem();
        sfx.PlaySelectSlotOnItemBarSound();
    }

    public void ResetToLastCheckpoint()
    {
        inventoryItemDatabase.RevertDatabaseAmountsToSnapshot();
        playerHands.EquipSnapshottedItem();
        RefreshItems();
        ForceCloseInventory();
        RemoveAllEquipment();
        RefreshAmmoInInventoryDisplay(inventoryItemDatabase.GetAmmoType(playerHands.EquippedItemType));

        if (onMoneyAmountChanged != null) onMoneyAmountChanged(inventoryItemDatabase.Money);
    }

    private void UnequipAllEquipmentFromDatabase(Enums.itemType itemTypeToIgnore)
    {
        if (itemTypeToIgnore != Enums.itemType.RedKeycard) inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.RedKeycard, false);
        if (itemTypeToIgnore != Enums.itemType.BlueKeycard) inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.BlueKeycard, false);
        if (itemTypeToIgnore != Enums.itemType.NightVisionGoggles) inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.NightVisionGoggles, false);
    }
    private void UnequipAllItemsFromDatabase()
    {
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.Wrench, false);
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.Pistol, false);
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.HackingTool, false);
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.LockPick, false);
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.CheckpointDevice, false);
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.AkRifle, false);
        inventoryItemDatabase.SetInventoryItemIsEquipped(Enums.itemType.M4Rifle, false);
    }

    public void EquipItemInDatabase(Enums.itemType itemType, bool active)
    {
        if (active) UnequipAllItemsFromDatabase();
        inventoryItemDatabase.SetInventoryItemIsEquipped(itemType, active);
    }

    public bool ItemIsCurrentlyEquipped(Enums.itemType itemType)
    {
        bool equippedInPlayerHands = (playerHands.EquippedItemType == itemType);
        bool equippedInInventory = inventoryItemDatabase.GetInventoryItemIsEquipped(itemType);

        if (equippedInPlayerHands != equippedInInventory)
        {
            Debug.LogError("DISCREPANCY BETWEEN EQUIPPED ITEM AND INVENTORY DATABASE");
            return false;
        }
        return (equippedInPlayerHands && equippedInInventory);
    }

}
