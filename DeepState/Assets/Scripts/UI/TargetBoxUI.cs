using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TargetBoxUI : MonoBehaviour
{
    private const float OFFSET_Y = 150f;
    private const float PADDING = 5f;
    private const float SIZE_DELTA_MIN = 100f;

    private Image targetBoxImage;
    private RectTransform targetBoxRect, canvasRect;
    private float targetBoxMaxBoundsEdgeLeft, targetBoxMaxBoundsEdgeRight, targetBoxMaxBoundsEdgeTop, targetBoxMaxBoundsEdgeBottom;

    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private Camera cam;
    [SerializeField]
    private TextMeshProUGUI text;

    private void Awake()
    {
        targetBoxImage = GetComponent<Image>();
        targetBoxRect = GetComponent<RectTransform>();
    }
    private void Start()
    {
        canvasRect = canvas.GetComponent<RectTransform>();
        SetTargetBoxMaxBounds();
    }

    private void SetBounds(Collider col)
    {
        Bounds bounds = col.bounds;
        Vector3[] screenSpaceCorners = new Vector3[8];

        screenSpaceCorners[0] = cam.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z + bounds.extents.z));
        screenSpaceCorners[1] = cam.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z - bounds.extents.z));
        screenSpaceCorners[2] = cam.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z + bounds.extents.z));
        screenSpaceCorners[3] = cam.WorldToScreenPoint(new Vector3(bounds.center.x + bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z - bounds.extents.z));
        screenSpaceCorners[4] = cam.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z + bounds.extents.z));
        screenSpaceCorners[5] = cam.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y + bounds.extents.y, bounds.center.z - bounds.extents.z));
        screenSpaceCorners[6] = cam.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z + bounds.extents.z));
        screenSpaceCorners[7] = cam.WorldToScreenPoint(new Vector3(bounds.center.x - bounds.extents.x, bounds.center.y - bounds.extents.y, bounds.center.z - bounds.extents.z));

        float minX = screenSpaceCorners[0].x;
        float minY = screenSpaceCorners[0].y;
        float maxX = screenSpaceCorners[0].x;
        float maxY = screenSpaceCorners[0].y;

        for (int i = 1; i < 8; i++)
        {
            if (screenSpaceCorners[i].x < minX) minX = screenSpaceCorners[i].x;
            if (screenSpaceCorners[i].y < minY) minY = screenSpaceCorners[i].y;
            if (screenSpaceCorners[i].x > maxX) maxX = screenSpaceCorners[i].x;
            if (screenSpaceCorners[i].y > maxY) maxY = screenSpaceCorners[i].y;
        }

        //set target box bottom left anchor point
        targetBoxRect.position = new Vector2(minX, minY);

        //get scale factor to account for different resolutions
        float scaleFactor = canvas.scaleFactor;
        if (scaleFactor <= 0f) scaleFactor = 1f;

        float sizeDeltaX = (maxX - minX) / scaleFactor;
        float sizeDeltaY = (maxY - minY) / scaleFactor;

        //force a minimum size if necessary
        if (sizeDeltaX < SIZE_DELTA_MIN) sizeDeltaX = SIZE_DELTA_MIN;
        if (sizeDeltaY < SIZE_DELTA_MIN) sizeDeltaY = SIZE_DELTA_MIN;

        //set target box size
        targetBoxRect.sizeDelta = new Vector2(sizeDeltaX, sizeDeltaY);

        //force target box to stay within bounds
        KeepRectWithinBounds(targetBoxRect);
    }

    private void KeepRectWithinBounds(RectTransform rect)
    {
        float minX = rect.offsetMin.x;
        float minY = rect.offsetMin.y;
        float maxX = rect.offsetMax.x;
        float maxY = rect.offsetMax.y;

        if (minX < targetBoxMaxBoundsEdgeLeft) minX = targetBoxMaxBoundsEdgeLeft;
        if (maxX > targetBoxMaxBoundsEdgeRight) maxX = targetBoxMaxBoundsEdgeRight;
        if (minY < targetBoxMaxBoundsEdgeBottom) minY = targetBoxMaxBoundsEdgeBottom;
        if (maxY > targetBoxMaxBoundsEdgeTop) maxY = targetBoxMaxBoundsEdgeTop;

        rect.offsetMin = new Vector2(minX, minY);
        rect.offsetMax = new Vector2(maxX, maxY);
    }

    public void DisableSprite() => DisplaySprite(false, "");
    private void DisplaySprite(bool active, string targetName)
    {
        if (!active && !targetBoxImage.isActiveAndEnabled) return; //----------

        if (active && text.text != targetName) text.text = targetName;
        else if (!active) text.text = "";

        if (active != targetBoxImage.isActiveAndEnabled) targetBoxImage.enabled = active;
    }
    public void DisplaySprite(bool active, Collider col, string targetName)
    {
        DisplaySprite(active, targetName);
        if (active) SetBounds(col);
    }

    private void SetTargetBoxMaxBounds()
    {
        targetBoxMaxBoundsEdgeLeft = PADDING;
        targetBoxMaxBoundsEdgeRight = canvasRect.sizeDelta.x - PADDING;
        targetBoxMaxBoundsEdgeTop = canvasRect.sizeDelta.y - PADDING;
        targetBoxMaxBoundsEdgeBottom = OFFSET_Y;
    }

}
