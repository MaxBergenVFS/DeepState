﻿using System.Collections;
using TMPro;
using UnityEngine;

public class NotificationUI : MonoBehaviour
{
    //notification strings
    private const string NOTIF_PLAYER_CANNOT_USE_ITEM = "I can't use that right now";
    private const string NOTIF_NOTE_ADDED = "Note added";

    private const float DIALOGUE_BOX_WIDTH_CLOSED = 0f;

    public static NotificationUI Instance { get { return instance; } }
    private static NotificationUI instance;

    [SerializeField]
    private float notificationDuration = 3f, fadeOutSpeed = 1f;
    [SerializeField]
    private TextMeshProUGUI notification, dialogueName, dialogueNotification;
    [SerializeField]
    private RectTransform dialogueBox;
    [SerializeField]
    private float dialogueBoxWidthOpen, dialogueBoxLerpSpeed;

    private float colorR, colorG, colorB, alphaValue = 1f, dialogueBoxHeight;
    private bool currentlyDisplayingDialogue;
    private GameObject dialogueBoxObj;

    public delegate void OnDialogueNotification();
    public static event OnDialogueNotification onDialogueNotification;

    private void Awake()
    {
        instance = this;
        colorR = notification.faceColor.r;
        colorG = notification.faceColor.g;
        colorB = notification.faceColor.b;
        dialogueBoxObj = dialogueBox.gameObject;
        dialogueBoxHeight = dialogueBox.sizeDelta.y;
        ResetTextDisplayValues();
    }

    public void ItemCannotBeUsed() => DisplayText(NOTIF_PLAYER_CANNOT_USE_ITEM);
    public void NoteAdded() => DisplayText(NOTIF_NOTE_ADDED);
    public void DisplayText(string str)
    {
        if (currentlyDisplayingDialogue) return; //----------------

        StopAllCoroutines();
        ResetTextDisplayValues();
        notification.text = str;
        StartCoroutine(FadeOut(notification));
    }
    public void DisplayText(Dialogue dialogue)
    {
        StopAllCoroutines();
        ResetTextDisplayValues();
        StartDialogueNotification();
        StartCoroutine(DisplayDialogue(dialogue.characterName, dialogue.sentences));
    }

    private void ResetTextDisplayValues() => ResetTextDisplayValues(1f);
    private void ResetTextDisplayValues(float alphaOverride)
    {
        notification.faceColor = new Color(colorR, colorG, colorB, alphaOverride);
        dialogueNotification.faceColor = new Color(colorR, colorG, colorB, alphaOverride);
        alphaValue = 1f;
        SetDialogueBoxWidth(DIALOGUE_BOX_WIDTH_CLOSED);
        dialogueBoxObj.SetActive(false);
        currentlyDisplayingDialogue = false;
        ClearAllText();
    }

    private void ClearAllText()
    {
        notification.text = "";
        dialogueName.text = "";
        dialogueNotification.text = "";
    }

    private void StartDialogueNotification()
    {
        StartCoroutine(LerpRectTransformWidth(dialogueBoxWidthOpen));
        currentlyDisplayingDialogue = true;
        dialogueBoxObj.SetActive(true);
        if (onDialogueNotification != null) onDialogueNotification();
    }
    private void EndDialogueNotification()
    {
        currentlyDisplayingDialogue = false;
        ClearAllText();
        StartCoroutine(LerpRectTransformWidth(DIALOGUE_BOX_WIDTH_CLOSED));
        if (onDialogueNotification != null) onDialogueNotification();
    }

    private void SetDialogueBoxWidth(float width)
    {
        dialogueBox.sizeDelta = new Vector2(width, dialogueBoxHeight);
    }
    
    private IEnumerator LerpRectTransformWidth(float targetWidth)
    {
        float t = 0f;
        float start = dialogueBox.sizeDelta.x;
        float width = start;
        while (t < 1f)
        {
            width = Mathf.Lerp(start, targetWidth, t);
            SetDialogueBoxWidth(width);
            t += Time.deltaTime * dialogueBoxLerpSpeed;
            yield return null;
        }
        SetDialogueBoxWidth(targetWidth);
    }
    private IEnumerator DisplayDialogue(string name, string[] sentences)
    {
        dialogueName.text = name;
        for (int i = 0; i < sentences.Length; i++)
        {
            dialogueNotification.text = sentences[i];
            yield return new WaitForSeconds(notificationDuration);
        }
        EndDialogueNotification();
    }
    private IEnumerator FadeOut(TextMeshProUGUI text)
    {
        yield return new WaitForSeconds(notificationDuration);
        float t = 0;
        while (t < 1f)
        {
            alphaValue = Mathf.Lerp(1f, 0f, t);
            t += Time.deltaTime * fadeOutSpeed;
            text.faceColor = new Color(colorR, colorG, colorB, alphaValue);
            yield return null;
        }
        ResetTextDisplayValues(0f);
    }
}
