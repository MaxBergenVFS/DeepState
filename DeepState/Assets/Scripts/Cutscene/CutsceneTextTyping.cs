using System.Collections;
using TMPro;
using UnityEngine;

public class CutsceneTextTyping : MonoBehaviour
{
    private const string DIALOGUE_TYPING_PATH = "";

    [SerializeField]
    private float timeBetweenLetters = 0.1f;
    [SerializeField]
    private string sentence;
    
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence, text));
    }

    IEnumerator TypeSentence(string sentence, TextMeshProUGUI textElement)
    {
        textElement.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            textElement.text += letter;
            FMODUnity.RuntimeManager.PlayOneShot(DIALOGUE_TYPING_PATH);
            yield return new WaitForSeconds(timeBetweenLetters);
        }
    }
}
