using UnityEngine;

public class CodecInput : InputOverride
{
    [SerializeField]
    private Codec codec;

    private void Update()
    {
        if (InputManagerIsEnabled) return; //------------------------------

        if (Input.GetButtonDown(INPUT_JUMP) || Input.GetButtonDown(INPUT_INTERACT)) codec.NextDialogue();
    }
}
