using UnityEngine;

public class Keypad : MonoBehaviour, IInteractable
{
    private const string NAME = "Keypad";

    [SerializeField]
    private Door door;
    [SerializeField]
    private int[] fourDigitCode = new int[4];
    [SerializeField]
    private Material unlocked;

    private Material locked;
    private KeypadUI keypadUI;
    private bool validCodeHasBeenEntered, validCodeHasBeenEnteredSnapshot;
    private Renderer rend;
    private Collider col;

    public string Name { get { return NAME; } }
    public int[] FourDigitCode { get { return fourDigitCode; } }

    private void Awake()
    {
        rend = GetComponent<Renderer>();
        col = GetComponent<Collider>();
        locked = rend.material;
        validCodeHasBeenEntered = validCodeHasBeenEnteredSnapshot = false;
    }
    private void Start()
    {
        keypadUI = PlayerController.Instance.GetComponent<PlayerInventory>().KeypadUI;
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    public void Interact()
    {
        if (validCodeHasBeenEntered)
        {
            Open();
        }
        else
        {
            keypadUI.SetKeypadRef(this);
            keypadUI.Show();
        }
    }

    public void Open()
    {
        col.enabled = false;
        door.UnlockAndOpen();
        Invoke("ActivateCol", door.InactiveTime);
    }

    private void ActivateCol()
    {
        col.enabled = true;
    }

    public void ValidCodeHasBeenEntered()
    {
        validCodeHasBeenEntered = true;
        rend.material = unlocked;
    }

    private void SnapshotState()
    {
        validCodeHasBeenEnteredSnapshot = validCodeHasBeenEntered;
    }
    private void RevertState()
    {
        //do nothing if snapshot is the same
        if (validCodeHasBeenEntered == validCodeHasBeenEnteredSnapshot) return;

        ActivateCol();
        if (validCodeHasBeenEnteredSnapshot)
        {
            validCodeHasBeenEntered = true;
            rend.material = unlocked;
        }
        else
        {
            //was unlocked after last checkpoint was set
            validCodeHasBeenEntered = false;
            rend.material = locked;
            door.RevertState();
        }
    }
}
