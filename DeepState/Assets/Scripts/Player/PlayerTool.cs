using UnityEngine;

[RequireComponent(typeof(ToolSFX))]
public class PlayerTool : MonoBehaviour, IEquippable
{
    [SerializeField]
    private GameObject itemModel;
    [SerializeField]
    private LayerMask whatIsInteractable;
    [SerializeField]
    protected Enums.itemType itemType;
    [SerializeField]
    protected Transform origin;
    [SerializeField]
    private float toolRadius = 1.5f;

    protected PlayerInventory playerInventory;
    protected ToolSFX sfx;
    protected InventoryItemDatabase inventoryItemDatabase;
    protected PlayerController playerController;
    protected PlayerHands playerHands;

    public bool ModelIsVisible { get { return itemModel.activeSelf; } }
    public Enums.itemType ItemType { get { return itemType; } }

    private void Awake()
    {
        sfx = GetComponent<ToolSFX>();
    }
    protected virtual void Start()
    {
        playerController = PlayerController.Instance;
        playerHands = playerController.GetComponent<PlayerHeldItem>().PlayerHands;
        playerInventory = playerController.GetComponent<PlayerInventory>();
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
        //PlayerHands.onItemEquipped += HideModel;
        PlayerHands.onMeleeEquipped += HideModelOnMeleeEquipped;
    }

    private void OnDestroy()
    {
        //PlayerHands.onItemEquipped -= HideModel;
        PlayerHands.onMeleeEquipped -= HideModelOnMeleeEquipped;
    }

    private Collider[] GetColsFromOverlapSphere() => GetColsFromOverlapSphere(toolRadius);
    protected Collider[] GetColsFromOverlapSphere(float radius)
    {
        return Physics.OverlapSphere(origin.position, radius, whatIsInteractable);
    }

    protected void AttemptBypassLock()
    {
        bool bypassLockSuccessful = false;
        bool playFeedbackSound = false;

        Collider[] cols = GetColsFromOverlapSphere();
        foreach (var col in cols)
        {
            if (col.GetComponent<Door>() != null)
            {
                playFeedbackSound = true;
                col.GetComponent<Door>().BypassLock(playerHands.EquippedItemType, out bypassLockSuccessful);
                if (bypassLockSuccessful)
                {
                    LockSuccessfullyBypassed();
                    return;
                }
            }
        }
        if (playFeedbackSound && !bypassLockSuccessful)
        {
            sfx.PlayNegativeFeedbackSound();
        }
    }

    private void LockSuccessfullyBypassed()
    {
        sfx.PlayPositiveFeedbackSound();
        playerHands.DecrementEquippedItemAmount();
    }
    // private void HideModel(Enums.itemType equippedType)
    // {
    //     if (equippedType == itemType || playerHands.CurrentlyPlayingHolsteringAnimation()) return; //---------
    //     ShowModel(false);
    // }
    private void HideModelOnMeleeEquipped(bool active)
    {
        if (active) ShowModel(false);
    }

    #region IEquippable API
    public void ShowModel(bool active)
    {
        itemModel.SetActive(active);
    }

    public virtual void Equip()
    {
        playerInventory.EquipItemInDatabase(itemType, true);
    }

    public virtual void Unequip()
    {
        playerInventory.EquipItemInDatabase(itemType, false);
    }

    public virtual void UsePrimary()
    {
        if (playerHands.CurrentlyHolsteringItem) return; //-------------
        AttemptBypassLock();
        sfx.PlayActionSound();
    }
    public virtual void UsePrimaryHeld() { }

    public virtual void UseSecondary()
    {
        if (!playerHands.EquippedItemHasSecondary() || playerHands.CurrentlyUsingItem) return; //------------------------
        if (itemType == Enums.itemType.Wrench) sfx.PlayWrenchSwingSound();
    }
    public virtual void UseSecondaryHeld() { }

    public void ReloadEquipped(out bool success)
    {
        success = false;
    }
    #endregion  
}
