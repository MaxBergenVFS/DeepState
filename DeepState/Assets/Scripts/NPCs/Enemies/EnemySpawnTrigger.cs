using UnityEngine;

public class EnemySpawnTrigger : MonoBehaviour
{
    [SerializeField]
    private EnemySpawnManager enemySpawnManager;

    private bool hasBeenTriggered;
    private Collider col;

    private void Awake()
    {
        hasBeenTriggered = false;
        col = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (hasBeenTriggered) return;

        if (other.CompareTag("Player"))
        {
            enemySpawnManager.ActivateSpawners();
            hasBeenTriggered = true;
            col.enabled = false;
        }
    }
}
