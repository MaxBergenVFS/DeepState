public class BodyArmor : BaseItem
{
    public override void UseFromInventory(out bool success)
    {
        playerInventory.UseArmorItem(itemType, out success);
    }
}
