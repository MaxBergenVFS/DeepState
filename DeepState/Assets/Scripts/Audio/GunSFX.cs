using UnityEngine;

public class GunSFX : SFX
{
    private bool playerWeapon;
    private string reloadPath, shootPath;

    private void Awake()
    {
        playerWeapon = false;
    }

    public void SetPlayerWeapon(Enums.gunType type)
    {
        playerWeapon = true;
        SetGunType(type);
    }
    public void SetGunType(Enums.gunType type)
    {
        InitShootPath(type);
        InitReloadPath(type);
    }

    private void InitReloadPath(Enums.gunType type)
    {
        switch (type)
        {
            case (Enums.gunType.Pistol):
                reloadPath = PISTOL_RELOAD_PATH;
                break;
            case (Enums.gunType.AK):
                reloadPath = AK_RELOAD_PATH;
                break;
            case (Enums.gunType.M4):
                reloadPath = M4_RELOAD_PATH;
                break;
            case (Enums.gunType.None):
            default:
                break;
        }
    }
    private void InitShootPath(Enums.gunType type)
    {
        switch (type)
        {
            case (Enums.gunType.Pistol):
                shootPath = PISTOL_SHOOT_PATH;
                break;
            case (Enums.gunType.AK):
                shootPath = AK_SHOOT_PATH;
                break;
            case (Enums.gunType.M4):
                shootPath = M4_SHOOT_PATH;
                break;
            case (Enums.gunType.None):
            default:
                break;
        }
    }

    public void PlayHitMarkerSFX() => PlaySound2D(HIT_MARKER_PATH);
    public void PlayImpactSFX(Vector3 pos) => PlaySound3D(BULLET_IMPACT_PATH, pos);
    public void PlayOutOfAmmoSFX() => PlayGunSFX(OUT_OF_AMMO_PATH);
    public void PlayReloadSFX() => PlayGunSFX(reloadPath);
    public void PlayShootSFX(bool silenced)
    {
        if (silenced) PlayGunSFX(SHOOT_SILENCED_PATH);
        else PlayGunSFX(shootPath);
    }

    private void PlayGunSFX(string path)
    {
        if (playerWeapon) PlaySound2D(PLAYER_PREFIX + path);
        else PlaySound3D(path);
    }
}