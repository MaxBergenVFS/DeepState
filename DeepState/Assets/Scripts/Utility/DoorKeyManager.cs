using UnityEngine;

public class DoorKeyManager : MonoBehaviour
{
    [SerializeField]
    private int redAccessLevel, blueAccessLevel;

    public int GetAccessLevelFromItemType(Enums.itemType itemType)
    {
        switch (itemType)
        {
            case (Enums.itemType.RedKeycard):
                return redAccessLevel;
            case (Enums.itemType.BlueKeycard):
                return blueAccessLevel;
            default:
                return 0;
        }
    }
}
