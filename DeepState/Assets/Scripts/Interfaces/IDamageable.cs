﻿using UnityEngine;
public interface IDamageable
{
    void TakeDamage(int amt, Vector3 pos, float force, Enums.damageType type);
}
