﻿using UnityEngine;

[RequireComponent(typeof(NoteToSave))]
public class NPCDialogue : DialogueTrigger, IInteractable
{
    private const float HIDE_ITEM_TO_GIVE_TO_PLAYER_DELAY = 0.5f;

    [SerializeField]
    private GameObject cancelDialogueTrigger;
    [SerializeField]
    private NPCFaceManager faceManager;
    [SerializeField]
    private NPCMultiAimConstraintController multiAimConstraintController;
    [SerializeField]
    private Animator NPCAnimator;
    [SerializeField]
    private NPCController controller;
    [SerializeField]
    private GameObject[] objsToEnableAfterDialogue;
    [SerializeField]
    private BaseItem itemToGiveToPlayer;
    [SerializeField]
    private int saveNoteDialogueIndex, itemToGiveToPlayerDialogueIndex;

    private bool isFriendly;
    private Collider col;
    private NoteToSave noteToSave;
    private PlayerInventory playerInventory;
    private CheckpointSettings snapshot = new CheckpointSettings();

    public string Name { get { return characterName; } }
    public bool IsFriendly { get { return controller.IsFriendly; } }
    public NPCFaceManager FaceManager { get { return faceManager; } }

    protected override void Awake()
    {
        base.Awake();
        noteToSave = GetComponent<NoteToSave>();
        col = GetComponent<Collider>();
        col.enabled = false;
        SnapshotState();
    }
    protected override void Start()
    {
        base.Start();
        if (itemToGiveToPlayer != null)
        {
            playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
            Invoke("HideItemToGiveToPlayer", HIDE_ITEM_TO_GIVE_TO_PLAYER_DELAY);
        }
        SnapshotState();
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    private void SnapshotState()
    {
        snapshot.currentIndex = currentIndex;
        snapshot.dialogueFinished = dialogueFinished;
        snapshot.isSaved = noteToSave.IsSaved;
    }
    private void RevertState()
    {
        currentIndex = snapshot.currentIndex;
        dialogueFinished = snapshot.dialogueFinished;
        noteToSave.SetIsSaved(snapshot.isSaved);
    }

    public void Interact()
    {
        TriggerDialogue();
        multiAimConstraintController.ActivateHeadAim(true);
        controller?.RotateToFacePlayer(dialogueManager.CurrentlyInDialogue);
        if (!NPCAnimator.GetBool(AnimParams.NPC_BOOL_AT_DEST)) NPCAnimator.SetBool(AnimParams.NPC_BOOL_STOPPED_MID_PAT, true);
        NPCAnimator.SetBool(AnimParams.NPC_BOOL_IN_DIALOGUE, true);
        faceManager.StartUpdateFaceOnTimer();
        if (!cancelDialogueTrigger.activeSelf) cancelDialogueTrigger.SetActive(true);
    }

    public void EnableDialogue()
    {
        if (col == null) return; //-----------------------------------
        col.enabled = true;
    }

    public void CancelDialogue()
    {
        if (dialogueManager != null) dialogueManager.EndDialogue();
        controller?.RotateToFacePlayer(false);
        faceManager?.ResetFace(true);
        NPCAnimator?.SetBool(AnimParams.NPC_BOOL_IN_DIALOGUE, false);
        multiAimConstraintController?.ActivateHeadAim(false);
        if (col != null) col.enabled = false;
    }

    protected override void DoAfterCurrentDialogue()
    {
        //ADD SAVED NOTE
        if (saveNoteDialogueIndex == currentIndex) noteToSave.Save(characterName);
        //ADD ITEM TO PLAYER INVENTORY
        if (itemToGiveToPlayer != null && itemToGiveToPlayerDialogueIndex == currentIndex)
        {
            itemToGiveToPlayer.transform.position = this.transform.position;
            itemToGiveToPlayer.gameObject.SetActive(true);
            itemToGiveToPlayer.Interact();
            playerInventory.AddItemToGrid(itemToGiveToPlayer);
        }
    }
    protected override void DoAfterFinalDialogue()
    {
        foreach (GameObject obj in objsToEnableAfterDialogue)
        {
            obj.SetActive(true);
        }
    }

    private void HideItemToGiveToPlayer()
    {
        itemToGiveToPlayer?.gameObject.SetActive(false);
    }

}
