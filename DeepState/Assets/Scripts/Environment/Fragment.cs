using UnityEngine;

public class Fragment : MonoBehaviour
{
    protected Rigidbody rb;
    protected Collider col;

    private Vector3 startingPos = new Vector3();
    private Quaternion startingRot = new Quaternion();

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        startingPos = transform.position;
        startingRot = transform.rotation;
    }

    public virtual void Reset()
    {
        transform.position = startingPos;
        transform.rotation = startingRot;
    }
}
