using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject enemySoldier;
    [SerializeField]
    private int numOfEnemiesToSpawn = 1;
    [SerializeField]
    private float secBetweenEnemySpawns = 2f;

    public void SpawnEnemies()
    {
        StopAllCoroutines();
        StartCoroutine(SpawnEnemy());
    }

    private IEnumerator SpawnEnemy()
    {
        for (int i = 0; i < numOfEnemiesToSpawn; i++)
        {
            yield return new WaitForSeconds(secBetweenEnemySpawns);
            Instantiate(enemySoldier, this.transform.position, Quaternion.identity);
        }
    }
}
