using UnityEngine;

public class PlayerEquippedItemDiscrepancyHandler : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<PlayerHands>().HandleEquippedItemAndShownModelDiscrepancy();
    }
}
