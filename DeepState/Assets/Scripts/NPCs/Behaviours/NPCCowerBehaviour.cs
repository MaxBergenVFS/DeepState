using UnityEngine;

public class NPCCowerBehaviour : StateMachineBehaviour
{
    private NPCController controller;
    private float cowerTimeRemaining;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponent<NPCController>();
        cowerTimeRemaining = controller.CowerTime;
        animator.SetFloat(AnimParams.NPC_FLOAT_COWER_TIME, cowerTimeRemaining);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        cowerTimeRemaining -= Time.deltaTime;
        animator.SetFloat(AnimParams.NPC_FLOAT_COWER_TIME, cowerTimeRemaining);
    }

    // override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {

    // }
}
