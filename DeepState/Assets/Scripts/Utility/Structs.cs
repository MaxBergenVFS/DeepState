using UnityEngine;

public class InventoryItem
{
    public int id, amount, healing;
    public string title, shortenedTitle, description, criticalInfo;
    public bool isUnique, isUsable, isConsumable, isEquippable, isEquipped;
    public Enums.itemType itemType;

    public InventoryItem(int id, Enums.itemType itemType, string title, string shortenedTitle, string description, string criticalInfo,
        int healing, bool isUnique, int amount, bool isConsumable, bool isEquippable, bool isEquipped)
    {
        this.id = id;
        this.itemType = itemType;
        this.title = title;
        this.description = description;
        this.criticalInfo = criticalInfo;
        this.healing = healing;
        this.isUnique = isUnique;
        this.amount = amount;
        this.isConsumable = isConsumable;
        this.isEquippable = isEquippable;
        this.isEquipped = isEquipped;
        this.isUsable = (isConsumable || isEquippable);
        if (shortenedTitle == "") this.shortenedTitle = title;
        else this.shortenedTitle = shortenedTitle;
    }
}

public struct FogSettings
{
    public FogMode fogMode;
    public Color fogColor;
    public float fogDensity, fogStartDistance, fogEndDistance;

    public FogSettings(FogMode fogMode, Color fogColor, float fogDensity, float fogStartDistance, float fogEndDistance)
    {
        this.fogMode = fogMode;
        this.fogColor = fogColor;
        this.fogDensity = fogDensity;
        this.fogStartDistance = fogStartDistance;
        this.fogEndDistance = fogEndDistance;
    }
}

public struct CheckpointSettings
{
    public bool isLocked, isOpen, isDestroyed, dialogueFinished, isSaved, isDeactivated, isHacked;
    public int health, currentIndex;

    public CheckpointSettings
    (bool isLocked, bool isOpen, bool isDestroyed, bool dialogueFinished, bool isSaved, bool isDeactivated, bool isHacked,
        int health, int currentIndex)
    {
        this.isLocked = isLocked;
        this.isOpen = isOpen;
        this.isDestroyed = isDestroyed;
        this.dialogueFinished = dialogueFinished;
        this.isSaved = isSaved;
        this.isDeactivated = isDeactivated;
        this.isHacked = isHacked;
        this.health = health;
        this.currentIndex = currentIndex;
    }
}

public struct NPCSettings
{
    public Vector3 pos;
    public Quaternion rot;

    public NPCSettings(Vector3 pos, Quaternion rot)
    {
        this.pos = pos;
        this.rot = rot;
    }
}

public struct UserSettings
{
    public bool isCalibrated;
    public float val, slider;

    public UserSettings(bool isCalibrated, float val, float slider)
    {
        this.isCalibrated = isCalibrated;
        this.val = val;
        this.slider = slider;
    }
}
