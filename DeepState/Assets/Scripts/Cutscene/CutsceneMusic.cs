using UnityEngine;

public class CutsceneMusic : MonoBehaviour
{
    private MusicManager musicManager;
    private string path;
    private enum cutsceneTrack
    {
        None,
        Monster,
        Bashir
    }

    [SerializeField]
    private cutsceneTrack cutsceneTrackToPlay;

    private void Init()
    {
        musicManager = GameManager.Instance.MusicManager;
        path = MusicParams.MUSIC_PREFIX;
        switch (cutsceneTrackToPlay)
        {
            case (cutsceneTrack.Monster):
                path += MusicParams.PATH_CUTSCENE_MONSTER;
                break;
            case (cutsceneTrack.Bashir):
                path += MusicParams.PATH_CUTSCENE_BASHIR;
                break;
            case (cutsceneTrack.None):
            default:
                break;
        }
    }

    private void OnEnable()
    {
        Init();
        musicManager.InitCutsceneMusic(path);
        musicManager.StopGameplayMusic();
        musicManager.PlayCutsceneMusic();
    }

    public void Stop() => musicManager.StopCutsceneMusic();
}
