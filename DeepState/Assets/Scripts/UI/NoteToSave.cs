using UnityEngine;

public class NoteToSave : MonoBehaviour
{
    [SerializeField]
    [TextArea(3, 10)]
    private string content;

    private bool isSaved;
    private SavedNotesManager savedNotesManager;

    public bool IsSaved { get { return isSaved; } }

    private void Awake()
    {
        SetIsSaved(false);
    }
    private void Start()
    {
        savedNotesManager = PlayerController.Instance.GetComponent<PlayerInventory>().SavedNotesManager;
    }

    public void SetIsSaved(bool active)
    {
        isSaved = active;
    }

    public void Save(string title)
    {
        if (content == "" || isSaved) return; //-------------------------------------------
        savedNotesManager.AddToSavedNotes(title, content);
        SetIsSaved(true);
    }
}
