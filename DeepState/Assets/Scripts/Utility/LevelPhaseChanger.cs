using UnityEngine;

public class LevelPhaseChanger : MonoBehaviour, IInteractable
{
    [SerializeField]
    private GameObject[] objsToEnable, objsToDisable;
    [SerializeField]
    private int phaseToChangeTo;
    [SerializeField]
    private alertStatus alertStatusToChangeTo;
    [SerializeField]
    private string interactName;
    [SerializeField]
    private bool changePhaseOnEnable;

    public string Name { get { return interactName; } }

    public delegate void OnPhaseChange(int phase);
    public static event OnPhaseChange onPhaseChange;

    private enum alertStatus
    {
        None,
        FullAlert,
        RemoveAllAlert
    }

    private AlertZoneManager alertZoneManager;
    private MusicManager musicManager;

    private void Start()
    {
        alertZoneManager = GameManager.Instance.AlertZoneManager;
        musicManager = GameManager.Instance.MusicManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) Interact();
    }

    public void Interact()
    {
        SetObjStates();
        SetAlertState();
        musicManager.SetPhase(phaseToChangeTo);
        this.gameObject.SetActive(false);
        if (onPhaseChange != null) onPhaseChange(phaseToChangeTo);
    }

    private void SetObjStates()
    {
        foreach (GameObject obj in objsToEnable)
        {
            obj.SetActive(true);
        }
        foreach (GameObject obj in objsToDisable)
        {
            obj.SetActive(false);
        }
    }
    private void SetAlertState()
    {
        switch (alertStatusToChangeTo)
        {
            case (alertStatus.FullAlert):
                alertZoneManager.SetLevelOnHighAlert();
                break;
            case (alertStatus.RemoveAllAlert):
                alertZoneManager.SetAlertZoneStatus(false);
                break;
            case (alertStatus.None):
            default:
                break;
        }
    }

    private void OnEnable()
    {
        if (changePhaseOnEnable) Interact();
    }

}
