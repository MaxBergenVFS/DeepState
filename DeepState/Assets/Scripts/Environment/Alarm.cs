using UnityEngine;

public class Alarm : MonoBehaviour, IInteractable
{
    private const string NAME = "Alarm";

    [SerializeField]
    private EnemySpawnManager enemySpawnManager;
    [SerializeField]
    private int alertZone = 0;
    [SerializeField]
    private BlastDoor[] blastDoors;
    [SerializeField]
    private PatPoint patPoint;

    private AlertZoneManager alertZoneManager;
    private NPCController assignedNPC;
    private bool activated;

    public int AlertZone { get { return alertZone; } }
    public PatPoint PatPoint { get { return patPoint; } }
    public bool AssignedToNPC { get { return (assignedNPC != null); } }
    public bool AlreadyActivated { get { return activated; } }
    public string Name { get { return NAME; } }

    private void Awake()
    {
        //assignedNPC prevents multiple NPCs from trying to trigger the same alarm
        assignedNPC = null;
        activated = false;
    }
    private void Start()
    {
        AlertZoneManager.onDropCombat += StopAlarm;
        alertZoneManager = GameManager.Instance.AlertZoneManager;
    }

    private void OnDestroy()
    {
        AlertZoneManager.onDropCombat -= StopAlarm;
    }

    public void Interact()
    {
        StartAlarm();
        if (enemySpawnManager != null)
        {
            enemySpawnManager.ActivateSpawners();
        }
        else
        {
            Debug.LogWarning($"Missing: EnemySpawnManager ref on {this.gameObject.name}");
        }
    }

    public void AssignNPC(NPCController npc)
    {
        if (assignedNPC != null) return;
        assignedNPC = npc;
    }
    public void TryUnassignNPC(NPCController npc)
    {
        if (assignedNPC == npc) assignedNPC = null;
    }
    private void UnassignNPC()
    {
        assignedNPC = null;
    }

    private void StartAlarm()
    {
        activated = true;
        alertZoneManager.SetAlertZoneStatus(alertZone, true, true);
        CloseBlastDoors();
    }
    private void StopAlarm()
    {
        activated = false;
        OpenBlastDoors();
        UnassignNPC();
    }

    private void CloseBlastDoors()
    {
        if (blastDoors.Length < 1) return;

        foreach (BlastDoor door in blastDoors)
        {
            door.Close();
        }
    }
    private void OpenBlastDoors()
    {
        if (blastDoors.Length < 1) return;

        foreach (BlastDoor door in blastDoors)
        {
            door.Open();
        }
    }
}
