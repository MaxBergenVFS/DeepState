using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private const string CHECKPOINT_NOTIF_TEXT = "Checkpoint";

    private Vector3 playerCheckpointPos = new Vector3();
    private CheckpointManager checkpointManager;
    private InventoryItemDatabase inventoryItemDatabase;
    private PlayerHands playerHands;
    private NotificationUI notificationUI;
    private bool hasBeenTriggered;

    public Vector3 PlayerCheckpointPos { get { return playerCheckpointPos; } }

    private void Awake()
    {
        hasBeenTriggered = false;
    }
    private void Start()
    {
        Init();
    }

    private void Init()
    {
        checkpointManager = GameManager.Instance.CheckpointManager;
        inventoryItemDatabase = PlayerController.Instance.GetComponent<PlayerInventory>().InventoryItemDatabase;
        playerHands = PlayerController.Instance.GetComponent<PlayerHeldItem>().PlayerHands;
        notificationUI = NotificationUI.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !hasBeenTriggered) ActivateThisCheckpoint();
    }

    public void ActivateThisCheckpoint()
    {
        if (hasBeenTriggered) return;
        checkpointManager.SetCurrentCheckpoint(this);
        playerCheckpointPos = PlayerController.Instance.transform.position;
        inventoryItemDatabase.SnapshotItemAmounts();
        playerHands.SnapshotEquippedItemType();
        if (notificationUI.isActiveAndEnabled) notificationUI.DisplayText(CHECKPOINT_NOTIF_TEXT);
        hasBeenTriggered = true;
    }

    public void InitAndActivateThisCheckpoint()
    {
        Init();
        ActivateThisCheckpoint();
    }
}
