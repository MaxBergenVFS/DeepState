using UnityEngine;

public class KeypadInput : InputOverride
{
    [SerializeField]
    private KeypadUI keypadUI;

    private void Update()
    {
        if (InputManagerIsEnabled) return; //---------------------------------

        if (Input.GetButtonDown(INPUT_CANCEL))
            keypadUI.Hide();
        if (Input.GetButtonDown(INPUT_SLOT_1))
            keypadUI.Enter1();
        if (Input.GetButtonDown(INPUT_SLOT_2))
            keypadUI.Enter2();
        if (Input.GetButtonDown(INPUT_SLOT_3))
            keypadUI.Enter3();
        if (Input.GetButtonDown(INPUT_SLOT_4))
            keypadUI.Enter4();
        if (Input.GetButtonDown(INPUT_SLOT_5))
            keypadUI.Enter5();
        if (Input.GetButtonDown(INPUT_SLOT_6))
            keypadUI.Enter6();
        if (Input.GetButtonDown(INPUT_SLOT_7))
            keypadUI.Enter7();
        if (Input.GetButtonDown(INPUT_SLOT_8))
            keypadUI.Enter8();
        if (Input.GetButtonDown(INPUT_SLOT_9))
            keypadUI.Enter9();
        if (Input.GetButtonDown(INPUT_SLOT_10))
            keypadUI.Enter0();
        if (Input.GetButtonDown(INPUT_INTERACT) || Input.GetButtonDown(INPUT_ENTER))
            keypadUI.EnterCode();
    }
}
