using UnityEngine;

public class NPCHat : MonoBehaviour, IDamageable
{
    private const float EXPLOSION_RADIUS = 5f;

    [SerializeField]
    private GameObject hatToSpawn;
    [SerializeField]
    private GameObject[] hatsToDisable;

    private void Awake()
    {
        if (hatToSpawn == null)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void TakeDamage(int amt, Vector3 hitPos, float force, Enums.damageType type)
    {
        if (amt < 1) return; //--------------------------------------------------------------------------

        GameObject hat = Instantiate(hatToSpawn, hatsToDisable[0].transform.position, hatsToDisable[0].transform.rotation);

        foreach (GameObject hatToDisable in hatsToDisable)
        {
            hatToDisable.SetActive(false);
        }

        Rigidbody rb = hat.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.AddExplosionForce(force, hitPos, EXPLOSION_RADIUS);
        this.gameObject.SetActive(false);
    }
}
