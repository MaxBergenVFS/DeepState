using UnityEngine;

public class NPCHearing : MonoBehaviour
{
    [SerializeField]
    protected NPCController controller;
    [SerializeField]
    protected bool disable;

    protected virtual void Awake()
    {
        if (disable) Disable();
    }
    private void Start()
    {
        if (controller.IsFriendly) Disable();
    }

    public void SetAlertedIfHostile()
    {
        if (controller.IsFriendly) return; //-----------
        SetAlerted();
    }
    public void SetAlerted()
    {
        if (disable) return; //------------------
        controller.SetAlerted(true);
    }

    protected void Disable()
    {
        disable = true;
        this.enabled = false;
    }
}
