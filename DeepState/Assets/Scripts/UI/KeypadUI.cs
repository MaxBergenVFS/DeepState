using TMPro;
using UnityEngine;

[RequireComponent(typeof(UISFX))]
public class KeypadUI : PadUI
{
    private const int CODE_LENGTH = 4;

    [SerializeField]
    private TextMeshProUGUI[] displayDigit = new TextMeshProUGUI[CODE_LENGTH];

    private Keypad keypadRef;
    private UISFX sfx;
    private int[] enteredDigits = new int[CODE_LENGTH];
    private int currentDigitIndex;

    protected override void Awake()
    {
        base.Awake();
        sfx = GetComponent<UISFX>();
        keypadRef = null;
        ResetEnteredDigits();
    }

    public void SetKeypadRef(Keypad keypad)
    {
        keypadRef = keypad;
    }

    public override void Hide()
    {
        if (!background.activeSelf) return; //---------------

        keypadRef = null;
        ResetEnteredDigits();
        base.Hide();
    }

    public void Enter0() => EnterNum(0);
    public void Enter1() => EnterNum(1);
    public void Enter2() => EnterNum(2);
    public void Enter3() => EnterNum(3);
    public void Enter4() => EnterNum(4);
    public void Enter5() => EnterNum(5);
    public void Enter6() => EnterNum(6);
    public void Enter7() => EnterNum(7);
    public void Enter8() => EnterNum(8);
    public void Enter9() => EnterNum(9);

    private void EnterNum(int num)
    {
        if (keypadRef == null)
        {
            Hide();
            ResetEnteredDigits();
            return;
        } //--------------------------------------------------------------------

        if (currentDigitIndex >= CODE_LENGTH)
        {
            ResetEnteredDigits();
        }
        else
        {
            enteredDigits[currentDigitIndex] = num;
            displayDigit[currentDigitIndex].text = num.ToString();
            currentDigitIndex++;
        }
        sfx.PlayKeypadButtonPress();
    }

    private void ResetEnteredDigits()
    {
        for (int i = 0; i < CODE_LENGTH; i++)
        {
            enteredDigits[i] = 0;
            displayDigit[i].text = "";
        }
        currentDigitIndex = 0;
    }

    public void EnterCode()
    {
        if (EnteredCodeIsValid())
        {
            keypadRef.Open();
            keypadRef.ValidCodeHasBeenEntered();
            Hide();
            sfx.PlayKeypadAccessGrantedSound();
        }
        else
        {
            ResetEnteredDigits();
            sfx.PlayKeypadAccessDeniedSound();
        }
    }

    private bool EnteredCodeIsValid()
    {
        for (int i = 0; i < CODE_LENGTH; i++)
        {
            if (enteredDigits[i] != keypadRef.FourDigitCode[i]) return false;
        }
        return true;
    }
}
