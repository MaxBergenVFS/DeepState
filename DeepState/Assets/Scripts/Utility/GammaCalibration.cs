using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class GammaCalibration : MonoBehaviour
{
    [SerializeField]
    private PostProcessProfile profile;
    [SerializeField]
    private Slider slider;

    private PostProcessVolume volume;
    private ColorGrading colorGrading;
    private float defaultGamma;

    private void Awake()
    {
        volume = GetComponent<PostProcessVolume>();
    }
    private void Start()
    {
        volume.profile = profile;
        profile.TryGetSettings<ColorGrading>(out colorGrading);
        defaultGamma = colorGrading.gamma.value.w;
        slider.value = GeneralParams.DEFAULT_GAMMA_SLIDER_VAL;
        slider.onValueChanged.AddListener(delegate { SetGamma(); });
    }

    private void SetGamma()
    {
        float val = Mathf.Lerp(GeneralParams.GAMMA_MIN, GeneralParams.GAMMA_MAX, slider.value);
        colorGrading.gamma.value.w = val;
        PersistentValues.Instance.SetGamma(val, slider.value);
    }

    private void OnDestroy()
    {
        slider.onValueChanged.RemoveListener(delegate { SetGamma(); });
    }
}
