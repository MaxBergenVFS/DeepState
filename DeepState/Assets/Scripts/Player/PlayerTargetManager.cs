using UnityEngine;

public class PlayerTargetManager : MonoBehaviour
{
    [SerializeField]
    private Transform headTarget, bodyTarget;

    public Transform HeadTarget { get { return headTarget; } }
    public Transform BodyTarget { get { return bodyTarget; } }
}
