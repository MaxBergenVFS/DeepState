using UnityEngine;

public class ResultsScreenCanvas : MonoBehaviour
{
    [SerializeField]
    ResultsScreenManager resultsScreenManager;

    private void OnEnable()
    {
        resultsScreenManager.Display();
    }

    private void OnDisable()
    {
        resultsScreenManager.Hide();
    }
}
