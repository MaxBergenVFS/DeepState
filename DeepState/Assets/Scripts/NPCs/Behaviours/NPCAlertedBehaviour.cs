using UnityEngine;

public class NPCAlertedBehaviour : StateMachineBehaviour
{
    private const float PURSUE_DELAY_SECONDS = 0.7f;
    private const float OVERLAP_SPHERE_RADIUS = 25f;

    private NPCController controller;
    private float pursueDelayTimeRemaining;

    [SerializeField]
    private LayerMask npcLayers;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        controller = animator.GetComponent<NPCController>();

        //if npc not set to enter alert on player seen then enter pursue as soon as alerted
        if (!controller.EnterAlertOnPlayerSeen || NearbyNPCsAreAlerted())
        {
            EnterPursueState(animator);
            return;
        } //------------------------------------------------------------------------------------

        pursueDelayTimeRemaining = PURSUE_DELAY_SECONDS;
        controller.SetNavSpeedToStop();
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, false);
        animator.SetBool(AnimParams.NPC_BOOL_CAN_SEE_PLAYER, false);
        controller.RotateToFacePlayer(true);
        //Debug.Log("WHAT WAS THAT NOISE?");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!controller.EnterAlertOnPlayerSeen) return; //-------------------------------
        
        pursueDelayTimeRemaining -= Time.deltaTime;
        if (pursueDelayTimeRemaining > 0f) return; //------------------------------------

        if (controller.PlayerInLoS())
        {
            EnterPursueState(animator);
        }
        else
        {
            EnterPatrolState();
        }
    }

    // override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {

    // }

    private void EnterPursueState(Animator animator)
    {
        animator.SetBool(AnimParams.NPC_BOOL_CAN_SEE_PLAYER, true);
    }

    private void EnterPatrolState()
    {
        controller.SetAlerted(false);
    }

    private bool NearbyNPCsAreAlerted()
    {
        Collider[] cols = Physics.OverlapSphere(controller.transform.position, OVERLAP_SPHERE_RADIUS, npcLayers);
        foreach (Collider col in cols)
        {
            if (col.GetComponent<NPCHitbox>() == null) continue;
            if (col.GetComponent<NPCHitbox>().Controller.IsAlerted) return true;
        }
        return false;
    }

}
