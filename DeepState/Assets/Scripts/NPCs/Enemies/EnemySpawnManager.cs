using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    [SerializeField]
    private EnemySpawner[] enemySpawners;

    public void ActivateSpawners()
    {
        foreach (var spawner in enemySpawners)
        {
            spawner.SpawnEnemies();
        }
    }
}
