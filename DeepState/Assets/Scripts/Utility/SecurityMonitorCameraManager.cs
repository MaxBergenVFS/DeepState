using TMPro;
using UnityEngine;

public class SecurityMonitorCameraManager : MonoBehaviour
{
    private const string CAM_NUM_PREFIX = "CAM_";
    private const string BTN_TEXT_TURRET_DISABLE = "Disable Turret";
    private const string BTN_TEXT_TURRET_ENABLE = "Enable Turret";

    [SerializeField]
    private GameObject camObj, canvasObj, noFeedOverlay, disableTurretButton, hackTurretButton;
    [SerializeField]
    private TextMeshProUGUI camNumText, camNameText, hackingToolAmtText, disableTurretBtnText;

    private Camera cam;
    private SecurityMonitor securityMonitorRef;
    private AutoTurretManager autoTurretRef;
    private InputManager inputManager;
    private PlayerCameraManager playerCameraManager;

    private void Awake()
    {
        disableTurretBtnText.text = BTN_TEXT_TURRET_DISABLE;
        DisplayTurretButtons(false);
    }

    private void Start()
    {
        inputManager = PlayerController.Instance.GetComponent<InputManager>();
        playerCameraManager = PlayerController.Instance.GetComponent<PlayerCameraManager>();
        cam = camObj.GetComponent<Camera>();
        cam.enabled = false;
        camObj.SetActive(false);
        canvasObj.SetActive(false);
        securityMonitorRef = null;
        DisplayNoFeed(false);
        PlayerHealth.onHealthChanged += DisableCameraOnHealthChanged;
    }

    public void EnableCamera(Transform parent, int camNum, string camName, bool isDestroyed)
    {
        if (!camObj.activeSelf || !canvasObj.activeSelf)
        {
            EnableCanvas();
            camObj.SetActive(true);
            cam.enabled = true;
        }

        if (isDestroyed) return; //-----------------------------------

        camNumText.text = CAM_NUM_PREFIX + camNum.ToString();
        camNameText.text = camName;
        cam.transform.parent = null;
        cam.transform.SetParent(parent);
        cam.transform.localPosition = Vector3.zero;
        cam.transform.localEulerAngles = Vector3.zero;
    }

    public void SetSecurityMonitor(SecurityMonitor securityMonitor)
    {
        securityMonitorRef = securityMonitor;
    }

    private void DisableCameraOnHealthChanged(int amt)
    {
        if (amt < 1) return;
        DisableCamera();
    }

    public void DisableCamera()
    {
        cam.enabled = false;
        camObj.SetActive(false);
        canvasObj.SetActive(false);
        cam.transform.parent = null;
        GameManager.Instance.PauseGame(false, false, false, false);
        playerCameraManager.EnableDefaultCamsAndUI(true);
        inputManager.enabled = true;
    }

    public void DisplayNoFeed(bool active)
    {
        if (noFeedOverlay.activeSelf == active) return; //-------------
        noFeedOverlay.SetActive(active);
    }

    public void EnableCanvas()
    {
        canvasObj.SetActive(true);
        inputManager.enabled = false;
        playerCameraManager.EnableDefaultCamsAndUI(false);
        SetAutoTurretRef();
        GameManager.Instance.PauseGame(false, true, false, false);
    }

    public void NextFeed()
    {
        securityMonitorRef.DisplayNextFeed();
        SetAutoTurretRef();
    }
    public void PreviousFeed()
    {
        securityMonitorRef.DisplayPreviousFeed();
        SetAutoTurretRef();
    }

    public void ToggleAutoTurretActivation()
    {
        if (autoTurretRef == null) return; //--------------------
        autoTurretRef.ToggleActivation();
        HandleButtonTextDisplay(autoTurretRef.IsDeactivated);
    }
    public void HandleButtonTextDisplay(bool isDeactivated)
    {
        if (autoTurretRef == null) return; //--------------------
        if (isDeactivated) disableTurretBtnText.text = BTN_TEXT_TURRET_ENABLE;
        else disableTurretBtnText.text = BTN_TEXT_TURRET_DISABLE;
    }
    public void HackAutoTurret()
    {
        if (autoTurretRef == null || autoTurretRef.GetHackingToolAmt() < 1) return; //--------------------
        autoTurretRef.Hack();
        hackingToolAmtText.text = autoTurretRef.GetHackingToolAmt().ToString();
    }

    private void SetAutoTurretRef()
    {
        autoTurretRef = securityMonitorRef.GetAutoTurretRef();
        DisplayTurretButtons(autoTurretRef != null);
    }

    private void DisplayTurretButtons(bool active)
    {
        disableTurretButton.SetActive(active);
        hackTurretButton.SetActive(active);
        if (active) hackingToolAmtText.text = autoTurretRef.GetHackingToolAmt().ToString();
    }

    private void OnDestroy()
    {
        PlayerHealth.onHealthChanged -= DisableCameraOnHealthChanged;
    }
}
