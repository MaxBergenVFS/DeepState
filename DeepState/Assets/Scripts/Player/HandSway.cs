using UnityEngine;

public class HandSway : MonoBehaviour
{
    [SerializeField]
    private float swayAmount = -5f, swayThreshold = 10f, swaySmooth = 3f, bobSpeed = 8f, bobAmount = 0.15f;

    private PlayerController playerController;
    private CameraController playerCamera;
    private PlayerHands playerHands;
    private Quaternion rot;
    private Vector3 pos;
    private float sinTime;

    private void Awake()
    {
        rot = transform.localRotation;
        pos = transform.localPosition;
        playerHands = GetComponent<PlayerHands>();
    }
    private void Start()
    {
        playerController = PlayerController.Instance;
        playerCamera = playerController.GetComponent<CameraController>();
    }

    private void Update()
    {
        if (!playerHands.HandsEnabled) return; //----------
        Sway();
        Bob();
    }

    private void Sway()
    {
        float x = playerCamera.MouseY * swayAmount;
        float y = -playerCamera.MouseX * swayAmount;

        if (x > swayThreshold) x = swayThreshold;
        if (x < -swayThreshold) x = -swayThreshold;
        if (y > swayThreshold) y = swayThreshold;
        if (y < -swayThreshold) y = -swayThreshold;

        Quaternion newRot = Quaternion.Euler(rot.x + x, rot.y + y, rot.z);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, newRot, (Time.deltaTime * swaySmooth));
    }

    private void Bob()
    {
        if (playerController.HasMoved) sinTime += Time.deltaTime * bobSpeed;
        else sinTime = 0f;

        float sinAmountY = Mathf.Abs(bobAmount * Mathf.Sin(sinTime));
        Vector3 sinAmountX = pos * bobAmount * Mathf.Cos(sinTime) * playerController.MovementSpeed;
        Vector3 newPos = new Vector3(pos.x, pos.y + sinAmountY, pos.z);
        newPos += sinAmountX;

        transform.localPosition = Vector3.Lerp(transform.localPosition, newPos, Time.deltaTime);
    }
}
