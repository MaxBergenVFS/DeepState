using UnityEngine;

public class ReflectiveSurface : MonoBehaviour
{
    [SerializeField]
    protected MirroredRig mirroredRig;

    protected Transform mirroredRigTransform, player;

    private GameObject mirroredRigObj;

    protected virtual void Start()
    {
        player = PlayerController.Instance.transform;
        mirroredRigObj = mirroredRig.gameObject;
        mirroredRigTransform = mirroredRig.transform;
        mirroredRigObj.SetActive(false);
    }

    protected virtual void SetMirroredRigPos() { }
    protected virtual void SetMirroredRigRot() { }

    protected virtual void OnPlayerEnter()
    {
        mirroredRigObj.SetActive(true);
    }
    private void OnPlayerStay()
    {
        SetMirroredRigPos();
        SetMirroredRigRot();
    }
    protected virtual void OnPlayerExit()
    {
        mirroredRigObj.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) OnPlayerEnter();
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player")) OnPlayerStay();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) OnPlayerExit();
    }
}
