using UnityEngine;

public class EnemyLoS : MonoBehaviour
{
    [SerializeField]
    private NPCController controller;

    private Collider observed;

    private void Awake()
    {
        ResetObserved();
    }
    private void Start()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint += ResetObserved;
    }

    private void OnDestroy()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint -= ResetObserved;
    }

    private void ResetObserved()
    {
        observed = null;
    }

    private void Update()
    {
        if (observed == null) return; //------------------

        if (!controller.PlayerOutsideOfDistThreshold) controller.AlertIfPlayerInLoS();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) observed = other;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) ResetObserved();
    }
}
