using UnityEngine;

public class Health : MonoBehaviour
{
    private const float INVINCIBILITY_TIME = 0.2f;

    [SerializeField]
    private int maxHealth = 10;

    protected int remainingHealth;
    protected bool invincible;

    public int MaxHealth { get { return maxHealth; } }
    public int RemainingHealth { get { return remainingHealth; } }
    public bool AtMaxHealth { get { return remainingHealth >= maxHealth; } }

    protected virtual void Awake()
    {
        remainingHealth = maxHealth;
        invincible = false;
    }

    protected void StartInvinciblityTimer()
    {
        invincible = true;
        Invoke("ResetInvincibility", INVINCIBILITY_TIME);
    }
    private void ResetInvincibility()
    {
        invincible = false;
    }

    public virtual void DecreaseRemainingHealth(int amt)
    {
        remainingHealth -= amt;
        if (remainingHealth > maxHealth) remainingHealth = maxHealth;
        else if (remainingHealth < 0) remainingHealth = 0;
    }

    public virtual void SetHealthToFull()
    {
        remainingHealth = maxHealth;
    }

    public virtual void SetRemainingHealth(int amt)
    {
        if (amt < 0) remainingHealth = 0;
        else if (amt > maxHealth) remainingHealth = maxHealth;
        else remainingHealth = amt;
    }
}
