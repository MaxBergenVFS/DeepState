public class HealingItem : BaseItem
{
    public override void UseFromInventory(out bool success)
    {
        playerInventory.UseHealingItem(itemType, out success);
    }
}
