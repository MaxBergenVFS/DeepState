using TMPro;
using UnityEngine;
using System.Collections.Generic;

public class NotesTab : InventoryTab
{
    [SerializeField]
    private TextMeshProUGUI noteTitle, noteContents;
    [SerializeField]
    private SavedNotesManager savedNotesManager;

    private int currentNoteIndex, maxNoteIndex;
    private List<SavedNote> savedNotes;

    private void Start()
    {
        currentNoteIndex = 0;
        RefreshSavedNotesRef();
    }

    public override void Display(bool active)
    {
        if (active) RefreshSavedNotesRef();
        SetTextToNoteAtIndex(currentNoteIndex);
        base.Display(active);
    }

    // public override void Increment()
    // {
    //     base.Increment();
    //     currentNoteIndex++;
    //     if (currentNoteIndex > maxNoteIndex - 1) currentNoteIndex = 0;
    //     SetTextToNoteAtIndex(currentNoteIndex);
    // }

    // public override void Decrement()
    // {
    //     base.Decrement();
    //     currentNoteIndex--;
    //     if (currentNoteIndex < 0) currentNoteIndex = maxNoteIndex - 1;
    //     SetTextToNoteAtIndex(currentNoteIndex);
    // }

    // public override void Select()
    // {
    //     base.Select();
    // }

    private void SetTextToNoteAtIndex(int index)
    {
        noteTitle.text = savedNotes[index].title;
        noteContents.text = savedNotes[index].content;
    }

    private void RefreshSavedNotesRef()
    {
        savedNotes = savedNotesManager.SavedNotes;
        maxNoteIndex = savedNotes.Count;
    }
}
