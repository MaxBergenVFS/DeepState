using System.Collections.Generic;
using UnityEngine;

public class InventoryItemDatabase : MonoBehaviour
{
    #region Item info
    private int credentialsId = 0;
    [SerializeField]
    private string credentialsTitle = "Credentials";
    [SerializeField]
    private string credentialsShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string credentialsDescription, credentialsCriticalInfo;

    private int akRifleId = 1;
    [SerializeField]
    private string akRifleTitle = "AK-47";
    [SerializeField]
    private string akRifleShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string akRifleDescription, akRifleCriticalInfo;

    private int m4RifleId = 17;
    [SerializeField]
    private string m4RifleTitle = "M4";
    [SerializeField]
    private string m4RifleShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string m4RifleDescription, m4RifleCriticalInfo;

    private int wrenchId = 2;
    [SerializeField]
    private string wrenchTitle = "Wrench";
    [SerializeField]
    private string wrenchShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string wrenchDescription, wrenchCriticalInfo;

    private int medpackId = 3;
    [SerializeField]
    private string medpackTitle = "Medpack";
    [SerializeField]
    private string medpackShortenedTitle = "";
    [SerializeField]
    private int medpackHealing = 25;
    [SerializeField]
    [TextArea(3, 10)]
    private string medpackDescription, medpackCriticalInfo;

    private int rifleAmmoId = 4;
    [SerializeField]
    private string rifleAmmoTitle = "Rifle Ammo";
    [SerializeField]
    private string rifleAmmoShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string rifleAmmoDescription, rifleAmmoCriticalInfo;

    private int chipsId = 5;
    [SerializeField]
    private string chipsTitle = "Bug Bag";
    [SerializeField]
    private string chipsShortenedTitle = "";
    [SerializeField]
    private int chipsHealing = 5;
    [SerializeField]
    [TextArea(3, 10)]
    private string chipsDescription, chipsCriticalInfo;

    private int sodaId = 6;
    [SerializeField]
    private string sodaTitle = "Soda";
    [SerializeField]
    private string sodaShortenedTitle = "";
    [SerializeField]
    private int sodaHealing = 4;
    [SerializeField]
    [TextArea(3, 10)]
    private string sodaDescription, sodaCriticalInfo;

    private int redKeycardId = 7;
    [SerializeField]
    private string redKeycardTitle = "Red Keycard";
    [SerializeField]
    private string redKeycardShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string redKeycardDescription, redKeycardCriticalInfo;

    private int blueKeycardId = 8;
    [SerializeField]
    private string blueKeycardTitle = "Blue Keycard";
    [SerializeField]
    private string blueKeycardShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string blueKeycardDescription, blueKeycardCriticalInfo;

    private int bodyArmorId = 9;
    [SerializeField]
    private string bodyArmorTitle = "Body Armor";
    [SerializeField]
    private string bodyArmorShortenedTitle = "";
    [SerializeField]
    private int bodyArmorHealing = 50;
    [SerializeField]
    [TextArea(3, 10)]
    private string bodyArmorDescription, bodyArmorCriticalInfo;

    private int lockPickId = 10;
    [SerializeField]
    private string lockPickTitle = "Lock Pick";
    [SerializeField]
    private string lockPickShortenedTitle = "k";
    [SerializeField]
    [TextArea(3, 10)]
    private string lockPickDescription, lockPickCriticalInfo;

    private int hackingToolId = 11;
    [SerializeField]
    private string hackingToolTitle = "Hacking Tool";
    [SerializeField]
    private string hackingToolShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string hackingToolDescription, hackingToolCriticalInfo;

    private int silencerId = 12;
    [SerializeField]
    private string silencerTitle = "Silencer";
    [SerializeField]
    private string silencerShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string silencerDescription, silencerCriticalInfo;

    private int nightVisionGogglesId = 13;
    [SerializeField]
    private string nightVisionGogglesTitle = "Night Vision Goggles";
    [SerializeField]
    private string nightVisionGogglesShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string nightVisionGogglesDescription, nightVisionGogglesCriticalInfo;

    private int pistolAmmoId = 14;
    [SerializeField]
    private string pistolAmmoTitle = "Pistol Ammo";
    [SerializeField]
    private string pistolAmmoShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string pistolAmmoDescription, pistolAmmoCriticalInfo;

    private int pistolId = 15;
    [SerializeField]
    private string pistolTitle = "Pistol";
    [SerializeField]
    private string pistolShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string pistolDescription, pistolCriticalInfo;

    private int checkpointDeviceId = 16;
    [SerializeField]
    private string checkpointDeviceTitle = "Checkpoint Device";
    [SerializeField]
    private string checkpointDeviceShortenedTitle = "";
    [SerializeField]
    [TextArea(3, 10)]
    private string checkpointDeviceDescription, checkpointDeviceCriticalInfo;
    #endregion

    private int money, moneySnapshot;
    private List<InventoryItem> inventoryItems;
    private Dictionary<int, int> itemAmountsSnapshot = new Dictionary<int, int>();

    public int Money { get { return money; } }

    private void Awake()
    {
        money = 0;
        BuildDatabase();
        InitItemAmountsSnapshot();
    }

    private void BuildDatabase()
    {
        inventoryItems = new List<InventoryItem>(){
            new InventoryItem(credentialsId, Enums.itemType.Credentials, credentialsTitle, credentialsShortenedTitle, credentialsDescription, credentialsCriticalInfo, 0, true, 0, false, false, false),
            new InventoryItem(akRifleId, Enums.itemType.AkRifle, akRifleTitle, akRifleShortenedTitle, akRifleDescription, akRifleCriticalInfo, 0, true, 0, false, true, false),
            new InventoryItem(wrenchId, Enums.itemType.Wrench, wrenchTitle, wrenchShortenedTitle, wrenchDescription, wrenchCriticalInfo, 0, true, 0, false, true, false),
            new InventoryItem(medpackId, Enums.itemType.Medpack, medpackTitle, medpackShortenedTitle, medpackDescription, medpackCriticalInfo, medpackHealing, false, 0, true, false, false),
            new InventoryItem(rifleAmmoId, Enums.itemType.Ammo, rifleAmmoTitle, rifleAmmoShortenedTitle, rifleAmmoDescription, rifleAmmoCriticalInfo, 0, false, 0, false, false, false),
            new InventoryItem(chipsId, Enums.itemType.Chips, chipsTitle, chipsShortenedTitle, chipsDescription, chipsCriticalInfo, chipsHealing, false, 0, true, false, false),
            new InventoryItem(sodaId, Enums.itemType.Soda, sodaTitle, sodaShortenedTitle, sodaDescription, sodaCriticalInfo, sodaHealing, false, 0, true, false, false),
            new InventoryItem(redKeycardId, Enums.itemType.RedKeycard, redKeycardTitle, redKeycardShortenedTitle, redKeycardDescription, redKeycardCriticalInfo, 0, true, 0, false, true, false),
            new InventoryItem(blueKeycardId, Enums.itemType.BlueKeycard, blueKeycardTitle, blueKeycardShortenedTitle, blueKeycardDescription, blueKeycardCriticalInfo, 0, true, 0, false, true, false),
            new InventoryItem(bodyArmorId, Enums.itemType.BodyArmor, bodyArmorTitle, bodyArmorShortenedTitle, bodyArmorDescription, bodyArmorCriticalInfo, bodyArmorHealing, false, 0, true, false, false),
            new InventoryItem(lockPickId, Enums.itemType.LockPick, lockPickTitle, lockPickShortenedTitle, lockPickDescription, lockPickCriticalInfo, 0, false, 0, true, true, false),
            new InventoryItem(hackingToolId, Enums.itemType.HackingTool, hackingToolTitle, hackingToolShortenedTitle, hackingToolDescription, hackingToolCriticalInfo, 0, false, 0, true, true, false),
            new InventoryItem(silencerId, Enums.itemType.Silencer, silencerTitle, silencerShortenedTitle, silencerDescription, silencerCriticalInfo, 0, true, 0, true, false, false),
            new InventoryItem(nightVisionGogglesId, Enums.itemType.NightVisionGoggles, nightVisionGogglesTitle, nightVisionGogglesShortenedTitle, nightVisionGogglesDescription, nightVisionGogglesCriticalInfo, 0, true, 0, false, true, false),
            new InventoryItem(pistolAmmoId, Enums.itemType.Ammo, pistolAmmoTitle, pistolAmmoShortenedTitle, pistolAmmoDescription, pistolAmmoCriticalInfo, 0, false, 0, false, false, false),
            new InventoryItem(pistolId, Enums.itemType.Pistol, pistolTitle, pistolShortenedTitle, pistolDescription, pistolCriticalInfo, 0, true, 0, false, true, false),
            new InventoryItem(checkpointDeviceId, Enums.itemType.CheckpointDevice, checkpointDeviceTitle, checkpointDeviceShortenedTitle, checkpointDeviceDescription, checkpointDeviceCriticalInfo, 0, false, 0, true, true, false),
            new InventoryItem(m4RifleId, Enums.itemType.M4Rifle, m4RifleTitle, m4RifleShortenedTitle, m4RifleDescription, m4RifleCriticalInfo, 0, true, 0, false, true, false),
        };
    }

    private void InitItemAmountsSnapshot()
    {
        moneySnapshot = money;
        foreach (InventoryItem inventoryItem in inventoryItems)
        {
            itemAmountsSnapshot.Add(inventoryItem.id, inventoryItem.amount);
        }
    }

    private int GetInventoryItemId(Enums.itemType itemType)
    {
        return inventoryItems.Find(inventoryItem => inventoryItem.itemType == itemType).id;
    }
    private int GetInventoryItemId(Enums.ammoType ammoType)
    {
        switch (ammoType)
        {
            case (Enums.ammoType.RifleAmmo):
                return rifleAmmoId;
            case (Enums.ammoType.PistolAmmo):
                return pistolAmmoId;
            default:
                return 0;
        }
    }

    private bool GetInventoryItemIsUnique(int id)
    {
        return GetInventoryItem(id).isUnique;
    }

    #region API
    public Enums.ammoType GetAmmoType(Enums.itemType itemType)
    {
        switch (itemType)
        {
            case (Enums.itemType.Pistol):
                return Enums.ammoType.PistolAmmo;
            case (Enums.itemType.AkRifle):
            case (Enums.itemType.M4Rifle):
                return Enums.ammoType.RifleAmmo;
            case (Enums.itemType.None):
            default:
                return Enums.ammoType.None;
        }
    }

    public InventoryItem GetInventoryItem(int id)
    {
        return inventoryItems.Find(inventoryItem => inventoryItem.id == id);
    }
    public InventoryItem GetInventoryItem(Enums.itemType itemType)
    {
        return inventoryItems.Find(inventoryItem => inventoryItem.itemType == itemType);
    }
    public InventoryItem GetInventoryItem(Enums.ammoType ammoType)
    {
        return GetInventoryItem(GetInventoryItemId(ammoType));
    }

    public bool GetInventoryItemIsUnique(Enums.itemType itemType)
    {
        return GetInventoryItem(itemType).isUnique;
    }
    public bool GetInventoryItemIsConsumable(Enums.itemType itemType)
    {
        if (itemType == Enums.itemType.None) return false;
        return GetInventoryItem(itemType).isConsumable;
    }
    public bool UniqueItemIsInInventory(Enums.itemType itemType)
    {
        InventoryItem item = GetInventoryItem(itemType);
        return item.isUnique && item.amount > 0;
    }

    public int GetInventoryItemAmount(int id)
    {
        return GetInventoryItem(id).amount;
    }
    public int GetInventoryItemAmount(Enums.ammoType ammoType)
    {
        return GetInventoryItemAmount(GetInventoryItemId(ammoType));
    }
    public int GetInventoryItemAmount(Enums.itemType itemType)
    {
        return GetInventoryItem(itemType).amount;
    }

    public bool GetInventoryItemIsEquipped(Enums.itemType itemType)
    {
        return GetInventoryItem(itemType).isEquipped;
    }
    public void SetInventoryItemIsEquipped(Enums.itemType itemType, bool active)
    {
        GetInventoryItem(itemType).isEquipped = active;
    }

    public bool GetInventoryItemIsEquippable(Enums.itemType itemType)
    {
        return GetInventoryItem(itemType).isEquippable;
    }

    public void SetInventoryItemAmount(int id, int amt)
    {
        if (amt < 1) amt = 0;
        else if (GetInventoryItemIsUnique(id) && amt > 1) amt = 1;
        GetInventoryItem(id).amount = amt;
    }

    public void AddToInventoryItemAmount(int id, int amt)
    {
        int newAmt = GetInventoryItemAmount(id) + amt;
        if (newAmt < 1) newAmt = 0;
        SetInventoryItemAmount(id, newAmt);
    }
    public void AddToInventoryItemAmount(Enums.ammoType ammoType, int amt)
    {
        AddToInventoryItemAmount(GetInventoryItemId(ammoType), amt);
    }
    public void AddToInventoryItemAmount(Enums.itemType itemType, int amt)
    {
        AddToInventoryItemAmount(GetInventoryItemId(itemType), amt);
    }

    public int GetInventoryItemHealing(int id)
    {
        return GetInventoryItem(id).healing;
    }
    public int GetInventoryItemHealing(Enums.itemType itemType)
    {
        return GetInventoryItem(itemType).healing;
    }

    public void AddToMoneyAmount(int amt)
    {
        money += amt;
        if (money < 0) money = 0;
    }

    public List<int> GetListOfValidIds()
    {
        List<int> ids = new List<int>();
        foreach (InventoryItem item in inventoryItems)
        {
            if (item.amount > 0) ids.Add(item.id);
        }
        return ids;
    }
    public List<int> GetListOfAllIds()
    {
        List<int> ids = new List<int>();
        foreach (InventoryItem item in inventoryItems)
        {
            ids.Add(item.id);
        }
        return ids;
    }

    public void SnapshotItemAmounts()
    {
        moneySnapshot = money;
        foreach (InventoryItem inventoryItem in inventoryItems)
        {
            itemAmountsSnapshot[inventoryItem.id] = inventoryItem.amount;
        }
    }
    public void RevertDatabaseAmountsToSnapshot()
    {
        money = moneySnapshot;
        foreach (InventoryItem inventoryItem in inventoryItems)
        {
            inventoryItem.amount = itemAmountsSnapshot[inventoryItem.id];
        }
    }

    public int GetSnapshotItemAmount(int id)
    {
        return itemAmountsSnapshot[id];
    }
    #endregion
}
