using System.Collections;
using TMPro;
using UnityEngine;

public class Codec : MonoBehaviour
{
    private const float DIALOGUE_BOX_WIDTH_CLOSED = 163f;
    private const float DIALOGUE_BOX_WIDTH_OPEN = 1923f;
    private const float DIALOGUE_BOX_HEIGHT_DEFAULT = 535f;
    private const float HANDLE_CODEC_DELAY = 1.5f;

    [SerializeField]
    private GameObject background, videoFeedOverlay;
    [SerializeField]
    private TextMeshProUGUI codecTextNPC, codecTextRex, characterName;
    [SerializeField]
    private RectTransform dialogueBoxNPC, dialogueBoxRex;
    [SerializeField]
    private float dialogueBoxLerpSpeed = 1f;
    [SerializeField]
    private bool playCodecMusic;

    private InputManager inputManager;
    private GameManager gameManager;
    private CodecTrigger codecTriggerRef = null;
    private CodecViewportCamera rexCam, npcCam;
    private PlayerCameraManager cameraManager;
    private PlayerHands playerHands;
    private PlayerController playerController;
    private MusicManager musicManager;
    private PlayerSFX sfx;

    public TextMeshProUGUI CodecTextNPC { get { return codecTextNPC; } }
    public TextMeshProUGUI CodecTextRex { get { return codecTextRex; } }
    public TextMeshProUGUI CharacterName { get { return characterName; } }
    public delegate void OnDisplayCodecUI(bool active);
    public static event OnDisplayCodecUI onDisplayCodecUI;

    private void Start()
    {
        playerController = PlayerController.Instance;
        inputManager = playerController.GetComponent<InputManager>();
        cameraManager = playerController.GetComponent<PlayerCameraManager>();
        sfx = playerController.GetComponent<PlayerSFX>();
        playerHands = playerController.GetComponent<PlayerHeldItem>().PlayerHands;
        gameManager = GameManager.Instance;
        musicManager = gameManager.MusicManager;
        rexCam = gameManager.CodecViewportCameraRex;
        npcCam = gameManager.CodecViewportCameraNPC;
        HideUI();
    }

    private void HideUI()
    {
        ShowNPCDialogue(false);
        ShowRexDialogue(false);
        characterName.text = "";
        background.SetActive(false);
    }

    private void ShowNPCDialogue(bool active)
    {
        if (active)
        {
            StartCoroutine(LerpRectTransformWidth(dialogueBoxNPC, DIALOGUE_BOX_WIDTH_OPEN));
        }
        else
        {
            StartCoroutine(LerpRectTransformWidth(dialogueBoxNPC, DIALOGUE_BOX_WIDTH_CLOSED));
            codecTextNPC.text = "";
        }
    }
    private void ShowRexDialogue(bool active)
    {
        if (active)
        {
            StartCoroutine(LerpRectTransformWidth(dialogueBoxRex, DIALOGUE_BOX_WIDTH_OPEN));
        }
        else
        {
            StartCoroutine(LerpRectTransformWidth(dialogueBoxRex, DIALOGUE_BOX_WIDTH_CLOSED));
            codecTextRex.text = "";
        }
    }

    private IEnumerator LerpRectTransformWidth(RectTransform rect, float targetWidth)
    {
        float t = 0f;
        float start = rect.sizeDelta.x;
        float width = start;
        while (t < 1f)
        {
            width = Mathf.Lerp(start, targetWidth, t);
            rect.sizeDelta = new Vector2(width, DIALOGUE_BOX_HEIGHT_DEFAULT);
            t += Time.deltaTime * dialogueBoxLerpSpeed;
            yield return null;
        }
        rect.sizeDelta = new Vector2(targetWidth, DIALOGUE_BOX_HEIGHT_DEFAULT);
    }

    public void StartCodecCall(CodecTrigger codecTrigger)
    {
        float delay;
        playerHands.TriggerWatchAnimation(out delay);
        cameraManager.LookAtWatch(true, false, delay);
        codecTriggerRef = codecTrigger;
        inputManager.enabled = false;
        sfx.PlayOpenInventorySound();
        musicManager.Muffle(true);
        Invoke("HandleCodecCall", HANDLE_CODEC_DELAY + delay);
        if (onDisplayCodecUI != null) onDisplayCodecUI(true);
    }

    private void HandleCodecCall()
    {
        gameManager.PauseGame(false, true, false, playCodecMusic);
        background.SetActive(true);
        rexCam.EnableCamera(true);
        npcCam.EnableCamera(true);
    }

    public void ShowNPCDialogue()
    {
        ShowNPCDialogue(true);
        ShowRexDialogue(false);
        videoFeedOverlay.SetActive(false);
    }
    public void ShowRexDialogue()
    {
        ShowNPCDialogue(false);
        ShowRexDialogue(true);
        videoFeedOverlay.SetActive(false);
    }
    public void ShowNPCVideoOverlay()
    {
        ShowNPCDialogue();
        videoFeedOverlay.SetActive(true);
    }

    public void EndCodecCall()
    {
        codecTriggerRef = null;
        inputManager.enabled = true;
        cameraManager.LookAtWatch(false);
        gameManager.PauseGame(false, false, false, false);
        rexCam.EnableCamera(false);
        npcCam.EnableCamera(false);
        musicManager.PlayMenuMusic(false, false, false);
        musicManager.Muffle(false);
        sfx.PlayCloseInventorySound();
        HideUI();
        if (onDisplayCodecUI != null) onDisplayCodecUI(false);
    }

    public void NextDialogue()
    {
        if (codecTriggerRef != null) codecTriggerRef.NextCodecDialogue();
    }

}
