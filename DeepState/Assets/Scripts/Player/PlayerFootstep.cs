﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerFootstep : SFX
{
    private const float JUMP_SOUND_BUFFER_TIME = 0.1f;

    [SerializeField]
    private float timeBetweenFootsteps = 6f, speedMultiplier = 1f;
    // [SerializeField]
    // private Enums.groundType defaultParam;

    private float timeUntilNextFootstep, timeUntilNextJumpSound, speed;
    private bool dontPlaySFXOnEnable;
    private PlayerController playerController;
    private FMOD.Studio.EventInstance footstepsEvent;
    private Dictionary<string, float> snapshot = new Dictionary<string, float>();

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        timeUntilNextFootstep = timeBetweenFootsteps;
        timeUntilNextJumpSound = 0f;
        dontPlaySFXOnEnable = true;
    }
    private void Start()
    {
        Init();
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    private float GetParamVal(string param)
    {
        float val;
        footstepsEvent.getParameterByName(param, out val);
        return val;
    }
    private void Init()
    {
        footstepsEvent = FMODUnity.RuntimeManager.CreateInstance(SFX_PREFIX + PLAYER_FOOTSTEP_PATH);
        snapshot.Add(FOOTSTEP_CONCRETE_PARAM, GetParamVal(FOOTSTEP_CONCRETE_PARAM));
        snapshot.Add(FOOTSTEP_DIRT_PARAM, GetParamVal(FOOTSTEP_DIRT_PARAM));
        snapshot.Add(FOOTSTEP_METAL_PARAM, GetParamVal(FOOTSTEP_METAL_PARAM));
    }

    private void SnapshotState()
    {
        snapshot[FOOTSTEP_CONCRETE_PARAM] = GetParamVal(FOOTSTEP_CONCRETE_PARAM);
        snapshot[FOOTSTEP_DIRT_PARAM] = GetParamVal(FOOTSTEP_DIRT_PARAM);
        snapshot[FOOTSTEP_METAL_PARAM] = GetParamVal(FOOTSTEP_METAL_PARAM);
    }
    private void RevertState()
    {
        footstepsEvent.setParameterByName(FOOTSTEP_CONCRETE_PARAM, snapshot[FOOTSTEP_CONCRETE_PARAM]);
        footstepsEvent.setParameterByName(FOOTSTEP_DIRT_PARAM, snapshot[FOOTSTEP_DIRT_PARAM]);
        footstepsEvent.setParameterByName(FOOTSTEP_METAL_PARAM, snapshot[FOOTSTEP_METAL_PARAM]);
    }

    private void OnEnable()
    {
        if (dontPlaySFXOnEnable) dontPlaySFXOnEnable = false;
        else if (timeUntilNextJumpSound <= 0f) PlayJumpSoundDown();
    }

    private void Update()
    {
        timeUntilNextJumpSound -= Time.deltaTime;
        timeUntilNextFootstep -= Time.deltaTime * speed;
        if (timeUntilNextFootstep < 0f) PlayFootstepSound();
    }

    public void PlayJumpSoundUp() => PlaySound2D(PLAYER_JUMP_PATH);
    public void PlayJumpSoundDown()
    {
        PlayFootstepSound();
        timeUntilNextJumpSound = JUMP_SOUND_BUFFER_TIME;
    }

    public void ResetJumpSoundTimer()
    {
        timeUntilNextJumpSound = 0f;
    }

    public void SetFootstepSpeed(float amt)
    {
        float newSpeed = amt * speedMultiplier;
        if (speed != newSpeed) speed = newSpeed;
    }

    private void PlayFootstepSound()
    {
        footstepsEvent.start();
        timeUntilNextFootstep = timeBetweenFootsteps;
    }

    public void DontPlaySFXOnEnable()
    {
        dontPlaySFXOnEnable = true;
    }

    public void SetFootstepParam(Enums.groundType type, bool active)
    {
        float val;
        string param = GetParamStringFromGroundType(type);

        if (param == "") return; //--------------------------------------------

        if (active) val = 1f;
        else val = 0f;

        float currentVal = GetParamVal(param);

        if (currentVal == val) return; //-----------------------------------------

        footstepsEvent.setParameterByName(param, val);
    }

    private string GetParamStringFromGroundType(Enums.groundType type)
    {
        switch (type)
        {
            case (Enums.groundType.Concrete):
                return FOOTSTEP_CONCRETE_PARAM;
            case (Enums.groundType.Dirt):
                return FOOTSTEP_DIRT_PARAM;
            case (Enums.groundType.Metal):
                return FOOTSTEP_METAL_PARAM;
            case (Enums.groundType.None):
            default:
                return "";
        }
    }
}
