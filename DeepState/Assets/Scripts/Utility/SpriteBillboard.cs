using UnityEngine;

public class SpriteBillboard : MonoBehaviour
{
    private Transform mainCamera;

    private void Start()
    {
        mainCamera = PlayerController.Instance.GetComponent<CameraController>().MainCamera.transform;
    }

    void Update()
    {
        transform.LookAt(mainCamera.position, Vector3.up);
    }
}
