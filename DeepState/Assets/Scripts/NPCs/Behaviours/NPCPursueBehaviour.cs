using UnityEngine.AI;
using UnityEngine;

public class NPCPursueBehaviour : StateMachineBehaviour
{
    private NavMeshAgent agent;
    private Transform player;
    private Transform transform;
    private NPCController controller;
    private float attackRange, attackRangeMin, attackRangeMax;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        player = PlayerController.Instance.transform;
        transform = animator.GetComponent<Transform>();
        agent = animator.GetComponent<NavMeshAgent>();
        controller = animator.GetComponent<NPCController>();

        attackRangeMin = controller.AttackRangeMin;
        attackRangeMax = controller.AttackRangeMax;
        attackRange = Random.Range(attackRangeMin, attackRangeMax);

        controller.DisableDialogue();
        controller.SetNavSpeedToRun();

        SetAnimBools(animator);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        agent.SetDestination(player.position);

        if ((transform.position - player.position).magnitude < attackRange)
        {
            animator.SetBool(AnimParams.NPC_BOOL_IN_ATTACK_RANGE, true);
            controller.SetNavSpeedToStop();
        }

        controller.RemoveAlertIfPlayerNotInLoS();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
    }

    private void SetAnimBools(Animator animator)
    {
        animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, true);
        animator.SetBool(AnimParams.NPC_BOOL_IN_DIALOGUE, false);
        animator.SetBool(AnimParams.NPC_BOOL_CAN_SEE_PLAYER, true);
        if (!animator.GetBool(AnimParams.NPC_BOOL_ALERTED)) animator.SetBool(AnimParams.NPC_BOOL_ALERTED, true);
    }
}
