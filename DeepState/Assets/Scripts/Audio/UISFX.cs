public class UISFX: SFX
{
    public void PlayShootSound() => PlaySound2D(PLAYER_PREFIX + PISTOL_SHOOT_PATH);
    public void PlayKeypadAccessGrantedSound() => PlaySound2D(KEYPAD_ACCESS_GRANTED_PATH);
    public void PlayKeypadAccessDeniedSound() => PlaySound2D(KEYPAD_ACCESS_DENIED_PATH);
    public void PlayKeypadButtonPress() => PlaySound2D(KEYPAD_BUTTON_PRESS_PATH);
}
