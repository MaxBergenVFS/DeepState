using System.Collections;
using UnityEngine;

public class Fog : MonoBehaviour
{
    [SerializeField]
    private bool changeFogOnEnable;
    [SerializeField]
    private float lerpTime;
    [SerializeField]
    private Color newColor;
    [SerializeField]
    private FogMode newFogMode;
    [Header("Linear")]
    [SerializeField]
    private float newStartDistance; 
    [SerializeField]
    private float newEndDistance; 
    [Header("Exponential")]
    [SerializeField]
    private float newDensity;

    private FogSettings defaultSettings, newSettings;

    private void Awake()
    {
        defaultSettings = new FogSettings(RenderSettings.fogMode, RenderSettings.fogColor, RenderSettings.fogDensity, RenderSettings.fogStartDistance, RenderSettings.fogEndDistance);
        newSettings = new FogSettings(newFogMode, newColor, newDensity, newStartDistance, newEndDistance);
    }
    private void Start()
    {
        //this makes the fog revert properly even if the settings are still lerping
        CheckpointManager.onResetGameStateToLastCheckpoint += StopAllCoroutines;
    }

    private void OnDestroy()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint -= StopAllCoroutines;
    }

    private void OnEnable()
    {
        if (changeFogOnEnable) ChangeFogToNewSettings();
    }
    private void OnDisable() => ResetFogSettings();

    private void OnTriggerEnter(Collider other)
    {
        if (!changeFogOnEnable && other.CompareTag("Player")) ChangeFogToNewSettings();
    }
    private void OnTriggerExit(Collider other)
    {
        if (!changeFogOnEnable && other.CompareTag("Player")) ChangeFogToDefaultSettings();
    }

    private void ChangeFogToNewSettings()
    {
        RenderSettings.fogMode = newSettings.fogMode;
        StopAllCoroutines();
        StartCoroutine(ChangeFog(newSettings));
    }
    private void ChangeFogToDefaultSettings()
    {
        RenderSettings.fogMode = defaultSettings.fogMode;
        StopAllCoroutines();
        StartCoroutine(ChangeFog(defaultSettings));
    }

    private void ResetFogSettings()
    {
        RenderSettings.fogMode = defaultSettings.fogMode;
        RenderSettings.fogColor = defaultSettings.fogColor;
        RenderSettings.fogDensity = defaultSettings.fogDensity;
        RenderSettings.fogStartDistance = defaultSettings.fogStartDistance;
        RenderSettings.fogEndDistance = defaultSettings.fogEndDistance;
    }

    IEnumerator ChangeFog(FogSettings fogSettings)
    {
        float t = 0f;

        Color color;
        Color colorStartValue = RenderSettings.fogColor;
        float density;
        float densityStartValue = RenderSettings.fogDensity;
        float startDistance;
        float startDistanceStartValue = RenderSettings.fogStartDistance;
        float endDistance;
        float endDistanceStartValue = RenderSettings.fogEndDistance;

        while (t < lerpTime)
        {
            color = Color.Lerp(colorStartValue, fogSettings.fogColor, t / lerpTime);
            density = Mathf.Lerp(densityStartValue, fogSettings.fogDensity, t / lerpTime);
            startDistance = Mathf.Lerp(startDistanceStartValue, fogSettings.fogStartDistance, t / lerpTime);
            endDistance = Mathf.Lerp(endDistanceStartValue, fogSettings.fogEndDistance, t / lerpTime);

            RenderSettings.fogColor = color;
            RenderSettings.fogDensity = density;
            RenderSettings.fogStartDistance = startDistance;
            RenderSettings.fogEndDistance = endDistance;

            t += Time.deltaTime;
            yield return null;
        }
        RenderSettings.fogColor = fogSettings.fogColor;
        RenderSettings.fogDensity = fogSettings.fogDensity;
        RenderSettings.fogStartDistance = fogSettings.fogStartDistance;
        RenderSettings.fogEndDistance = fogSettings.fogEndDistance;
    }
}

