using UnityEngine;

public class CodecViewportCamera : ViewportCamera
{
    [SerializeField]
    private NPCFaceManager faceManager;

    public void StartUpdateFaceOnTimer() => faceManager.StartUpdateFaceOnTimer();
    public void ResetFace() => faceManager.ResetFace(true);
}
