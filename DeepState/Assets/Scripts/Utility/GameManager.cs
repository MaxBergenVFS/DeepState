using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    [SerializeField]
    private bool showCursorOnStart;
    [SerializeField]
    private ObjectPool bulletHolePool, bulletTracerPool, bloodPool,
        bulletImpactParticlePool, woodImpactParticlePool, bloodImpactParticlePool;
    [SerializeField]
    private DoorKeyManager doorKeyManager;
    [SerializeField]
    private ModelViewportCamera modelViewportCamera;
    [SerializeField]
    private EquipmentViewportCamera equipmentViewportCamera;
    [SerializeField]
    private CodecViewportCamera codecViewportCameraRex, codecViewportCameraNPC;
    [SerializeField]
    private CheckpointManager checkpointManager;
    [SerializeField]
    private AlertZoneManager alertZoneManager;
    [SerializeField]
    private CollectablesManager collectablesManager;
    [SerializeField]
    private MusicManager musicManager;
    [SerializeField]
    private SecurityMonitorCameraManager securityMonitorCameraManager;
    [SerializeField]
    private GameObject checkpointItemPlacementMarker;

    public ObjectPool BulletHolePool { get { return bulletHolePool; } }
    public ObjectPool BulletImpactParticlePool { get { return bulletImpactParticlePool; } }
    public ObjectPool BulletTracerPool { get { return bulletTracerPool; } }
    public ObjectPool BloodPool { get { return bloodPool; } }
    public ObjectPool WoodImpactParticlePool { get { return woodImpactParticlePool; } }
    public ObjectPool BloodImpactParticlePool { get { return bloodImpactParticlePool; } }
    public DoorKeyManager DoorKeyManager { get { return doorKeyManager; } }
    public ModelViewportCamera ModelViewportCamera { get { return modelViewportCamera; } }
    public EquipmentViewportCamera EquipmentViewportCamera { get { return equipmentViewportCamera; } }
    public CodecViewportCamera CodecViewportCameraRex { get { return codecViewportCameraRex; } }
    public CodecViewportCamera CodecViewportCameraNPC { get { return codecViewportCameraNPC; } }
    public CheckpointManager CheckpointManager { get { return checkpointManager; } }
    public AlertZoneManager AlertZoneManager { get { return alertZoneManager; } }
    public CollectablesManager CollectablesManager { get { return collectablesManager; } }
    public MusicManager MusicManager { get { return musicManager; } }
    public SecurityMonitorCameraManager SecurityMonitorCameraManager { get { return securityMonitorCameraManager; } }
    public GameObject CheckpointItemPlacementMarker { get { return checkpointItemPlacementMarker; } }
    public static GameManager Instance { get { return instance; } }

    public delegate void OnGamePaused();
    public static event OnGamePaused onGamePaused;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        PauseGame(false, showCursorOnStart, false, false);
    }

    public void PauseGame(bool pauseGame, bool unlockCursor, bool playerIsDead, bool inCodecCall)
    {
        PauseGame(pauseGame);
        UnlockCursor(unlockCursor);
        musicManager?.PlayMenuMusic(inCodecCall || pauseGame, playerIsDead, inCodecCall);
    }
    private void PauseGame(bool active)
    {
        if (active)
        {
            Time.timeScale = 0f;
            if (onGamePaused != null) onGamePaused();
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    public void UnlockCursor(bool active)
    {
        Cursor.visible = active;
        if (active) Cursor.lockState = CursorLockMode.None;
        else Cursor.lockState = CursorLockMode.Locked;
    }
}

