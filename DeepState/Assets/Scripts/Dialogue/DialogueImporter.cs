using System.Xml.Linq;
using UnityEngine;

public class DialogueImporter : MonoBehaviour
{
    private void Start()
    {
        TextAsset xmlSourceAsset = null;

        XDocument xmlDoc = null;

        xmlSourceAsset = Resources.Load("XML/Dialogue") as TextAsset;

        if (xmlSourceAsset != null) xmlDoc = XDocument.Parse(xmlSourceAsset.text);

        print($"{xmlDoc.Element("character").Value}");
    }
}
