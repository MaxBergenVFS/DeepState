using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCAttackBehaviour : StateMachineBehaviour
{
    private const float DELAY_BEFORE_ATTACK = 0.75f;
    private const float EXIT_CROUCH_TIME = 0.83f;

    private Transform player, transform;
    private float disengageRange, attackTime, attackTimeRemaining, delayTimeRemaining, attackPhaseTimeRemaining;
    private List<float> attackTimeDurationsRef;
    private NPCController controller;
    private NavMeshAgent agent;
    private bool inAttackPhase;

    [SerializeField]
    private float attackTimeMin = 3f, attackTimeMax = 10f,
        attackPhaseMin = 1f, attackPhaseMax = 3f,
        disengageRangeMin = 15f, disengageRangeMax = 30f,
        repositionChance = 0.4f, strafeChance = 0.7f, crouchChance = 0.2f;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        SetAnimBools(animator);

        player = PlayerController.Instance.transform;
        controller = animator.GetComponent<NPCController>();
        agent = controller.GetComponent<NavMeshAgent>();
        transform = animator.GetComponent<Transform>();

        ResetAttackTime();
        delayTimeRemaining = DELAY_BEFORE_ATTACK;

        controller.ActivateBodyAim(true);
        controller.SetNavSpeedToStop();
        controller.RotateToFacePlayer(true);
        TryEnterCrouch(animator);

        float distanceToPlayer = (transform.position - player.position).magnitude;
        disengageRange = Random.Range(distanceToPlayer + disengageRangeMin, distanceToPlayer + disengageRangeMax);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (controller.OtherNPCInLoS())
        {
            Reposition(animator, false);
            return;
        } //-------------------------------------------------------------------------------------------------------

        transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.position.z));
        attackTimeRemaining -= Time.deltaTime;

        if (delayTimeRemaining > 0f)
        {
            delayTimeRemaining -= Time.deltaTime;
        }
        else
        {
            if (controller.PlayerInLoS())
            {
                Attack(animator);
            }
            else
            {
                Reposition(animator, false);
                return; //-----------------------------------------------------------------------------------------
            }

            if (attackTimeRemaining <= EXIT_CROUCH_TIME) ExitCrouch(animator, false);

            if (attackTimeRemaining < 0f)
            {
                float distanceToPlayer = (transform.position - player.position).magnitude;

                if (distanceToPlayer > disengageRange)
                {
                    Pursue(animator);
                }
                else
                {
                    float reposition = Random.Range(0f, 1f);
                    if (repositionChance > reposition) Reposition(animator, true);
                    else ResetAttackTime();
                }
            }
        }
        controller.RemoveAlertIfPlayerNotInLoS();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller.ActivateBodyAim(false);
        ExitCrouch(animator, true);
    }

    private void ResetAttackTime()
    {
        attackTime = Random.Range(attackTimeMin, attackTimeMax);
        attackTimeRemaining = attackPhaseTimeRemaining = attackTime;
    }
    private void StartNewAttackPhase(bool active)
    {
        attackPhaseTimeRemaining = Random.Range(attackPhaseMin, attackPhaseMax);
        inAttackPhase = active;
    }

    private void SetAnimBools(Animator animator)
    {
        animator.SetBool(AnimParams.NPC_BOOL_IS_WALKING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IN_DIALOGUE, false);
        animator.SetBool(AnimParams.NPC_BOOL_AT_REPOSITION_POINT, false);
        if (!animator.GetBool(AnimParams.NPC_BOOL_ALERTED)) animator.SetBool(AnimParams.NPC_BOOL_ALERTED, true);
    }

    private void Attack(Animator animator)
    {
        if (attackPhaseTimeRemaining >= attackTime) StartNewAttackPhase(true);

        if (inAttackPhase)
        {
            controller.Attack();
            if (controller.DamagedOtherNPC()) Reposition(animator, false);
        }

        if (attackPhaseTimeRemaining > 0f) attackPhaseTimeRemaining -= Time.deltaTime;
        else StartNewAttackPhase(!inAttackPhase);
    }

    private void Reposition(Animator animator, bool randStrafe)
    {
        if (!randStrafe)
        {
            ExitCrouch(animator, true);
            animator.SetBool(AnimParams.NPC_BOOL_IS_REPOSITIONING, true);
            return;
        } //-----------------------------------------------------------

        float randVal = Random.Range(0f, 1f);
        if (randVal < strafeChance) animator.SetBool(AnimParams.NPC_BOOL_IS_STRAFING, true);
        else animator.SetBool(AnimParams.NPC_BOOL_IS_REPOSITIONING, true);
    }
    private void Pursue(Animator animator)
    {
        if (PathToPlayerExists()) animator.SetBool(AnimParams.NPC_BOOL_IN_ATTACK_RANGE, false);
    }

    private void TryEnterCrouch(Animator animator)
    {
        float randVal = Random.Range(0f, 1f);
        if (randVal < crouchChance) animator.SetBool(AnimParams.NPC_BOOL_IS_CROUCHING, true);
    }
    private void ExitCrouch(Animator animator, bool skipUncrouchAnim)
    {
        if (!animator.GetBool(AnimParams.NPC_BOOL_IS_CROUCHING)) return; //-----------

        animator.SetBool(AnimParams.NPC_BOOL_IS_CROUCHING, false);
        if (skipUncrouchAnim) animator.SetTrigger(AnimParams.NPC_TRIGGER_SKIP_UNCROUCH);
    }

    private bool PathToPlayerExists()
    {
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(player.position, path);
        return path.status == NavMeshPathStatus.PathComplete;
    }
}
