﻿using UnityEngine;

public class PauseAnimation : MonoBehaviour
{
    private const string ANIM_SPEED_MULTIPLIER = "speedMultiplier";
    protected Animator anim;

    protected void Awake() => anim = GetComponent<Animator>();

    public void PauseAnim() => anim.enabled = false;
    public void UnPauseAnim() => anim.enabled = true;
    public void ReverseAnim() => anim.SetFloat(ANIM_SPEED_MULTIPLIER, -1.0f);

    public bool CheckIfPaused() { return !anim.enabled; }

    public void ResetAnim()
    {
        if (!anim.enabled) anim.enabled = true;
        if (anim.GetFloat(ANIM_SPEED_MULTIPLIER) != 1.0f) anim.SetFloat(ANIM_SPEED_MULTIPLIER, 1.0f);
    }
}

