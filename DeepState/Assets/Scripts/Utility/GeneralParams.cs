public static class GeneralParams
{
    #region tags
    public const string TAG_PLAYER = "Player";
    #endregion
    
    #region layers
    public const string LAYER_DEFAULT = "Default";
    public const string LAYER_TRANSPARENT_FX = "TransparentFX";
    public const string LAYER_IGNORE_RAYCAST = "Ignore Raycast";
    public const string LAYER_DYNAMIC = "Dynamic";
    public const string LAYER_WATER = "Water";
    public const string LAYER_UI = "UI";
    public const string LAYER_PLAYER = "Player";
    public const string LAYER_PLAYER_BODY = "PlayerBody";
    public const string LAYER_DIALOGUE_BOX = "DialogueBox";
    public const string LAYER_INTERACTABLE_ITEM = "InteractableItem";
    public const string LAYER_LIGHTPOST = "Lightpost";
    public const string LAYER_CLOUDS = "Clouds";
    public const string LAYER_INTERIOR = "Interior";
    #endregion

    #region calibrations
    public const float GAMMA_MAX = 1f;
    public const float GAMMA_MIN = -1f;
    public const float DEFAULT_GAMMA_SLIDER_VAL = 0.5f;
    public const float MOUSE_SENSITIVITY_MIN = 1f;
    public const float DEFAULT_MOUSE_SENSITIVITY_SLIDER_VAL = 0.5f;
    #endregion
}
