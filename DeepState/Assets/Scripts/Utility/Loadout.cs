using System.Collections.Generic;
using UnityEngine;

public class Loadout : MonoBehaviour
{
    private const float INTERACT_DELAY = 0.25f;

    [SerializeField]
    private BaseItem[] loadoutItems;
    [SerializeField]
    private Checkpoint checkpointToActivate;

    private List<BaseItem> items = new List<BaseItem>();
    private PlayerInventory playerInventory;

    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        Vector3 spawnPos = PlayerController.Instance.transform.position;
        foreach (BaseItem item in loadoutItems)
        {
            items.Add(Instantiate(item, spawnPos, Quaternion.identity));
        }
        Invoke("HandleLoadout", INTERACT_DELAY);
    }

    private void HandleLoadout()
    {
        foreach (BaseItem item in items)
        {
            item.Interact();
            playerInventory.AddItemToGridFromLoadout(item);
        }
        if (checkpointToActivate != null) checkpointToActivate.ActivateThisCheckpoint();
    }
}
