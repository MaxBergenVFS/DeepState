using UnityEngine;

public class BloodPool : MonoBehaviour
{
    private BloodPoolSpread spread;
    private Renderer rend;

    private void Awake()
    {
        spread = GetComponent<BloodPoolSpread>();
        rend = GetComponent<Renderer>();
    }

    private void Start()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint += Hide;
    }

    private void OnDestroy()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint -= Hide;
    }

    public void Reset()
    {
        rend.enabled = true;
        spread.enabled = false;
        spread.enabled = true;
    }

    private void Hide()
    {
        rend.enabled = false;
    }
}
