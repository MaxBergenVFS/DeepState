using UnityEngine;

public class EnemyGun : Gun
{
    private const Enums.damageType DAMAGE_TYPE = Enums.damageType.RangedNPC;

    private Transform player, gunToDropParent;
    private GameObject gunToDrop, gunToDisableOnDeath;
    private BaseItem gunToDropItemRef;
    private Vector3 gunToDropPos = new Vector3();
    private Quaternion gunToDropRot = new Quaternion();
    private enum enemyGunType
    {
        None,
        AK,
        M4
    }

    [SerializeField]
    private enemyGunType gunType;
    [SerializeField]
    private GameObject akPickupable, akModel, m4Pickupable, m4Model;
    [SerializeField]
    private ParticleSystem akMuzzleFlash, m4MuzzleFlash;

    protected override void Awake()
    {
        base.Awake();
        damageType = DAMAGE_TYPE;
    }
    protected override void Start()
    {
        base.Start();
        player = PlayerController.Instance.transform;
        InitGunToDropAndDisable();
    }

    private void InitGunToDropAndDisable()
    {
        if (akPickupable != null) akPickupable.SetActive(false);
        if (akModel != null) akModel.SetActive(false);
        if (m4Pickupable != null) m4Pickupable.SetActive(false);
        if (m4Model != null) m4Model.SetActive(false);
        switch (gunType)
        {
            case (enemyGunType.AK):
                gunToDrop = akPickupable;
                gunToDisableOnDeath = akModel;
                akModel.SetActive(true);
                InitGunToDrop(akPickupable);
                sfx.SetGunType(Enums.gunType.AK);
                break;
            case (enemyGunType.M4):
                gunToDrop = m4Pickupable;
                gunToDisableOnDeath = m4Model;
                m4Model.SetActive(true);
                InitGunToDrop(m4Pickupable);
                sfx.SetGunType(Enums.gunType.M4);
                break;
            case (enemyGunType.None):
            default:
                Debug.Log($"Missing gun ref on {this.gameObject.name}");
                break;
        }
    }
    private void InitGunToDrop(GameObject gun)
    {
        if (gun == null) return; //-------------------

        gunToDropParent = gun.transform.parent;
        gunToDropPos = gun.transform.localPosition;
        gunToDropRot = gun.transform.localRotation;
        if (gun.GetComponent<BaseItem>() != null) gunToDropItemRef = gun.GetComponent<BaseItem>();
        else if (gun.GetComponentInChildren<BaseItem>() != null) gunToDropItemRef = gun.GetComponentInChildren<BaseItem>();
    }

    protected override Vector3 GetDirection()
    {
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        return (player.position - origin.position).normalized + new Vector3(x, y, 0);
    }

    public RaycastHit CheckLineOfSight()
    {
        RaycastHit hit;
        Vector3 dir = (player.position - origin.position).normalized;
        Physics.Raycast(origin.position, dir, out hit, 1000, whatIsDamagable);
        return hit;
    }

    public void DropGun()
    {
        gunToDisableOnDeath.SetActive(false);
        gunToDrop.SetActive(true);
        gunToDrop.transform.parent = null;
        gunToDropItemRef?.BypassCheckpoint();
        gunToDropItemRef?.EnableCol();
    }
    public void ResetGun()
    {
        gunToDisableOnDeath.SetActive(true);
        if (gunToDrop != null)
        {
            gunToDrop.transform.parent = gunToDropParent;
            gunToDrop.transform.localPosition = gunToDropPos;
            gunToDrop.transform.localRotation = gunToDropRot;
            gunToDrop.SetActive(false);
        }
    }

    protected override void MuzzleFlash()
    {
        switch (gunType)
        {
            case (enemyGunType.AK):
                akMuzzleFlash.Play();
                break;
            case (enemyGunType.M4):
                m4MuzzleFlash.Play();
                break;
            case (enemyGunType.None):
            default:
                Debug.Log($"Missing gun ref on {this.gameObject.name}");
                break;
        }
    }
}
