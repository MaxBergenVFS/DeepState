using UnityEngine;

public class PadUI : MonoBehaviour
{
    [SerializeField]
    protected GameObject background;

    private InputManager inputManager;
    private PlayerCameraManager playerCameraManager;

    public delegate void OnDisplayPadUI(bool active);
    public static event OnDisplayPadUI onDisplayPadUI;

    protected virtual void Awake()
    {
        background.SetActive(false);
    }

    protected virtual void Start()
    {
        inputManager = PlayerController.Instance.GetComponent<InputManager>();
        playerCameraManager = PlayerController.Instance.GetComponent<PlayerCameraManager>();
        PlayerHealth.onHealthChanged += HideOnHealthChanged;
    }

    private void HideOnHealthChanged(int amt) => Hide();

    public virtual void Hide()
    {
        if (!background.activeSelf) return; //---------------

        GameManager.Instance.PauseGame(false, false, false, false);
        background.SetActive(false);
        playerCameraManager.ActivateHeadBob(true);
        inputManager.enabled = true;
        if (onDisplayPadUI != null) onDisplayPadUI(false);
    }

    public virtual void Show()
    {
        if (background.activeSelf) return; //----------------------

        playerCameraManager.ActivateHeadBob(false);
        background.SetActive(true);
        inputManager.enabled = false;
        GameManager.Instance.PauseGame(false, true, false, false);
        if (onDisplayPadUI != null) onDisplayPadUI(true);
    }

    private void OnDestroy()
    {
        PlayerHealth.onHealthChanged -= HideOnHealthChanged;
    }
}
