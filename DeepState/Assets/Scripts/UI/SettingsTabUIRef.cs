using UnityEngine;
using UnityEngine.UI;

public class SettingsTabUIRef : MonoBehaviour
{
    [SerializeField]
    private Slider gamma, mouseSensitivity, musicVolume;
    [SerializeField]
    private GameObject toggleSprintCheckmark;

    public Slider Gamma { get { return gamma; } }
    public Slider MouseSensitivity { get { return mouseSensitivity; } }
    public Slider MusicVolume { get { return musicVolume; } }
    public GameObject ToggleSprintCheckmark { get { return toggleSprintCheckmark; } }
}
