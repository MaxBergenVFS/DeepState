using System;
using UnityEngine;

public interface IInventoryItem
{
    string Name { get; }
    string ShortenedName { get; }
    int AmountInt { get; }
    string AmountStr { get; }
    string Description { get; }
    string CriticalInfo { get; }
    bool IsConsumable { get; }
    bool IsEquippable { get; }
    bool IsEquipped { get; }
    Sprite Image { get; }
    Enums.itemType ItemType { get; }
    void OnPickup();
    void OnDrop();
    void UseFromInventory(out bool success);
}

public class InventoryEventArgs : EventArgs
{
    public InventoryEventArgs(IInventoryItem item)
    {
        Item = item;
    }

    public IInventoryItem Item;
}
