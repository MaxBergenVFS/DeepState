using UnityEngine;

public class PlayerHealth : Health
{
    private const float PAIN_SOUND_DISABLE_TIME = 0.5f;

    [SerializeField]
    private int maxArmor = 50;
    [SerializeField]
    private ScreenShake screenShake;
    [SerializeField]
    private AlphaFadeOnEnable damageTakenUI;

    private PlayerController playerController;
    private PlayerSFX sfx;
    private int remainingArmor;
    private bool painSoundDisabled;

    public bool AtMaxArmor { get { return (remainingArmor >= maxArmor); } }
    public int MaxArmor { get { return maxArmor; } }
    public int RemainingArmor { get { return remainingArmor; } }

    public delegate void OnHealthChanged(int amt);
    public static event OnHealthChanged onHealthChanged;
    public delegate void OnArmorChanged(int amt);
    public static event OnArmorChanged onArmorChanged;

    protected override void Awake()
    {
        base.Awake();
        playerController = GetComponent<PlayerController>();
        sfx = GetComponent<PlayerSFX>();
        painSoundDisabled = false;
    }

    public override void DecreaseRemainingHealth(int amt)
    {
        if (invincible || amt < 1) return; //---------------------------

        TakeDamageFeedback(amt);
        
        int damageTaken = amt;

        if (remainingArmor > 0)
        {
            int newArmorVal = remainingArmor - amt;

            if (newArmorVal >= 0)
            {
                remainingArmor = newArmorVal;
                damageTaken = 0;
            }
            else
            {
                remainingArmor = 0;
                damageTaken -= newArmorVal;
            }

            if (onArmorChanged != null) onArmorChanged(remainingArmor);
        }

        base.DecreaseRemainingHealth(damageTaken);

        if (onHealthChanged != null) onHealthChanged(remainingHealth);

        if (remainingHealth <= 0)
        {
            playerController.Die();
            sfx.PlayDeathSound();
        }
        else
        {
            StartInvinciblityTimer();
        }
    }

    public void IncreaseArmor(int amt)
    {
        if (remainingArmor >= maxArmor) return; //---------------

        remainingArmor += amt;
        if (remainingArmor > maxArmor) remainingArmor = maxArmor;

        if (onArmorChanged != null) onArmorChanged(remainingArmor);
    }

    public void IncreaseHealth(int amt)
    {
        if (AtMaxHealth) return; //------------------

        base.DecreaseRemainingHealth(-amt);

        if (onHealthChanged != null) onHealthChanged(remainingHealth);
    }

    private void StopDamageFeedback()
    {
        damageTakenUI.enabled = false;
        screenShake.Shake(false);
    }
    private void TakeDamageFeedback(float amt)
    {
        damageTakenUI.enabled = true;
        screenShake.Shake(true, amt);
        HandlePainSound();
    }

    private void HandlePainSound()
    {
        if (painSoundDisabled) return; //---------
        sfx.PlayPainSound();
        painSoundDisabled = true;
        Invoke("ResetPainSound", PAIN_SOUND_DISABLE_TIME);
    }
    private void ResetPainSound()
    {
        painSoundDisabled = false;
    }

    public override void SetHealthToFull()
    {
        base.SetHealthToFull();
        StopDamageFeedback();

        if (onHealthChanged != null) onHealthChanged(remainingHealth);
    }

    public override void SetRemainingHealth(int amt)
    {
        base.SetRemainingHealth(amt);

        if (onHealthChanged != null) onHealthChanged(remainingHealth);
    }
    public void SetRemainingArmor(int amt)
    {
        if (amt < 0) remainingArmor = 0;
        else if (amt > maxArmor) remainingArmor = maxArmor;
        else remainingArmor = amt;

        if (onArmorChanged != null) onArmorChanged(remainingArmor);
    }
}
