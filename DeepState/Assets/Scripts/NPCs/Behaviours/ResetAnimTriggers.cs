using UnityEngine;

public class ResetAnimTriggers : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_FLINCH_BODY_RIGHT);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_FLINCH_BODY_LEFT);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_FLINCH_SHOULDER_RIGHT);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_FLINCH_SHOULDER_LEFT);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_FLINCH_LEG_RIGHT);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_FLINCH_LEG_LEFT);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_BULLET_FLY_BY);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_RUN_FOR_ALARM);
        animator.ResetTrigger(AnimParams.NPC_TRIGGER_SKIP_UNCROUCH);
    }

    // override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {

    // }

    // override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {

    // }
}
