﻿using UnityEngine;

public class HeadBob : MonoBehaviour
{
    [SerializeField]
    private float baseSpeed = 4f, baseAmount = 0.01f;

    private float defaultPos, standingPos, timer, speedMultiplier;

    private void Awake()
    {
        defaultPos = transform.localPosition.y;
        standingPos = defaultPos;
    }

    private void Update()
    {
        Bob();
    }

    public void SetCameraCrouchHeight(float val)
    {
        defaultPos = val;
    }
    public void SetCameraCrouchHeight(float val, float lerpVal)
    {
        defaultPos = Mathf.Lerp(defaultPos, val, lerpVal);
    }
    public void SetCameraStandingHeight()
    {
        defaultPos = standingPos;
    }
    public void SetCameraStandingHeight(float lerpVal)
    {
        defaultPos = Mathf.Lerp(defaultPos, standingPos, lerpVal);
    }

    private void Bob()
    {
        timer += Time.deltaTime * baseSpeed * speedMultiplier;
        transform.localPosition = new Vector3
        (
            transform.localPosition.x,
            defaultPos + Mathf.Sin(timer) * baseAmount,
            transform.localPosition.z
        );
    }

    public void SetSpeed(bool active, float speed)
    {
        if (active && speedMultiplier != speed) speedMultiplier = speed;
        else if (!active && speedMultiplier != 1f) speedMultiplier = 1f;
    }
}
