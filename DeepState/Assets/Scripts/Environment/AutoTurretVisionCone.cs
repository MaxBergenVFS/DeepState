using UnityEngine;

public class AutoTurretVisionCone : MonoBehaviour
{
    private AutoTurret autoTurretRef;
    private Transform player, observed;

    public enum autoTurretTargetType
    {
        None,
        Player,
        NPC
    };

    private autoTurretTargetType target;

    private void Start()
    {
        player = PlayerController.Instance.transform;
    }

    public void SetAutoTurretRef(AutoTurret autoTurret)
    {
        autoTurretRef = autoTurret;
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (target)
        {
            case (autoTurretTargetType.Player):
                if (other.CompareTag(GeneralParams.TAG_PLAYER)) SetObserved(player);
                break;
            case (autoTurretTargetType.NPC):
                NPCHitbox npc = other.GetComponent<NPCHitbox>();
                if (npc != null) TrySetNPCAsObserved(npc.transform);
                break;
            case (autoTurretTargetType.None):
            default:
                return;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        switch (target)
        {
            case (autoTurretTargetType.Player):
                if (other.CompareTag(GeneralParams.TAG_PLAYER)) ResetAutoTurret();
                break;
            case (autoTurretTargetType.NPC):
                if (other.GetComponent<NPCHitbox>()) ResetAutoTurret();
                break;
            case (autoTurretTargetType.None):
            default:
                return;
        }
    }

    private void Update()
    {
        if (observed == null || target == autoTurretTargetType.None) return; //---------------
        autoTurretRef.AttackTarget(observed);
    }

    private void SetObserved(Transform target)
    {
        observed = target;
    }
    private void ResetObserved()
    {
        observed = null;
    }
    private void TrySetNPCAsObserved(Transform npc)
    {
        if (observed == null || observed != ReturnCloserTransform(observed, npc)) SetObserved(npc);
    }

    private Transform ReturnCloserTransform(Transform t1, Transform t2)
    {
        float t1Dist = Vector3.Distance(t1.position, this.transform.position);
        float t2Dist = Vector3.Distance(t2.position, this.transform.position);
        if (t1Dist < t2Dist) return t1;
        return t2;
    }

    private void ResetAutoTurret()
    {
        ResetObserved();
        if (!autoTurretRef.IsDeactivated) autoTurretRef.PauseAnim(false);
    }

    public void SetPlayerTarget()
    {
        ResetObserved();
        target = autoTurretTargetType.Player;
    }
    public void SetNPCTarget()
    {
        ResetObserved();
        target = autoTurretTargetType.NPC;
    }
    public void ResetTarget()
    {
        ResetObserved();
        target = autoTurretTargetType.None;
    }
}
