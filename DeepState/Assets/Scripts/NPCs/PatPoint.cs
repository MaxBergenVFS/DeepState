using UnityEngine;

public class PatPoint : MonoBehaviour
{
    [SerializeField]
    private float idleTimeOverride;
    [SerializeField]
    private Transform rotationTarget;

    public float IdleTimeOverride { get { return idleTimeOverride; } }
    public Transform RotationTarget { get { return rotationTarget; } }
}
