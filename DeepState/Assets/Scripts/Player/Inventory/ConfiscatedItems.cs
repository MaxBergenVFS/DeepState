using System.Collections.Generic;

public class ConfiscatedItems : ItemContainer
{
    private Dictionary<int, int> confiscatedItemsDict = new Dictionary<int, int>();
    private List<IInventoryItem> confistactedItemsList = new List<IInventoryItem>();

    private void Awake()
    {
        containsItems = false;
    }

    public void AddItemToDict(int id, int amt)
    {
        confiscatedItemsDict.Add(id, amt);
        if (!containsItems) containsItems = true;
    }

    public void AddItemsToList(IInventoryItem item)
    {
        confistactedItemsList.Add(item);
    }

    public override void ReturnItemsToPlayer()
    {
        foreach (KeyValuePair<int, int> item in confiscatedItemsDict)
        {
            int id = item.Key;
            int amt = item.Value;
            inventoryItemDatabase.AddToInventoryItemAmount(id, amt);
        }
        foreach (IInventoryItem item in confistactedItemsList)
        {
            playerInventory.AddItemToGrid(item);
        }
    }
}
