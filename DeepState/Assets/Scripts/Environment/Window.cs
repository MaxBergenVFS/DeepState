using UnityEngine;

public class Window : MonoBehaviour, IDamageable, IRespawnable
{
    [SerializeField]
    private WindowShard[] shards;
    [SerializeField]
    private GameObject intact, broken;

    private Collider col;
    private ObjectSFX sfx;
    private CheckpointManager checkpointManager;

    private void Awake()
    {
        col = GetComponent<Collider>();
        sfx = GetComponent<ObjectSFX>();
    }
    private void Start()
    {
        checkpointManager = GameManager.Instance.CheckpointManager;
    }

    public void TakeDamage(int amt, Vector3 pos, float force, Enums.damageType type)
    {
        if (amt < 1) return;

        checkpointManager.AddRespawnable(this);
        col.enabled = false;
        intact.SetActive(false);
        broken.SetActive(true);
        sfx.PlaySound(shards[0].transform.position);
        foreach(var shard in shards)
        {
            shard.AddForce(pos, force);
        }
    }

    public void Respawn()
    {
        foreach(var shard in shards)
        {
            shard.Reset();
        }
        intact.SetActive(true);
        broken.SetActive(false);
        col.enabled = true;
    }
}
