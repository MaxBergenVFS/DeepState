using UnityEngine;

[RequireComponent(typeof(ObjectSFX))]
public class DestructibleObject : MonoBehaviour, IDamageable, IInteractable, IRespawnable
{
    protected const string PARAM_DARKNESS = "_Darkness";

    [SerializeField]
    protected float darknessAmount = 0.25f;
    [SerializeField]
    private GameObject particleEffect;
    [SerializeField]
    private Transform particleSpawnPosOverride;
    [SerializeField]
    private string notificationText;
    [SerializeField]
    protected string itemName;

    protected Collider col;
    protected Material mat;
    protected ObjectSFX sfx;

    private bool breakOnNextHit;
    private Renderer rend;
    private Rigidbody rb;
    private NotificationUI notificationUI;
    private CheckpointManager checkpointManager;
    private Transform player;

    public string Name { get { return itemName; } }

    protected virtual void Awake()
    {
        col = GetComponent<Collider>();
        rend = GetComponent<Renderer>();
        sfx = GetComponent<ObjectSFX>();
        if (GetComponent<Rigidbody>() != null) rb = GetComponent<Rigidbody>();
        mat = rend.material;
    }
    protected virtual void Start()
    {
        player = PlayerController.Instance.transform;
        notificationUI = NotificationUI.Instance;
        checkpointManager = GameManager.Instance.CheckpointManager;
    }

    public void TakeDamage(int amt, Vector3 pos, float force, Enums.damageType type)
    {
        if (amt < 1) return; //---------------------------------------

        SpawnImpactParticle(pos);
        if (breakOnNextHit)
        {
            TakeDamageAndBreak();
            checkpointManager.AddRespawnable(this);
        }
        else
        {
            TryAddForce(force);
            TakeDamageAndDontBreak();
        }
    }

    protected void TryAddForce(float force)
    {
        if (rb == null) return; //------------
        Vector3 dir = transform.position - player.position;
        rb.AddForce(dir * force);
    }

    private void TakeDamageAndDontBreak()
    {
        breakOnNextHit = true;
        mat.SetFloat(PARAM_DARKNESS, darknessAmount);
    }
    protected virtual void TakeDamageAndBreak()
    {
        Vector3 spawnPos = new Vector3();
        if (particleSpawnPosOverride != null) spawnPos = particleSpawnPosOverride.position;
        else spawnPos = transform.position;

        Instantiate(particleEffect, spawnPos, Quaternion.identity);
        col.enabled = false;
        this.gameObject.SetActive(false);
        sfx.PlaySound();
    }

    public virtual void Respawn()
    {
        breakOnNextHit = false;
        mat.SetFloat(PARAM_DARKNESS, 0f);
        this.gameObject.SetActive(true);
        col.enabled = true;
    }

    protected virtual void SpawnImpactParticle(Vector3 pos) { }

    public void Interact()
    {
        notificationUI.DisplayText(notificationText);
    }
}
