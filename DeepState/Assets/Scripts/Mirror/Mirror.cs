using UnityEngine;

public class Mirror : ReflectiveSurface
{
    private const float Y_AXIS_OFFSET = 1.7f;

    [SerializeField]
    private MirrorReflectionPoint reflectionPoint;

    private Transform reflectionPointTransform;

    protected override void Start()
    {
        base.Start();
        reflectionPointTransform = reflectionPoint.transform;
        reflectionPoint.Init();
        reflectionPoint.enabled = false;
    }

    protected override void SetMirroredRigPos()
    {
        float x = reflectionPoint.GetMirroredXAxis();
        float y = player.position.y - Y_AXIS_OFFSET;
        float z = reflectionPoint.GetMirroredZAxis();
        mirroredRigTransform.position = new Vector3(x, y, z);
    }

    protected override void SetMirroredRigRot()
    {
        mirroredRigTransform.rotation = reflectionPoint.GetMirroredRot();
    }

    protected override void OnPlayerEnter()
    {
        base.OnPlayerEnter();
        reflectionPoint.enabled = true;
    }

    protected override void OnPlayerExit()
    {
        base.OnPlayerExit();
        reflectionPoint.enabled = false;
    }

}
