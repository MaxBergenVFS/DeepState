using UnityEngine;

public class DatapadInput : InputOverride
{
    [SerializeField]
    private DatapadUI datapadUI;

    private void Update()
    {
        if (InputManagerIsEnabled) return; //---------------------------------

        if (Input.GetButtonDown(INPUT_CANCEL) || Input.GetButtonDown(INPUT_INTERACT)) datapadUI.Hide();
    }
}
