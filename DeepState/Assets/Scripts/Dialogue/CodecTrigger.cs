using UnityEngine;

public class CodecTrigger : DialogueTrigger
{
    private const string REX_NAME_LOWER = "rex";

    private CodecViewportCamera codecViewportCameraNPC, codecViewportCameraRex;

    [SerializeField]
    private int num;

    protected override void Start()
    {
        base.Start();
        codecViewportCameraNPC = GameManager.Instance.CodecViewportCameraNPC;
        codecViewportCameraRex = GameManager.Instance.CodecViewportCameraRex;
        PersistentValues.Instance.InitCodecCall(num);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (PersistentValues.Instance.CodecCallHasBeenTriggered(num)) return; //----------

            dialogueManager.StartCodecCall(this);
            PersistentValues.Instance.TriggerCodecCall(num);
            TriggerDialogue();
        }
    }

    public void NextCodecDialogue() => TriggerDialogue();

    protected override void TriggerDialogue()
    {
        dialogueManager.StartCodecDialogue(dialogue[currentIndex]);

        if (dialogue[currentIndex].characterName.ToLower() == REX_NAME_LOWER)
        {
            //REX IS TALKING
            codecViewportCameraRex.StartUpdateFaceOnTimer();
        }
        else
        {
            //NPC IS TALKING
            codecViewportCameraNPC.StartUpdateFaceOnTimer();
        }

        if (!dialogueManager.CurrentlyInDialogue)
        {
            if (currentIndex >= dialogue.Length - 1)
            {
                //FINISH CODEC CALL
                dialogueManager.EndCodecCall();
                codecViewportCameraNPC.ResetFace();
                codecViewportCameraRex.ResetFace();
                this.gameObject.SetActive(false);
            }
            else
            {
                //FINISH CURRENT DIALOGUE AND GET NEXT DIALOGUE
                currentIndex++;
                NextCodecDialogue();
            }
        }
    }
}
