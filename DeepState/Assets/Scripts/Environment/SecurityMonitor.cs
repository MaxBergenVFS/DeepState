using UnityEngine;

public class SecurityMonitor : MonoBehaviour, IInteractable
{
    private const string NAME = "Security Monitor";

    [SerializeField]
    private GameObject[] securityFeeds;
    [SerializeField]
    private GameObject noFeed;
    [SerializeField]
    private Material[] renderMaterials;
    [SerializeField]
    private RenderTexture[] renderTextures;
    [SerializeField]
    private SecurityCamera[] cameras;

    private int currentFeedIndex;
    private SecurityMonitorCameraManager securityMonitorCameraManager;

    public string Name { get { return NAME; } }

    private void Awake()
    {
        currentFeedIndex = 0;
    }
    private void Start()
    {
        securityMonitorCameraManager = GameManager.Instance.SecurityMonitorCameraManager;
        for (int i = 0; i < cameras.Length; i++)
        {
            securityFeeds[i].GetComponent<Renderer>().material = renderMaterials[i];
            cameras[i].SetSecurityFeed(renderTextures[i]);
        }
    }

    public void Interact()
    {
        ForceEnableManager();
        securityMonitorCameraManager.SetSecurityMonitor(this);
        DisplayCurrentFeed();
    }

    private void ForceEnableManager()
    {
        if (!securityMonitorCameraManager.gameObject.activeSelf) securityMonitorCameraManager.gameObject.SetActive(true);
        if (!securityMonitorCameraManager.isActiveAndEnabled) securityMonitorCameraManager.enabled = true;
    }

    public void DisplayNextFeed()
    {
        ClearFeeds();
        IncrementFeedIndex();
        DisplayCurrentFeed();
    }
    public void DisplayPreviousFeed()
    {
        ClearFeeds();
        DecrementFeedIndex();
        DisplayCurrentFeed();
    }
    private void IncrementFeedIndex()
    {
        currentFeedIndex++;
        if (currentFeedIndex >= cameras.Length) currentFeedIndex = 0;
    }
    private void DecrementFeedIndex()
    {
        currentFeedIndex--;
        if (currentFeedIndex < 0) currentFeedIndex = cameras.Length - 1;
    }

    public void DisplayCurrentFeed() => DisplayCurrentFeed(true);
    public void DisplayCurrentFeedOnMonitor() => DisplayCurrentFeed(false);
    private void DisplayCurrentFeed(bool fullscreen)
    {
        //camera is destoyed
        if (cameras[currentFeedIndex].IsDestroyed)
        {
            if (fullscreen) securityMonitorCameraManager.EnableCanvas();
            securityMonitorCameraManager.DisplayNoFeed(true);
            noFeed.SetActive(true);
        }
        else //camera is NOT destroyed
        {
            if (noFeed.activeSelf) noFeed.SetActive(false);
            securityMonitorCameraManager.DisplayNoFeed(false);
            cameras[currentFeedIndex].EnableCam(true, fullscreen, currentFeedIndex);
            securityFeeds[currentFeedIndex].SetActive(true);
        }
    }

    public void ClearFeeds()
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            securityFeeds[i].SetActive(false);
            cameras[i].EnableCam(false, false, currentFeedIndex);
        }
    }

    public AutoTurretManager GetAutoTurretRef()
    {
        return cameras[currentFeedIndex].GetComponent<AutoTurretManager>();
    }
}
