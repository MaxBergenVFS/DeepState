using System.Collections;
using UnityEngine.Animations.Rigging;
using UnityEngine;

public class NPCMultiAimConstraintController : MonoBehaviour
{
    [SerializeField]
    private MultiAimConstraint headAim, bodyAim, bodyAimHip;
    [SerializeField]
    private float headAimSpeed = 0.5f, bodyAimSpeed = 0.1f;

    private PlayerTargetManager playerTarget;

    private void Start()
    {
        playerTarget = PlayerController.Instance.GetComponent<PlayerTargetManager>();
        InitSourceObject(headAim, playerTarget.HeadTarget);
        InitSourceObject(bodyAim, playerTarget.BodyTarget);
        if (bodyAimHip == null) bodyAimHip = bodyAim;
        else InitSourceObject(bodyAimHip, playerTarget.BodyTarget);
    }

    private void InitSourceObject(MultiAimConstraint constraint, Transform sourceObject)
    {
        var sourceObjects = constraint.data.sourceObjects;
        sourceObjects.SetTransform(0, sourceObject);
        constraint.data.sourceObjects = sourceObjects;
    }

    public void ActivateHeadAim(bool active) => ActivateConstraint(active, headAim, headAimSpeed);
    public void ActivateBodyAim(bool active) => ActivateConstraint(active, bodyAim, bodyAimSpeed);
    public void ActivateBodyAimHip(bool active) => ActivateConstraint(active, bodyAimHip, bodyAimSpeed);
    private void ActivateConstraint(bool active, MultiAimConstraint constraint, float speed)
    {
        float val;
        if (active) val = 1f;
        else val = 0f;
        StartCoroutine(LerpAimWeight(constraint, val, speed));
    }
    private IEnumerator LerpAimWeight(MultiAimConstraint constraint, float target, float speed)
    {
        if (constraint.weight == target) yield break; //--------------------------------------

        float start = constraint.weight;
        float t = 0;
        while (t < 1f)
        {
            float val = Mathf.Lerp(start, target, t);
            t += Time.deltaTime * speed;
            constraint.weight = val;
            yield return null;
        }
        constraint.weight = target;
    }
}
