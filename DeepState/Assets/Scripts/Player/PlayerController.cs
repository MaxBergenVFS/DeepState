using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour, IDamageable
{
    #region inits
    private const float MOVEMENT_CHECKER_THRESHOLD = 0.05f;
    private const float CROUCH_LERP_SPEED = 8f;

    [SerializeField]
    private float movementSpeed = 5f, jumpSpeed = 1f, gravity = 20f, sprintMultiplier = 2f,
        crouchSpeedMultiplier = 0.75f, backwardsMovementSpeedMultiplier = 0.75f, crouchCheckDistance = 1f,
        xOffset = 1f, zOffset = 1f, crouchHeightMultiplier = 0.5f, cameraCrouchHeight = 0.05f;

    private CharacterController characterController;
    private CameraController cameraController;
    private PlayerCameraManager cameraManager;
    private PlayerFootstep playerFootstep;
    private PlayerInventory playerInventory;
    private HeadBob headBob;
    private PlayerHealth playerHealth;
    private CheckpointManager checkpointManager;
    private PauseGame pauseGame;
    private float verticalVelocity, defaultSpeed, sprintSpeed, crouchSpeed, charContHeight,
        crouchedHeight, currentCoolDownTime, uncrouchLerpValue, crouchLerpValue, axisH, axisV;
    private bool alreadySprinting, alreadyJumping, uncrouching, uncrouchLerp, isCrouching, isSprinting,
        jumping, climbingLadder, isDead, inShadows;

    public bool IsDead { get { return isDead; } }
    public bool IsGrounded { get { return characterController.isGrounded; } }
    public bool IsCrouching { get { return isCrouching; } }
    public bool InShadows { get { return inShadows; } }
    public bool HasMoved { get { return PlayerHasMoved(); } }
    public float AxisH { get { return axisH; } }
    public float AxisV { get { return axisV; } }
    public float MaxMovementSpeed { get { return sprintSpeed; } }
    public float MovementSpeed
    {
        get
        {
            if (PlayerHasMoved()) return movementSpeed;
            return 0f;
        }
    }

    private static PlayerController instance;
    public static PlayerController Instance { get { return instance; } }

    public delegate void OnPlayerDeath(bool isDead);
    public static event OnPlayerDeath onPlayerDeath;
    public delegate void OnMovementSpeedUpdate(float speed);
    public static event OnMovementSpeedUpdate onMovementSpeedUpdate;
    #endregion

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        cameraController = GetComponent<CameraController>();
        cameraManager = GetComponent<PlayerCameraManager>();
        playerFootstep = GetComponent<PlayerFootstep>();
        headBob = GetComponentInChildren<HeadBob>();
        playerHealth = GetComponent<PlayerHealth>();
        pauseGame = GetComponent<PauseGame>();
        playerInventory = GetComponent<PlayerInventory>();
        instance = this;
        charContHeight = characterController.height;
        crouchedHeight = charContHeight * crouchHeightMultiplier;
        defaultSpeed = movementSpeed;
        sprintSpeed = movementSpeed * sprintMultiplier;
        crouchSpeed = movementSpeed * crouchSpeedMultiplier;
        isDead = climbingLadder = inShadows = false;
    }
    private void Start()
    {
        checkpointManager = GameManager.Instance.CheckpointManager;
    }

    private void Update()
    {
        SetFootstepSFXState();
        if (IsGrounded)
        {
            if (PlayerHasMoved(true))
            {
                playerFootstep.SetFootstepSpeed(movementSpeed);
                if (onMovementSpeedUpdate != null) onMovementSpeedUpdate(movementSpeed);
            }
            else
            {
                playerFootstep.SetFootstepSpeed(0);
                if (onMovementSpeedUpdate != null) onMovementSpeedUpdate(0);
            }

            var canUncrouch = (!isCrouching || (isCrouching && AttemptUncrouch()));

            if (jumping && canUncrouch)
            {
                verticalVelocity = jumpSpeed;
                alreadyJumping = true;
                playerFootstep.PlayJumpSoundUp();
            }
            else
            {
                //PLAYER IS ON THE GROUND
                verticalVelocity -= 0;
                if (alreadyJumping)
                {
                    //PLAYER JUST LANDED AFTER JUMPING
                    UpdateMovementSpeed();
                    alreadyJumping = false;
                }
            }
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
            verticalVelocity = Mathf.Clamp(verticalVelocity, -gravity, gravity);
            jumping = false;
        }

        if (uncrouching)
        {
            if (AttemptUncrouch())
            {
                uncrouching = false;
                uncrouchLerp = true;
            }
        }
        else if (uncrouchLerp)
        {
            Uncrouch();
        }

        MovePlayer();
    }

    #region Input Manager
    public void ControlPlayer(float h, float v)
    {
        Vector3 inputs = new Vector3(h, 0, v);
        if (inputs.magnitude > 1) inputs = inputs.normalized;

        axisH = inputs.x * movementSpeed;
        if (v > 0) axisV = inputs.z * movementSpeed;
        else axisV = inputs.z * movementSpeed * backwardsMovementSpeedMultiplier;
    }

    public void Jump()
    {
        jumping = true;
        playerFootstep.ResetJumpSoundTimer();
    }

    public void CrouchDown()
    {
        if (!IsGrounded) return;
        StopCoroutine("CrouchDownLerp");
        StartCoroutine("CrouchDownLerp");
        uncrouching = false;
        isCrouching = true;
        UpdateMovementSpeed();
        playerFootstep.DontPlaySFXOnEnable();
        if (alreadySprinting || isSprinting) { SprintUp(); }
    }
    public void CrouchUp()
    {
        uncrouching = true;
    }

    public void SprintHold()
    {
        if (isCrouching) return; //-----------------------
        if (!isSprinting) SprintDown();
        if (PlayerHasMoved(true) && !alreadySprinting)
        {
            //PLAYER HAS MOVED AND ISN'T ALREADY SPRINTING
            Sprint(true);
        }
        else if (!PlayerHasMoved(true) || alreadySprinting)
        {
            //PLAYER HAS NOT MOVED OR IS ALREADY SPRINTING
            Sprint(false);
        }
    }
    public void SprintDown()
    {
        if (isCrouching) return; //-----------------------
        isSprinting = true;
        UpdateMovementSpeed();
    }
    public void SprintUp()
    {
        if (!isSprinting) return; //-----------------------
        isSprinting = false;
        Sprint(false);
        UpdateMovementSpeed();
    }
    public void ToggleSprint()
    {
        if (isSprinting)
        {
            SprintUp();
        }
        else
        {
            SprintDown();
            Sprint(true);
        }
    }
    #endregion

    private void Sprint(bool sprinting)
    {
        alreadySprinting = sprinting;
        UpdateMovementSpeed();
    }

    private void UpdateMovementSpeed()
    {
        //DON'T CHANGE SPEED IF PLAYER IS MID AIR
        if (!IsGrounded) return; //----------------------------------------------------------

        //PLAYER IS STANDING AND WALKING
        if (!isSprinting && !isCrouching) movementSpeed = defaultSpeed;
        //PLAYER IS CROUCHING
        else if (!isSprinting && isCrouching) movementSpeed = crouchSpeed;
        //PLAYER IS STANDING AND SPRINTING
        else if (isSprinting && !isCrouching) movementSpeed = sprintSpeed;
    }

    private void SetFootstepSFXState()
    {
        if (playerFootstep.enabled != IsGrounded) playerFootstep.enabled = IsGrounded;
    }

    private bool AttemptUncrouch()
    {
        return (AttemptUncrouchRaycast(xOffset, 0f) && AttemptUncrouchRaycast(-xOffset, 0f) && AttemptUncrouchRaycast(0f, zOffset) && AttemptUncrouchRaycast(0f, -zOffset));
    }
    private bool AttemptUncrouchRaycast(float xOffset, float zOffset)
    {
        Vector3 origin = new Vector3(transform.position.x + xOffset, transform.position.y, transform.position.z + zOffset);
        Ray ray = new Ray(origin, transform.up);
        RaycastHit hit;
        return (!Physics.Raycast(ray, out hit, crouchCheckDistance));
    }
    private void Uncrouch()
    {
        if (uncrouchLerpValue < 1f)
        {
            uncrouchLerpValue += Time.deltaTime * CROUCH_LERP_SPEED;
            SetStandingHeight(uncrouchLerpValue);
        }
        else if (uncrouchLerpValue >= 1f)
        {
            SetStandingHeight();
            uncrouchLerp = isCrouching = false;
            uncrouchLerpValue = 0f;
            UpdateMovementSpeed();
        }
    }

    private void SetStandingHeight(float lerpVal)
    {
        characterController.height = (Mathf.Lerp(crouchedHeight, charContHeight, lerpVal));
        headBob.SetCameraStandingHeight(lerpVal);
    }
    private void SetStandingHeight()
    {
        characterController.height = charContHeight;
        headBob.SetCameraStandingHeight();
    }
    private void SetCrouchingHeight(float lerpVal)
    {
        characterController.height = (Mathf.Lerp(charContHeight, crouchedHeight, lerpVal));
        headBob.SetCameraCrouchHeight(cameraCrouchHeight, lerpVal);
    }
    private void SetCrouchingHeight()
    {
        characterController.height = crouchedHeight;
        headBob.SetCameraCrouchHeight(cameraCrouchHeight);
    }

    private IEnumerator CrouchDownLerp()
    {
        if (crouchLerpValue < 1f)
        {
            crouchLerpValue += Time.deltaTime * CROUCH_LERP_SPEED;
            SetCrouchingHeight(crouchLerpValue);
            yield return null;
        }
        SetCrouchingHeight();
        crouchLerpValue = 0f;
    }

    private bool PlayerHasMoved() => PlayerHasMoved(false);
    private bool PlayerHasMoved(bool affectHeadBob)
    {
        bool hasMoved = (axisH > MOVEMENT_CHECKER_THRESHOLD || axisH < -MOVEMENT_CHECKER_THRESHOLD || axisV > MOVEMENT_CHECKER_THRESHOLD || axisV < -MOVEMENT_CHECKER_THRESHOLD);
        if (affectHeadBob) headBob.SetSpeed(hasMoved, movementSpeed);
        return hasMoved;
    }

    private void MovePlayer()
    {
        Vector3 moveDirection;
        if (climbingLadder) moveDirection = new Vector3(axisH, axisV, 0f);
        else moveDirection = new Vector3(axisH, verticalVelocity, axisV);

        moveDirection = transform.TransformDirection(moveDirection);
        characterController.Move(moveDirection * Time.deltaTime);
    }

    public void SetClimbingLadder(bool active)
    {
        if (climbingLadder == active) return; //----------------------------
        if (active)
        {
            climbingLadder = true;
            playerFootstep.DontPlaySFXOnEnable();
        }
        else
        {
            climbingLadder = false;
        }
    }
    public void SetInShadows(bool active)
    {
        if (inShadows != active) inShadows = active;
    }

    public void TakeDamage(int amt, Vector3 pos, float force, Enums.damageType type) => playerHealth.DecreaseRemainingHealth(amt);

    public void Die()
    {
        isDead = true;
        pauseGame.DisplayGameOverScreen();

        if (onPlayerDeath != null) onPlayerDeath(true);
    }

    public void Respawn()
    {
        isDead = false;
        MoveToLastCheckpoint();
        ResetHealthToLastCheckpoint();
        cameraManager.ResetPostProcessing(checkpointManager.FogSettingsAtCheckpoint);
        playerInventory.ResetToLastCheckpoint();

        if (onPlayerDeath != null) onPlayerDeath(false);
    }

    private void MoveToLastCheckpoint()
    {
        characterController.enabled = false;
        this.transform.position = checkpointManager.GetCurrentCheckpointPos();
        characterController.enabled = true;
    }
    private void ResetHealthToLastCheckpoint()
    {
        playerHealth.SetRemainingHealth(checkpointManager.PlayerHealthAtCheckpoint);
        playerHealth.SetRemainingArmor(checkpointManager.PlayerArmorAtCheckpoint);
    }

}

