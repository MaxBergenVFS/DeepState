using UnityEngine;

[System.Serializable]
public class SavedNote
{
    public string title;
    [TextArea(3, 10)]
    public string content;
    public int id;

    public SavedNote(string title, string content, int id)
    {
        this.title = title;
        this.content = content;
        this.id = id;
    }
}
