using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIManager : MonoBehaviour
{
    private const float FADE_IN_SPEED = 2.5f;
    private const float FADE_IN_DELAY = 0.8f;

    [SerializeField]
    private GameObject deathScreenUI, pauseScreenUI, inventoryUI, inventoryTab, savedNotesTab, settingsTab, exitTab, controls;
    [SerializeField]
    private Transform itemGrid;
    [SerializeField]
    private ItemBar itemBar;
    [SerializeField]
    private SelectedItemDescription selectedItemDescription;
    [SerializeField]
    private TextMeshProUGUI moneyAmountDisplay, datapadFoundCounter, datapadsInSceneCounter;
    [SerializeField]
    private Image pauseBackground;

    private PlayerInventory playerInventory;
    private ModelViewportCamera modelViewportCamera;
    private IInventoryItem selectedItemRef;
    private PlayerHands playerHands;
    private Color pauseBackgroundColor = new Color();
    private float pauseBackgroundAlpha, holsterDelay;

    public bool ExitTabIsDisplayed { get { return exitTab.activeSelf; } }
    public bool DeathScreenIsDisplayed { get { return deathScreenUI.activeSelf; } }

    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        playerHands = PlayerController.Instance.GetComponent<PlayerHeldItem>().PlayerHands;
        modelViewportCamera = GameManager.Instance.ModelViewportCamera;
        pauseBackgroundColor = pauseBackground.color;
        pauseBackgroundAlpha = pauseBackgroundColor.a;
        holsterDelay = playerHands.HolsterDelay;

        playerInventory.OnItemAddedToGrid += AddItemToGrid;
        playerInventory.OnItemRemovedFromGrid += RemoveItemFromGrid;
        playerInventory.OnItemSelectedOnGrid += SetSelectedItemDescription;
        PlayerInventory.onItemsRefreshedOnBar += RefreshItemsOnBar;
        PlayerInventory.onItemsRefreshedOnGrid += RefreshItemsOnGrid;
        PlayerInventory.onMoneyAmountChanged += SetMoneyAmountDisplay;
        CollectablesManager.onDatapadAddedToScene += SetDatapadsInSceneCounter;
        CollectablesManager.onDatapadFound += SetDatapadFoundCounter;

        DisplayInventoryTab();
    }

    public void Init()
    {
        //allow these values to be initiated manually to avoid order of operations errors
        if (modelViewportCamera == null) modelViewportCamera = GameManager.Instance.ModelViewportCamera;
    }

    private void AddItemToGrid(object sender, InventoryEventArgs e)
    {
        foreach (Transform slot in itemGrid)
        {
            ItemSlot itemSlot = slot.GetComponent<ItemSlot>();
            Image itemImage = itemSlot.ItemImage;
            TextMeshProUGUI itemName = itemSlot.ItemName;
            TextMeshProUGUI itemAmount = itemSlot.ItemAmount;
            ItemDragHandler itemDragHandler = itemSlot.ItemDragHandler;

            if (itemImage.enabled && itemName.text == e.Item.Name)
            {
                itemAmount.text = e.Item.AmountStr;
                itemDragHandler.ItemRef = e.Item;
                break;
            }
            else if (!itemImage.enabled)
            {
                itemImage.enabled = true;
                itemImage.sprite = e.Item.Image;
                itemName.text = e.Item.Name;
                itemAmount.text = e.Item.AmountStr;
                itemDragHandler.ItemRef = e.Item;
                break;
            }
        }
    }

    private void RemoveItemFromGrid(object sender, InventoryEventArgs e)
    {
        foreach (Transform slot in itemGrid)
        {
            ItemSlot itemSlot = slot.GetComponent<ItemSlot>();
            IInventoryItem itemRef = itemSlot.ItemDragHandler.ItemRef;

            if (itemRef != null && itemRef.Equals(e.Item))
            {
                if (e.Item.AmountInt < 1)
                {
                    itemSlot.ItemImage.enabled = false;
                    itemSlot.ItemImage.sprite = null;
                    itemSlot.ItemName.text = "";
                    itemSlot.ItemAmount.text = "";
                    itemSlot.ItemDragHandler.ItemRef = null;
                    break;
                }
                else
                {
                    itemSlot.ItemAmount.text = e.Item.AmountStr;
                    break;
                }
            }
        }
    }

    private void RefreshItemsOnGrid()
    {
        foreach (Transform slot in itemGrid)
        {
            ItemSlot itemSlot = slot.GetComponent<ItemSlot>();
            IInventoryItem itemRef = itemSlot.ItemDragHandler.ItemRef;

            if (itemRef != null)
            {
                if (itemRef.AmountInt < 1)
                {
                    itemSlot.ItemImage.enabled = false;
                    itemSlot.ItemImage.sprite = null;
                    itemSlot.ItemName.text = "";
                    itemSlot.ItemAmount.text = "";
                    itemSlot.ItemDragHandler.ItemRef = null;
                }
                else
                {
                    itemSlot.ItemAmount.text = itemRef.AmountStr;
                }
            }
        }
    }

    private void RefreshItemsOnBar()
    {
        ItemSlot[] itemSlots = itemBar.ItemSlots;
        foreach (ItemSlot itemSlot in itemSlots)
        {
            IInventoryItem itemRef = itemSlot.ItemDragHandler.ItemRef;
            TextMeshProUGUI itemName = itemSlot.ItemName;
            TextMeshProUGUI itemAmount = itemSlot.ItemAmount;
            Image itemImage = itemSlot.ItemImage;
            Image itemEquippedImage = itemSlot.ItemEquippedImage;

            if (itemRef != null && itemRef.AmountInt > 0)
            {
                //TO DO refactor this system and find cause of this error
                if (itemImage != null)
                {
                    itemImage.enabled = true;
                    itemImage.sprite = itemRef.Image;
                }
                else
                {
                    Debug.LogError($"Missing itemImage ref on {itemRef.Name}");
                }
                itemName.text = itemRef.ShortenedName;
                itemAmount.text = itemRef.AmountStr;
                itemEquippedImage.enabled = itemRef.IsEquipped;
            }
            else
            {
                //TO DO refactor this system and find cause of this error
                if (itemImage != null)
                {
                    itemImage.enabled = false;
                    itemImage.sprite = null;
                }
                else
                {
                    Debug.LogError($"Missing itemImage ref on {itemRef.Name}");
                }
                itemName.text = "";
                itemAmount.text = "";
                itemEquippedImage.enabled = false;
                itemSlot.ItemDragHandler.ItemRef = null;
            }
        }
    }

    private void SetSelectedItemDescription(object sender, InventoryEventArgs e)
    {
        if (e.Item == null)
        {
            selectedItemDescription.SetItemDescription("", "", "");
            modelViewportCamera.HideAllModels();
            selectedItemRef = null;
        }
        else
        {
            selectedItemRef = e.Item;
            selectedItemDescription.SetItemDescription(selectedItemRef.Name, selectedItemRef.Description, selectedItemRef.CriticalInfo);
            modelViewportCamera.DisplayModel(selectedItemRef.ItemType);
        }
    }

    public void AssignSelectedItemToSlotOnItemBar(int slotIndex)
    {
        if (selectedItemRef == null) return; //----------------------
        itemBar.AddItemToSlotOnBar(selectedItemRef, slotIndex);
        playerInventory.RefreshItemsOnBar();
    }
    public void UseSelectedItem()
    {
        if (selectedItemRef == null) return; //----------------------
        bool success;
        selectedItemRef.UseFromInventory(out success);
        playerInventory.RefreshItemsOnBar();
    }

    private void DisplayPauseBackgroundImage(bool active)
    {
        pauseBackground.enabled = active;
    }

    public void FadeInBackgroundImage()
    {
        float delay = FADE_IN_DELAY;
        if (playerHands.CurrentlyHolsteringItem) delay += holsterDelay;
        StopAllCoroutines();
        StartCoroutine(FadeInBackgroundImageLerp(delay));
    }
    private IEnumerator FadeInBackgroundImageLerp(float delay)
    {
        yield return new WaitForSeconds(delay);

        float t = 0f;
        float a = 0f;

        pauseBackground.color = new Color(pauseBackgroundColor.r, pauseBackgroundColor.g, pauseBackgroundColor.b, a);
        DisplayPauseBackgroundImage(true);

        while (t < 1f)
        {
            a = Mathf.Lerp(0f, pauseBackgroundAlpha, t);
            t += Time.deltaTime * FADE_IN_SPEED;
            pauseBackground.color = new Color(pauseBackgroundColor.r, pauseBackgroundColor.g, pauseBackgroundColor.b, a);
            yield return null;
        }

        pauseBackground.color = new Color(pauseBackgroundColor.r, pauseBackgroundColor.g, pauseBackgroundColor.b, pauseBackgroundAlpha);
    }

    public void DisplayInventoryUI(bool active)
    {
        modelViewportCamera.EnableCamera(active);
        inventoryUI.SetActive(active);
        DisplayPauseBackgroundImage(active);
        if (!active) itemBar.HideUI(false, true);
        else if (active && !inventoryTab.activeSelf) itemBar.HideUI(true, true);
    }
    private void DisplayDeathScreenUI(bool active)
    {
        if (DeathScreenIsDisplayed == active) return; //-------------
        deathScreenUI.SetActive(active);
        itemBar.HideUI(active, false);
    }
    private void DisplayPauseScreenUI(bool active)
    {
        pauseScreenUI.SetActive(active);
        itemBar.HideUI(active, false);
    }

    public void DisplayPauseScreenUI(bool active, bool gameOver)
    {
        if (active && gameOver)
        {
            //PLAYER IS DEAD
            DisplayInventoryUI(false);
            DisplayPauseScreenUI(false);
            DisplayDeathScreenUI(true);
            return;
        } //-------------------------------------------------------------------------

        if (active)
        {
            //DISPLAY PAUSE SCREEN
            DisplayPauseScreenUI(true);
        }
        else
        {
            //HIDE PAUSE SCREEN
            DisplayDeathScreenUI(false);
            DisplayPauseScreenUI(false);
            DisplayInventoryUI(false);
            itemBar.HideUI(false, true);
        }
    }

    public void DisplayInventoryTab() => DisplayTab(inventoryTab, false);
    public void DisplaySavedNotesTab() => DisplayTab(savedNotesTab, true);
    public void DisplaySettingsTab() => DisplayTab(settingsTab, true);
    public void DisplayExitTab() => DisplayTab(exitTab, true);
    private void DisplayTab(GameObject obj, bool hideItemBar)
    {
        HideAllTabs();
        obj.SetActive(true);
        itemBar.HideUI(hideItemBar, true);
    }
    private void HideAllTabs()
    {
        controls.SetActive(false);
        inventoryTab.SetActive(false);
        savedNotesTab.SetActive(false);
        settingsTab.SetActive(false);
        exitTab.SetActive(false);
    }

    public void ToggleControls()
    {
        if (!settingsTab.activeSelf) return;
        controls.SetActive(!controls.activeSelf);
    }

    private void SetDatapadsInSceneCounter(int amt) => SetIntAsText(datapadsInSceneCounter, amt);
    private void SetDatapadFoundCounter(int amt) => SetIntAsText(datapadFoundCounter, amt);
    private void SetMoneyAmountDisplay(int amt) => SetIntAsText(moneyAmountDisplay, amt);
    private void SetIntAsText(TextMeshProUGUI text, int amt)
    {
        text.text = amt.ToString();
    }

    private void OnDestroy()
    {
        playerInventory.OnItemAddedToGrid -= AddItemToGrid;
        playerInventory.OnItemRemovedFromGrid -= RemoveItemFromGrid;
        playerInventory.OnItemSelectedOnGrid -= SetSelectedItemDescription;
        PlayerInventory.onItemsRefreshedOnBar -= RefreshItemsOnBar;
        PlayerInventory.onItemsRefreshedOnGrid -= RefreshItemsOnGrid;
        PlayerInventory.onMoneyAmountChanged -= SetMoneyAmountDisplay;
        CollectablesManager.onDatapadAddedToScene -= SetDatapadsInSceneCounter;
        CollectablesManager.onDatapadFound -= SetDatapadFoundCounter;
    }
}
