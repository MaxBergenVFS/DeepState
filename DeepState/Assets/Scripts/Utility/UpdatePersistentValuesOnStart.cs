using UnityEngine;

public class UpdatePersistentValuesOnStart : MonoBehaviour
{
    [SerializeField]
    private bool setHasDisplayedFakeLoadingScreen;

    private void Start()
    {
        if (setHasDisplayedFakeLoadingScreen) PersistentValues.Instance.SetHasDisplayedFakeLoadingScreen();
    }
}
