using UnityEngine;

public class Crate : DestructibleObject
{
    private const string NAME = "Crate";

    [SerializeField]
    private float secondsBeforeFadeout = 2f;
    [SerializeField]
    private float fadeoutTime = 1f;
    [SerializeField]
    private float explosiveForce = 100f;
    [SerializeField]
    private GameObject itemToSpawn;
    [SerializeField]
    private Transform itemSpawnPoint;
    [SerializeField]
    private GameObject intactCrate, damagedCrate;
    [SerializeField]
    private DestructibleFragment[] fragments;

    private ObjectPool woodImpactParticlePool;

    public float SecondsBeforeFadeout { get { return secondsBeforeFadeout; } }
    public float FadeoutTime { get { return fadeoutTime; } }
    public float ExplosiveForce { get { return explosiveForce; } }
    public Vector3 Origin { get { return itemSpawnPoint.position; } }

    protected override void Awake()
    {
        col = GetComponent<Collider>();
        sfx = GetComponent<ObjectSFX>();
        damagedCrate.SetActive(false);
        intactCrate.SetActive(true);
        mat = intactCrate.GetComponent<Renderer>().material;
        if (itemName == "") itemName = NAME;
    }
    protected override void Start()
    {
        base.Start();
        woodImpactParticlePool = GameManager.Instance.WoodImpactParticlePool;
    }

    protected override void SpawnImpactParticle(Vector3 pos)
    {
        GameObject obj = woodImpactParticlePool.GetPooledObject();
        obj.transform.position = pos;
        obj.transform.rotation = Quaternion.identity;
    }

    protected override void TakeDamageAndBreak()
    {
        col.enabled = false;
        intactCrate.SetActive(false);
        damagedCrate.SetActive(true);
        sfx.PlaySound();
        if (itemToSpawn != null)
        {
            GameObject obj = Instantiate(itemToSpawn, itemSpawnPoint.position, Quaternion.identity);
            //bypassing checkpoint to avoid duplicate items. Crate already handles respawn on checkpoint
            if (obj.GetComponent<BaseItem>() != null)
            {
                obj.GetComponent<BaseItem>().BypassCheckpoint();
            }
            else if (obj.GetComponentInChildren<BaseItem>() != null)
            {
                //some item objects don't have the BaseItem script on the parent object
                obj.GetComponentInChildren<BaseItem>().BypassCheckpoint();
            }
        }
    }

    public override void Respawn()
    {
        base.Respawn();
        intactCrate.SetActive(true);
        damagedCrate.SetActive(false);
        foreach (DestructibleFragment fragment in fragments)
        {
            fragment.Reset();
        }
    }
}