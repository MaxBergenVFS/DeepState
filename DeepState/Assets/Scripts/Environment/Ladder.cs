using UnityEngine;

public class Ladder : MonoBehaviour
{
    [SerializeField]
    private Collider ladderTop;

    private PlayerController playerController;

    private void Start()
    {
        playerController = PlayerController.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerController.SetClimbingLadder(true);
            if (ladderTop != null) ladderTop.enabled = false;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerController.SetClimbingLadder(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerController.SetClimbingLadder(false);
            if (ladderTop != null) ladderTop.enabled = true;
        }
    }
}