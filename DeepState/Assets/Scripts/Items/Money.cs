using UnityEngine;

public class Money : BaseItem
{
    private const string MONEY_NAME = "dollars";
    private const string NAME_OVERRIDE = "Cash";

    [SerializeField]
    private int amount = 5;

    private CollectablesManager collectables;

    protected override void Start()
    {
        base.Start();
        collectables = GameManager.Instance.CollectablesManager;
        nameOverride = NAME_OVERRIDE;
    }

    public override void Interact()
    {
        PickUp();
        AddToCheckpointManager();
        DisplayNotificationText(true, $"{amount} {MONEY_NAME}");
        playerInventory.AddToMoneyAmount(amount);
        collectables.IncreaseMoneyFound(amount);

    }

    private void AddToCheckpointManager()
    {
        /* OnPickup() is never called because this item doesn't get added to the ItemGrid
        so it must be added to the CheckpointManager manually */
        checkpointManager.AddRespawnable(this);
    }
}
