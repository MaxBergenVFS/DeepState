using UnityEngine;

public class BloodPoolSpread : MonoBehaviour
{
    private Vector3 defaultScale;

    [SerializeField]
    private float spreadSpeed = 1f;

    private void Awake()
    {
        defaultScale = transform.localScale;
        transform.localScale = Vector3.zero;
    }

    private void OnEnable()
    {
        transform.localScale = Vector3.zero;
    }

    private void Update()
    {
        if (transform.localScale == defaultScale) this.enabled = false;
        transform.localScale = Vector3.Lerp(transform.localScale, defaultScale, Time.deltaTime * spreadSpeed);
    }
}
