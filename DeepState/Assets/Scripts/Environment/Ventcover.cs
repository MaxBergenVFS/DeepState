using UnityEngine;

[RequireComponent(typeof(ObjectSFX))]
public class Ventcover : MonoBehaviour, IDamageable
{
    [SerializeField]
    private Material intact, broken;

    private Collider col;
    private Renderer rend;
    private ObjectSFX sfx;

    private void Awake()
    {
        sfx = GetComponent<ObjectSFX>();
        col = GetComponent<Collider>();
        rend = GetComponent<Renderer>();
        rend.material = intact;
    }

    public void TakeDamage(int amt, Vector3 hitPos, float force, Enums.damageType type)
    {
        if (amt < 1) return;

        sfx.PlaySound();
        col.enabled = false;
        rend.material = broken;
    }
}
