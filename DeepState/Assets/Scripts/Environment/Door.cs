using UnityEngine;

[RequireComponent(typeof(ObjectSFX))]
public class Door : MonoBehaviour, IInteractable
{
    private const string TEXT_LOCKED = "It's locked";
    private const string TEXT_UNLOCKED = "It's unlocked";
    private const string TEXT_BYPASS_LOCK_FAILED = "This won't work";

    [SerializeField]
    private Animator anim;
    [SerializeField]
    private bool onlyOpensOnce, isLocked;
    [SerializeField]
    private float inactiveTime = 1.5f;
    [SerializeField]
    private int securityLevel;
    [SerializeField]
    private Enums.itemType bypassLockType;
    [SerializeField]
    private string targetedName = "Door";

    private Collider[] cols;
    private ObjectSFX sfx;
    private PlayerInventory playerInventory;
    private CheckpointSettings snapshot = new CheckpointSettings();

    public string Name { get { return targetedName; } }
    public float InactiveTime { get { return inactiveTime; } }

    protected virtual void Awake()
    {
        cols = GetComponents<Collider>();
        sfx = GetComponent<ObjectSFX>();
    }
    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        SnapshotState();
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    public void Interact()
    {
        if (isLocked)
        {
            int keycardAccessLevel = playerInventory.GetEquippedKeycardAccessLevel();
            if (keycardAccessLevel == securityLevel)
            {
                //UNLOCK SUCCESSFUL
                Unlock();
            }
            else
            {
                //UNLOCK FAILED
                NotificationUI.Instance.DisplayText(TEXT_LOCKED);
                sfx.PlaySlidingDoorLockedSound();
            }
        }
        if (!isLocked) Open();
    }

    protected virtual void Open()
    {
        anim.SetTrigger(AnimParams.DOOR_TRIGGER_INTERACT);
        sfx.PlaySound();
        ActivateCol(false);
        if (!onlyOpensOnce) Invoke("ActivateCol", inactiveTime);
    }

    private void ActivateCol() => ActivateCol(true);
    private void ActivateCol(bool active)
    {
        foreach (var col in cols)
        {
            col.enabled = active;
        }
    }

    private void Unlock()
    {
        isLocked = false;
        anim.SetTrigger(AnimParams.DOOR_TRIGGER_UNLOCK);
        NotificationUI.Instance.DisplayText(TEXT_UNLOCKED);
    }

    public void BypassLock(Enums.itemType itemType, out bool success)
    {
        if (isLocked && bypassLockType != Enums.itemType.None)
        {
            if (itemType == bypassLockType)
            {
                Unlock();
                success = true;
                return; //-------------------------------------------
            }
            else
            {
                NotificationUI.Instance.DisplayText(TEXT_BYPASS_LOCK_FAILED);
                success = false;
                return; //------------------------------------------
            }
        }
        success = false;
    }

    public void UnlockAndOpen()
    {
        Unlock();
        Interact();
    }

    private void SnapshotState()
    {
        snapshot.isOpen = anim.GetBool(AnimParams.DOOR_BOOL_IS_OPEN);
        snapshot.isLocked = isLocked;
    }
    public void RevertState()
    {
        if (snapshot.isOpen != anim.GetBool(AnimParams.DOOR_BOOL_IS_OPEN)) Interact();
        isLocked = snapshot.isLocked;
    }
    public void OpenIfUnlockedAndClosed()
    {
        if (isLocked || anim.GetBool(AnimParams.DOOR_BOOL_IS_OPEN)) return; //----------
        Open();
    }
}
