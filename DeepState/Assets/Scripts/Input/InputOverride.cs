public class InputOverride : Inputs
{
    private InputManager inputManager;

    protected bool InputManagerIsEnabled { get { return inputManager.isActiveAndEnabled; } }
    protected bool InventoryIsClosed { get { return !inputManager.InventoryIsOpen; } }

    private void Start()
    {
        inputManager = PlayerController.Instance.GetComponent<InputManager>();
    }
}
