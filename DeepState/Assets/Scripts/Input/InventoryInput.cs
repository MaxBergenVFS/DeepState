using UnityEngine;

public class InventoryInput : InputOverride
{
    [SerializeField]
    private InventoryUIManager inventoryUIManager;

    private void Update()
    {
        if (InventoryIsClosed) return; //-----------------------------------

        if (Input.GetButtonDown(INPUT_SLOT_1))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(0);
        if (Input.GetButtonDown(INPUT_SLOT_2))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(1);
        if (Input.GetButtonDown(INPUT_SLOT_3))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(2);
        if (Input.GetButtonDown(INPUT_SLOT_4))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(3);
        if (Input.GetButtonDown(INPUT_SLOT_5))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(4);
        if (Input.GetButtonDown(INPUT_SLOT_6))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(5);
        if (Input.GetButtonDown(INPUT_SLOT_7))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(6);
        if (Input.GetButtonDown(INPUT_SLOT_8))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(7);
        if (Input.GetButtonDown(INPUT_SLOT_9))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(8);
        if (Input.GetButtonDown(INPUT_SLOT_10))
            inventoryUIManager.AssignSelectedItemToSlotOnItemBar(9);
        /*
            removed this for now as it introduces more problems
        if (Input.GetButtonDown(INPUT_INTERACT) || Input.GetButtonDown(INPUT_ENTER))
            inventoryUIManager.UseSelectedItem();
        */
    }
}
