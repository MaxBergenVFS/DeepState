using UnityEngine;

public class MirrorReflectionPoint : MonoBehaviour
{
    private Transform player;
    private enum axis
    {
        x,
        z
    };

    [SerializeField]
    private axis mirroredAxis;

    public void Init()
    {
        player = PlayerController.Instance.transform;
    }

    private void Update()
    {
        switch (mirroredAxis)
        {
            case (axis.x):
                SetNewZAxis();
                break;
            case (axis.z):
                SetNewXAxis();
                break;
            default:
                break;
        }
    }

    private void SetNewXAxis()
    {
        transform.position = new Vector3(player.position.x, transform.position.y, transform.position.z);
    }
    private void SetNewZAxis()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, player.position.z);
    }

    private float GetPlayerDistOnXAxis()
    {
        return player.position.x - transform.position.x;
    }
    private float GetPlayerDistOnZAxis()
    {
        return player.position.z - transform.position.z;
    }

    public float GetMirroredXAxis()
    {
        switch (mirroredAxis)
        {
            case (axis.x):
                return transform.position.x - GetPlayerDistOnXAxis();
            case (axis.z):
                return player.position.x;
            default:
                return transform.position.x;
        }
    }
    public float GetMirroredZAxis()
    {
        switch (mirroredAxis)
        {
            case (axis.x):
                return player.position.z;
            case (axis.z):
                return transform.position.z - GetPlayerDistOnZAxis();
            default:
                return transform.position.z;
        }
    }

    public Quaternion GetMirroredRot()
    {
        if (mirroredAxis == axis.z)
        {
            Vector3 rot = new Vector3();
            rot = (Quaternion.Inverse(player.rotation).eulerAngles);
            rot.y += 180f;
            return Quaternion.Euler(rot);
        }
        return Quaternion.Inverse(player.rotation);
    }
}
