using UnityEngine;

public class NPCRaiseAlarmBehaviour : StateMachineBehaviour
{
    private const float ALARM_INTERACT_DELAY = 0.8f;

    private NPCController controller;
    private float delay;
    private bool interactedWithAlarm;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        delay = 0f;
        interactedWithAlarm = false;
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
        controller = animator.GetComponent<NPCController>();
        controller.TryRotateToFaceAlarm();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (interactedWithAlarm) return; //--------------------------------

        if (delay < ALARM_INTERACT_DELAY)
        {
            delay += Time.deltaTime;
        }
        else
        {
            controller.AlarmRef.Interact();
            interactedWithAlarm = true;
        }
    }

    // override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {

    // }
}
