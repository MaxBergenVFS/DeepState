using UnityEngine;

[RequireComponent(typeof(NoteToSave))]
[RequireComponent(typeof(ObjectSFX))]
public class Datapad : MonoBehaviour, IInteractable
{
    private const string NAME = "Datapad";

    [SerializeField]
    private string noteTitle;
    [SerializeField]
    [TextArea(3, 10)]
    private string noteContent;

    private DatapadUI datapadUI;
    private NoteToSave noteToSave;
    private ObjectSFX sfx;
    private CollectablesManager collectablesManager;
    private bool primeNoteToSave;

    public string Name { get { return NAME; } }

    private void Awake()
    {
        noteToSave = GetComponent<NoteToSave>();
        sfx = GetComponent<ObjectSFX>();
        primeNoteToSave = false;
    }
    private void Start()
    {
        datapadUI = PlayerController.Instance.GetComponent<PlayerInventory>().DatapadUI;
        collectablesManager = GameManager.Instance.CollectablesManager;
        collectablesManager.AddToDatapadsInScene(this);
        DatapadUI.onDisplayPadUI += SaveNoteOnHideUI;
    }

    private void OnDestroy()
    {
        DatapadUI.onDisplayPadUI -= SaveNoteOnHideUI;
    }

    public void Interact()
    {
        datapadUI.Show(noteTitle, noteContent);
        sfx.PlaySound();
        if (!collectablesManager.DatapadHasBeenFound(this))
        {
            primeNoteToSave = true;
            collectablesManager.AddToDatapadsFound(this);
        }
    }

    private void SaveNoteOnHideUI(bool displayed)
    {
        if (displayed || !primeNoteToSave) return; //----------------------

        noteToSave.Save(noteTitle);
        primeNoteToSave = false;
    }
}
