using UnityEngine;

public class ObjectSFX : SFX
{
    private string path;

    [SerializeField]
    private Enums.simpleObjSfxType sound;

    private void Awake()
    {
        InitPath();
    }

    private void InitPath()
    {
        path = OBJECT_PREFIX;
        switch (sound)
        {
            case (Enums.simpleObjSfxType.HandDryer):
                path += HAND_DRYER_PATH;
                break;
            case (Enums.simpleObjSfxType.Faucet):
                path += FAUCET_PATH;
                break;
            case (Enums.simpleObjSfxType.VendingMachine):
                path += VENDING_MACHINE_PATH;
                break;
            case (Enums.simpleObjSfxType.Window):
                path += WINDOW_PATH;
                break;
            case (Enums.simpleObjSfxType.Crate):
                path += CRATE_PATH;
                break;
            case (Enums.simpleObjSfxType.Hatch):
                path += HATCH_PATH;
                break;
            case (Enums.simpleObjSfxType.SlidingDoor):
                path += SLIDING_DOOR_PATH;
                break;
            case (Enums.simpleObjSfxType.StallDoor):
                path += STALL_DOOR_PATH;
                break;
            case (Enums.simpleObjSfxType.SecurityCamera):
                path += SECURITY_CAMERA_PATH;
                break;
            case (Enums.simpleObjSfxType.LockerDoor):
                path += LOCKER_DOOR_PATH;
                break;
            case (Enums.simpleObjSfxType.Vent):
                path += VENT_PATH;
                break;
            case (Enums.simpleObjSfxType.CardboardBox):
                path += CARDBOARD_BOX_PATH;
                break;
            case (Enums.simpleObjSfxType.Binder):
                path += BINDER_PATH;
                break;
            case (Enums.simpleObjSfxType.Paper):
                path += PAPER_PATH;
                break;
            case (Enums.simpleObjSfxType.Plate):
                path += PLATE_PATH;
                break;
            case (Enums.simpleObjSfxType.Computer):
                path += COMPUTER_PATH;
                break;
            case (Enums.simpleObjSfxType.ComputerMouse):
                path += COMPUTER_MOUSE_PATH;
                break;
            case (Enums.simpleObjSfxType.Keyboard):
                path += KEYBOARD_PATH;
                break;
            case (Enums.simpleObjSfxType.Trashcan):
                path += TRASHCAN_PATH;
                break;
            case (Enums.simpleObjSfxType.VegetableBox):
                path += VEGETABLE_BOX_PATH;
                break;
            case (Enums.simpleObjSfxType.Datapad):
                path += DATAPAD_PATH;
                break;
            case (Enums.simpleObjSfxType.None):
            default:
                break;
        }
    }

    //generic
    public void PlaySound() => PlaySound3D(path);
    public void PlaySound(Vector3 pos) => PlaySound3D(path, pos);
    //specific
    public void PlaySlidingDoorLockedSound() => PlaySound3D(SLIDING_DOOR_LOCKED_PATH);
    public void PlayVendingMachineNoMoneySound() => PlaySound3D(VENDING_MACHINE_NO_MONEY_PATH);
    public void PlayCheckpointDeviceDeploySound() => PlaySound3D(CHECKPOINT_DEVICE_OBJECT_DEPLOY);
    public void PlayCheckpointDeviceIdleSound() => PlaySound3D(CHECKPOINT_DEVICE_OBJECT_IDLE);
}
