﻿using System.Collections;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    private const float SCREEN_SHAKE_DEFAULT_INTENSITY = 1f;

    [SerializeField]
    private float shakeDuration, shakeAmount, decreaseFactor;

    private Transform camTransform;
    private Vector3 originalPos;
    private float currentShakeDuration;
    private HeadBob headBob;
    private bool currentlyShaking;

    private void Awake()
    {
        camTransform = GetComponent<Transform>();
        headBob = GetComponent<HeadBob>();
        currentShakeDuration = 0;
        currentlyShaking = false;
    }

    public void Shake() => Shake(true);
    public void Shake(bool cond) => Shake(cond, SCREEN_SHAKE_DEFAULT_INTENSITY);
    public void Shake(float intensity) => Shake(true, intensity);
    public void Shake(bool cond, float intensity)
    {
        headBob.enabled = !cond;
        originalPos = camTransform.localPosition;
        if (!currentlyShaking)
        {
            StopAllCoroutines();
            if (cond) StartCoroutine(StartTimedShake(intensity));
        }
    }

    private IEnumerator StartTimedShake(float intensity)
    {
        currentShakeDuration = shakeDuration;
        currentlyShaking = true;
        while (currentShakeDuration > 0)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount * intensity;
            currentShakeDuration -= Time.deltaTime * decreaseFactor;
            yield return null;
        }

        currentShakeDuration = 0f;
        camTransform.localPosition = originalPos;
        headBob.enabled = true;
        currentlyShaking = false;
    }
}
