public class NPCHealth : Health
{
    private NPCController controller;
    private NPCSFX sfx;

    protected override void Awake()
    {
        base.Awake();
        controller = GetComponent<NPCController>();
        sfx = GetComponent<NPCSFX>();
    }

    public override void DecreaseRemainingHealth(int amt) => DecreaseRemainingHealth(amt, Enums.damageType.None);
    public void DecreaseRemainingHealth(int amt, Enums.damageType type)
    {
        if (invincible || amt < 1) return; //----------------------------------

        base.DecreaseRemainingHealth(amt);
        if (remainingHealth <= 0)
        {
            controller.Die(type);
        }
        else
        {
            sfx?.PlayPainSound();
            StartInvinciblityTimer();
        }
    }
}
