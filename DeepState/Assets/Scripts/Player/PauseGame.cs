using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseGame : MonoBehaviour
{
    [SerializeField]
    private InventoryUIManager inventoryUIManager;
    [SerializeField]
    private bool useExitTabAsPauseScreen;

    private InputManager inputManager;
    private PlayerInventory playerInventory;
    private PlayerController playerController;
    private CheckpointManager checkpointManager;

    public bool UseExitTabAsPauseScreen { get { return useExitTabAsPauseScreen; } }

    private void Awake()
    {
        inputManager = GetComponent<InputManager>();
        playerInventory = GetComponent<PlayerInventory>();
        playerController = GetComponent<PlayerController>();
    }
    private void Start()
    {
        inventoryUIManager.Init();
        ForceClosePauseScreen();
        checkpointManager = GameManager.Instance.CheckpointManager;
    }

    public void TogglePauseScreen()
    {
        if (playerController.IsDead) return; //-----------------

        bool active = !inputManager.InputIsDisabled;

        playerInventory.ForceCloseInventory();
        inventoryUIManager.DisplayPauseScreenUI(active, false);
        GameManager.Instance.PauseGame(active, active, false, false);
        inputManager.DisableInput(active);
    }

    public void DisplayGameOverScreen()
    {
        playerInventory.ForceCloseInventory();
        inventoryUIManager.DisplayPauseScreenUI(true, true);
        GameManager.Instance.PauseGame(true, true, playerController.IsDead, false);
        inputManager.DisableInput(true);
    }

    private void ForceClosePauseScreen()
    {
        playerInventory.ForceCloseInventory();
        inventoryUIManager.DisplayPauseScreenUI(false, false);
        GameManager.Instance.PauseGame(false, false, playerController.IsDead, false);
        inputManager.DisableInput(false);
    }

    #region Buttons
    public void RestartCheckpoint()
    {
        playerController.Respawn();
        checkpointManager.ResetGameStateToLastCheckpoint();
        ForceClosePauseScreen();
    }

    public void RestartLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        ForceClosePauseScreen();
    }

    public void Quit()
    {
        print("You've quit!");
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    #endregion

}
