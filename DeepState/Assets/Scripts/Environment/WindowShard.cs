using UnityEngine;

public class WindowShard : Fragment
{
    protected override void Awake()
    {
        base.Awake();
        rb.isKinematic = true;
        col.enabled = false;
    }

    public override void Reset()
    {
        rb.isKinematic = true;
        col.enabled = false;
        base.Reset();
    }
    
    public void AddForce(Vector3 origin, float explosionForce)
    {
        rb.isKinematic = false;
        col.enabled = true;
        rb.AddExplosionForce(explosionForce, origin, 50f);
    }

}
