using UnityEngine;

public class NightVisionGogglesBattery : MonoBehaviour
{
    private const int MAX_CHARGE_AMOUNT = 99;

    [SerializeField]
    private float drainSpeed = 1f, rechargeSpeed = 1f, rechargeBuffer = 1f;
    [SerializeField]
    private GameObject showWhileEquipped, showWhileOnItemBar;

    private bool gogglesInUse;
    private float chargeAmount, bufferRemaining;
    private PlayerInventory playerInventory;
    private InventoryItemDatabase inventoryItemDatabase;

    public delegate void OnBatteryChargeAmountChanged(float amt);
    public static event OnBatteryChargeAmountChanged onBatteryChargeAmountChanged;


    private void Awake()
    {
        showWhileEquipped.SetActive(false);
        showWhileOnItemBar.SetActive(false);
        gogglesInUse = false;
        chargeAmount = MAX_CHARGE_AMOUNT;
        bufferRemaining = rechargeBuffer;
    }
    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
        CheckpointManager.onResetGameStateToLastCheckpoint += HideBarUIIfNvGogglesNotInInventory;
    }

    private void OnDestroy()
    {
        CheckpointManager.onResetGameStateToLastCheckpoint -= HideBarUIIfNvGogglesNotInInventory;
    }

    public void ShowNvGoggleOnBarUI(bool active)
    {
        showWhileOnItemBar.SetActive(active);
    }
    private void HideBarUIIfNvGogglesNotInInventory()
    {
        ShowNvGoggleOnBarUI(inventoryItemDatabase.GetInventoryItemAmount(Enums.itemType.NightVisionGoggles) > 0);
    }

    public void StartBatteryDrain()
    {
        showWhileEquipped.SetActive(true);
        gogglesInUse = true;
    }
    public void StartBatteryRecharge()
    {
        showWhileEquipped.SetActive(false);
        gogglesInUse = false;
    }

    private void Update()
    {
        if (chargeAmount == MAX_CHARGE_AMOUNT && !gogglesInUse) return; //----------------

        if (gogglesInUse) DrainBattery();
        else RechargeBattery();
    }

    private void DrainBattery()
    {
        if (bufferRemaining < rechargeBuffer) bufferRemaining = rechargeBuffer;

        chargeAmount -= Time.deltaTime * drainSpeed;
        if (chargeAmount < 0f)
        {
            chargeAmount = 0f;
            playerInventory.EquipNightVision(false);
        }
        if (onBatteryChargeAmountChanged != null) onBatteryChargeAmountChanged(chargeAmount);
    }
    private void RechargeBattery()
    {
        if (bufferRemaining > 0f)
        {
            bufferRemaining -= Time.deltaTime;
            return;
        } //---------------------------------------------

        chargeAmount += Time.deltaTime * rechargeSpeed;
        if (chargeAmount > MAX_CHARGE_AMOUNT) chargeAmount = MAX_CHARGE_AMOUNT;
        if (onBatteryChargeAmountChanged != null) onBatteryChargeAmountChanged(chargeAmount);
    }

}
