using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class CameraPostProcessing : MonoBehaviour
{
    [SerializeField]
    private PostProcessProfile defaultProfile;
    [SerializeField]
    private PostProcessProfile nightVisionProfile;

    private Slider gammaSlider;
    private PostProcessVolume volume;
    private ColorGrading colorGrading;
    private float defaultGamma;

    public bool defaultProfileIsActive { get { return volume.profile == defaultProfile; } }

    private void Awake()
    {
        volume = GetComponent<PostProcessVolume>();
    }
    private void Start()
    {
        gammaSlider = PlayerController.Instance.GetComponent<PlayerInventory>().SettingsTabUIRef.Gamma;
        InitValues();
    }

    private void InitValues()
    {
        volume.profile = defaultProfile;
        defaultProfile.TryGetSettings<ColorGrading>(out colorGrading);

        UserSettings gammaSettings = PersistentValues.Instance.GammaSettings;
        if (gammaSettings.isCalibrated)
        {
            colorGrading.gamma.value.w = gammaSettings.val;
            gammaSlider.value = gammaSettings.slider;
        }
        else
        {
            gammaSlider.value = GeneralParams.DEFAULT_GAMMA_SLIDER_VAL;
        }

        defaultGamma = colorGrading.gamma.value.w;
        gammaSlider.onValueChanged.AddListener(delegate { SetGamma(); });
    }

    public void SetDefaultProfile()
    {
        volume.profile = defaultProfile;
    }
    public void SetNightVision()
    {
        volume.profile = nightVisionProfile;
    }

    public void SetDefaultGamma()
    {
        //this calls slider.onValueChanged which calls SetGamma()
        gammaSlider.value = GeneralParams.DEFAULT_GAMMA_SLIDER_VAL;
    }
    private void SetGamma()
    {
        float gammaVal = Mathf.Lerp(GeneralParams.GAMMA_MIN, GeneralParams.GAMMA_MAX, gammaSlider.value);
        colorGrading.gamma.value.w = gammaVal;
        PersistentValues.Instance.SetGamma(gammaVal, gammaSlider.value);
    }

    private void OnDestroy()
    {
        gammaSlider.onValueChanged.RemoveListener(delegate { SetGamma(); });
        //colorGrading.gamma.value.w = defaultGamma;
    }
}
