using UnityEngine;

public class CutsceneInput : Inputs
{
    [SerializeField]
    private GameObject button;
    [SerializeField]
    private GameObject[] objsToEnable;
    [SerializeField]
    private SkipTimeline skipTimeline;

    private void Update()
    {
        if (Input.GetButtonDown(INPUT_FIRE_1))
        {
            if (skipTimeline != null && skipTimeline.Skipped) return; //---------------------------

            GameManager.Instance.UnlockCursor(true);
            button.SetActive(true);
        }

        if (Input.GetButtonDown(INPUT_CANCEL) || Input.GetButtonDown(INPUT_JUMP) || Input.GetButtonDown(INPUT_INTERACT))
        {
            if (skipTimeline != null && skipTimeline.Skipped) return; //---------------------------

            if (button.activeSelf)
            {
                EnableObjsOnBtnClick();
                GameManager.Instance.UnlockCursor(false);
                button.SetActive(false);
                if (skipTimeline != null) skipTimeline.enabled = true;
            }
            else
            {
                GameManager.Instance.UnlockCursor(true);
                button.SetActive(true);
            }
        }
    }

    public void EnableObjsOnBtnClick()
    {
        foreach (GameObject obj in objsToEnable)
        {
            obj.SetActive(true);
        }
    }
}
