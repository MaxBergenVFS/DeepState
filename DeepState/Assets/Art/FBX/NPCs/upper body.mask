%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: upper body
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: mesh_char_cabal
    m_Weight: 0
  - m_Path: mesh_char_hazmat
    m_Weight: 0
  - m_Path: mesh_char_killgore
    m_Weight: 0
  - m_Path: mesh_char_mib
    m_Weight: 0
  - m_Path: mesh_char_otacon
    m_Weight: 0
  - m_Path: mesh_char_rex
    m_Weight: 0
  - m_Path: mesh_char_scientist
    m_Weight: 0
  - m_Path: mesh_char_worker
    m_Weight: 0
  - m_Path: mesh_guard_new
    m_Weight: 0
  - m_Path: mesh_npc_m4
    m_Weight: 0
  - m_Path: rig_deepstate
    m_Weight: 0
  - m_Path: rig_deepstate/spine
    m_Weight: 1
  - m_Path: rig_deepstate/spine/pelvis.L
    m_Weight: 1
  - m_Path: rig_deepstate/spine/pelvis.L/pelvis.L_end
    m_Weight: 1
  - m_Path: rig_deepstate/spine/pelvis.R
    m_Weight: 1
  - m_Path: rig_deepstate/spine/pelvis.R/pelvis.R_end
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001/spine.002
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.L
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L_end
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.R
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R_end
    m_Weight: 0
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/spine.004
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/spine.004/spine.005
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006
    m_Weight: 1
  - m_Path: rig_deepstate/spine/spine.001/spine.002/spine.003/spine.004/spine.005/spine.006/spine.006_end
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.L
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.L/shin.L
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.L/shin.L/foot.L
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.L/shin.L/foot.L/toe.L
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.L/shin.L/foot.L/toe.L/toe.L_end
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.R
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.R/shin.R
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.R/shin.R/foot.R
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.R/shin.R/foot.R/toe.R
    m_Weight: 1
  - m_Path: rig_deepstate/spine/thigh.R/shin.R/foot.R/toe.R/toe.R_end
    m_Weight: 1
