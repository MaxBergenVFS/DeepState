public interface IEquippable
{
    bool ModelIsVisible { get; }
    void Equip();
    void Unequip();
    void ShowModel(bool active);
    void UsePrimary();
    void UsePrimaryHeld();
    void UseSecondary();
    void UseSecondaryHeld();
    void ReloadEquipped(out bool success);
}
