public class NPCSFX : SFX
{
    public void PlayPainSound() => PlaySound3D(NPC_PAIN_PATH);
    public void PlayDeathSound() => PlaySound3D(NPC_DEATH_PATH);
    public void PlayFootstepSound() => PlaySound3D(NPC_FOOTSTEP_PATH);
    public void PlayBloodSplatterSound() => PlaySound3D(NPC_BLOOD_SPLATTER_PATH);
    public void PlayDiegeticHeadShotSound() => PlaySound3D(NPC_HEADSHOT_DIEGETIC_PATH);
    public void PlayNonDiegeticHeadShotSound() =>  PlaySound2D(NPC_HEADSHOT_NONDIEGETIC_PATH);
}
