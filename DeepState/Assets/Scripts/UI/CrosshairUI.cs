using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CrosshairUI : MonoBehaviour
{
    private const float HIT_MARKER_DURATION = 0.1f;

    private Image image;
    private RectTransform rect;
    private float defaultSizeDelta, minSizeDelta, maxSizeDelta, maxPlayerMovementSpeed, currentSpeed;

    [SerializeField]
    private float lerpSpeed = 1f;
    [SerializeField]
    private Image hitMarker;

    private void Awake()
    {
        image = GetComponent<Image>();
        rect = GetComponent<RectTransform>();
        hitMarker.enabled = false;
        defaultSizeDelta = rect.sizeDelta.x;
        minSizeDelta = defaultSizeDelta - (defaultSizeDelta / 2);
        maxSizeDelta = defaultSizeDelta + (defaultSizeDelta / 2);
    }
    private void Start()
    {
        maxPlayerMovementSpeed = PlayerController.Instance.MaxMovementSpeed;
        NPCController.onNPCDeath += DisplayHitMarker;
    }

    private void OnDestroy()
    {
        NPCController.onNPCDeath -= DisplayHitMarker;
    }

    public void DisplayImage(bool active)
    {
        if (active == image.isActiveAndEnabled) return;
        image.enabled = active;
    }

    public void DisplayHitMarker() => DisplayHitMarker(false, Enums.damageType.RangedPlayer);
    private void DisplayHitMarker(bool isFriendly, Enums.damageType type)
    {
        if (isFriendly || type != Enums.damageType.RangedPlayer) return; //--------------------
        CancelInvoke("HideHitMarker");
        hitMarker.enabled = true;
        Invoke("HideHitMarker", HIT_MARKER_DURATION);
    }

    private void HideHitMarker()
    {
        hitMarker.enabled = false;
    }

    public void SetScale(float playerMovementSpeed)
    {
        if (!image.isActiveAndEnabled || currentSpeed == playerMovementSpeed) return;
        currentSpeed = playerMovementSpeed;
        float targetSize = Mathf.Lerp(minSizeDelta, maxSizeDelta, playerMovementSpeed / maxPlayerMovementSpeed);
        StopAllCoroutines();
        StartCoroutine(LerpCrosshairSize(rect.sizeDelta.x, targetSize));
    }

    private IEnumerator LerpCrosshairSize(float start, float end)
    {
        float t = 0f;
        while (t < 1f)
        {
            float size = Mathf.Lerp(start, end, t);
            rect.sizeDelta = new Vector2(size, size);
            t += Time.deltaTime * lerpSpeed;
            yield return null;
        }
        rect.sizeDelta = new Vector2(end, end);
    }
}
