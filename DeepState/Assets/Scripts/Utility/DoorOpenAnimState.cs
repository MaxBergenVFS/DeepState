using UnityEngine;

public class DoorOpenAnimState : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.DOOR_BOOL_IS_OPEN, true);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(AnimParams.DOOR_BOOL_IS_OPEN, false);
    }
}
