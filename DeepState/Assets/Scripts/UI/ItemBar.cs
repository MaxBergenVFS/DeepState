using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemBar : ItemDropHandler
{
    private const float RECT_Y_POS_HIDDEN = -100f;
    private const float LERP_SPEED = 4f;

    [SerializeField]
    private ItemSlot[] itemSlots;
    [SerializeField]
    private GameObject background, nvGoggleBatteryAmount;
    [SerializeField]
    private Color slotSelectedColor, equipmentBorderColor;

    private PlayerSFX sfx;
    private PlayerHands playerHands;
    private RectTransform backgroundRect;
    private float backgroundRectDefaultYPos;
    private InventoryUIManager inventoryUI;
    private GameObject equipmentRenderObj;

    public delegate void OnItemAddedToSlot(Enums.itemType type, ItemSlot slot);
    public static event OnItemAddedToSlot onItemAddedToSlot;

    public ItemSlot[] ItemSlots { get { return itemSlots; } }

    protected override void Start()
    {
        base.Start();
        backgroundRect = background.GetComponent<RectTransform>();
        backgroundRectDefaultYPos = backgroundRect.localPosition.y;
        InitItemSlotColors();
        playerHands = PlayerController.Instance.GetComponent<PlayerHeldItem>().PlayerHands;
        sfx = PlayerController.Instance.GetComponent<PlayerSFX>();
        inventoryUI = playerInventory.InventoryUI;
        equipmentRenderObj = playerInventory.EquipmentRenderObj;
        PlayerController.onPlayerDeath += HideUI;
        PadUI.onDisplayPadUI += HideUI;
        Codec.onDisplayCodecUI += HideUI;
    }

    private void OnDestroy()
    {
        PlayerController.onPlayerDeath -= HideUI;
        PadUI.onDisplayPadUI -= HideUI;
        Codec.onDisplayCodecUI -= HideUI;
    }

    public void UseItemAtSlotIndex(int index)
    {
        if (index >= itemSlots.Length || index < 0 || playerHands.CurrentlyUsingItem) return; //---

        ItemSlot itemSlot = itemSlots[index];
        IInventoryItem item = itemSlot.ItemDragHandler.ItemRef;

        if (item == null)
        {
            itemSlot.DisplaySlotSelectedImage();
            return;
        } //-----------------------------------------------------------------------

        //item exists
        bool itemWasSuccessfullyUsed;
        item.UseFromInventory(out itemWasSuccessfullyUsed);

        if (!itemWasSuccessfullyUsed)
        {
            itemSlot.DisplayItemCannotBeUsedImage();
            return;
        } //----------------------------------------------------

        //item was successfully used
        itemSlot.DisplayItemCanBeUsedImage();
        playerInventory.RefreshItemsOnBar();
        if (item.IsEquippable)
        {
            itemSlot.SetItemEquippedImageColor(ItemTypeIsEquipment(item.ItemType));
            itemSlot.ItemEquippedImage.enabled = item.IsEquipped;
        }
    }

    public void IncrementEquippedItem() => IncrementEquippedItemByAmount(1);
    public void DecrementEquippedItem() => IncrementEquippedItemByAmount(-1);

    private void IncrementEquippedItemByAmount(int amt)
    {
        if (playerHands.CurrentlyUsingItem) return; //--------------

        int equippedItemIndex, itemToEquipIndex;
        List<int> equippableIndicies = new List<int>();
        equippableIndicies = GetEquippableIndicies(out equippedItemIndex);

        if (equippableIndicies.Count < 1) return; //---------------------------------

        itemToEquipIndex = equippedItemIndex + amt;
        if (itemToEquipIndex >= equippableIndicies.Count) itemToEquipIndex = 0;
        else if (itemToEquipIndex < 0) itemToEquipIndex = equippableIndicies.Count - 1;

        UseItemAtSlotIndex(equippableIndicies[itemToEquipIndex]);
    }
    private List<int> GetEquippableIndicies(out int equippedItemIndex)
    {
        equippedItemIndex = 0;
        List<int> indicies = new List<int>();
        for (int i = 0; i < itemSlots.Length; i++)
        {
            IInventoryItem item = itemSlots[i].ItemDragHandler.ItemRef;

            if (item == null || ItemTypeIsEquipment(item.ItemType)) continue; //----------

            if (item.IsEquippable) indicies.Add(i);
            if (item.IsEquipped) equippedItemIndex = indicies.Count - 1;
        }
        return indicies;
    }

    private bool ItemTypeIsEquipment(Enums.itemType itemType)
    {
        return (itemType == Enums.itemType.RedKeycard || itemType == Enums.itemType.BlueKeycard || itemType == Enums.itemType.NightVisionGoggles);
    }

    protected override void OnDropInside(PointerEventData eventData)
    {
        ItemDragHandler itemDragHandler = eventData.pointerDrag.gameObject.GetComponent<ItemDragHandler>();

        if (itemDragHandler == null) return; //--------------------------------------------------------------

        IInventoryItem draggedItem = itemDragHandler.ItemRef;
        Enums.itemSlotType slotType = itemDragHandler.SlotType;

        if (draggedItem == null) return; //-------------------------------------------------------------------

        List<GameObject> objs = new List<GameObject>();
        objs = eventData.hovered;

        foreach (GameObject obj in objs)
        {
            if (obj.GetComponent<ItemSlot>() != null)
            {
                //ITEM SLOT FOUND
                ItemSlot slotHovered = obj.GetComponent<ItemSlot>();
                int index;

                //REMOVE ITEM FROM PREVIOUS SLOT IF IT'S ALREADY ON THE BAR
                if (ItemBarAlreadyContainsItem(draggedItem, out index)) RemoveItemFromSlot(itemSlots[index]);

                //SWAP ITEMS IF HOVERED SLOT ALREADY HAS AN ITEM
                IInventoryItem itemOnHoveredSlot = slotHovered.ItemDragHandler.ItemRef;
                if (itemOnHoveredSlot != null)
                {
                    //ONLY SWAP ITEMS IF DRAGGED ITEM IS ALSO FROM BAR
                    if (slotType == Enums.itemSlotType.Bar) AddItemToSlot(itemOnHoveredSlot, itemSlots[index]);
                }

                //SET HOVERED SLOT ITEM TO DRAGGED ITEM
                AddItemToSlot(draggedItem, slotHovered);
                playerInventory.RefreshItemsOnBar();
                return; //-------------------------------------------------------------------------------------
            }
        }
        //ITEM SLOT NOT FOUND
    }

    public void AddItemToAvailableSlotOnBar(IInventoryItem itemRef, bool playSfx)
    {
        if (ItemBarAlreadyContainsItem(itemRef)) return; //---------------------

        foreach (ItemSlot itemSlot in itemSlots)
        {
            if (itemSlot.ItemDragHandler.ItemRef == null)
            {
                AddItemToSlot(itemRef, itemSlot, playSfx);
                return;
            } //--------------------------------------------
        }
    }
    public void AddItemToSlotOnBar(IInventoryItem itemRef, int slotIndex)
    {
        if (slotIndex >= itemSlots.Length) return; //---------------

        int index;
        if (ItemBarAlreadyContainsItem(itemRef, out index)) RemoveItemFromSlot(itemSlots[index]);
        AddItemToSlot(itemRef, itemSlots[slotIndex]);
    }

    private void AddItemToSlot(IInventoryItem itemRef, ItemSlot itemSlot) => AddItemToSlot(itemRef, itemSlot, true);
    private void AddItemToSlot(IInventoryItem itemRef, ItemSlot itemSlot, bool playSfx)
    {
        if (playSfx) sfx.PlayAddItemToItemBarSound();
        itemSlot.ItemDragHandler.ItemRef = itemRef;

        if (onItemAddedToSlot != null) onItemAddedToSlot(itemRef.ItemType, itemSlot);
    }
    private void RemoveItemFromSlot(ItemSlot itemSlot)
    {
        //TODO maybe add sfx here. Not sure if necessary
        itemSlot.ItemDragHandler.ItemRef = null;
    }

    private bool ItemBarAlreadyContainsItem(IInventoryItem itemRef)
    {
        int index;
        return ItemBarAlreadyContainsItem(itemRef, out index);
    }
    private bool ItemBarAlreadyContainsItem(IInventoryItem itemRef, out int index)
    {
        for (int i = 0; i < itemSlots.Length; i++)
        {
            IInventoryItem itemOnBar = itemSlots[i].ItemDragHandler.ItemRef;

            if (itemOnBar == null) continue; //------------

            if (itemOnBar.ItemType == itemRef.ItemType)
            {
                index = i;
                return true;
            }
        }
        index = 0;
        return false;
    }

    private void InitItemSlotColors()
    {
        foreach (ItemSlot itemSlot in itemSlots)
        {
            itemSlot.InitColors(equipmentBorderColor, slotSelectedColor);
        }
    }

    private void HideUI(bool active) => HideUI(active, false);
    public void HideUI(bool active, bool lerp)
    {
        if (backgroundRect == null) return; //-------------------

        if (lerp)
        {
            StopAllCoroutines();
            if (active)
            {
                HideExtraUI(true);
                StartCoroutine(LerpBackgroundRectYPos(RECT_Y_POS_HIDDEN, false));
            }
            else
            {
                StartCoroutine(LerpBackgroundRectYPos(backgroundRectDefaultYPos, true));
            }
        }
        else
        {
            background.SetActive(!active);
            HideExtraUI(active);
        }
    }

    private void HideExtraUI(bool active)
    {
        if (!active && inventoryUI.DeathScreenIsDisplayed) return; //-----------
        nvGoggleBatteryAmount.SetActive(!active);
        equipmentRenderObj.SetActive(!active);
    }

    private IEnumerator LerpBackgroundRectYPos(float targetYPos, bool enableExtraUIAfterLerp)
    {
        float t = 0f;
        float startingYPos = backgroundRect.localPosition.y;
        float y = startingYPos;

        while (t < 1f)
        {
            t += Time.unscaledDeltaTime * LERP_SPEED;
            y = Mathf.Lerp(startingYPos, targetYPos, t);
            backgroundRect.localPosition = new Vector3(0, y, 0);
            yield return null;
        }
        backgroundRect.localPosition = new Vector3(0, targetYPos, 0);
        if (enableExtraUIAfterLerp) HideExtraUI(false);
    }
}
