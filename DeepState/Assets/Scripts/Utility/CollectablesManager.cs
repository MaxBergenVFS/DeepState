using System.Collections.Generic;
using UnityEngine;

public class CollectablesManager : MonoBehaviour
{
    //the bool tracks if the datapad has been found by the player
    private Dictionary<Datapad, bool> datapads = new Dictionary<Datapad, bool>();
    private List<Datapad> datapadsFoundSinceLastCheckpoint = new List<Datapad>();
    private int moneyFound = 0;

    public delegate void OnDatapadFound(int amt);
    public static event OnDatapadFound onDatapadFound;
    public delegate void OnDatapadAddedToScene(int amt);
    public static event OnDatapadAddedToScene onDatapadAddedToScene;
    public delegate void OnMoneyFound(int amt);
    public static event OnMoneyFound onMoneyFound;

    private void Start()
    {
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
    }

    private void SnapshotState()
    {
        datapadsFoundSinceLastCheckpoint.Clear();
    }
    private void RevertState()
    {
        foreach (Datapad datapad in datapadsFoundSinceLastCheckpoint)
        {
            datapads[datapad] = false;
        }
        UpdateDatapadsFound();
        datapadsFoundSinceLastCheckpoint.Clear();
    }

    public void AddToDatapadsInScene(Datapad datapad)
    {
        datapads.Add(datapad, false);
        if (onDatapadAddedToScene != null) onDatapadAddedToScene(datapads.Count);
    }
    public void AddToDatapadsFound(Datapad datapad)
    {
        datapads[datapad] = true;
        datapadsFoundSinceLastCheckpoint.Add(datapad);
        UpdateDatapadsFound();
    }
    private void UpdateDatapadsFound()
    {
        int amt = 0;
        foreach (var datapad in datapads)
        {
            //if Value = true, then the datapad in the Dict has been found by the player
            if (datapad.Value) amt++;
        }
        if (onDatapadFound != null) onDatapadFound(amt);
    }
    public bool DatapadHasBeenFound(Datapad datapad)
    {
        return datapads[datapad];
    }

    public void IncreaseMoneyFound(int amt)
    {
        if (amt < 0) return; //------------
        moneyFound += amt;
        if (onMoneyFound != null) onMoneyFound(moneyFound);
    }
}
