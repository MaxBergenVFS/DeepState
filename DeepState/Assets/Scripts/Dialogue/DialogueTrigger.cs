﻿using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField] 
    protected Dialogue[] dialogue;

    protected DialogueManager dialogueManager;
    protected int currentIndex;
    protected bool dialogueFinished;
    protected string characterName { get { return dialogue[currentIndex].characterName; } }

    protected virtual void Awake()
    {
        dialogueFinished = false;
        currentIndex = 0;
    }
    protected virtual void Start()
    {
        dialogueManager = DialogueManager.Instance;
    }

    protected virtual void TriggerDialogue()
    {
        dialogueManager.StartDialogue(dialogue[currentIndex]);
        if (!dialogueManager.CurrentlyInDialogue)
        {
            if (currentIndex >= dialogue.Length - 1)
            {
                //FINISH AND RESTART FINAL DIALOGUE
                currentIndex = dialogue.Length - 1;
                if (!dialogueFinished) DialogueFinished();
            }
            else
            {
                //FINISH AND GET NEXT DIALOGUE
                DoAfterCurrentDialogue();
                currentIndex++;
            }
        }
    }

    private void DialogueFinished()
    {
        dialogueFinished = true;
        DoAfterFinalDialogue();
    }
    protected virtual void DoAfterFinalDialogue() { }
    protected virtual void DoAfterCurrentDialogue() { }
}
