using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDropHandler : MonoBehaviour, IDropHandler
{
    [SerializeField]
    private RectTransform rectOverride;

    private RectTransform rect;
    protected PlayerInventory playerInventory;

    private void Awake()
    {
        if (rectOverride != null) rect = rectOverride;
        else rect = GetComponent<RectTransform>();
    }
    protected virtual void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        //TODO make OnDropOutside actually work
        if (!RectTransformUtility.RectangleContainsScreenPoint(rect, Input.mousePosition)) OnDropOutside(eventData);
        else OnDropInside(eventData);
        //print("OnDrop");
        //print($"RectangleContainsScreenPoint: {RectTransformUtility.RectangleContainsScreenPoint(rect, Input.mousePosition)}");
    }

    private void OnDropOutside(PointerEventData eventData)
    {
        IInventoryItem item = eventData.pointerDrag.gameObject.GetComponent<ItemDragHandler>().ItemRef;
        if (item != null) playerInventory.RemoveItemFromGrid(item);
        //print("OnDropOutside");
    }

    protected virtual void OnDropInside(PointerEventData eventData)
    {
        //print("OnDropInside");
    }
}
