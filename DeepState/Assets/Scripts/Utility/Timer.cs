using UnityEngine;

public class Timer : MonoBehaviour
{
    private float timeElapsed;

    private void Awake()
    {
        ResetTimer();
    }

    private void Update()
    {
        timeElapsed += Time.deltaTime;
    }

    public void StopTimer(out int time)
    {
        time = (int)timeElapsed;
        this.enabled = false;
    }
    public void ResetTimer()
    {
        timeElapsed = 0f;
    }
}
