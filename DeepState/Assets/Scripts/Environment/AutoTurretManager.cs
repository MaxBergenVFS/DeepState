using UnityEngine;

public class AutoTurretManager : MonoBehaviour
{
    private const string NOTIF_DEACTIVATED = "Auto Turrets are OFFLINE";
    private const string NOTIF_TARGET_PLAYER = "Auto Turrets are ONLINE";
    private const string NOTIF_TARGET_NPC = "Now targeting non-hostiles";

    [SerializeField]
    private AutoTurret[] autoTurrets;
    [SerializeField]
    private int phaseToDeactivateOn = 2;

    private bool isDeactivated, isHacked;
    private NotificationUI notif;
    private PlayerInventory playerInventory;
    private InventoryItemDatabase inventoryItemDatabase;
    private SecurityMonitorCameraManager securityMonitorCameraManager;
    private CheckpointSettings snapshot = new CheckpointSettings();

    public bool IsDeactivated { get { return isDeactivated; } }

    private void Awake()
    {
        isHacked = isDeactivated = false;
    }
    private void Start()
    {
        notif = NotificationUI.Instance;
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
        securityMonitorCameraManager = GameManager.Instance.SecurityMonitorCameraManager;
        SnapshotState();
        CheckpointManager.onSetCheckpoint += SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint += RevertState;
        LevelPhaseChanger.onPhaseChange += DeactivateAutoTurretsOnPhaseChange;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= SnapshotState;
        CheckpointManager.onResetGameStateToLastCheckpoint -= RevertState;
        LevelPhaseChanger.onPhaseChange -= DeactivateAutoTurretsOnPhaseChange;
    }

    private void SnapshotState()
    {
        snapshot.isDeactivated = isDeactivated;
        snapshot.isHacked = isHacked;
        foreach (AutoTurret autoTurret in autoTurrets)
        {
            autoTurret.SnapshotState();
        }
    }
    private void RevertState()
    {
        isDeactivated = snapshot.isDeactivated;
        isHacked = snapshot.isHacked;
        foreach (AutoTurret autoTurret in autoTurrets)
        {
            autoTurret.RevertState();
        }
        securityMonitorCameraManager.HandleButtonTextDisplay(isDeactivated);
        if (isDeactivated) DeactivateAutoTurrets();
        else ActivateAutoTurrets();
    }

    public void ToggleActivation()
    {
        if (isDeactivated) ActivateAutoTurrets();
        else DeactivateAutoTurrets();
    }

    public void Hack()
    {
        if (isHacked) return; //------------------

        isHacked = true;
        SetAutoTurretsToAttackNPC();
        playerInventory.DecrementItemAmount(Enums.itemType.HackingTool);
    }

    public int GetHackingToolAmt()
    {
        return inventoryItemDatabase.GetInventoryItemAmount(Enums.itemType.HackingTool);
    }

    private void DeactivateAutoTurrets()
    {
        foreach (AutoTurret autoTurret in autoTurrets)
        {
            autoTurret.Deactivate();
        }
        notif.DisplayText(NOTIF_DEACTIVATED);
        isDeactivated = true;
    }
    private void DeactivateAutoTurretsOnPhaseChange(int phase)
    {
        if (phase == phaseToDeactivateOn) DeactivateAutoTurrets();
    }
    private void ActivateAutoTurrets()
    {
        if (isHacked) SetAutoTurretsToAttackNPC();
        else SetAutoTurretsToAttackPlayer();
        isDeactivated = false;
    }
    private void SetAutoTurretsToAttackPlayer()
    {
        foreach (AutoTurret autoTurret in autoTurrets)
        {
            autoTurret.AttackPlayer();
        }
        notif.DisplayText(NOTIF_TARGET_PLAYER);
    }
    private void SetAutoTurretsToAttackNPC()
    {
        foreach (AutoTurret autoTurret in autoTurrets)
        {
            autoTurret.AttackNPCs();
        }
        notif.DisplayText(NOTIF_TARGET_NPC);
    }

}
