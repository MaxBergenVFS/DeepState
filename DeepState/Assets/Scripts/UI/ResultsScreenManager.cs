using TMPro;
using UnityEngine;

public class ResultsScreenManager : MonoBehaviour
{
    [SerializeField]
    private Timer timer;
    [SerializeField]
    private TextMeshProUGUI enemiesKilledText, friendliesKilledText, headshotsText,
        deathsText, moneyFoundText, datapadsFoundText, datapadsInSceneText, timeMinText, timeSecText;

    private int enemiesKilled, friendliesKilled, headshots,
        deaths, moneyFound, datapadsFound, datapadsInScene;
    
    private void Start()
    {
        PlayerController.onPlayerDeath += IncrementDeaths;
        NPCController.onNPCDeath += IncrementNPCsKilled;
        NPCController.onNPCHeadshot += IncrementHeadshots;
        CollectablesManager.onDatapadFound += SetDatapadsFound;
        CollectablesManager.onDatapadAddedToScene += SetDatapadsInScene;
        CollectablesManager.onMoneyFound += SetMoneyFound;
    }

    private void OnDestroy()
    {
        PlayerController.onPlayerDeath -= IncrementDeaths;
        NPCController.onNPCDeath -= IncrementNPCsKilled;
        NPCController.onNPCHeadshot -= IncrementHeadshots;
        CollectablesManager.onDatapadFound -= SetDatapadsFound;
        CollectablesManager.onDatapadAddedToScene -= SetDatapadsInScene;
        CollectablesManager.onMoneyFound -= SetMoneyFound;
    }

    // private void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.I))
    //         Display();
    //     if (Input.GetKeyDown(KeyCode.O))
    //         Hide();
    // }

    public void Display()
    {
        enemiesKilledText.text = enemiesKilled.ToString();
        friendliesKilledText.text = friendliesKilled.ToString();
        headshotsText.text = headshots.ToString();
        deathsText.text = deaths.ToString();
        datapadsFoundText.text = datapadsFound.ToString();
        datapadsInSceneText.text = datapadsInScene.ToString();
        moneyFoundText.text = moneyFound.ToString();
        DisplayTime();
    }
    public void Hide()
    {
        timer.enabled = true;
    }

    private void DisplayTime()
    {
        int totalSecondsElapsed;
        timer.StopTimer(out totalSecondsElapsed);

        int mins = totalSecondsElapsed / 60;
        timeMinText.text = mins.ToString();

        int sec = (totalSecondsElapsed - (mins * 60));
        string secText;
        if (sec < 10) secText = "0" + sec.ToString();
        else secText = sec.ToString();
        timeSecText.text = secText;
    }

    private void IncrementNPCsKilled(bool isFriendly, Enums.damageType type)
    {
        if (type == Enums.damageType.RangedNPC || type == Enums.damageType.None) return; //------------

        if (isFriendly)
        {
            if (friendliesKilled < 0) friendliesKilled = 0;
            friendliesKilled++;
        }
        else
        {
            if (enemiesKilled < 0) enemiesKilled = 0;
            enemiesKilled++;
        }
    }
    private void IncrementHeadshots(bool isFriendly)
    {
        if (isFriendly) return; //-----------------
        if (headshots < 0) headshots = 0;
        headshots++;
    }
    private void IncrementDeaths(bool playerIsDead)
    {
        if (!playerIsDead) return; //--------------
        if (deaths < 0) deaths = 0;
        deaths++;
    }
    private void SetDatapadsFound(int amt)
    {
        if (amt < 0) amt = 0;
        datapadsFound = amt;
    }
    private void SetDatapadsInScene(int amt)
    {
        if (amt < 0) amt = 0;
        datapadsInScene = amt;
    }
    private void SetMoneyFound(int amt)
    {
        if (amt < 0) amt = 0;
        moneyFound = amt;
    }
}
