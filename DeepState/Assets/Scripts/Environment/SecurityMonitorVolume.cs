using UnityEngine;

public class SecurityMonitorVolume : MonoBehaviour
{
    [SerializeField]
    private SecurityMonitor securityMonitor;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            securityMonitor.DisplayCurrentFeedOnMonitor();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            securityMonitor.ClearFeeds();
        }
    }
}
