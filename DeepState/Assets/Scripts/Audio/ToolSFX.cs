public class ToolSFX : SFX
{
    private Enums.itemType itemType;

    private string actionPath, positiveFeedbackPath, negativeFeedbackPath;

    private void Start()
    {
        InitPaths();
    }

    private void InitPaths()
    {
        itemType = GetComponent<PlayerTool>().ItemType;
        actionPath = positiveFeedbackPath = negativeFeedbackPath = TOOL_PREFIX;
        switch (itemType)
        {
            case (Enums.itemType.HackingTool):
                actionPath += HACKING_TOOL_ACTION_PATH;
                positiveFeedbackPath += HACKING_TOOL_POSITIVE_FEEDBACK_PATH;
                negativeFeedbackPath += HACKING_TOOL_NEGATIVE_FEEDBACK_PATH;
                break;
            case (Enums.itemType.LockPick):
                actionPath += LOCKPICK_ACTION_PATH;
                positiveFeedbackPath += LOCKPICK_POSITIVE_FEEDBACK_PATH;
                negativeFeedbackPath += LOCKPICK_NEGATIVE_FEEDBACK_PATH;
                break;
            case (Enums.itemType.Wrench):
                actionPath += WRENCH_ACTION_PATH;
                positiveFeedbackPath += WRENCH_POSITIVE_FEEDBACK_PATH;
                negativeFeedbackPath += WRENCH_NEGATIVE_FEEDBACK_PATH;
                break;
            case (Enums.itemType.CheckpointDevice):
                //TODO change these to unique sounds
                actionPath += CHECKPOINT_DEVICE_ACTION_PATH;
                positiveFeedbackPath += CHECKPOINT_DEVICE_POSITIVE_FEEDBACK_PATH;
                negativeFeedbackPath += CHECKPOINT_DEVICE_NEGATIVE_FEEDBACK_PATH;
                break;
            case (Enums.itemType.None):
            default:
                break;
        }
    }

    public void PlayWrenchSwingSound() => PlaySound2D(TOOL_PREFIX + WRENCH_SWING_PATH);
    public void PlayWrenchHitSound() => PlaySound2D(TOOL_PREFIX + WRENCH_HIT_PATH);
    public void PlayActionSound() => PlaySound2D(actionPath);
    public void PlayPositiveFeedbackSound() => PlaySound2D(positiveFeedbackPath);
    public void PlayNegativeFeedbackSound() => PlaySound2D(negativeFeedbackPath);
}
