using UnityEngine;

public class AlarmSFX : SFX
{
    private FMOD.Studio.EventInstance alarmEventInstance;

    [SerializeField]
    private int alertZone = 0;

    private void Start()
    {
        alarmEventInstance = FMODUnity.RuntimeManager.CreateInstance(SFX_PREFIX + ALARM_PATH);
        AlertZoneManager.onAlertZoneStatusChange += AlarmSound;
    }

    private void OnDestroy()
    {
        StopInstance(alarmEventInstance);
        AlertZoneManager.onAlertZoneStatusChange -= AlarmSound;
    }

    private void AlarmSound(int zone, bool alert)
    {
        if (alertZone != zone) return;

        FMOD.Studio.PLAYBACK_STATE state;
        alarmEventInstance.getPlaybackState(out state);
        if (alert && state != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            StartInstanceAttached(alarmEventInstance);
        }
        else if (!alert && state != FMOD.Studio.PLAYBACK_STATE.STOPPED)
        {
            StopInstance(alarmEventInstance);
        }
    }
}
