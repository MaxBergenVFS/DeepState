using System.Collections;
using UnityEngine;

[RequireComponent(typeof(HandSway))]
public class PlayerHands : MonoBehaviour
{
    #region inits
    private const float MELEE_DAMAGE_FORCE = 0f;
    private const float HOLSTER_DELAY = 0.4f;
    private const Enums.damageType DAMAGE_TYPE = Enums.damageType.Melee;

    [SerializeField]
    private GameObject hands, meleeEquippedUI;
    [SerializeField]
    private PlayerGun akRifle, m4Rifle, pistol;
    [SerializeField]
    private PlayerTool wrench, hackingTool, lockPick, checkpointDevice;
    [SerializeField]
    private InventoryItemDatabase inventoryItemDatabase;
    [SerializeField]
    protected Transform meleeDamageOrigin;
    [SerializeField]
    private float meleeDamageRadius = 1.0f;
    [SerializeField]
    private int meleeDamageAmount = 8;
    [SerializeField]
    private LayerMask whatIsDamageableByMelee;

    private IEquippable equippedItem;
    private PlayerController player;
    private PlayerInventory playerInventory;
    private CameraController playerCamera;
    private InputManager inputManager;
    private Enums.itemType equippedItemType;
    private Enums.itemType snapshottedItemType;
    private Animator anim;
    private PlayerSFX sfx;
    private HandSway handSway;
    private bool meleeIsEquipped;

    public bool HandsEnabled { get { return hands.activeSelf; } }
    public float HolsterDelay { get { return HOLSTER_DELAY; } }
    public Enums.itemType EquippedItemType { get { return equippedItemType; } }
    public bool CurrentlyUsingItem { get { return CurrentlyPlayingAnimation(AnimParams.PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION); } }
    public bool CurrentlyHolsteringItem { get { return CurrentlyPlayingAnimation(AnimParams.PLAYER_BOOL_HOLSTERING); } }

    public delegate void OnItemEquipped(Enums.itemType itemType);
    public static event OnItemEquipped onItemEquipped;
    public delegate void OnMeleeEquipped(bool active);
    public static event OnMeleeEquipped onMeleeEquipped;
    #endregion

    private void Awake()
    {
        anim = GetComponent<Animator>();
        handSway = GetComponent<HandSway>();
        equippedItem = null;
        equippedItemType = snapshottedItemType = Enums.itemType.None;
        meleeIsEquipped = false;
        meleeEquippedUI?.SetActive(false);
        anim.SetBool(AnimParams.PLAYER_BOOL_HOLSTERING, false);
        anim.SetBool(AnimParams.PLAYER_BOOL_CURRENTLY_PLAYING_USE_ANIMATION, false);
        HideHandsIfNothingEquipped();
    }
    private void Start()
    {
        PlayerGun.onAmmoInClipChanged += SetEquippedGunIsEmpty;
        PlayerController.onPlayerDeath += HideHandsOnPlayerDeath;
        player = PlayerController.Instance;
        playerInventory = player.GetComponent<PlayerInventory>();
        playerCamera = player.GetComponent<CameraController>();
        inputManager = player.GetComponent<InputManager>();
        sfx = player.GetComponent<PlayerSFX>();
    }

    private void OnDestroy()
    {
        PlayerGun.onAmmoInClipChanged -= SetEquippedGunIsEmpty;
        PlayerController.onPlayerDeath -= HideHandsOnPlayerDeath;
    }

    private void SetEquippedGunIsEmpty(int amtInClip) => anim.SetBool(AnimParams.PLAYER_BOOL_EQUIPPED_GUN_IS_EMPTY, amtInClip < 1);
    public void EquipSilencerOnPistol(bool active) => pistol.EquipSilencer(active);
    public void EquipSilencerOnPistol() => pistol.EquipSilencer();

    private void Holster() => anim.SetBool(AnimParams.PLAYER_BOOL_HOLSTERING, true);

    public void Equip(Enums.itemType itemType)
    {
        if ((equippedItemType != Enums.itemType.None || meleeIsEquipped) && !inputManager.InventoryIsOpen) Holster();
        if (meleeIsEquipped) ForceUnequipMelee();

        switch (itemType)
        {
            case (Enums.itemType.AkRifle):
                Equip(akRifle, itemType);
                playerInventory.RefreshAmmoInInventoryDisplay(inventoryItemDatabase.GetAmmoType(itemType));
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_AK);
                break;
            case (Enums.itemType.M4Rifle):
                Equip(m4Rifle, itemType);
                playerInventory.RefreshAmmoInInventoryDisplay(inventoryItemDatabase.GetAmmoType(itemType));
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_M4);
                break;
            case (Enums.itemType.Wrench):
                Equip(wrench, itemType);
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_WRENCH);
                break;
            case (Enums.itemType.Pistol):
                Equip(pistol, itemType);
                playerInventory.RefreshAmmoInInventoryDisplay(inventoryItemDatabase.GetAmmoType(itemType));
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_PISTOL);
                break;
            case (Enums.itemType.HackingTool):
                Equip(hackingTool, itemType);
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_HACKING_TOOL);
                break;
            case (Enums.itemType.LockPick):
                Equip(lockPick, itemType);
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_LOCKPICK);
                break;
            case (Enums.itemType.CheckpointDevice):
                Equip(checkpointDevice, itemType);
                AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_CHECKPOINT_ITEM);
                break;
            default:
                break;
        }
    }
    private void Equip(IEquippable item, Enums.itemType itemType)
    {
        if (inventoryItemDatabase.GetInventoryItemAmount(itemType) < 1) return; //-----------

        ForceUnequipMelee();
        equippedItem?.Unequip();
        ShowHands();
        equippedItem = item;
        equippedItemType = itemType;
        equippedItem.Equip();
        if (onItemEquipped != null) onItemEquipped(equippedItemType);
    }
    public void Unequip()
    {
        ResetEquippedItem();
        Holster();
        HideHandsIfNothingEquippedAfterHolstering();
        if (meleeIsEquipped)
        {
            meleeIsEquipped = false;
            meleeEquippedUI?.SetActive(false);
        }
    }
    public void ReEquipItem()
    {
        if (equippedItemType == Enums.itemType.None) ShowHands(false);
        else Equip(equippedItemType);
    }

    public void ShowModel(Enums.itemType itemType, bool active)
    {
        switch (itemType)
        {
            case (Enums.itemType.AkRifle):
                akRifle.ShowModel(active);
                break;
            case (Enums.itemType.M4Rifle):
                m4Rifle.ShowModel(active);
                break;
            case (Enums.itemType.Pistol):
                pistol.ShowModel(active);
                break;
            case (Enums.itemType.Wrench):
                wrench.ShowModel(active);
                break;
            case (Enums.itemType.HackingTool):
                hackingTool.ShowModel(active);
                break;
            case (Enums.itemType.LockPick):
                lockPick.ShowModel(active);
                break;
            case (Enums.itemType.CheckpointDevice):
                checkpointDevice.ShowModel(active);
                break;
            case (Enums.itemType.None):
            default:
                break;
        }
    }
    public void HandleEquippedItemAndShownModelDiscrepancy()
    {
        if (equippedItemType == Enums.itemType.None)
        {
            HideAllModels();
            if (!meleeIsEquipped) ShowHands(false);
            return;
        } //-------------------------------------------------------------

        ShowHands();

        if (equippedItemType == Enums.itemType.CheckpointDevice && !checkpointDevice.ModelIsVisible) checkpointDevice.ShowModel(true);

        // if (equippedItemType != Enums.itemType.AkRifle && akRifle.ModelIsVisible) akRifle.ShowModel(false);
        // if (equippedItemType != Enums.itemType.M4Rifle && m4Rifle.ModelIsVisible) m4Rifle.ShowModel(false);
        // if (equippedItemType != Enums.itemType.Pistol && pistol.ModelIsVisible) pistol.ShowModel(false);
        // if (equippedItemType != Enums.itemType.Wrench && wrench.ModelIsVisible) wrench.ShowModel(false);
        // if (equippedItemType != Enums.itemType.HackingTool && hackingTool.ModelIsVisible) hackingTool.ShowModel(false);
        // if (equippedItemType != Enums.itemType.LockPick && lockPick.ModelIsVisible) lockPick.ShowModel(false);
        // if (equippedItemType != Enums.itemType.CheckpointDevice && checkpointDevice.ModelIsVisible) checkpointDevice.ShowModel(false);
    }
    public void HideAllModels()
    {
        akRifle.ShowModel(false);
        m4Rifle.ShowModel(false);
        pistol.ShowModel(false);
        wrench.ShowModel(false);
        hackingTool.ShowModel(false);
        lockPick.ShowModel(false);
        checkpointDevice.ShowModel(false);
    }

    public void ToggleMelee()
    {
        if (CurrentlyUsingItem) return; //--------------
        if (meleeIsEquipped)
        {
            Holster();
            HideHandsIfNothingEquippedAfterHolstering();
            meleeEquippedUI?.SetActive(false);
            meleeIsEquipped = false;
        }
        else
        {
            if (equippedItemType != Enums.itemType.None) Holster();
            ResetEquippedItem();
            AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_MELEE);
            ShowHands();
            meleeEquippedUI?.SetActive(true);
            meleeIsEquipped = true;
            if (onItemEquipped != null) onItemEquipped(Enums.itemType.None);
        }
        if (onMeleeEquipped != null) onMeleeEquipped(meleeIsEquipped);
        playerInventory.RefreshItemsOnBar();
    }
    private void ForceUnequipMelee()
    {
        meleeIsEquipped = false;
        HideHandsIfNothingEquippedAfterHolstering();
        meleeEquippedUI?.SetActive(false);
        if (onMeleeEquipped != null) onMeleeEquipped(false);
    }

    private void ResetEquippedItem()
    {
        if (equippedItem != null)
        {
            equippedItem.Unequip();
            equippedItem = null;
        }
        if (equippedItemType != Enums.itemType.None) equippedItemType = Enums.itemType.None;
    }

    public void UnequipIfDepleted()
    {
        if (meleeIsEquipped || !inventoryItemDatabase.GetInventoryItemIsConsumable(equippedItemType)) return; //------------
        if (inventoryItemDatabase.GetInventoryItemAmount(equippedItemType) < 1) Unequip();
    }

    private void AnimSetTrigger(string trigger) => AnimSetTrigger(trigger, false);
    private void AnimSetTrigger(string trigger, bool bypassDelay)
    {
        if (!bypassDelay && CurrentlyHolsteringItem)
        {
            StopAllCoroutines();
            StartCoroutine(DelayedAnimSetTrigger(trigger));
        }
        else
        {
            anim.SetTrigger(trigger);
        }
    }
    private IEnumerator DelayedAnimSetTrigger(string trigger)
    {
        yield return new WaitForSeconds(HOLSTER_DELAY);
        anim.SetTrigger(trigger);
    }

    public void TriggerWatchAnimation(out float delay)
    {
        if (meleeIsEquipped) ToggleMelee();
        else if (HandsEnabled || equippedItemType != Enums.itemType.None) Holster();

        CancelInvoke();
        ShowHands();
        AnimSetTrigger(AnimParams.PLAYER_TRIGGER_EQUIP_WATCH);

        if (CurrentlyHolsteringItem) delay = HOLSTER_DELAY;
        else delay = 0f;
    }

    private void ShowHands() => ShowHands(true);
    private void ShowHands(bool active)
    {
        if (hands.activeSelf != active) hands.SetActive(active);
        if (handSway.isActiveAndEnabled != active) handSway.enabled = active;
    }
    private void HideHandsIfNothingEquippedAfterHolstering()
    {
        if (CurrentlyHolsteringItem)
        {
            CancelInvoke("HideHandsIfNothingEquipped");
            Invoke("HideHandsIfNothingEquipped", HOLSTER_DELAY);
        }
        else
        {
            HideHandsIfNothingEquipped();
        }

    }
    private void HideHandsIfNothingEquipped()
    {
        if (equippedItemType == Enums.itemType.None) ShowHands(false);
    }

    public void DecrementEquippedItemAmount() => playerInventory.DecrementItemAmount(equippedItemType);

    public void SnapshotEquippedItemType()
    {
        snapshottedItemType = equippedItemType;
    }
    public void EquipSnapshottedItem() => Equip(snapshottedItemType);

    private bool CurrentlyPlayingAnimation(string animation)
    {
        if (anim == null) return false;
        return anim.GetBool(animation);
    }

    public void UsePrimary()
    {
        //NOTHING IS EQUIPPED
        if (equippedItem == null && !meleeIsEquipped) return; //-----------------------------------------
        //ANIMATION IS STILL PLAYING
        if (CurrentlyUsingItem) return; //-------------------------------

        if (!meleeIsEquipped)
        {
            equippedItem.UsePrimary();

            //THE ANIMATION FOR SHOOTING GUNS IS HANDLED ON THE GUN ITSELF
            if (EquippedItemIsAGun()) return; //---------------------------------
        }
        AnimSetTrigger(AnimParams.PLAYER_TRIGGER_USE_PRIMARY, true);
    }

    private bool EquippedItemIsAGun()
    {
        return (equippedItemType == Enums.itemType.Pistol || equippedItemType == Enums.itemType.AkRifle || equippedItemType == Enums.itemType.M4Rifle);
    }
    public bool EquippedItemHasSecondary()
    {
        return (equippedItemType == Enums.itemType.Wrench);
    }

    public void UsePrimaryHeld()
    {
        if (equippedItem == null) return; //--------------------------
        equippedItem.UsePrimaryHeld();
    }
    public void UseSecondary()
    {
        bool valid = (EquippedItemHasSecondary() || EquippedItemIsAGun());
        if (!valid || CurrentlyUsingItem) return; //--------

        equippedItem.UseSecondary();
        //THE ANIMATION FOR GUN SECONDARY IS HANDLED ON THE GUN ITSELF
        if (EquippedItemHasSecondary()) AnimSetTrigger(AnimParams.PLAYER_TRIGGER_USE_SECONDARY, true);
    }
    public void ReloadEquipped()
    {
        if (equippedItem == null) return; //----------------------------------

        bool success;
        equippedItem.ReloadEquipped(out success);
        if (success) AnimSetTrigger(AnimParams.PLAYER_TRIGGER_RELOAD, true);
    }

    private void HideHandsOnPlayerDeath(bool playerIsDead)
    {
        if (playerIsDead) Unequip();
    }

    public void DoMeleeDamage()
    {
        bool hitDamageable = false;
        Collider[] cols = Physics.OverlapSphere(meleeDamageOrigin.position, meleeDamageRadius, whatIsDamageableByMelee);
        foreach (var col in cols)
        {
            if (col.GetComponent<IDamageable>() != null)
            {
                col.GetComponent<IDamageable>().TakeDamage(meleeDamageAmount, meleeDamageOrigin.position, MELEE_DAMAGE_FORCE, DAMAGE_TYPE);
                if (!hitDamageable) hitDamageable = true;
            }
        }

        if (hitDamageable)
        {
            playerCamera.Recoil();
            sfx.PlayMeleeSwingHitSound();
        }
        else
        {
            sfx.PlayMeleeSwingNoHitSound();
        }
    }
}
