using UnityEngine;

public class EnableObjsOnPlayerEnter : MonoBehaviour
{
    [SerializeField]
    GameObject[] enableOnEnter, disableOnEnter, enableOnExit, disableOnExit;
    [SerializeField]
    private bool onlyTriggerOnce;

    private Collider col;

    private void Awake()
    {
        col = GetComponent<Collider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER))
        {
            EnableObjs(enableOnEnter, true);
            EnableObjs(disableOnEnter, false);
            if (onlyTriggerOnce) col.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER))
        {
            EnableObjs(enableOnExit, true);
            EnableObjs(disableOnExit, false);
            if (onlyTriggerOnce) col.enabled = false;
        }
    }

    private void EnableObjs(GameObject[] objs, bool active)
    {
        if (objs.Length < 1) return; //----------------------
        foreach (GameObject obj in objs)
        {
            obj.SetActive(active);
        }
    }
}
