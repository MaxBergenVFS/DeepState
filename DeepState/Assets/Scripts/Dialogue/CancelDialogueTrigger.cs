using UnityEngine;

public class CancelDialogueTrigger : MonoBehaviour
{
    [SerializeField]
    private NPCDialogue NPCDialogue;

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER)) NPCDialogue.CancelDialogue();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(GeneralParams.TAG_PLAYER)) NPCDialogue.EnableDialogue();
    }
}
