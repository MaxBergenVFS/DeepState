public class Silencer : BaseItem
{
    public override void UseFromInventory(out bool success)
    {
        base.UseFromInventory(out success);
        playerHands.EquipSilencerOnPistol();
    }

    public override void OnDrop()
    {
        base.OnDrop();
        playerHands.EquipSilencerOnPistol(false);
    }
}
