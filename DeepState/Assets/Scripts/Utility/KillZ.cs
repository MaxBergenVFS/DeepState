using UnityEngine;

public class KillZ : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerController.Instance.Die();
        }
        else
        {
            other.gameObject.SetActive(false);
        }
    }
}

