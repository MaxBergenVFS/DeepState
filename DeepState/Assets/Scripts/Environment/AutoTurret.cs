using System.Collections;
using UnityEngine;

public class AutoTurret : Gun
{
    private const Enums.damageType DAMAGE_TYPE = Enums.damageType.RangedTurret;
    private const float LOOK_AT_TARGET_Y_AXIS_THRESHOLD = 90f;
    private const float LERP_SPEED = 0.8f;

    [SerializeField]
    private ParticleSystem muzzleFlash, smokeDestroyed;
    [SerializeField]
    private AutoTurretVisionCone visionCone;
    [SerializeField]
    private GameObject[] friendlyLasers, hostileLasers;
    [SerializeField]
    private Transform rotTarget, bone, raycastOrigin;

    private bool isDestroyed, isDeactivated, isUnPausing;
    private Animator turretAnim;
    private Transform targetTransform, player;
    private Health health;
    private Quaternion pausedAnimRot = new Quaternion();
    private CheckpointSettings snapshot = new CheckpointSettings();

    public bool IsDeactivated { get { return isDeactivated; } }

    protected override void Awake()
    {
        base.Awake();
        InitAnim();
        pausedAnimRot = bone.rotation;
        isDestroyed = isUnPausing = false;
        damageType = DAMAGE_TYPE;
        health = GetComponent<Health>();
        if (raycastOrigin == null) raycastOrigin = origin;
        AttackPlayer();
    }
    protected override void Start()
    {
        base.Start();
        visionCone.SetAutoTurretRef(this);
        player = PlayerController.Instance.transform;
        sfx.SetGunType(Enums.gunType.M4);
    }

    private void InitAnim()
    {
        //if Gun.anim == null then Shoot and Reload won't be called
        turretAnim = anim;
        anim = null;
    }

    public void SnapshotState()
    {
        snapshot.isDestroyed = isDestroyed;
        snapshot.health = health.RemainingHealth;
    }
    public void RevertState()
    {
        isDestroyed = snapshot.isDestroyed;
        health.SetRemainingHealth(snapshot.health);
        if (isDestroyed) Destroy();
        else smokeDestroyed.Stop();
    }

    public void AttackTarget(Transform target)
    {
        if (isDestroyed || target == null) return; //-------------------

        targetTransform = target;

        if (targetTransform == null) return; //-----------

        RotateTowardsTarget(targetTransform);
        float y = (360f % bone.eulerAngles.y);
        if (y > LOOK_AT_TARGET_Y_AXIS_THRESHOLD || y < -LOOK_AT_TARGET_Y_AXIS_THRESHOLD)
        {
            PauseAnim(false);
        }
        else if (TargetIsInLoS(targetTransform))
        {
            PauseAnim(true);
            Shoot();
        }
    }

    protected override Vector3 GetDirection()
    {
        float x = Random.Range(-spread, spread);
        float y = Random.Range(-spread, spread);

        return (targetTransform.position - origin.position).normalized + new Vector3(x, y, 0);
    }

    protected override void MuzzleFlash() => muzzleFlash.Play();

    public void PauseAnim(bool active)
    {
        if (isDestroyed || active == !turretAnim.isActiveAndEnabled || isUnPausing) return; //------------------

        if (active)
        {
            //immediately stop idle animation and have RotateTowards take over
            pausedAnimRot = bone.rotation;
            turretAnim.enabled = false;
        }
        else
        {
            //lerp current rotation back to saved rotation and resume idle animation
            StopAllCoroutines();
            isUnPausing = true;
            StartCoroutine(UnPauseAnim());
        }

    }

    private IEnumerator UnPauseAnim()
    {
        float t = 0f;
        Quaternion rot = new Quaternion();
        rot = bone.rotation;
        while (t < 1f)
        {
            bone.rotation = Quaternion.Lerp(rot, pausedAnimRot, t);
            t += Time.deltaTime * LERP_SPEED;
            yield return null;
        }
        bone.rotation = pausedAnimRot;
        turretAnim.enabled = true;
        isUnPausing = false;
    }

    private void RotateTowardsTarget(Transform target)
    {
        rotTarget.position = new Vector3(target.position.x, rotTarget.position.y, target.position.z);
        bone.LookAt(rotTarget);
    }

    public void Destroy()
    {
        smokeDestroyed.Play();
        Deactivate();
        isDestroyed = true;
    }

    private void ActivateFriendlyLasers(bool active)
    {
        foreach (GameObject laser in friendlyLasers)
        {
            laser.SetActive(active);
        }
    }
    private void ActivateHostileLasers(bool active)
    {
        foreach (GameObject laser in hostileLasers)
        {
            laser.SetActive(active);
        }
    }

    public void AttackPlayer()
    {
        if (isDestroyed) return; //---------------

        PauseAnim(false);
        visionCone.SetPlayerTarget();
        ActivateHostileLasers(true);
        ActivateFriendlyLasers(false);
        if (isDeactivated) isDeactivated = false;
    }
    public void AttackNPCs()
    {
        if (isDestroyed) return; //---------------

        PauseAnim(false);
        visionCone.SetNPCTarget();
        ActivateHostileLasers(false);
        ActivateFriendlyLasers(true);
        if (isDeactivated) isDeactivated = false;
    }
    public void Deactivate()
    {
        if (isDestroyed) return; //---------------

        PauseAnim(true);
        visionCone.ResetTarget();
        ActivateHostileLasers(false);
        ActivateFriendlyLasers(false);
        if (!isDeactivated) isDeactivated = true;
    }

    private bool TargetIsInLoS(Transform target)
    {
        RaycastHit hit;
        Vector3 dir = (target.position - origin.position).normalized;
        Physics.Raycast(raycastOrigin.position, dir, out hit, 1000, whatIsDamagable);
        Collider col = hit.collider;
        if (col == null) return false;

        NPCHitbox npc = col.GetComponent<NPCHitbox>();
        if (npc != null) return !npc.IsDead;
        else return col.GetComponent<IDamageable>() != null;
    }
}
