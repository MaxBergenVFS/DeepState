using UnityEngine;

public class InteractableObjectTrigger : MonoBehaviour, IInteractable
{
    [SerializeField]
    private InteractableObject[] interactableObjects;

    public string Name { get { return ""; } }

    public void Interact()
    {
        foreach (InteractableObject obj in interactableObjects)
        {
            obj.Interact();
        }
    }
}
