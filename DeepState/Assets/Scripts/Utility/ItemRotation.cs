using UnityEngine;

public class ItemRotation : MonoBehaviour
{
    [SerializeField]
    private float xRot, yRot, zRot;

    private void Update()
    {
        transform.Rotate(xRot * Time.fixedDeltaTime, yRot * Time.fixedDeltaTime, zRot * Time.fixedDeltaTime, Space.Self);
    }
}