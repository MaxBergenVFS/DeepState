﻿using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private float mouseSensitivity = 5f;
    [SerializeField]
    private float clampAngle = 85.0f;
    [SerializeField]
    private float interactDistance = 3f;
    [SerializeField]
    private float recoilSnappiness = 5f;
    [SerializeField]
    private float recoilReturnSpeed = 3f;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private GameObject damagableReticleEnemy, damagableReticleFriendly;
    [SerializeField]
    private ScreenShake screenShake;
    [SerializeField]
    private TargetBoxUI targetBoxUI;
    [SerializeField]
    protected LayerMask whatIsInteractable;

    private Slider mouseSensitivitySlider;
    private CameraPostProcessing postProcessing;
    private DatapadUI datapadUI;
    private PlayerController playerController;
    private PlayerInventory playerInventory;
    private float currentPlayerRot, currentCamRot, mouseX, mouseY, mouseSensitivityMax;

    public Camera MainCamera { get { return mainCamera; } }
    public float MouseX { get { return mouseX; } }
    public float MouseY { get { return mouseY; } }

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        playerInventory = GetComponent<PlayerInventory>();
        mouseSensitivityMax = mouseSensitivity + (mouseSensitivity - GeneralParams.MOUSE_SENSITIVITY_MIN);
    }
    private void Start()
    {
        mouseSensitivitySlider = playerInventory.SettingsTabUIRef.MouseSensitivity;
        datapadUI = playerInventory.DatapadUI;
        currentCamRot = mainCamera.transform.localRotation.eulerAngles.x;
        postProcessing = mainCamera.GetComponent<CameraPostProcessing>();
        mouseSensitivitySlider.onValueChanged.AddListener(delegate { SetMouseSensitivity(); });
        SetDefaultMouseSensitivity();
    }

    private void Update()
    {
        currentPlayerRot += mouseX * mouseSensitivity;
        transform.rotation = Quaternion.Euler(0f, currentPlayerRot, 0f);
        currentCamRot = Mathf.Clamp(currentCamRot + mouseY * mouseSensitivity, -clampAngle, clampAngle);
        mainCamera.transform.localRotation = Quaternion.identity;
        mainCamera.transform.Rotate(Vector3.left, currentCamRot);
        RefreshReticle();
    }

    private void SetMouseSensitivity()
    {
        float sliderVal = mouseSensitivitySlider.value;
        mouseSensitivity = Mathf.Lerp(GeneralParams.MOUSE_SENSITIVITY_MIN, mouseSensitivityMax, sliderVal);
        PersistentValues.Instance.SetMouseSensitivity(mouseSensitivity, sliderVal);
    }
    private void SetDefaultMouseSensitivity()
    {
        UserSettings mouseSensitivitySettings = PersistentValues.Instance.MouseSensitivitySettings;
        if (mouseSensitivitySettings.isCalibrated)
        {
            mouseSensitivitySlider.value = mouseSensitivitySettings.slider;
            mouseSensitivity = mouseSensitivitySettings.val;
        }
        else
        {
            ResetMouseSensitivity();
        }
    }
    public void ResetMouseSensitivity()
    {
        mouseSensitivitySlider.value = GeneralParams.DEFAULT_MOUSE_SENSITIVITY_SLIDER_VAL;
    }

    private void OnEnable()
    {
        currentPlayerRot = transform.localRotation.eulerAngles.y;
    }

    private void DisplayReticle(GameObject reticle, bool active)
    {
        if (reticle.activeSelf == active) return;
        reticle.SetActive(active);
    }

    public void DisplayDamageableReticle(bool active) => DisplayDamageableReticle(active, false);
    public void DisplayDamageableReticle(bool active, bool targetIsFriendly)
    {
        if (active)
        {
            if (targetIsFriendly)
            {
                DisplayReticle(damagableReticleEnemy, false);
                DisplayReticle(damagableReticleFriendly, true);
            }
            else
            {
                DisplayReticle(damagableReticleFriendly, false);
                DisplayReticle(damagableReticleEnemy, true);
            }
        }
        else
        {
            DisplayReticle(damagableReticleFriendly, false);
            DisplayReticle(damagableReticleEnemy, false);
        }
    }

    public void ControlCamera(float x, float y)
    {
        mouseX = x;
        mouseY = y;
    }

    public void Recoil() => screenShake.Shake();
    public void Recoil(float amt)
    {
        if (amt <= 0f) return; //-----------------------
        screenShake.Shake(amt);
    }

    private void RefreshReticle()
    {
        RaycastHit hit = RaycastFromCamera();
        Transform target = hit.transform;

        if (target == null)
        {
            HideUI();
            return;
        } //------------------------------------------------------------------

        IInteractable interactable = target.GetComponent<IInteractable>();

        if (interactable == null) HideUI();
        else DisplayTargetBoxUI(true, hit.collider, interactable.Name);
    }

    private void HideUI()
    {
        DisableTargetBoxUI();
        datapadUI.Hide();
    }

    private void DisableTargetBoxUI() => targetBoxUI.DisableSprite();
    private void DisplayTargetBoxUI(bool active, Collider col, string targetName) => targetBoxUI.DisplaySprite(active, col, targetName);

    public void SetNightVision(bool active)
    {
        if (active) postProcessing.SetNightVision();
        else postProcessing.SetDefaultProfile();
    }

    private RaycastHit RaycastFromCamera()
    {
        Ray ray = mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        Physics.Raycast(ray, out hit, interactDistance, whatIsInteractable);
        return hit;
    }

    public void Interact()
    {
        RaycastHit hit = RaycastFromCamera();

        if (hit.transform == null) return; //--------------------------------------------------------------

        IInteractable interactable = hit.transform.GetComponent<IInteractable>();

        if (interactable == null) return; //---------------------------------------------------------------

        interactable.Interact();
        IInventoryItem item = hit.transform.GetComponent<IInventoryItem>();

        if (item == null) return; //-----------------------------------------------------------------------

        playerInventory.AddItemToGrid(item);
    }

    private void OnDestroy()
    {
        mouseSensitivitySlider.onValueChanged.RemoveListener(delegate { SetMouseSensitivity(); });
    }
}
