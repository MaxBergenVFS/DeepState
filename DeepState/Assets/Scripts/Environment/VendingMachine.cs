using UnityEngine;

[RequireComponent(typeof(ObjectSFX))]
public class VendingMachine : MonoBehaviour, IInteractable
{
    private const string NAME = "Vending Machine";

    [SerializeField]
    private Transform spawnPoint;
    [SerializeField]
    private GameObject foodPrefab;
    [SerializeField]
    private int cost = 5;
    [SerializeField]
    private string positiveNotification = "credits removed";
    [SerializeField]
    private string negativeNotification = "Not enough cash, stranger";

    private ObjectSFX sfx;
    private PlayerInventory playerInventory;
    private NotificationUI notificationUI;

    public string Name { get { return NAME; } }

    private void Awake()
    {
        sfx = GetComponent<ObjectSFX>();
    }

    private void Start()
    {
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        notificationUI = NotificationUI.Instance;
    }

    public void Interact()
    {
        bool success;
        playerInventory.AttemptToRemoveMoneyAmount(cost, out success);
        if (success)
        {
            Instantiate(foodPrefab, spawnPoint.position, Quaternion.identity);
            notificationUI.DisplayText($"{cost} {positiveNotification}");
            sfx.PlaySound();
        }
        else
        {
            notificationUI.DisplayText(negativeNotification);
            sfx.PlayVendingMachineNoMoneySound();
        }
    }
}
