using UnityEngine;
using UnityEngine.AI;

public class NPCStrafeAttackBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float strafeTimeMin = 1f, strafeTimeMax = 5f;

    private float strafeTime;
    private NPCController controller;
    private Vector3 strafePoint;
    private NavMeshAgent agent;
    private Transform transform;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = animator.GetComponent<NPCController>();
        agent = animator.GetComponent<NavMeshAgent>();
        transform = animator.GetComponent<Transform>();
        animator.SetBool(AnimParams.NPC_BOOL_IS_REPOSITIONING, false);
        StartStrafing(animator);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        strafeTime -= Time.deltaTime;

        if (strafeTime < 0f)
        {
            animator.SetBool(AnimParams.NPC_BOOL_IS_STRAFING, false);
        }
        else
        {
            if (!controller.OtherNPCInLoS()) controller.Attack();
            agent.SetDestination(strafePoint);
            Vector3 distanceToWalkPoint = transform.position - strafePoint;
            if (distanceToWalkPoint.magnitude <= agent.stoppingDistance)
            {
                bool success;
                Vector3 pos = controller.GetNewStrafePoint(out success);
                if (success) strafePoint = pos;
                else StopStrafing(animator);
            }
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        StopStrafing(animator);
    }

    private void StartStrafing(Animator animator)
    {
        bool success;
        Vector3 pos = controller.GetNewStrafePoint(out success);
        if (success)
        {
            strafePoint = pos;
            animator.SetBool(AnimParams.NPC_BOOL_IS_STRAFING, true);
            animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, true);
            controller.ActivateBodyAimHip(true);
            strafeTime = Random.Range(strafeTimeMin, strafeTimeMax);
            agent.updateRotation = false;
            controller.RotateToFacePlayer(true);
            controller.SetNavSpeedToRun();
        }
        else
        {
            StopStrafing(animator);
        }
    }
    private void StopStrafing(Animator animator)
    {
        if (animator.GetBool(AnimParams.NPC_BOOL_IS_STRAFING)) animator.SetBool(AnimParams.NPC_BOOL_IS_STRAFING, false);
        animator.SetBool(AnimParams.NPC_BOOL_IS_RUNNING, false);
        controller.ActivateBodyAimHip(false);
        agent.updateRotation = true;
    }
}
