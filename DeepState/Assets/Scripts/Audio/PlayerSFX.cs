public class PlayerSFX : SFX
{
    private void Start()
    {
        NotificationUI.onDialogueNotification += PlayNotificationSound;
        SavedNotesManager.onNoteSaved += PlayNoteAddedSound;
    }

    private void OnDestroy()
    {
        NotificationUI.onDialogueNotification -= PlayNotificationSound;
        SavedNotesManager.onNoteSaved -= PlayNoteAddedSound;
    }

    //private
    private void PlayNotificationSound() => PlaySound2D(NOTIFICATION_PATH);
    private void PlayNoteAddedSound() => PlaySound2D(NOTE_ADDED_PATH);
    //public
    public void PlayPainSound() => PlaySound2D(PLAYER_PAIN_PATH);
    public void PlayMoneySound() => PlaySound2D(PLAYER_MONEY_PATH);
    public void PlayDeathSound() => PlaySound2D(PLAYER_DEATH_PATH);
    public void PlayPickupAmmoSound() => PlaySound2D(PLAYER_AMMO_PICKUP_PATH);
    public void PlayCloseInventorySound() => PlaySound2D(INVENTORY_CLOSE_PATH);
    public void PlayOpenInventorySound() => PlaySound2D(INVENTORY_OPEN_PATH);
    public void PlayMeleeSwingHitSound() => PlaySound2D(MELEE_SWING_HIT_PATH);
    public void PlayMeleeSwingNoHitSound() => PlaySound2D(MELEE_SWING_NO_HIT_PATH);
    public void PlayPickupItemSound() => PlaySound2D(ITEM_PICK_UP_PATH);
    public void PlayDropItemSound() => PlaySound2D(ITEM_DROP_PATH);
    public void PlayAddItemToItemBarSound() => PlaySound2D(ITEM_ADD_TO_BAR_PATH);
    public void PlaySelectSlotOnItemBarSound() => PlaySound2D(ITEM_SELECT_ON_BAR_PATH);
    public void PlayEquipNVGogglesSound() => PlaySound2D(NV_GOGGLES_ON_PATH);
    public void PlayUnequipNVGogglesSound() => PlaySound2D(NV_GOGGLES_OFF_PATH);
    public void PlayAlreadyAtFullHealthSound() => PlaySound2D(AT_FULL_HEALTH_PATH);
    public void PlayConsumeHealingItemSound(Enums.itemType itemType)
    {
        switch (itemType)
        {
            case (Enums.itemType.Medpack):
                PlaySound2D(CONSUME_MEDPACK_PATH);
                break;
            case (Enums.itemType.Chips):
                PlaySound2D(CONSUME_CHIPS_PATH);
                break;
            case (Enums.itemType.Soda):
                PlaySound2D(CONSUME_SODA_PATH);
                break;
            case (Enums.itemType.BodyArmor):
                PlaySound2D(EQUIP_BODY_ARMOR_PATH);
                break;
            case (Enums.itemType.None):
            default:
                break;
        }
    }
}
