using UnityEngine;

public class DynamicAudioManager : MonoBehaviour
{
    private MusicManager musicManager;
    private float lerpDuration;
    private int count;
    private enum parameter
    {
        None,
        Intensity,
        Muffle
    }

    [SerializeField]
    private parameter param;

    public float LerpDuration { get { return lerpDuration; } }
    public int Count { get { return count; } }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        count = 0;
        musicManager = GameManager.Instance.MusicManager;

        switch (param)
        {
            case (parameter.Intensity):
                lerpDuration = musicManager.IntensityLerpDuration;
                break;
            case (parameter.Muffle):
                lerpDuration = musicManager.MuffledLerpDuration;
                break;
            case (parameter.None):
            default:
                break;
        }
    }

    public void Activate(bool active) => Activate(param, active);
    private void Activate(parameter param, bool active)
    {
        switch (param)
        {
            case (parameter.Intensity):
                musicManager.Intensity(active);
                break;
            case (parameter.Muffle):
                musicManager.Muffle(active);
                break;
            case (parameter.None):
            default:
                break;
        }
    }

    public void IncrementCount() => count++;
    public void DecrementCount()
    {
        count--;
        if (count < 0) count = 0;
    }
}
