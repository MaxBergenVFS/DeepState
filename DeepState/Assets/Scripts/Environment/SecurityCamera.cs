using UnityEngine;

[RequireComponent(typeof(ObjectSFX))]
public class SecurityCamera : MonoBehaviour, IDamageable, IInteractable
{
    private const string NAME = "Security Camera";

    [SerializeField]
    private Camera cam;
    [SerializeField]
    private GameObject model, explosionPrefab;
    [SerializeField]
    private string notificationText = "It's a security camera";
    [SerializeField]
    private string camName;

    private NotificationUI notificationUI;
    private SecurityMonitorCameraManager securityMonitorCameraManager;
    private bool isDestroyed;
    private Collider col;
    private Animator anim;

    public string Name { get { return NAME; } }
    public bool IsDestroyed { get { return isDestroyed; } }

    private void Awake()
    {
        cam.enabled = false;
        isDestroyed = false;
        col = GetComponent<Collider>();
        anim = GetComponent<Animator>();
    }
    private void Start()
    {
        notificationUI = NotificationUI.Instance;
        securityMonitorCameraManager = GameManager.Instance.SecurityMonitorCameraManager;
    }

    public void TakeDamage(int dmg, Vector3 pos, float force, Enums.damageType type)
    {
        if (dmg < 1) return;
        DestroyCam();
    }

    private void DestroyCam()
    {
        if (isDestroyed) return; //--------------------
        Instantiate(explosionPrefab, model.transform.position, Quaternion.identity);
        EnableCam(false, 0);
        model.SetActive(false);
        col.enabled = false;
        anim.enabled = false;
        isDestroyed = true;
    }

    public void SetSecurityFeed(RenderTexture texture)
    {
        cam.targetTexture = texture;
    }

    private void EnableCam(bool active, int num) => EnableCam(active, false, num);
    public void EnableCam(bool active, bool fullscreen, int num)
    {
        cam.enabled = active;
        if (!fullscreen) return; //-------------------------------

        if (active) securityMonitorCameraManager.EnableCamera(cam.transform, num, camName, isDestroyed);
        else securityMonitorCameraManager.DisableCamera();
    }

    public void Interact()
    {
        notificationUI.DisplayText(notificationText);
    }
}
