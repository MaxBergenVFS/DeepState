using UnityEngine;

public class ItemContainer : MonoBehaviour, IInteractable
{
    [SerializeField]
    private string positiveNotificationText = "Confiscated items returned";
    [SerializeField]
    private string negativeNotificationText = "There's nothing in here";
    [SerializeField]
    private string targetedName = "Confiscated Items";

    protected PlayerInventory playerInventory;
    protected InventoryItemDatabase inventoryItemDatabase;
    private NotificationUI notificationUI;
    protected bool containsItems;

    public string Name { get { return targetedName; } }

    protected virtual void Start()
    {
        Init();
    }

    protected void Init()
    {
        notificationUI = NotificationUI.Instance;
        playerInventory = PlayerController.Instance.GetComponent<PlayerInventory>();
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;
    }

    public void Interact()
    {
        if (containsItems)
        {
            ReturnItemsToPlayer();
            notificationUI.DisplayText(positiveNotificationText);
            containsItems = false;
        }
        else
        {
            notificationUI.DisplayText(negativeNotificationText);
        }
    }

    public virtual void ReturnItemsToPlayer() { }
}
