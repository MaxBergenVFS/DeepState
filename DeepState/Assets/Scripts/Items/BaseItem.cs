using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BaseItem : MonoBehaviour, IInteractable, IInventoryItem, IRespawnable
{
    #region Inits
    private const float PICKUP_LERP_SPEED = 4f;
    private const string NOTIF_PREFIX = "Picked up:";
    private const string NOTIF_UNIQUE_ITEM_ALREADY_IN_INVENTORY = "I already have one of these";

    [SerializeField]
    protected Enums.itemType itemType;
    [SerializeField]
    private Sprite itemImage;

    protected int databaseId;
    protected InventoryItemDatabase inventoryItemDatabase;
    protected PlayerInventory playerInventory;
    protected PlayerController playerController;
    protected Transform player;
    protected PlayerHands playerHands;
    protected string nameOverride;
    protected CheckpointManager checkpointManager;

    private InventoryItem inventoryItem;
    private Collider col;
    private Rigidbody rb;
    private Camera mainCamera;
    private NotificationUI notification;
    private Transform playerTransform;
    private string itemTitle;
    private Vector3 startingPos = new Vector3();
    private Quaternion startingRot = new Quaternion();
    private bool bypassCheckpoint;

    public string Name
    {
        get
        {
            if (nameOverride != "") return nameOverride;
            if (inventoryItem == null) return "";
            return inventoryItem.title;
        }
    }
    public Sprite Image { get { return itemImage; } }
    public string AmountStr
    {
        get
        {
            if (inventoryItem.isUnique) return "";
            return inventoryItem.amount.ToString();
        }
    }
    public string ShortenedName { get { return inventoryItem.shortenedTitle; } }
    public int AmountInt { get { return inventoryItem.amount; } }
    public string Description { get { return inventoryItem.description; } }
    public string CriticalInfo { get { return inventoryItem.criticalInfo; } }
    public Enums.itemType ItemType { get { return itemType; } }
    public bool IsConsumable { get { return inventoryItem.isConsumable; } }
    public bool IsEquippable { get { return inventoryItem.isEquippable; } }
    public bool IsEquipped { get { return inventoryItem.isEquipped; } }
    #endregion

    private void Awake()
    {
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        nameOverride = "";
        startingPos = transform.position;
        startingRot = transform.rotation;
        bypassCheckpoint = false;
    }
    protected virtual void Start()
    {
        playerController = PlayerController.Instance;
        player = playerController.transform;
        playerInventory = playerController.GetComponent<PlayerInventory>();
        playerHands = playerController.GetComponent<PlayerHeldItem>().PlayerHands;
        mainCamera = playerController.GetComponent<CameraController>().MainCamera;
        playerTransform = playerController.transform;
        notification = NotificationUI.Instance;
        checkpointManager = GameManager.Instance.CheckpointManager;
        SetInventoryItemDatabaseRefs();
        /*
        Objects that instantiate this item can manually BypassCheckpoint to avoid duplicates.
        If a checkpoint is set after this item is instantiated but before it's picked up then 
        ResetBypassCheckpoint will revert it to default behaviour 
        */
        CheckpointManager.onSetCheckpoint += ResetBypassCheckpoint;
    }

    private void OnDestroy()
    {
        CheckpointManager.onSetCheckpoint -= ResetBypassCheckpoint;
    }

    public virtual void Interact()
    {
        if (inventoryItemDatabase.UniqueItemIsInInventory(itemType))
        {
            DisplayNotificationText(false, NOTIF_UNIQUE_ITEM_ALREADY_IN_INVENTORY);
        }
        else
        {
            PickUp();
            DisplayNotificationText();
        }
    }

    protected void PickUp()
    {
        col.enabled = false;
        StopAllCoroutines();
        if (this.gameObject.activeSelf) StartCoroutine(LerpPickUp());
    }
    private IEnumerator LerpPickUp()
    {
        Vector3 start = transform.position;
        Vector3 end = player.position;
        float t = 0f;

        while (t < 1f)
        {
            transform.position = Vector3.Lerp(start, end, t);
            t += Time.deltaTime * PICKUP_LERP_SPEED;
            yield return null;
        }
        this.gameObject.SetActive(false);
    }

    protected void DisplayNotificationText() => DisplayNotificationText(true, itemTitle);
    protected void DisplayNotificationText(bool includePrefix, string str)
    {
        string notif;
        if (includePrefix)
        {
            notif = $"{NOTIF_PREFIX} {str}";
        }
        else
        {
            notif = str;
        }
        if (notification.isActiveAndEnabled) notification.DisplayText(notif);
    }

    protected void AddToPlayerInventory()
    {
        inventoryItemDatabase.AddToInventoryItemAmount(databaseId, 1);
    }
    private void RemoveFromPlayerInventory()
    {
        inventoryItemDatabase.AddToInventoryItemAmount(databaseId, -1);
        if (inventoryItemDatabase.GetInventoryItemAmount(databaseId) < 1) inventoryItemDatabase.SetInventoryItemIsEquipped(itemType, false);
    }

    protected int GetAmountInInventoryDatabase()
    {
        return inventoryItemDatabase.GetInventoryItemAmount(databaseId);
    }

    private void SetInventoryItemDatabaseRefs()
    {
        inventoryItemDatabase = playerInventory.InventoryItemDatabase;

        if (itemType == Enums.itemType.None) return; //-------------------------------------------

        inventoryItem = inventoryItemDatabase.GetInventoryItem(itemType);
        databaseId = inventoryItem.id;
        itemTitle = inventoryItem.title;
    }

    public void BypassCheckpoint()
    {
        bypassCheckpoint = true;
    }
    private void ResetBypassCheckpoint()
    {
        bypassCheckpoint = false;
    }
    public void EnableCol()
    {
        if (col.enabled != true) col.enabled = true;
    }

    public virtual void OnPickup()
    {
        if (!bypassCheckpoint) checkpointManager.AddRespawnable(this);
        if (itemType == Enums.itemType.None || itemType == Enums.itemType.Ammo) return; //---------
        AddToPlayerInventory();
    }

    public virtual void OnDrop()
    {
        RaycastHit hit = new RaycastHit();
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit, 1000);
        int amt = inventoryItemDatabase.GetInventoryItemAmount(databaseId);
        if (amt == 1)
        {
            SetDroppedItemPos(this.gameObject, hit.point);
        }
        else if (amt > 1)
        {
            GameObject obj = Instantiate(this.gameObject, Vector3.zero, Quaternion.identity);
            SetDroppedItemPos(obj, hit.point);
        }
        RemoveFromPlayerInventory();
        playerInventory.RefreshItemsOnBar();
    }

    private void SetDroppedItemPos(GameObject obj, Vector3 hitPoint)
    {
        float dist;
        Vector3 pos = new Vector3();

        dist = playerInventory.DistanceToDropItemFromInventory;
        pos = Vector3.MoveTowards(mainCamera.ScreenToWorldPoint(Input.mousePosition), hitPoint, dist);

        obj.SetActive(true);
        obj.transform.position = pos;
        obj.GetComponent<Rigidbody>().isKinematic = false;
        obj.GetComponent<Collider>().enabled = true;
    }

    public virtual void UseFromInventory(out bool itemIsEquippable)
    {
        itemIsEquippable = inventoryItemDatabase.GetInventoryItemIsEquippable(itemType);
        if (itemIsEquippable) inventoryItemDatabase.SetInventoryItemIsEquipped(itemType, playerHands.EquippedItemType == itemType);
    }

    public void Respawn()
    {
        this.gameObject.SetActive(true);
        transform.position = startingPos;
        transform.rotation = startingRot;
        rb.isKinematic = true;
        col.enabled = true;
    }
}
